<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BrandingController extends Controller
{
    public $statusCode = 200;
    public function getCategory()
    {
        $query = DB::table('kategori')->get();
        foreach ($query as $data) {
            $data->foto = "http://localhost/branding/public/branding/" . $data->foto;
        }
        return response()->json(array(
            'message' => "Succsess",
            'category' => $query
        ), $this->statusCode);
    }
    public function getBranding($idCat = null)
    {
        $mac_address = request('mac_address');
        if ($idCat == null || $idCat == "null") {
            $query = DB::table('branding')->get();
        } else {
            $query = DB::table('branding')->where('id_kategori', $idCat)->get();
        }
        foreach ($query as $data) {
            $data->foto_branding = "http://localhost/branding/public/branding/" . $data->foto_branding;
            $liked = DB::table('likes')
                ->where('mac_address', $mac_address)
                ->where('id_branding', $data->id)
                ->first();


            $likedTotal = DB::table('likes')
                ->where('id_branding', $data->id)
                ->get();
            $commentTotal = DB::table('comments')
                ->where('id_branding', $data->id)
                ->get();
            $data->liked = $liked != null;
            $data->likedTotal = count($likedTotal);
            $data->commentTotal = count($commentTotal);
        }

	   return response()->json(array(
            'message' => "Succsess",
            'branding' => $query
        ), $this->statusCode);
    }

    public function likesBranding()
    {
        $mac_address = request('mac_address');
        $id_branding = request('id_branding');
        $checkData = DB::table('likes')
            ->where('mac_address', $mac_address)
            ->where('id_branding', $id_branding)
            ->first();
        if ($checkData != null) {
            $query = DB::table('likes')
                ->where('mac_address', $mac_address)
                ->where('id_branding', $id_branding)
                ->delete();
        } else {
            $query = DB::table('likes')->insert(
                [
                    'mac_address' => $mac_address,
                    'id_branding' => $id_branding,
                ]
            );
        }


        return response()->json(array(
            'message' => "Succsess",
            'branding' => $query
        ), $this->statusCode);
    }
    public function commentsBranding()
    {
        $mac_address = request('mac_address');
        $id_branding = request('id_branding');
        $comment = request('comment');
        $query = DB::table('comments')->insert(
            [
                'mac_address' => $mac_address,
                'id_branding' => $id_branding,
                'comment' => $comment,
            ]
        );
        return response()->json(array(
            'message' => "Succsess",
            'branding' => $query
        ), $this->statusCode);
    }
    //
}
