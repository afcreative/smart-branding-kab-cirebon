<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use File;
use Session;

class App extends Controller
{

    public function cek_login(request $request)
    {
		
		
					$pass= md5($request->password);
					$cek = DB::table('users')
					 ->where('username', '=', $request->username)
					 ->where('password', '=', $pass);
					 
					 $ada = $cek ->count();
					 $data = $cek ->first();
					 
					 $request->session()->put(['nama_lengkap'=>$data->nama_lengkap,
					 'foto_user'=>$data->foto_user,
					 'username'=>$data->username,
					 'level'=>$data->level,
					 'blokir'=>$data->blokir]);
					 
					$record = DB::select('select * from kategori');
						if ($ada > 0 ){
						return redirect('/cari');
						}
						else {
						return redirect('/');	
						}
			
    }
   
   
    public function kategori()
    {
        $record = DB::select('select * from kategori');

        return view('admin/mod_master/kategori', compact('record'));
    }
	public function cari_home(request $request)
    {
			$cari=$request->cari;
			$datacari = DB::select("select * from submenu where nama_submenu like '%".$cari."%'  ");
		

        return view('admin/mod_master/home', compact('datacari'));
    }

    public function hapus_kategori($id)
    {

        DB::delete("delete from kategori where id_kategori ='$id' ");
        return redirect()->back();
    }
    public function simpan_kategori(request $request)
    {
        $request->validate([
                'foto'=>'required|mimes:jpg,png,jpeg',
                'nama_kategori'=>'required',
            ]);
        $foto = $request->foto;
        $rename = date('dmYHis') . "." . $foto->extension();
        $foto->move('kategori', $rename);

        DB::update("insert into kategori(nama_kategori, foto) values ('$request->nama_kategori ', '$rename') ");

        return redirect()->back();
    }
    public function edit_kategori(request $request)
    {
        if ($request->foto == NULL) {

            DB::update("update kategori set nama_kategori = '$request->nama_kategori' where id_kategori = '$request->id'");

            return redirect()->back();
        }

        $gambar = DB::table('kategori')->where('id_kategori', $request->id)->first();
        $foto = $request->foto;
        $rename = date('dmYHis') . "." . $foto->extension();
        File::delete(public_path('kategori/' . $gambar->foto));
        $foto->move('kategori', $rename);

        DB::update("update kategori set nama_kategori = '$request->nama_kategori', foto='$rename' where id = '$request->id'");
        return redirect()->back();
    }
    public function branding()
    {

        $kategori = DB::select('select * from kategori');
        $record = DB::select('select * from branding a, kategori b where a.id_kategori=b.id_kategori ');
        return view('admin/mod_master/branding', compact('record', 'kategori'));
    }
    public function simpan_branding(request $request)
    {
        $request->validate([
                'foto'=>'required|mimes:jpg,png,jpeg',
            ]);
        $foto = $request->foto;
        $rename = date('dmYHis') . "." . $foto->extension();
        $foto->move('branding', $rename);

        DB::update("insert into branding(id_kategori,
         nama_tempat,
         latitude,
         longitude,
         deskripsi,
         foto_branding
         ) values ('$request->id_kategori ',
         '$request->nama_tempat',
         '$request->latitude',
         '$request->longitude',
         '$request->deskripsi',
          '$rename') ");
        return redirect()->back();
    }
    public function hapus_branding($id)
    {

	
        $gambar = DB::table('branding')->where('id_branding', $id)->first();

        File::delete(public_path('branding/' . $gambar->foto_branding));

        DB::delete("delete from branding where id_branding ='$id' ");
        return redirect()->back();
    }
    public function edit_branding(request $request)
    {
        if ($request->foto == NULL) {

            DB::update("update branding set id_kategori = '$request->id_kategori',
            nama_tempat = '$request->nama_tempat',
            latitude = '$request->latitude',
            longitude = '$request->longitude',
            deskripsi = '$request->deskripsi'
            
            where id_branding = '$request->id'");
            return redirect()->back();
        }
        $gambar = DB::table('branding')->where('id_branding', $request->id)->first();

        File::delete(public_path('branding/' . $gambar->foto_branding));
        $foto = $request->foto;
        $rename = date('dmYHis') . "." . $foto->extension();
        $foto->move('branding', $rename);

        DB::update("update branding set id_kategori = '$request->id_kategori',
            nama_tempat = '$request->nama_tempat',
            latitude = '$request->latitude',
            longitude = '$request->longitude',
            deskripsi = '$request->deskripsi',
            foto_branding = '$rename'
            where id = '$request->id'");
        return redirect()->back();
    }
    public function menu()
    {

        $record = DB::select('select * from menu');
        //return $edulevels;
        return view('admin/mod_master/menu', compact('record'));
    }
    public function hapus_menu($id)
    {

        DB::delete("delete from menu where id_menu ='$id' ");
        return redirect()->back();
    }
    public function simpan_menu(request $request)
    {


        DB::update("insert into menu(nama_menu, icon, aktif, teks, urutan ) values ('$request->nama_menu', '$request->icon','$request->aktif','$request->teks','$request->urutan') ");

        return redirect()->back();
    }
    public function edit_menu(request $request)
    {


        DB::update("update menu set nama_menu = '$request->nama_menu', 
			icon = '$request->icon',
			aktif = '$request->aktif',
			teks = '$request->teks',
			urutan = '$request->urutan' 
			where id_menu = '$request->id'");

        return redirect()->back();

        return redirect()->back();
    }
    public function submenu()
    {
        $menu = DB::select('select * from menu');
        $record = DB::select('select * from submenu a, menu b where a.id_menu=b.id_menu');
        //return $edulevels;
        return view('admin/mod_master/submenu', compact('record', 'menu'));
    }
    public function hapus_submenu($id)
    {

        DB::delete("delete from submenu where id_submenu ='$id' ");
        return redirect()->back();
    }
    public function simpan_submenu(request $request)
    {


        DB::update("insert into submenu(id_menu, nama_submenu, icons, aktifs, tekss, app_link ) values ('$request->id_menu','$request->nama_submenu', '$request->icons','$request->aktifs','$request->tekss','$request->app_link') ");

        return redirect()->back();
    }
    public function edit_submenu(request $request)
    {


        DB::update("update submenu set nama_submenu = '$request->nama_submenu', 
			id_menu = '$request->id_menu',
			icons = '$request->icons',
			aktifs = '$request->aktifs',
			tekss = '$request->tekss',
			app_link = '$request->app_link' 
			where id_submenu = '$request->id'");

        return redirect()->back();
    }
    public function user()
    {

        $record = DB::select('select * from users');

        return view('admin/mod_user/users', compact('record'));
    }
    public function hapus_user($id)
    {

        DB::delete("delete from users where id_users ='$id' ");
        return redirect()->back();
    }
    public function simpan_user(request $request)
    {
        $foto = $request->foto;
        $rename = date('dmYHis') . "." . $foto->extension();
        $foto->move('foto_user', $rename);
		
		$pass= md5($request->password);
        DB::update("insert into users(username,password, nama_lengkap, email, no_telp, level, blokir, foto_user) values ('$request->username','$pass','$request->nama_lengkap','$request->email ','$request->no_telp','$request->level','$request->blokir', '$rename') ");

        return redirect()->back();
    }
    public function edit_user(request $request)
    {
        $password = bcrypt($request->password);
        if ($request->foto == NULL) {

            DB::update("update users set username = '$request->username',
			password = '$password',
            nama_lengkap = '$request->nama_lengkap',
			email = '$request->email',
			no_telp = '$request->no_telp',
			level = '$request->level',
			blokir = '$request->blokir'
			where id_users = '$request->id'");

            return redirect()->back();
        }

        $gambar = DB::table('users')->where('id_users', $request->id)->first();
        $foto = $request->foto;
        $rename = date('dmYHis') . "." . $foto->extension();
        File::delete(public_path('foto_user/' . $gambar->foto_user));
        $foto->move('foto_user', $rename);


        DB::update("update users set username = '$request->username',
            password = '$password',
			nama_lengkap = '$request->nama_lengkap',
			email = '$request->email',
			no_telp = '$request->no_telp',
			level = '$request->level',
			blokir = '$request->blokir',
			foto_user='$rename'
			where id_users = '$request->id'");
        return redirect()->back();
    }
    public function website()
    {

        $row = DB::table('website')->where('id_website', 1)->first();
        return view('admin/mod_master/website', compact('row'));
    }
	public function dokumentasi()
    {

        $row = DB::table('website')->where('id_website', 1)->first();
        return view('admin/mod_master/dokumentasi', compact('row'));
    }
    public function edit_website(request $request)
    {
        $request->validate([
            'nama_website' => 'nama_website',
            'title' => 'required',
            'meta_deskripsi' => 'required',
			'owner' => 'required',
			'link_url' => 'required',
			]);
        if ($request->foto == NULL) {

            DB::update("update website set nama_website = '$request->nama_website',
			title = '$request->title',
            meta_deskripsi = '$request->meta_deskripsi',
			owner = '$request->owner',
			link_url = '$request->link_url'
			where id_website = '$request->id_website'");

            return redirect()->back();
        }
         $request->validate([
            'foto' => 'required|mimes:jpg,jpeg,png',
			]);
        $gambar = DB::table('website')->where('id_website', $request->id_website)->first();
        $foto = $request->foto;
        $rename = date('dmYHis') . "." . $foto->extension();
        File::delete(public_path('logo/' . $gambar->logo));
        $foto->move('logo', $rename);

        DB::update("update website set nama_website = '$request->nama_website',
			title = '$request->title',
            meta_deskripsi = '$request->meta_deskripsi',
			owner = '$request->owner',
			link_url = '$request->link_url',
            logo = '$rename'
			where id_website = '$request->id_website'");
        return redirect()->back();
    }
}
