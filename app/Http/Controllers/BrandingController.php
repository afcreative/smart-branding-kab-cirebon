<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BrandingController extends Controller
{
    public $statusCode = 200;
    public function getCategory()
    {
        $query = DB::table('kategori')->get();
        foreach ($query as $data) {
            $data->foto = "http://jagat.digital/kategori/" . $data->foto;
        }
        
		return response()->json(array(
            'message' => "Succsess",
            'category' => $query
        ), $this->statusCode);
    }
    public function getBranding($idCat = null)
    {
        $mac_address = request('mac_address');
        if ($idCat == null || $idCat == "null") {
            $query = DB::table('branding')->get();
        } else {
            $query = DB::table('branding')->where('id_kategori', $idCat)->get();
        }
        foreach ($query as $data) {
            $data->foto_branding = "http://jagat.digital/branding/" . $data->foto_branding;
            $liked = DB::table('likes')
                ->where('mac_address', $mac_address)
                ->where('id_branding', $data->id_branding)
                ->first();

            $likedTotal = DB::table('likes')
                ->where('id_branding', $data->id_branding)
                ->get();
            $commentTotal = DB::table('comments')
                ->where('id_branding', $data->id_branding)
                ->get();
            $data->liked = $liked != null;
            $data->likedTotal = count($likedTotal);
            $data->commentTotal = count($commentTotal);
        }
        return response()->json(array(
            'message' => "Succsess",
            'branding' => $query
        ), $this->statusCode);
    }

    public function likesBranding()
    {
        $mac_address = request('mac_address');
        $id_branding = request('id_branding');
        $checkData = DB::table('likes')
            ->where('mac_address', $mac_address)
            ->where('id_branding', $id_branding)
            ->first();
        if ($checkData != null) {
            $query = DB::table('likes')
                ->where('mac_address', $mac_address)
                ->where('id_branding', $id_branding)
                ->delete();
        } else {
            $query = DB::table('likes')->insert(
                [
                    'mac_address' => $mac_address,
                    'id_branding' => $id_branding,
                ]
            );
        }


        return response()->json(array(
            'message' => "Succsess",
            'branding' => $query
        ), $this->statusCode);
    }
    public function commentsBranding()
    {
        $email = request('email');
        $id_branding = request('id_branding');
        $comment = request('comment');
        $foto_url = request('foto_url');
        $query = DB::table('comments')->insert(
            [
                'email' => $email,
                'id_branding' => $id_branding,
                'comment' => $comment,
                'foto_url' => $foto_url,
            ]
        );
        return response()->json(array(
            'message' => "Succsess",
            'branding' => $query
        ), $this->statusCode);
    }
    public function getComment()
    {
       
        $idBranding = request('id_branding');
        $query = DB::table('comments')->where('id_branding',$idBranding)->get();
        
        return response()->json(array(
            'message' => "Succsess",
            'branding' => $query
        ), $this->statusCode);
    }
}
