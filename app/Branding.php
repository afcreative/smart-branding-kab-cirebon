<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branding extends Model
{
    protected $table = 'branding';
    protected $fillable = [
        'id_kategori', 'nama_tempat', 'latitude', 'longitude', 'deskripsi', 'foto'
    ];
    public function category()
    {
        return $this->belongsTo('App\Kategori','id_kategori');
    }
}
