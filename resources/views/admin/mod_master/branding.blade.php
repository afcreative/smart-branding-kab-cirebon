@extends('admin/template')
@section('content')
    

<div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
              @if($errors->has('foto'))
            <p class=" alert alert-danger">{{ $errors->first('foto') }}</p>
        @endif
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#addbranding">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Branding</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>Nama Tempat</th>
                    <th>latitude</th>
                    <th>longtitude</th>
                    <th>Kategori</th>
                    <th>Gambar</th>
                    <th>Action</th>
                  </tr>
                </thead>
                
                <tbody>
                    @foreach ($record as $item)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->nama_tempat}}</td>
                    <td>{{$item->latitude}}</td>
                    <td>{{$item->longitude}}</td>
                    <td>{{$item->nama_kategori}}</td>
                     <td><span class="avatar avatar-sm rounded-circle"><img src="{{asset('branding/'.$item->foto_branding)}}" alt="" title="{{$item->foto_branding}}"></span></td>
                    
                    <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#branding{{$item->id_branding}}">
                          <i class="fa fa-edit" ></i>
                        </button>
                        <a href="{{asset('hapus-branding/'.$item->id_branding)}}"><button title="Hapus" type="button" class="btn btn-danger btn-sm" onclick=" confirm('Apa anda yakin untuk hapus Data ini?')">
                          <i class="fa fa-trash" ></i>
                        </button></a></td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
    <form action="{{asset('simpan-branding')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="addbranding" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Branding</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                    <div class="form-group">
                          
                        <label>Kategori</label>
                        <select name="id_kategori" class="form-control">
                            @foreach($kategori as $r)
                            <option value="{{$r->id_kategori}}">{{$r->nama_kategori}}</option>
                            @endforeach
                        </select>
                    </div>
					  <div class="form-group">
                          
                        <label>Nama Tempat </label>
                        <input type="text" name="nama_tempat"  required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                          
                        <label>Latitude </label>
                        <input type="text" name="latitude"required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                          
                        <label>Longitude</label>
                        <input type="text" name="longitude" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                          
                        <label>Deskripsi</label>
                        <input type="text" name="deskripsi" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      
                      <div class="form-group">
                        <label>Foto </label>
                        <input type="file" name="foto"  class="form-control" placeholder="Foto kategori ..">
                      </div>
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>

      @foreach ($record as $item)
            <form action="{{asset('edit-branding')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="branding{{$item->id_branding}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Branding</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     <input type="hidden" name="id" value="{{$item->id_branding}}" >
                     <div class="form-group">
                          
                        <label>Kategori</label>
                        <select name="id_kategori" class="form-control">
                            @foreach($kategori as $r)
                            <option value="{{$r->id_kategori}}">{{$r->nama_kategori}}</option>
                            @endforeach
                        </select>
                    </div>
					  <div class="form-group">
                          
                        <label>Nama Tempat </label>
                        <input type="text" name="nama_tempat" value="{{$item->nama_tempat}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                          
                        <label>Latitude </label>
                        <input type="text" name="latitude" value="{{$item->latitude}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                          
                        <label>Longitude</label>
                        <input type="text" name="longitude" value="{{$item->longitude}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                          
                        <label>Deskripsi</label>
                        <input type="text" name="deskripsi" value="{{$item->deskripsi}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      
                      <div class="form-group">
                        <label>Foto </label>
                        <input type="file" name="foto"  class="form-control" placeholder="Foto kategori ..">
                      </div>
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @endforeach
        
     @include('admin/footer')
    </div>
    @endsection

  