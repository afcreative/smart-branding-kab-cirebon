@extends('admin/template')
@section('content')
    

<div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col-lg-6">
          <div class="card-wrapper">
            <!-- Input groups -->
            <div class="card">
              <!-- Card header -->
              <div class="card-header">
                <h3 class="mb-0">Video Penggunaan</h3>
              </div>
              
              <!-- Card body -->
              <div class="card-body">
                
                
                  <!-- Input groups with icon -->
                  <div class="row">
                   
                    <iframe width="100%" height="450" src="https://www.youtube.com/embed/jCwqMWg_2iM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                    
                    
                  </div>
                  
                  <!-- Input groups with icon -->
                  
                
              </div>
             
            </div>
            
            <!-- Dropdowns -->
            
            <!-- Datepicker -->
           
            <!-- Text editor -->
           
          </div>
        </div>
		<div class="col-lg-6">
          <div class="card-wrapper">
            <!-- Input groups -->
            <div class="card">
              <!-- Card header -->
              <div class="card-header">
                <h3 class="mb-0">Modul Tutorial</h3>
				<div class="col text-right">
                  <a href="{{asset('assets/buku_panduan.pdf')}}" target="_blank" class="btn btn-sm btn-primary">Download</a>
                </div>
              </div>
			  
              
              <!-- Card body -->
              <div class="card-body">
                
                
                  <!-- Input groups with icon -->
                  <div class="row">
                   
                   <img alt="Image placeholder" src="{{asset('assets/logo_smart.jpg')}}" width="100%">
                    
                    
                  </div>
                  
                  <!-- Input groups with icon -->
                  
                
              </div>
             
            </div>
            
            <!-- Dropdowns -->
            
            <!-- Datepicker -->
           
            <!-- Text editor -->
           
          </div>
        </div>
		
        
      </div>
       
      <!-- Footer -->
     @include('admin/footer')
    </div>
    @endsection

  