@extends('admin/template')
@section('content')
    

<div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Menu</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>Nama Menu</th>
                    <th>Icon</th>
                    <th>Aktif</th>
                    <th>Teks</th>
                    <th>Urutan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                
                <tbody>
                    @foreach ($record as $item)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->nama_menu}}</td>
                    <td><i class="ni ni-{{$item->icon}} text-{{$item->teks}}" style="font-size: 25px;"></i></td>
                    <td>{{$item->aktif}}</td>
                    <td>{{$item->teks}}</td>
                    <td>{{$item->urutan}}</td>
                    <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#menu{{$item->id_menu}}">
                          <i class="fa fa-edit" ></i>
                        </button>
                        <a href="{{asset('hapus-menu/'.$item->id_menu)}}"><button title="Hapus" type="button" class="btn btn-danger btn-sm" onclick=" confirm('Apa anda yakin untuk hapus Data ini?')">
                          <i class="fa fa-trash" ></i>
                        </button></a>
                    </td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
        <!-- Modal Input  -->
            <form action="{{asset('simpan-menu')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Menu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     
					  <div class="form-group">
                        <label>Nama Menu </label>
                        <input type="text" name="nama_menu" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                        <label>Icon</label>
                        <input type="text" name="icon" required="required" class="form-control" placeholder="Nama  ..">
                        <br> <a href="{{asset('icon')}}" target="_blank" >Lihat icon</a>
                      </div>
                      <div class="form-group">
                                    <label for="exampleFormControlSelect2">Aktif</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="aktif">
                                        <option value="Y">Aktif</option>
                                        <option value="N">Non Aktif</option>
                                    </select>
                                </div>
                      <div class="form-group">
                                    <label for="exampleFormControlSelect2">Warna Teks</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="teks">
                                        <option value="primary">primary</option>
                                        <option value="orange">orange</option>
										<option value="info">info</option>
										<option value="default">default</option>
                                    </select>
                                </div>
                      <div class="form-group">
                        <label>Urutan</label>
                        <input type="text" name="urutan" required="required" class="form-control" placeholder="urutan  ..">
                      </div>                 
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @foreach ($record as $item)
            <form action="{{asset('edit-menu')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="menu{{$item->id_menu}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Menu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     <input type="hidden" name="id" value="{{$item->id_menu}}" >
					  
                     <div class="form-group">
                        <label>Nama Menu </label>
                        <input type="text" name="nama_menu" value="{{$item->nama_menu}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                        <label>Icon</label>
                        <input type="text" name="icon" value="{{$item->icon}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                                    <label for="exampleFormControlSelect2">Level akses</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="aktif">
									<?php if($item->aktif=="Y") { ?>
                                        <option value="Y" selected>Aktif</option>
                                        <option value="N">Non Aktif</option>
									<?php } else{ ?>
									<option value="Y" >Aktif</option>
                                        <option value="N" selected>Non Aktif</option>
									<?php } ?>
                                    </select>
									</div>
                      <div class="form-group">
                                    <label for="exampleFormControlSelect2">Warna Teks</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="teks">
                                        <option value="primary">primary</option>
                                        <option value="orange">orange</option>
										<option value="info">info</option>
										<option value="default">default</option>
                                    </select>
                                </div>
                      <div class="form-group">
                        <label>Urutan</label>
                        <input type="text" name="urutan" value="{{$item->urutan}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>  
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @endforeach
      <!-- Footer -->
     @include('admin/footer')
    </div>
    @endsection

  