@extends('admin/template')
@section('content')
    

<div class="container-fluid mt--6">
      <!-- Table -->
      
      <div class="row">
	  <?php
		$jml_kategori = DB::table('kategori')->count();
	  ?>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Jumlah Kategori</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $jml_kategori; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> </span>
                    <span class="text-nowrap"></span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
				<?php
					$jml_branding = DB::table('branding')->count();
				  ?>
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Jumlah Branding</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $jml_branding; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                        <i class="ni ni-chart-pie-35"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i></span>
                    <span class="text-nowrap"></span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
				<?php
					$jml_pengguna = DB::table('users')->count();
				  ?>
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Pengguna</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $jml_pengguna; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="ni ni-money-coins"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i></span>
                    <span class="text-nowrap"></span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
				<?php
					$jml_like = DB::table('likes')->count();
				  ?>
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Kunjungan</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $jml_like; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                        <i class="ni ni-chart-bar-32"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> </span>
                    <span class="text-nowrap"></span>
                  </p>
                </div>
              </div>
            </div>
			 <div class="col-xl-4 col-md-12">
              <div class="card card-stats">
                <!-- Card body -->
				 <?php 
					$iden = DB::table('website')->where('id_website', 1)->first();

					?>
                <div class="card-body" align="center">
                  <div class="row col-auto" >
                    <img src="{{asset('logo/'.$iden->logo)}}" class="navbar-brand-img" alt="...">
                  </div>
                  
                </div>
              </div>
            </div>
			<div class="col-xl-8 col-md-12">
              <div class="card card-stats">
                <!-- Card body -->
				 <?php 
					$iden = DB::table('website')->where('id_website', 1)->first();

					?>
                <div class="card-body" align="center">
                  <p>
				  <h1><?php echo $iden->nama_website; ?></h1>
				  <h3><?php echo $iden->owner; ?></h3>
				  <h3><?php echo date('Y-m-d'); ?></h3>
				  </p>
                  
                </div>
              </div>
            </div>
			<div class="col-xl-12 col-md-12">
              <div class="card card-stats">
                <!-- Card body -->
				 <?php 
					
					foreach ($datacari as $rc ){
					?>
                <div class="card-body" align="center">
                  <p>
				  <div class="col-lg-3 col-md-6">
                  <a href="/<?php echo $rc->app_link ?>" button type="button" class="btn-icon-clipboard" data-clipboard-text="active-40" title="" data-original-title="Copy to clipboard">
                    <div>
                      <i class="ni ni-<?php echo $rc->icons; ?>"></i>
                      <span><?php echo $rc->nama_submenu; ?></span>
                    </div>
                  </button></a>
                </div>
				  </p>
					<?php } ?>
                </div>
              </div>
            </div>
          </div>
		
        
      <!-- Footer -->
     @include('admin/footer')
    </div>
    @endsection

  