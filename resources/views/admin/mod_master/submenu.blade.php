@extends('admin/template')
@section('content')
    

<div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Sub Menu</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                   <tr>
                    <th width="50px">No</th>
						<th>Menu</th>
						<th>submenu</th>
						<th>Link</th>
						<th>aktifs</th>
						<th>icons</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
                    @foreach ($record as $item)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->nama_menu}}</td>
                    <td>{{$item->nama_submenu}}</td>
                    <td>{{$item->app_link}}</td>
                    <td>{{$item->aktifs}}</td>
                    <td><i class="ni ni-{{$item->icons}} text-{{$item->tekss}}" style="font-size: 25px;"></i></td>
                    <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#menu{{$item->id_menu}}">
                          <i class="fa fa-edit" ></i>
                        </button>
                        <a href="{{asset('hapus-menu/'.$item->id_menu)}}"><button title="Hapus" type="button" class="btn btn-danger btn-sm" onclick=" confirm('Apa anda yakin untuk hapus Data ini?')">
                          <i class="fa fa-trash" ></i>
                        </button></a>
                    </td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
        <!-- Modal Input  -->
            <form action="{{asset('simpan-submenu')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Menu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                          
                        <label>Menu Utama</label>
                        <select name="id_menu" class="form-control">
                            
                            @foreach($menu as $r)
                            <option value="{{$r->id_menu}}">{{$r->nama_menu}}</option>
                            @endforeach
                        </select>
                    </div>
					  <div class="form-group">
                        <label>Nama sub Menu </label>
                        <input type="text" name="nama_submenu" required="required" class="form-control" placeholder="Nama sub menu  ..">
                      </div>
                      <div class="form-group">
                        <label>Icon</label>
                        <input type="text" name="icons" required="required" class="form-control" placeholder="Nama  ..">
                        <br> <a href="{{asset('icon')}}" target="_blank" >Lihat icon</a>
                      </div>
                      <div class="form-group">
                                    <label for="exampleFormControlSelect2">Aktif</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="aktifs">
                                        <option value="Y">Aktif</option>
                                        <option value="N">Non Aktif</option>
                                    </select>
                                </div>
                      <div class="form-group">
                                    <label for="exampleFormControlSelect2">Warna Teks</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="tekss">
                                        <option value="primary">primary</option>
                                        <option value="orange">orange</option>
										<option value="info">info</option>
										<option value="default">default</option>
                                    </select>
                                </div>
                      <div class="form-group">
                        <label>App Link</label>
                        <input type="text" name="app_link" required="required" class="form-control" placeholder="url link  ..">
                      </div>                 
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @foreach ($record as $item)
            <form action="{{asset('edit-submenu')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="menu{{$item->id_menu}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Sub Menu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     <input type="hidden" name="id" value="{{$item->id_menu}}" >
					  <div class="form-group">
                          
                        <label>Menu Utama</label>
                        <select name="id_menu" class="form-control">
                            <option value="{{$item->id_menu}}">{{$item->nama_menu}}</option>
                            @foreach($menu as $r)
                            
                            <option value="{{$r->id_menu}}">{{$r->nama_menu}}</option>
                            @endforeach
                        </select>
                    </div>
                     <div class="form-group">
                        <label>Nama Sub Menu </label>
                        <input type="text" name="nama_submenu" value="{{$item->nama_submenu}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                        <label>Icon</label>
                        <input type="text" name="icon" value="{{$item->icons}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                                    <label for="exampleFormControlSelect2">Level akses</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="aktifs">
									<?php if($item->aktif=="Y") { ?>
                                        <option value="Y" selected>Aktif</option>
                                        <option value="N">Non Aktif</option>
									<?php } else{ ?>
									<option value="Y" >Aktif</option>
                                        <option value="N" selected>Non Aktif</option>
									<?php } ?>
                                    </select>
									</div>
                      <div class="form-group">
                                    <label for="exampleFormControlSelect2">Warna Teks</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="tekss">
                                        <option value="primary">primary</option>
                                        <option value="orange">orange</option>
										<option value="info">info</option>
										<option value="default">default</option>
                                    </select>
                                </div>
                      <div class="form-group">
                        <label>Link url</label>
                        <input type="text" name="app_link" value="{{$item->app_link}}" required="required" class="form-control" placeholder="Link url  ..">
                      </div>  
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @endforeach
      <!-- Footer -->
     @include('admin/footer')
    </div>
    @endsection

  