@extends('admin/template')
@section('content')
    

<div class="container-fluid mt--6">
      <!-- Table -->
      

          @if($errors->has('foto'))
            <p class="text-danger">{{ $errors->first('foto') }}</p>
        @endif
      <div class="row">
        <div class="col">
          <div class="card">
              @if($errors->has('foto'))
            <p class=" alert alert-danger">{{ $errors->first('foto') }}</p>
        @endif
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Kategori</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>Nama Kategori</th>
                    <th>Foto Kategori</th>
                    <th>Action</th>
                  </tr>
                </thead>
                
                <tbody>
                    @foreach ($record as $item)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->nama_kategori}}</td>
                    <td><span class="avatar avatar-sm rounded-circle"><img src="{{asset('kategori/'.$item->foto)}}" alt=""></span></td>
                    <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#kategori{{$item->id_kategori}}">
                          <i class="fa fa-edit" ></i>
                        </button>
                        <a href="{{asset('hapus-kategori/'.$item->id_kategori)}}"><button title="Hapus" type="button" class="btn btn-danger btn-sm" onclick=" confirm('Apa anda yakin untuk hapus Data ini?')">
                          <i class="fa fa-trash" ></i>
                        </button></a>
                    </td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
        <!-- Modal Input  -->
            <form action="{{asset('simpan-kategori')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     
					  <div class="form-group">
                        <label>Nama Kategori </label>
                        <input type="text" name="nama_kategori" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                        <label>Foto </label>
                        <input type="file" name="foto" required="required" class="form-control" placeholder="Foto kategori ..">
                      </div>
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @foreach ($record as $item)
            <form action="{{asset('edit-kategori')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="kategori{{$item->id_kategori}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     
					  <div class="form-group">
                          <input type="hidden" name="id" value="{{$item->id_kategori}}" >
                        <label>Nama Kategori </label>
                        <input type="text" name="nama_kategori" value="{{$item->nama_kategori}}" required="required" class="form-control" placeholder="Nama  ..">
                      </div>
                      <div class="form-group">
                        <label>Foto </label>
                        <input type="file" name="foto"  class="form-control" placeholder="Foto kategori ..">
                      </div>
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @endforeach
      <!-- Footer -->
     @include('admin/footer')
    </div>
    @endsection

  