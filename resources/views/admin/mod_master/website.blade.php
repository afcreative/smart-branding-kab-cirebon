@extends('admin/template')
@section('content')
    

<div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col-lg-12">
          <div class="card-wrapper">
            <!-- Input groups -->
            <div class="card">
              <!-- Card header -->
              <div class="card-header">
                <h3 class="mb-0">Identitas Website</h3>
              </div>
              <div class="row col-12" align="center">
                    <div class="col-md-12">
                        <span class="avatar avatar-lg rounded-circle"><img src="{{asset('logo/'.$row->logo)}}" alt=""></span>
                    </div>
              </div>
              <!-- Card body -->
              <div class="card-body">
                <form action="{{asset('edit-websites')}}" method="post"  enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id_website" value="{{$row->id_website}}">
                  <!-- Input groups with icon -->
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-award"></i></span>
                          </div>
                          <input class="form-control" value="{{$row->nama_website}}" name="nama_website" placeholder="Nama website" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-info"></i></span>
                          </div>
                          <input class="form-control" value="{{$row->nama_website}}" name="nama_website" placeholder="Title .." type="text">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-star"></i></span>
                          </div>
                          <input class="form-control" value="{{$row->meta_deskripsi}}" name="meta_deskripsi" placeholder="Meta deskripsi" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-street-view"></i></span>
                          </div>
                          <input class="form-control" value="{{$row->owner}}" name="owner" placeholder="Owner" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                          </div>
                          <input class="form-control" value="{{$row->link_url}}" name="link_url" placeholder="Link Url" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-group input-group-merge">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-eye"></i></span>
                          </div>
                          <input class="form-control" value="{{$row->logo}}" name="foto" placeholder="Logo" type="file">
                        </div>
                      </div>
                    </div>
                    
                    
                  </div>
                  
                  <!-- Input groups with icon -->
                  
                
              </div>
              <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
            </div>
            </form>
            <!-- Dropdowns -->
            
            <!-- Datepicker -->
           
            <!-- Text editor -->
           
          </div>
        </div>
        
      </div>
       
      <!-- Footer -->
     @include('admin/footer')
    </div>
    @endsection

  