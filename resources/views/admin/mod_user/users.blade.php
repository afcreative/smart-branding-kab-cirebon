@extends('admin/template')
@section('content')
    

<div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Users</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Nama Lengkap</th>
                    <th>Email</th>
                    <th>No </th>
                    <th>Level</th>
                    <th>Blokir</th>
                    <th>Foto</th>
                    <th>Action</th>
                  </tr>
                </thead>
                
                <tbody>
                    @foreach ($record as $item)
                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$item->username}}</td>
                    <td>{{$item->nama_lengkap}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->no_telp}}</td>
                    <td>{{$item->level}}</td>
                    <td>{{$item->blokir}}</td>
                    <td><span class="avatar avatar-sm rounded-circle"><img src="{{asset('foto_user/'.$item->foto_user)}}" alt=""></span></td>
                    <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#kategori{{$item->id_users}}">
                          <i class="fa fa-edit" ></i>
                        </button>
                        <a href="{{asset('hapus-user/'.$item->id_users)}}"><button title="Hapus" type="button" class="btn btn-danger btn-sm" onclick=" confirm('Apa anda yakin untuk hapus Data ini?')">
                          <i class="fa fa-trash" ></i>
                        </button></a>
                    </td>
                  </tr>
                 @endforeach
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
        <!-- Modal Input  -->
            <form action="{{asset('simpan-user')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     
					  <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" required="required" class="form-control" placeholder="Username  ..">
                      </div>
                      <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="password" required="required" class="form-control" placeholder="Password  ..">
                      </div>
                      <div class="form-group">
                        <label>Nama lengkap</label>
                        <input type="text" name="nama_lengkap" required="required" class="form-control" placeholder="Nama lengkap  ..">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" required="required" class="form-control" placeholder="Email  ..">
                      </div>
                      <div class="form-group">
                        <label>No Telp</label>
                        <input type="text" name="no_telp" required="required" class="form-control" placeholder="No Telp  ..">
                      </div>
                      <div class="form-group">
                        <label>Level</label>
                        <input type="text" name="level" required="required" class="form-control" placeholder="Level  ..">
                      </div>
                      <div class="form-group">
                        <label>Blokir</label>
                        <input type="text" name="blokir" required="required" class="form-control" placeholder="Blokir  ..">
                      </div>
                      
                      <div class="form-group">
                        <label>Foto </label>
                        <input type="file" name="foto" required="required" class="form-control" placeholder="Foto user ..">
                      </div>
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @foreach ($record as $item)
            <form action="{{asset('edit-user')}}" method="post"  enctype="multipart/form-data">
                @csrf
              <div class="modal fade" id="kategori{{$item->id_users}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     <input type="hidden" name="id" value="{{$item->id_users}}" >
					  
                      <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" value="{{$item->username}}" required="required" class="form-control" placeholder="Username  ..">
                      </div>
                      <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="password" value="{{$item->password}}" required="required" class="form-control" placeholder="Password  ..">
                      </div>
                      <div class="form-group">
                        <label>Nama lengkap</label>
                        <input type="text" name="nama_lengkap" value="{{$item->nama_lengkap}}" required="required" class="form-control" placeholder="Nama lengkap  ..">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" value="{{$item->email}}" required="required" class="form-control" placeholder="Email  ..">
                      </div>
                      <div class="form-group">
                        <label>No Telp</label>
                        <input type="text" name="no_telp" value="{{$item->no_telp}}" required="required" class="form-control" placeholder="No Telp  ..">
                      </div>
                      <div class="form-group">
                        <label>Level</label>
                        <input type="text" name="level" value="{{$item->level}}" value="{{$item->id_users}}" required="required" class="form-control" placeholder="Level  ..">
                      </div>
                      <div class="form-group">
                        <label>Blokir</label>
                        <input type="text" name="blokir" value="{{$item->blokir}}" required="required" class="form-control" placeholder="Blokir  ..">
                      </div>
                      <div class="form-group">
                        <label>Foto </label>
                        <input type="file" name="foto"  class="form-control" placeholder="Foto kategori ..">
                      </div>
					  
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            @endforeach
      <!-- Footer -->
     @include('admin/footer')
    </div>
    @endsection

  