 <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
        <a class="navbar-brand">
          <img src="{{asset('logo/'.$iden->logo)}}" class="navbar-brand-img" alt="...">
        </a>
        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
           
             <?php 
            $record = DB::select('select * from menu order by urutan ASC');

            ?>
          @foreach ($record as $ritem)
          <li class="nav-item">
              <a class="nav-link" href="#navbar-<?php echo $ritem->id_menu; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-dashboards">
                <i class="ni ni-<?php echo $ritem->icon; ?> text-<?php echo $ritem->teks; ?>"></i>
                <span class="nav-link-text"><?php echo $ritem->nama_menu; ?></span>
              </a>
              <div class="collapse" id="navbar-<?php echo $ritem->id_menu; ?>">
                <ul class="nav nav-sm flex-column">
              <?php 
              $submenu = DB::select("select * from submenu where id_menu='".$ritem->id_menu."'");
              foreach($submenu as $r) { 
					
					
					      ?>
                  <li class="nav-item">
                    <a href="/<?php echo $r->app_link ?>" class="nav-link"><i class="ni ni-<?php echo $r->icons; ?> text-<?php echo $r->tekss; ?>"></i><?php echo $r->nama_submenu ?></a>
                  </li>
                <?php  } ?> 
                  
                </ul>
              </div>
            </li>
          @endforeach 
            
          </ul>
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">Documentation</h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="/dokumentasi">
                <i class="ni ni-spaceship"></i>
                <span class="nav-link-text">Aplikasi</span>
              </a>
            </li>
            
          </ul>
        </div>
      </div>
    </div>
  </nav>