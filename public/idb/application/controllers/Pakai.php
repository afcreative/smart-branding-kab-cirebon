<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Pakai extends CI_Controller {

	function pemakaian(){
		if (isset($_POST['submit'])){
			$data = array(
								'no_pemakaian'=>$this->db->escape_str($this->input->post('no_poorder')),
								'tgl_pemakaian'=>$this->db->escape_str($this->input->post('tgl_pemakaian')),
								'id_pemakaian'=>$this->db->escape_str($this->input->post('id_pemakaian')),
								'id_gudang'=>$this->db->escape_str($this->input->post('id_gudang')),
								'tgl_keluar'=>$this->db->escape_str($this->input->post('tgl_keluar')),
								'id_users'=>$this->session->id_users);
                    
            
            $this->model_app->insert('tb_pemakaian',$data);
			redirect('pakai/pemakaian');
		}else
		{
			$data['gudang'] = $this->db->query("Select * From tb_pemakaian a, pemakaian b, gudang c where a.id_gudang=c.id_gudang and a.id_pemakaian=b.id_pemakaian ");
			$this->template->load('app/template','app/mod_produksi/view_pemakaian',$data);
		}
	}
	function basket(){
		
		$id_barang=$this->input->post('id_barang');
		$qty=$this->input->post('qty');
		$harga=$this->input->post('harga');
		$no_poorder=$this->input->post('no_poorder');
		$id_detpo=$this->input->post('id_detpo');
		$data=array(
				'id_barang'=>$id_barang,
				'qty'=>$qty,
				'harga'=>$harga,
				'no_pemakaian'=>$no_poorder
				);
		if($id_detpo !=""){
			$where = array('id_detail' => $id_detpo);
            $this->model_app->update('detail_pemakaian', $data, $where);	
		}else{
		$this->model_app->insert('detail_pemakaian',$data);	
		}		
				
		
		
		echo json_encode($dataBarang);
	}
	function ambilbasket(){
		$no_poorder=$this->input->post('no_poorder');
		$dataBarang=$this->db->query("Select * From detail_pemakaian a, barang b where a.id_barang=b.id_barang and a.no_pemakaian='$no_poorder' ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function hapusdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		
		$id = array('id_detail' => $id_detpo);
		$this->model_app->delete('detail_pemakaian',$id);
		
	}
	function editdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		$dataBarang=$this->db->query("Select * From detail_pemakaian a, barang b where a.id_barang=b.id_barang and a.id_detail='$id_detpo' ")->result_array();
		echo json_encode($dataBarang);
	}
	function hapus_pemakaian(){
		$id_pemakaian=$this->uri->segment(3);
		$cari=$this->db->query("Select * From tb_pemakaian where id_pemakaian='$id_pemakaian' ")->row_array();
		$id = array('id_pemakaian' => $id_pemakaian);
		$this->model_app->delete('tb_pemakaian',$id);
		$id2 = array('no_pemakaian' => $cari['no_pemakaian']);
		$this->model_app->delete('detail_pemakaian',$id2);
		redirect('pakai/pemakaian');
	}
	
	
	
	
}
?>