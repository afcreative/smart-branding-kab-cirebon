<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Produk extends CI_Controller {

	function produksi(){
		if (isset($_POST['submit'])){
			$data = array(
								'no_produksi'=>$this->db->escape_str($this->input->post('no_poorder')),
								'tgl_produksi'=>$this->db->escape_str($this->input->post('tgl_produksi')),
								'nama_proses'=>$this->db->escape_str($this->input->post('nama_proses')),
								'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')),
								'id_users'=>$this->session->id_users);
                    
            
            $this->model_app->insert('produksi',$data);
			redirect('produk/produksi');
		}else
		{
			$data['gudang'] = $this->db->query("Select * From produksi ");
			$this->template->load('app/template','app/mod_produksi/view_produksi',$data);
		}
	}
	function basket(){
		
		$id_barang=$this->input->post('id_barang');
		$qty=$this->input->post('qty');
		$no_poorder=$this->input->post('no_poorder');
		$id_detpo=$this->input->post('id_detpo');
		$data=array(
				'id_barang'=>$id_barang,
				'qty'=>$qty,
				'no_produksi'=>$no_poorder
				);
		if($id_detpo !=""){
			$where = array('id_detail' => $id_detpo);
            $this->model_app->update('detail_produksi', $data, $where);	
		}else{
		$this->model_app->insert('detail_produksi',$data);	
		}		
				
		
		
		echo json_encode($dataBarang);
	}
	function ambilbasket(){
		$no_poorder=$this->input->post('no_poorder');
		$dataBarang=$this->db->query("Select * From detail_produksi a, barang b where a.id_barang=b.id_barang and a.no_produksi='$no_poorder' ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function hapusdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		
		$id = array('id_detail' => $id_detpo);
		$this->model_app->delete('detail_produksi',$id);
		
	}
	function editdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		$dataBarang=$this->db->query("Select * From detail_produksi a, barang b where a.id_barang=b.id_barang and a.id_detail='$id_detpo' ")->result_array();
		echo json_encode($dataBarang);
	}
	function hapus_produksi(){
		$id_produksi=$this->uri->segment(3);
		$cari=$this->db->query("Select * From produksi where id_produksi='$id_produksi' ")->row_array();
		$id = array('id_produksi' => $id_produksi);
		$this->model_app->delete('produksi',$id);
		$id2 = array('no_produksi' => $cari['no_produksi']);
		$this->model_app->delete('detail_produksi',$id2);
		redirect('produk/produksi');
	}
	
	
	
	
}
?>