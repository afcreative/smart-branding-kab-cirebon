<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Import extends CI_Controller {


        public function excel()
        {
            if(isset($_FILES["file"]["name"])){
                  // upload
                $file_tmp = $_FILES['file']['tmp_name'];
                $file_name = $_FILES['file']['name'];
                $file_size =$_FILES['file']['size'];
                $file_type=$_FILES['file']['type'];
                // move_uploaded_file($file_tmp,"uploads/".$file_name); // simpan filenya di folder uploads
                
                $object = PHPExcel_IOFactory::load($file_tmp);
        
                foreach($object->getWorksheetIterator() as $worksheet){
        
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
        
                    for($row=5; $row<=$highestRow; $row++){
						$tgl_absensi = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                        $id_karyawan = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                        $datang = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $pulang = $worksheet->getCellByColumnAndRow(3, $row)->getValue();

                        $data[] = array(
                            'tgl_absensi'    => $tgl_absensi,
							'id_karyawan'    => $id_karyawan,
                            'datang'          =>$datang,
                            'pulang'         =>$pulang,
							'ket'         =>"Hadir",
                        );
						
                    } 
					
        
                }
				
				$this->db->insert_batch('absensi', $data);
				                
                redirect('transaksi/absensi');
            }
            else
            {
            
                redirect('transaksi/absensi');
            }
        }
	
	
	
}
?>