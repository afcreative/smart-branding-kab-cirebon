<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Keuangan extends CI_Controller {
	function transaksi(){
		
		if (isset($_POST['submit'])){
		$id_users=$this->session->id_users;
				    $data = array('id_akun'=>$this->db->escape_str($this->input->post('id_akun')),
								'jumlah'=>$this->db->escape_str($this->input->post('jumlah')),
								'keterangan'=>$this->db->escape_str($this->input->post('keterangan')),
								'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
								'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
								'id_users'=>$id_users);
                    
            
            $this->model_app->insert('transaksi',$data);
			redirect('keuangan/transaksi');
			
		}else{
            $data['record'] = $this->db->query("Select * From transaksi a, akun b where a.id_akun=b.id_akun");
			$this->template->load('app/template','app/mod_keuangan/view_transaksi',$data);
		}
	}
	function edit_transaksi(){
			$id=$this->uri->segment(3);
			 $data = array('id_akun'=>$this->db->escape_str($this->input->post('id_akun')),
								'jumlah'=>$this->db->escape_str($this->input->post('jumlah')),
								'keterangan'=>$this->db->escape_str($this->input->post('keterangan')),
								'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
								'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
								'id_users'=>$id_users);
            $where = array('id_transaksi' => $id);
            $this->model_app->update('transaksi', $data, $where);

             

			redirect('keuangan/transaksi');
	
		
	}
	function hapus_transaksi(){
		$id = array('id_transaksi' => $this->uri->segment(3));
		$this->model_app->delete('transaksi',$id);
		
		redirect('keuangan/transaksi');
		
	}
	function detail_transaksi(){
			$data['record'] = $this->db->query("Select * From transaksi a, akun b where a.id_akun=b.id_akun and a.id_bank ='".$this->uri->segment(3)."'");
			$data['id_bank'] = $this->uri->segment(3);
			$this->template->load('app/template','app/mod_keuangan/detail_transaksi',$data);
	}
	function getAkun(){
		$jenis=$this->input->post('jenis');
		$data=$this->db->query("Select * From akun where jenis='$jenis' ")->result_array();
		
		echo json_encode($data);
	}
	function saldo_keuangan(){
		$data['record'] = $this->db->query("Select * From bank");
			$this->template->load('app/template','app/mod_keuangan/view_keuangan',$data);	
	}
	function piutang(){
		if (isset($_POST['submit'])){
		$id_users=$this->session->id_users;
				    $data = array('id_pelanggan'=>$this->db->escape_str($this->input->post('id_pelanggan')),
								'jml_piutang'=>$this->db->escape_str($this->input->post('jml_piutang')),
								'tgl_piutang'=>$this->db->escape_str($this->input->post('tgl_piutang')),
								'keterangan'=>$this->db->escape_str($this->input->post('keterangan')),
								'id_users'=>$id_users);
                    
            
            $this->model_app->insert('piutang',$data);
			redirect('keuangan/piutang');
			
		}else{
			$data['record'] = $this->db->query("Select * From piutang a, pelanggan b where a.id_pelanggan=b.id_pelanggan ");
			$this->template->load('app/template','app/mod_piutang/view_piutang',$data);	
		}
	}
	function hutang(){
		if (isset($_POST['submit'])){
			$b=$this->input->post('tgl_hutang');
			$a=$this->input->post('lama_pinjaman');
			$ab=$this->input->post('tot_hutang');
			$x="+$a month";
			$tgl_tagihan=date('Y-m-d', strtotime("$x", strtotime( $b ))); //tambah tanggal sebanyak 6 bulan
		$id_users=$this->session->id_users;
		$jml_tagihan=$ab/$a;
				    $data = array('id_karyawan'=>$this->db->escape_str($this->input->post('id_karyawan')),
								'tot_hutang'=>$this->db->escape_str($this->input->post('tot_hutang')),
								'tgl_hutang'=>$this->db->escape_str($this->input->post('tgl_hutang')),
								'lama_pinjaman'=>$this->db->escape_str($this->input->post('lama_pinjaman')),
								'jml_tagihan'=>$jml_tagihan,
								'sampai_tgl'=>$tgl_tagihan,
								'ket'=>$this->db->escape_str($this->input->post('ket')),
								'id_users'=>$id_users);
                    
            
            $this->model_app->insert('hutang',$data);
			redirect('keuangan/hutang');
			
		}else{
			$data['record'] = $this->db->query("Select * From hutang a, karyawan b where a.id_karyawan=b.id_karyawan ");
			$this->template->load('app/template','app/mod_hutang/view_hutang',$data);	
		}
	}
	function edit_hutang(){
	$b=$this->input->post('tgl_hutang');
			$a=$this->input->post('lama_pinjaman');
			$ab=$this->input->post('tot_hutang');
			$x="+$a month";
			$tgl_tagihan=date('Y-m-d', strtotime("$x", strtotime( $b ))); //tambah tanggal sebanyak 6 bulan
		$id_users=$this->session->id_users;
		$jml_tagihan=$ab/$a;
				    $data = array('id_karyawan'=>$this->db->escape_str($this->input->post('id_karyawan')),
								'tot_hutang'=>$this->db->escape_str($this->input->post('tot_hutang')),
								'tgl_hutang'=>$this->db->escape_str($this->input->post('tgl_hutang')),
								'lama_pinjaman'=>$this->db->escape_str($this->input->post('lama_pinjaman')),
								'jml_tagihan'=>$jml_tagihan,
								'sampai_tgl'=>$tgl_tagihan,
								'ket'=>$this->db->escape_str($this->input->post('ket')),
								'id_users'=>$id_users);
                    
            
            $where = array('id_hutang' => $this->uri->segment(3));
            $this->model_app->update('hutang', $data, $where);
			redirect('keuangan/hutang');	
		
	}
	function hapus_hutang(){
		$id = array('id_hutang' => $this->uri->segment(3));
		$this->model_app->delete('hutang',$id);
		
		redirect('keuangan/hutang');
	}
}
?>