<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Finance extends CI_Controller {

	function advance(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('id_order'=>$this->uri->segment(3),
								'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
								'jml_bayar'=>$this->db->escape_str($this->input->post('tot_Tagihan')),
								'tgl_bayar'=>$this->db->escape_str($this->input->post('tgl_bayar')),
								'id_users'=>$this->session->id_users,
								'PPN'=>$this->db->escape_str($this->input->post('PPN')));
                    
            
            $this->model_app->insert('uangmuka',$data);
			redirect('finance/advance');
			
		}else{
            $data['record'] = $this->db->query("Select * From customer a, orders b where a.id_customer=b.id_customer ");
			$this->template->load('app/template','app/mod_finance/view_advance',$data);
		}
	}
	function purchase(){
		if (isset($_POST['submit'])){
	
				    $data = array('tgl_poorder'=>$this->db->escape_str($this->input->post('tgl_poorder')),
					'no_poorder'=>$this->db->escape_str($this->input->post('no_poorder')),
								'keterangan'=>$this->db->escape_str($this->input->post('keterangan')),
								'id_suplier'=>$this->db->escape_str($this->input->post('id_suplier')),
								'id_gudang'=>$this->db->escape_str($this->input->post('id_gudang')),
								'termin_bayar'=>$this->db->escape_str($this->input->post('termin_bayar')),
								'currency'=>$this->db->escape_str($this->input->post('currency')),
								'no_posuplier'=>$this->db->escape_str($this->input->post('no_posuplier')),
								'tgl_posuplier'=>$this->db->escape_str($this->input->post('tgl_posuplier')),
								'tgl_kirim'=>$this->db->escape_str($this->input->post('tgl_kirim')),
								'jenis_pemakaian'=>$this->db->escape_str($this->input->post('jenis_pemakaian')),
								'id_users'=>$this->session->id_users);
                    
            
            $this->model_app->insert('poorders',$data);
			redirect('finance/purchase');
			
		}else{
			$data['record'] = $this->db->query("Select * From suplier a, poorders b where a.id_suplier=b.id_suplier ");
			$this->template->load('app/template','app/mod_po/view_purchase',$data);	
		}
	}
	function ambilsuplier(){
		$id_suplier=$this->input->post('id_suplier');
		$dataBarang=$this->db->query("Select * From suplier where id_suplier='$id_suplier' ")->result_array();
		
		echo json_encode($dataBarang);
	}
	function basket(){
		
		$id_barang=$this->input->post('id_barang');
		$qty=$this->input->post('qty');
		$harga=$this->input->post('harga');
		$no_poorder=$this->input->post('no_poorder');
		$id_detpo=$this->input->post('id_detpo');
		$data=array(
				'id_barang'=>$id_barang,
				'qty'=>$qty,
				'harga'=>$harga,
				'no_poorder'=>$no_poorder
				);
		if($id_detpo !=""){
			$where = array('id_detpo' => $id_detpo);
            $this->model_app->update('detail_poorder', $data, $where);	
		}else{
		$this->model_app->insert('detail_poorder',$data);	
		}		
				
		
		
		echo json_encode($dataBarang);
	}
	function ambilbasket(){
		$no_poorder=$this->input->post('no_poorder');
		$dataBarang=$this->db->query("Select * From detail_poorder a, barang b where a.id_barang=b.id_barang and a.no_poorder='$no_poorder' ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function ambilbarang(){
		$id_barang=$this->input->post('id_barang');
		$dataBarang=$this->db->query("Select * From barang where id_barang='$id_barang' ")->result_array();
		
		echo json_encode($dataBarang);
	}
	function grandbasket(){
		$no_poorder=$this->input->post('no_poorder');
		$data=$this->db->query("Select sum(qty*harga)as grantot From detail_poorder  where no_poorder='$no_poorder'  ")->result_array();
		echo json_encode($data);
		
	}
	function hapusdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		
		$id = array('id_detpo' => $id_detpo);
		$this->model_app->delete('detail_poorder',$id);
		
	}
	function editdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		$dataBarang=$this->db->query("Select * From detail_poorder a, barang b where a.id_barang=b.id_barang and a.id_detpo='$id_detpo' ")->result_array();
		echo json_encode($dataBarang);
	}
	function penerimaanpo(){
		if (isset($_POST['submit'])){
			
			$data = array(
					'no_poorder'=>$this->db->escape_str($this->input->post('no_poorder')),
					'id_users'=>$this->session->id_users,
					'tgl_penerimaanpo'=>$this->db->escape_str($this->input->post('tgl_penerimaanpo')));
                    
            
            $this->model_app->insert('penerimaan_po',$data);
			$last_id = $this->db->insert_id();
			
			$jml=count($this->input->post('box'));
			$qty=$this->input->post('qty');
			$id_barang=$this->input->post('id_barang');
			$harga=$this->input->post('harga');
			
			for($i=0; $i < $jml; $i++){
			$data2 = array(
					'id_penerimaanpo'=>$last_id,
					'qty'=>$qty[$i],
					'id_barang'=>$id_barang[$i],
					'harga'=>$harga[$i]);	
			$this->model_app->insert('detail_penerimaanpo',$data2);	
			}
		
			redirect('finance/penerimaanpo');
			
		}else{
		$data['record'] = $this->db->query("Select * From poorders a, suplier b, gudang c where a.id_suplier=b.id_suplier and a.id_gudang=c.id_gudang ");
		$this->template->load('app/template','app/mod_po/penerimaan_po',$data);		
		}
	}
	function detail_penerimaanpo(){
		$data['row']=$this->db->query("Select * From poorders a, suplier b, gudang c where a.id_suplier=b.id_suplier and a.id_gudang=c.id_gudang and a.no_poorder='".$this->uri->segment(3)."'")->row_array();
	$data['record'] = $this->db->query("Select * From detail_poorder a, barang b where a.id_barang=b.id_barang and a.no_poorder='".$this->uri->segment(3)."' ");
			$this->template->load('app/template','app/mod_po/detail_penerimaanpo',$data);	
	}
	function penerimaan(){
		$id_pembelian=$this->uri->segment(3);
		if (isset($_POST['submit'])){
			
			$data = array(
					'tgl_penerimaan'=>$this->db->escape_str($this->input->post('tgl_penerimaan')),
					'id_users'=>$this->session->id_users,
					'id_pembelian'=>$this->db->escape_str($this->input->post('id_pembelian')));
                    
            
            $this->model_app->insert('penerimaan',$data);
			$last_id = $this->db->insert_id();
			
			$jml=count($this->input->post('box'));
			$qty=$this->input->post('qty');
			$id_bahanbaku=$this->input->post('id_bahanbaku');
			$harga=$this->input->post('harga');
			
			for($i=0; $i < $jml; $i++){
			$data2 = array(
					'id_penerimaan'=>$last_id,
					'qty'=>$qty[$i],
					'id_bahanbaku'=>$id_bahanbaku[$i],
					'harga'=>$harga[$i]);	
			$this->model_app->insert('detail_penerimaan',$data2);	
			}
		
			redirect('finance/penerimaan');
			
		}else{
			
			$data['row']=$this->db->query("Select * From pembelian a, suplier b where a.id_suplier=b.id_suplier and a.id_pembelian='$id_pembelian' ")->row_array();
			$data['record'] = $this->db->query("Select * From detail_pembelian a, bahanbaku b where a.id_bahanbaku=b.id_bahanbaku and a.id_pembelian='$id_pembelian'  ");
			$this->template->load('app/template','app/mod_pembelian/view_penerimaan',$data);
		}
	}
	function hapus_penerimaanpo(){
		$id = array('id_penerimaanpo' => $this->uri->segment(3));
		$this->model_app->delete('penerimaan_po',$id);
		$this->model_app->delete('detail_penerimaanpo',$id);
		
		redirect('finance/penerimaanpo');
		
	}
	
	function update_penerimaanpo(){
		
		$data = array(
					
					'tgl_penerimaanpo'=>$this->db->escape_str($this->input->post('tgl_penerimaanpo')));
                    
            
            $where = array('id_penerimaanpo' => $this->input->post('id_penerimaanpo'));
            $this->model_app->update('penerimaan_po', $data, $where);	
			
			
			$jml=count($this->input->post('box'));
			$qty=$this->input->post('qty');
			$id_detpenerimaan=$this->input->post('id_detpenerimaan');
			$harga=$this->input->post('harga');
			
			for($i=0; $i < $jml; $i++){
			$data2 = array(
					'qty'=>$qty[$i],
					'harga'=>$harga[$i]);	
			

			$where = array('id_detpenerimaan' => $id_detpenerimaan[$i]);
            $this->model_app->update('detail_penerimaanpo', $data2, $where);			
			}
		
			redirect('finance/penerimaanpo');
		
	}
	function tagihanpo(){
		if (isset($_POST['submit'])){
			
			$data = array(
					'id_poorder'=>$this->uri->segment(3),
					'id_users'=>$this->session->id_users,
					'no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
					'tgl_faktur'=>$this->db->escape_str($this->input->post('tgl_faktur')),
					'jml_bayar'=>$this->db->escape_str($this->input->post('jml_bayar')),
					'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
					'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')));
                    
            $this->model_app->insert('pembayaran',$data);
            
			
		
			redirect('finance/tagihanpo');
			
		}else{
		$data['record'] = $this->db->query("Select * From poorders a, suplier b, gudang c where a.id_suplier=b.id_suplier and a.id_gudang=c.id_gudang ");
		$this->template->load('app/template','app/mod_po/tagihan_po',$data);		
		}
	}
	function edit_tagihanpo(){
		$data = array(
					
					'id_users'=>$this->session->id_users,
					'no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
					'tgl_faktur'=>$this->db->escape_str($this->input->post('tgl_faktur')),
					'jml_bayar'=>$this->db->escape_str($this->input->post('jml_bayar')),
					'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
					'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')));
                    
            $where = array('id_pembayaran' => $this->uri->segment(3));
            $this->model_app->update('pembayaran', $data, $where);
            
			
		
			redirect('finance/tagihanpo');
		
	}
	function hapus_pembayaran(){
		$id = array('id_pembayaran' => $this->uri->segment(3));
		$this->model_app->delete('pembayaran',$id);
		
		redirect('finance/tagihanpo');
		
	}
	function jurnal(){
	$data['record'] = $this->db->query("Select * From jurnal a, bank b where a.cost_center=b.id_bank");
		$this->template->load('app/template','app/mod_finance/jurnal',$data);	
	}
	function creditnote(){
	$data['record'] = $this->db->query("Select * From jurnal a, bank b where a.cost_center=b.id_bank and a.credit !=0 ");
		$this->template->load('app/template','app/mod_finance/credit_note',$data);	
	}
	function debtnote(){
	$data['record'] = $this->db->query("Select * From jurnal a, bank b where a.cost_center=b.id_bank and a.debt !=0 ");
		$this->template->load('app/template','app/mod_finance/debt_note',$data);	
	}
	
	
	
	
}
?>