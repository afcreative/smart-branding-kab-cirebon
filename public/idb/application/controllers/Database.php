<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Database extends CI_Controller {

	function jabatan(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_jabatan'=>$this->db->escape_str($this->input->post('nama_jabatan')),
					'gapok'=>$this->db->escape_str($this->input->post('gapok')));
                    
            
            $this->model_app->insert('jabatan',$data);
			redirect('database/jabatan');
			
		}else{
            $data['record'] = $this->db->query("Select * From jabatan ");
			$this->template->load('app/template','app/mod_master/view_jabatan',$data);
		}
	}
	function edit_jabatan(){
			$id=$this->uri->segment(3);
			 $data = array('nama_jabatan'=>$this->db->escape_str($this->input->post('nama_jabatan')),
					'gapok'=>$this->db->escape_str($this->input->post('gapok')));
            $where = array('id_jabatan' => $id);
            $this->model_app->update('jabatan', $data, $where);

             

			redirect('database/jabatan');
	
		
	}
	function hapus_jabatan(){
		$id = array('id_jabatan' => $this->uri->segment(3));
		$this->model_app->delete('jabatan',$id);
		
		redirect('database/jabatan');
		
	}
	function intensif(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_intensif'=>$this->db->escape_str($this->input->post('nama_intensif')),
					'jumlah'=>$this->db->escape_str($this->input->post('jumlah')));
                    
            
            $this->model_app->insert('intensif',$data);
			redirect('database/intensif');
			
		}else{
            $data['record'] = $this->db->query("Select * From intensif ");
			$this->template->load('app/template','app/mod_master/view_intensif',$data);
		}
	}
	function edit_intensif(){
			$id=$this->uri->segment(3);
			 $data = array('nama_intensif'=>$this->db->escape_str($this->input->post('nama_intensif')),
					'jumlah'=>$this->db->escape_str($this->input->post('jumlah')));
            $where = array('id_intensif' => $id);
            $this->model_app->update('intensif', $data, $where);

             

			redirect('database/intensif');
	
		
	}
	function hapus_intensif(){
		$id = array('id_intensif' => $this->uri->segment(3));
		$this->model_app->delete('intensif',$id);
		
		redirect('database/intensif');
		
	}
	function karyawan(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nik'=>$this->db->escape_str($this->input->post('nik')),
					'nama_karyawan'=>$this->db->escape_str($this->input->post('nama_karyawan')),
					'id_jabatan'=>$this->db->escape_str($this->input->post('id_jabatan')),
					'id_bagian'=>$this->db->escape_str($this->input->post('id_bagian')),
					'tgl_lahir'=>$this->db->escape_str($this->input->post('tgl_lahir')),
					'status'=>$this->db->escape_str($this->input->post('status')),
					'alamat'=>$this->db->escape_str($this->input->post('alamat')),
					'no_hp'=>$this->db->escape_str($this->input->post('no_hp')),
					'no_rekening'=>$this->db->escape_str($this->input->post('no_rekening')),
					'tgl_masuk'=>$this->db->escape_str($this->input->post('tgl_masuk')),
					'keterangan'=>$this->db->escape_str($this->input->post('keterangan')));
                    
            
            $this->model_app->insert('karyawan',$data);
			redirect('database/karyawan');
			
		}else{
            $data['record'] = $this->db->query("Select * From karyawan a, jabatan b, bagian c where a.id_bagian=c.id_bagian and a.id_jabatan=b.id_jabatan ");
			$this->template->load('app/template','app/mod_master/view_karyawan',$data);
		}
	}
	function edit_karyawan(){
			$id=$this->uri->segment(3);
						    $data = array('nik'=>$this->db->escape_str($this->input->post('nik')),
					'nama_karyawan'=>$this->db->escape_str($this->input->post('nama_karyawan')),
					'id_jabatan'=>$this->db->escape_str($this->input->post('id_jabatan')),
					'id_bagian'=>$this->db->escape_str($this->input->post('id_bagian')),
					'tgl_lahir'=>$this->db->escape_str($this->input->post('tgl_lahir')),
					'status'=>$this->db->escape_str($this->input->post('status')),
					'alamat'=>$this->db->escape_str($this->input->post('alamat')),
					'no_hp'=>$this->db->escape_str($this->input->post('no_hp')),
					'no_rekening'=>$this->db->escape_str($this->input->post('no_rekening')),
					'tgl_masuk'=>$this->db->escape_str($this->input->post('tgl_masuk')),
					'keterangan'=>$this->db->escape_str($this->input->post('keterangan')));
            $where = array('id_karyawan' => $id);
            $this->model_app->update('karyawan', $data, $where);

             

			redirect('database/karyawan');
	
		
	}
	function hapus_karyawan(){
		$id = array('id_karyawan' => $this->uri->segment(3));
		$this->model_app->delete('karyawan',$id);
		
		redirect('database/karyawan');
		
	}
	
	
}
?>