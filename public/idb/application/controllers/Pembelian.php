<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Pembelian extends CI_Controller {

	function bahanbaku(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('id_satuan'=>$this->db->escape_str($this->input->post('id_satuan')),
								'nama_bahanbaku'=>$this->db->escape_str($this->input->post('nama_bahanbaku')),
								'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')));
                    
            
            $this->model_app->insert('bahanbaku',$data);
			redirect('pembelian/bahanbaku');
			
		}else{
            $data['record'] = $this->db->query("Select * From bahanbaku a, satuan b where a.id_satuan=b.id_satuan ");
			$this->template->load('app/template','app/mod_pembelian/view_bahanbaku',$data);
		}
	}
	function edit_bahanbaku(){
			$id=$this->uri->segment(3);
			$data = array('id_satuan'=>$this->db->escape_str($this->input->post('id_satuan')),
								'nama_bahanbaku'=>$this->db->escape_str($this->input->post('nama_bahanbaku')),
								'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')));
            $where = array('id_bahanbaku' => $id);
            $this->model_app->update('bahanbaku', $data, $where);

             

			redirect('pembelian/bahanbaku');
	
		
	}
	function hapus_bahanbaku(){
		$id = array('id_bahanbaku' => $this->uri->segment(3));
		$this->model_app->delete('bahanbaku',$id);
		
		redirect('pembelian/bahanbaku');
		
	}
	function po(){
	$data['record'] = $this->db->query("Select * From pembelian a, suplier b where a.id_suplier=b.id_suplier ");
	$this->template->load('app/template','app/mod_pembelian/view_po',$data);	
	}
	function hapus_po(){
	$id = array('id_pembelian' => $this->uri->segment(3));
		$this->model_app->delete('pembelian',$id);
		$this->model_app->delete('detail_pembelian',$id);
		
		redirect('pembelian/po');	
	}
	function permintaan(){
		
		if (isset($_POST['submit'])){
			$session_id=session_id();
			$data = array('no_invoice'=>$this->db->escape_str($this->input->post('no_invoice')),
					'tgl_invoice'=>$this->db->escape_str($this->input->post('tgl_invoice')),
					'id_users'=>$this->session->id_users,
					'id_suplier'=>$this->db->escape_str($this->input->post('id_suplier')));
                    
            
            $this->model_app->insert('pembelian',$data);
			$last_id = $this->db->insert_id();
			
			$this->db->query("INSERT INTO detail_pembelian SELECT * FROM
			temp_beli ");
			$this->db->query("UPDATE detail_pembelian SET id_pembelian = '$last_id' WHERE session='$session_id' ");
			$this->db->query("Delete From temp_beli");
			session_regenerate_id();
			redirect('pembelian/permintaan');
			
		}else{
            $data['record'] = $this->db->query("Select * From bahanbaku a, satuan b where a.id_satuan=b.id_satuan ");
			$this->template->load('app/template','app/mod_pembelian/view_pembelian',$data);
		}
	}
	function ambilbahanbaku(){
		$id_bahanbaku=$this->input->post('id_bahanbaku');
		$dataBarang=$this->db->query("Select * From bahanbaku where id_bahanbaku='$id_bahanbaku' ")->result_array();
		
		echo json_encode($dataBarang);
	}
	function ambilbarang(){
		$id_barang=$this->input->post('id_barang');
		$dataBarang=$this->db->query("Select * From barang where id_barang='$id_barang' ")->result_array();
		
		echo json_encode($dataBarang);
	}
	function ambilcustomer(){
		$id_customer=$this->input->post('id_customer');
		$dataBarang=$this->db->query("Select * From customer where id_customer='$id_customer' ")->result_array();
		
		echo json_encode($dataBarang);
	}
	function keranjang(){
		$session_id=session_id();
		$id_bahanbaku=$this->input->post('id_bahanbaku');
		$id_temp=$this->input->post('id_temp');
		$qty=$this->input->post('jml_beli');
		$harga_beli=$this->input->post('harga_beli');
		if($id_temp !=""){
		$data=array(
				'id_bahanbaku'=>$id_bahanbaku,
				'qty'=>$qty,
				'harga'=>$harga_beli,
				'session'=>$session_id
				);
				
			$where = array('id_temp' => $id_temp);
            $this->model_app->update('temp_beli', $data, $where);
		}else{
		$data=array(
				'id_bahanbaku'=>$id_bahanbaku,
				'qty'=>$qty,
				'harga'=>$harga_beli,
				'session'=>$session_id
				);
				
			$this->model_app->insert('temp_beli',$data);	
		}
		
		echo json_encode($dataBarang);
	}
	function basket(){
		
		$id_barang=$this->input->post('id_barang');
		$qty=$this->input->post('qty');
		$harga=$this->input->post('harga');
		$no_order=$this->input->post('no_order');
		$id_detpo=$this->input->post('id_detpo');
		$data=array(
				'id_barang'=>$id_barang,
				'qty'=>$qty,
				'harga'=>$harga,
				'no_order'=>$no_order
				);
		if($id_detpo !=""){
			$where = array('id_detpo' => $id_detpo);
            $this->model_app->update('detail_pobarang', $data, $where);	
		}else{
		$this->model_app->insert('detail_pobarang',$data);	
		}		
				
		
		
		echo json_encode($dataBarang);
	}
	function ambilbasket(){
		$no_order=$this->input->post('no_order');
		$dataBarang=$this->db->query("Select * From detail_pobarang a, barang b where a.id_barang=b.id_barang and a.no_order='$no_order' ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function grandbasket(){
		$no_order=$this->input->post('no_order');
		$data=$this->db->query("Select sum(qty*harga)as grantot From detail_pobarang  where no_order='$no_order'  ")->result_array();
		echo json_encode($data);
		
	}
	function ambildata(){
		$session_id=session_id();
		$dataBarang=$this->db->query("Select * From temp_beli a, bahanbaku b  where a.id_bahanbaku=b.id_bahanbaku  ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function ambiltemp(){
		$session_id=session_id();
		$dataBarang=$this->db->query("Select sum(qty * harga)as total From temp_beli ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function edittemp(){
		$id_temp=$this->input->post('id_temp');
		$dataBarang=$this->db->query("Select * From temp_beli a, bahanbaku b where a.id_bahanbaku=b.id_bahanbaku and a.id_temp='$id_temp' ")->result_array();
		echo json_encode($dataBarang);
	}
	function editdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		$dataBarang=$this->db->query("Select * From detail_pobarang a, barang b where a.id_barang=b.id_barang and a.id_detpo='$id_detpo' ")->result_array();
		echo json_encode($dataBarang);
	}
	function hapustemp(){
		$id_temp=$this->input->post('id_temp');
		
		$id = array('id_temp' => $id_temp);
		$this->model_app->delete('temp_beli',$id);
		
	}
	function hapusdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		
		$id = array('id_detpo' => $id_detpo);
		$this->model_app->delete('detail_pobarang',$id);
		
	}
	function ambilsuplier(){
		$id_suplier=$this->input->post('id_suplier');
		$data=$this->db->query("Select * From suplier where id_suplier='$id_suplier' ")->result_array();
		
		echo json_encode($data);
	}
	function edit_pembelian(){
		$id=$this->uri->segment(3);
		$data['row'] = $this->db->query("Select * From detail_pembelian a, pembelian b where a.id_pembelian=b.id_pembelian and a.id_pembelian='$id'")->row_array();
			$this->template->load('app/template','app/mod_pembelian/edit_pembelian',$data);
	}
	function update_pembelian(){
		
			$data=array(
				'id_bahanbaku'=>$this->db->escape_str($this->input->post('id_bahanbaku')),
				'qty'=>$this->db->escape_str($this->input->post('qty')),
				'harga'=>$this->db->escape_str($this->input->post('harga')),
				'id_pembelian'=>$this->db->escape_str($this->input->post('id_pembelian'))
				);
				
			$this->model_app->insert('detail_pembelian',$data);	

             

			$id=$this->input->post('id_pembelian');
		$data['row'] = $this->db->query("Select * From detail_pembelian a, pembelian b where a.id_pembelian=b.id_pembelian and a.id_pembelian='$id'")->row_array();
			$this->template->load('app/template','app/mod_pembelian/edit_pembelian',$data);
		
	}
	function update_keranjang(){
		
		$id_bahanbaku=$this->input->post('id_bahanbaku');
		$id_pembelian=$this->input->post('id_pembelian');
		$qty=$this->input->post('jml_beli');
		$harga_beli=$this->input->post('harga_beli');
		
		$data=array(
				'id_bahanbaku'=>$id_bahanbaku,
				'qty'=>$qty,
				'harga'=>$harga_beli,
				'id_pembelian'=>$id_pembelian
				);
				
			$this->model_app->insert('detail_pembelian',$data);	
		
		
		$id=$this->input->post('id_pembelian');
		$data['row'] = $this->db->query("Select * From detail_pembelian a, pembelian b where a.id_pembelian=b.id_pembelian and a.id_pembelian='$id'")->row_array();
			$this->template->load('app/template','app/mod_pembelian/edit_pembelian',$data);
	}
	function hapus_detailpembelian(){
		$id_detail=$this->uri->segment(3);
		$xc=$this->db->query("Select * From detail_pembelian where id_detail='$id_detail' ")->row_array();
		$id2 = array('id_detail' => $id_detail);
		$this->model_app->delete('detail_pembelian',$id2);
		
		
		$data['row'] = $this->db->query("Select * From detail_pembelian a, pembelian b where a.id_pembelian=b.id_pembelian and a.id_pembelian='".$xc['id_pembelian']."'")->row_array();
			$this->template->load('app/template','app/mod_pembelian/edit_pembelian',$data);	
	}
	function edit_detail(){
		
		$id=$this->uri->segment(3);
			$data = array('qty'=>$this->db->escape_str($this->input->post('qty')),
								'id_bahanbaku'=>$this->db->escape_str($this->input->post('id_bahanbaku')),
								'harga'=>$this->db->escape_str($this->input->post('harga')));
            $where = array('id_detail' => $id);
            $this->model_app->update('detail_pembelian', $data, $where);
		$xc=$this->db->query("Select * From detail_pembelian where id_detail='$id' ")->row_array();
		$data['row'] = $this->db->query("Select * From detail_pembelian a, pembelian b where a.id_pembelian=b.id_pembelian and a.id_pembelian='".$xc['id_pembelian']."'")->row_array();
			$this->template->load('app/template','app/mod_pembelian/edit_pembelian',$data);
		
	}
	function penerimaan(){
		$id_pembelian=$this->uri->segment(3);
		if (isset($_POST['submit'])){
			
			$data = array(
					'tgl_penerimaan'=>$this->db->escape_str($this->input->post('tgl_penerimaan')),
					'id_users'=>$this->session->id_users,
					'id_pembelian'=>$this->db->escape_str($this->input->post('id_pembelian')));
                    
            
            $this->model_app->insert('penerimaan',$data);
			$last_id = $this->db->insert_id();
			
			$jml=count($this->input->post('box'));
			$qty=$this->input->post('qty');
			$id_bahanbaku=$this->input->post('id_bahanbaku');
			$harga=$this->input->post('harga');
			
			for($i=0; $i < $jml; $i++){
			$data2 = array(
					'id_penerimaan'=>$last_id,
					'qty'=>$qty[$i],
					'id_bahanbaku'=>$id_bahanbaku[$i],
					'harga'=>$harga[$i]);	
			$this->model_app->insert('detail_penerimaan',$data2);	
			}
		
			redirect('pembelian/po');
			
		}else{
			
			$data['row']=$this->db->query("Select * From pembelian a, suplier b where a.id_suplier=b.id_suplier and a.id_pembelian='$id_pembelian' ")->row_array();
			$data['record'] = $this->db->query("Select * From detail_pembelian a, bahanbaku b where a.id_bahanbaku=b.id_bahanbaku and a.id_pembelian='$id_pembelian'  ");
			$this->template->load('app/template','app/mod_pembelian/view_penerimaan',$data);
		}
	}
	function receive (){
		$data['record'] = $this->db->query("Select * From penerimaan c, pembelian a, suplier b where a.id_suplier=b.id_suplier and c.id_pembelian=a.id_pembelian ");
		$this->template->load('app/template','app/mod_penerimaan/view_receive',$data);
	}
	function retur(){
		$id_penerimaan=$this->uri->segment(3);
		if (isset($_POST['submit'])){
			
			$data = array(
					'id_penerimaan'=>$this->db->escape_str($this->input->post('id_penerimaan')),
					'id_users'=>$this->session->id_users,
					'tgl_retur'=>$this->db->escape_str($this->input->post('tgl_retur')));
                    
            
             $this->model_app->insert('retur',$data);
			$last_id = $this->db->insert_id();
			$jml=count($this->input->post('box'));
			$qty=$this->input->post('qty');
			$id_bahanbaku=$this->input->post('id_bahanbaku');
			$harga=$this->input->post('harga');
			
			for($i=0; $i < $jml; $i++){
			$data2 = array(
					'id_retur'=>$last_id,
					'qty'=>$qty[$i],
					'id_bahanbaku'=>$id_bahanbaku[$i],
					'harga'=>$harga[$i]);	
			$this->model_app->insert('detail_retur',$data2);	
			}
		
			redirect('pembelian/receive');
			
		}else{
		$data['row']=$this->db->query("Select * From penerimaan c, pembelian a, suplier b where a.id_suplier=b.id_suplier and c.id_pembelian=a.id_pembelian and c.id_penerimaan='$id_penerimaan' ")->row_array();
			$data['record'] = $this->db->query("Select * From detail_penerimaan a, bahanbaku b where a.id_bahanbaku=b.id_bahanbaku and a.id_penerimaan='$id_penerimaan'  ");
		$this->template->load('app/template','app/mod_penerimaan/view_retur',$data);
		}
	}
	function lihat_retur(){
		$data['record'] = $this->db->query("Select * From retur d, penerimaan c, pembelian a, suplier b where a.id_suplier=b.id_suplier and c.id_pembelian=a.id_pembelian and d.id_penerimaan=c.id_penerimaan ");
		$this->template->load('app/template','app/mod_penerimaan/lihat_retur',$data);
		
	}
	function hapus_retur(){
	$id = array('id_retur' => $this->uri->segment(3));
		$this->model_app->delete('retur',$id);
		$this->model_app->delete('detail_retur',$id);
		
		redirect('pembelian/lihat_retur');		
		
	}
	function edit_retur(){
		$id=$this->uri->segment(3);
		if (isset($_POST['submit'])){
			
			$data = array(
					
					'tgl_retur'=>$this->db->escape_str($this->input->post('tgl_retur')));
                    
            $where = array('id_retur' => $id);
            $this->model_app->update('retur', $data, $where);
			
           
			$id2 = array('id_retur' => $this->uri->segment(3));
		
			$this->model_app->delete('detail_retur',$id2);
			
			
			$jml=count($this->input->post('box'));
			$qty=$this->input->post('qty');
			$id_bahanbaku=$this->input->post('id_bahanbaku');
			$harga=$this->input->post('harga');
			
			for($i=0; $i < $jml; $i++){
			$data2 = array(
					'id_retur'=>$id,
					'qty'=>$qty[$i],
					'id_bahanbaku'=>$id_bahanbaku[$i],
					'harga'=>$harga[$i]);	
			$this->model_app->insert('detail_retur',$data2);	
			}
		
			redirect('pembelian/receive');
			
		}else{
		$data['row'] = $this->db->query("Select * From retur d, penerimaan c, pembelian a, suplier b where a.id_suplier=b.id_suplier and c.id_pembelian=a.id_pembelian and d.id_penerimaan=c.id_penerimaan  and d.id_retur='$id'")->row_array();
		
		$data['record']=$this->db->query("Select * From detail_retur a, bahanbaku b where a.id_bahanbaku=b.id_bahanbaku and a.id_retur='$id' ");
			
		$this->template->load('app/template','app/mod_penerimaan/edit_retur',$data);	
		}	
	}
	function hapus_detailretur(){
		$id = array('id_det' => $this->uri->segment(3));
		
		$this->model_app->delete('detail_retur',$id);
		
		redirect('pembelian/lihat_retur');
	}
	
	
	
}
?>