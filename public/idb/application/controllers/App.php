<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class App extends CI_Controller {

	function index(){
		if (isset($_POST['submit'])){
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			$cek = $this->model_users->cek_login($username,$password,'users');
		    $row = $cek->row_array();
		    $total = $cek->num_rows();
			
			
			if ($total > 0){
				$this->session->set_userdata(array('id_users'=>$row['id_users'],
											   'username'=>$row['username'],
							   				   'level'=>$row['level']));
				redirect('app/home');
			}
			else{
				$data['title'] = 'Users &rsaquo; Log In';
				$this->load->view('app/view_login',$data);
			}
		}else{
			if ($this->session->level != ''){
				redirect('app/home');
			}else{
				$data['title'] = 'Users &rsaquo; Log In';
				$this->load->view('app/view_login',$data);
			}
		}
	}
	
	
	function home(){
		cek_session_admin();
		$data['title'] = 'Users &rsaquo; Log In';
		$this->template->load('app/template','app/home');
	}
	function users(){
		$id = $this->session->username;
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'assets/foto_user/';
        $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
        $config['max_size'] = '3000'; // kb
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto');
        $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                    $data = array('username'=>$this->db->escape_str($this->input->post('username')),
                                    'password'=>md5($this->input->post('password')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('nama_lengkap')),
                                    'email'=>$this->db->escape_str($this->input->post('email')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('no_telp')),
                                    'level'=>$this->db->escape_str($this->input->post('level')),
                                    'blokir'=>'N',
                                    'id_session'=>md5($this->input->post('a')).'-'.date('YmdHis'));
            }else{
				    $data = array('username'=>$this->db->escape_str($this->input->post('username')),
                                    'password'=>md5($this->input->post('password')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('nama_lengkap')),
                                    'email'=>$this->db->escape_str($this->input->post('email')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('no_telp')),
									'foto_user'=>$hasil['file_name'],
                                    'level'=>$this->db->escape_str($this->input->post('level')),
                                    'blokir'=>'N',
                                    'id_session'=>md5($this->input->post('a')).'-'.date('YmdHis'));
                    
            }
            $this->model_app->insert('users',$data);
			$data['record'] = $this->model_app->view('users');
			$this->template->load('app/template','app/mod_users/view_users',$data);
		}else{
            $data['record'] = $this->model_app->view('users');
			$this->template->load('app/template','app/mod_users/view_users',$data);
		}
	}
	function edit_users(){
			$id = $this->uri->segment(3);
	
		$config['upload_path'] = 'assets/foto_user/';
        $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
        $config['max_size'] = '3000'; // kb
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto');
        $hasil=$this->upload->data();
            if ($hasil['file_name']=='' ){
                    $data = array('username'=>$this->db->escape_str($this->input->post('username')),
								'password'=>md5($this->input->post('password')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('nama_lengkap')),
                                    'email'=>$this->db->escape_str($this->input->post('email')),
                                    'no_telp'=>$this->db->escape_str($this->input->post('no_telp')));
            }else {
                           $data = array('username'=>$this->db->escape_str($this->input->post('username')),
								'password'=>md5($this->input->post('password')),
                                    'nama_lengkap'=>$this->db->escape_str($this->input->post('nama_lengkap')),
                                    'email'=>$this->db->escape_str($this->input->post('email')),
									'foto_user'=>$hasil['file_name'],
                                    'no_telp'=>$this->db->escape_str($this->input->post('no_telp')));
            }
            $where = array('id_users' => $id);
            $this->model_app->update('users', $data, $where);

             

			redirect('app/users');
	
		
	}
	function hapus_users(){
		$id = array('id_users' => $this->uri->segment(3));
		$this->model_app->delete('users',$id);
		
		redirect('app/users');
		
	}
	function profile_users(){
	$id_users=$this->uri->segment(3);	
	$data['rows'] = $this->db->query("Select * From users where id_users='$id_users' ")->row_array();
	$this->template->load('app/template','app/mod_users/profile_users',$data);	
		
		
	}
	
	function kategori(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_kategori'=>$this->db->escape_str($this->input->post('nama_kategori')));
                    
            
            $this->model_app->insert('kategori',$data);
			redirect('app/kategori');
			
		}else{
            $data['record'] = $this->db->query("Select * From kategori ");
			$this->template->load('app/template','app/mod_master/view_kategori',$data);
		}
	}
	function edit_kategori(){
			$id=$this->uri->segment(3);
			 $data = array('nama_kategori'=>$this->db->escape_str($this->input->post('nama_kategori')));
            $where = array('id_kategori' => $id);
            $this->model_app->update('kategori', $data, $where);

             

			redirect('app/kategori');
	
		
	}
	function hapus_kategori(){
		$id = array('id_kategori' => $this->uri->segment(3));
		$this->model_app->delete('kategori',$id);
		
		redirect('app/kategori');
		
	}
	function satuan(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_satuan'=>$this->db->escape_str($this->input->post('nama_satuan')));
                    
            
            $this->model_app->insert('satuan',$data);
			redirect('app/satuan');
			
		}else{
            $data['record'] = $this->db->query("Select * From satuan ");
			$this->template->load('app/template','app/mod_master/view_satuan',$data);
		}
	}
	function edit_satuan(){
			$id=$this->uri->segment(3);
			 $data = array('nama_satuan'=>$this->db->escape_str($this->input->post('nama_satuan')));
            $where = array('id_satuan' => $id);
            $this->model_app->update('satuan', $data, $where);

             

			redirect('app/satuan');
	
		
	}
	function hapus_satuan(){
		$id = array('id_satuan' => $this->uri->segment(3));
		$this->model_app->delete('satuan',$id);
		
		redirect('app/satuan');
		
	}
	function suplier(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_suplier'=>$this->db->escape_str($this->input->post('nama_suplier')),
					'email'=>$this->db->escape_str($this->input->post('email')),
					'no_telp'=>$this->db->escape_str($this->input->post('no_telp')),
					'alamat'=>$this->db->escape_str($this->input->post('alamat')),
					'status'=>$this->db->escape_str($this->input->post('status')));
                    
            
            $this->model_app->insert('suplier',$data);
			redirect('app/suplier');
			
		}else{
            $data['record'] = $this->db->query("Select * From suplier ");
			$this->template->load('app/template','app/mod_master/view_suplier',$data);
		}
	}
	function edit_suplier(){
			$id=$this->uri->segment(3);
			$data = array('nama_suplier'=>$this->db->escape_str($this->input->post('nama_suplier')),
					'email'=>$this->db->escape_str($this->input->post('email')),
					'no_telp'=>$this->db->escape_str($this->input->post('no_telp')),
					'alamat'=>$this->db->escape_str($this->input->post('alamat')),
					'status'=>$this->db->escape_str($this->input->post('status')));
            $where = array('id_suplier' => $id);
            $this->model_app->update('suplier', $data, $where);

             

			redirect('app/suplier');
	
		
	}
	function hapus_suplier(){
		$id = array('id_suplier' => $this->uri->segment(3));
		$this->model_app->delete('suplier',$id);
		
		redirect('app/suplier');
		
	}
	function barang(){
		
		if (isset($_POST['submit'])){
	
				    $data = array(
					'nama_barang'=>$this->db->escape_str($this->input->post('nama_barang')),
					'keterangan'=>$this->db->escape_str($this->input->post('keterangan')),
					'id_users'=>$this->session->id_users);
                    
            
            $this->model_app->insert('barang',$data);
			redirect('app/barang');
			
		}else{
            $data['record'] = $this->db->query("Select * From barang ");
			$this->template->load('app/template','app/mod_master/view_barang',$data);
		}
	}
	function edit_barang(){
			$id=$this->uri->segment(3);
			 $data = array(
					'nama_barang'=>$this->db->escape_str($this->input->post('nama_barang')),
					'keterangan'=>$this->db->escape_str($this->input->post('keterangan')),
					'id_users'=>$this->session->id_users);
            $where = array('id_barang' => $id);
            $this->model_app->update('barang', $data, $where);

             

			redirect('app/barang');
	
		
	}
	function hapus_barang(){
		$id = array('id_barang' => $this->uri->segment(3));
		$this->model_app->delete('barang',$id);
		
		redirect('app/barang');
		
	}
	function pelanggan(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_pelanggan'=>$this->db->escape_str($this->input->post('nama_pelanggan')),
					'email'=>$this->db->escape_str($this->input->post('email')),
					'kode_pelanggan'=>$this->db->escape_str($this->input->post('kode_pelanggan')),
					'no_telp'=>$this->db->escape_str($this->input->post('no_telp')),
					'alamat'=>$this->db->escape_str($this->input->post('alamat')),
					'jk'=>$this->db->escape_str($this->input->post('jk')),
					'tanggal_lahir'=>$this->db->escape_str($this->input->post('tanggal_lahir')),
					'status'=>$this->db->escape_str($this->input->post('status')));
                    
            
            $this->model_app->insert('pelanggan',$data);
			redirect('app/pelanggan');
			
		}else{
            $data['record'] = $this->db->query("Select * From pelanggan ");
			$this->template->load('app/template','app/mod_master/view_pelanggan',$data);
		}
	}
	function edit_pelanggan(){
			$id=$this->uri->segment(3);
			    $data = array('nama_pelanggan'=>$this->db->escape_str($this->input->post('nama_pelanggan')),
					'email'=>$this->db->escape_str($this->input->post('email')),
					'kode_pelanggan'=>$this->db->escape_str($this->input->post('kode_pelanggan')),
					'no_telp'=>$this->db->escape_str($this->input->post('no_telp')),
					'alamat'=>$this->db->escape_str($this->input->post('alamat')),
					'jk'=>$this->db->escape_str($this->input->post('jk')),
					'tanggal_lahir'=>$this->db->escape_str($this->input->post('tanggal_lahir')),
					'status'=>$this->db->escape_str($this->input->post('status')));
            $where = array('id_pelanggan' => $id);
            $this->model_app->update('pelanggan', $data, $where);

             

			redirect('app/pelanggan');
	
		
	}
	function hapus_pelanggan(){
		$id = array('id_pelanggan' => $this->uri->segment(3));
		$this->model_app->delete('pelanggan',$id);
		
		redirect('app/pelanggan');
		
	}
	function akun(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('kode_akun'=>$this->db->escape_str($this->input->post('kode_akun')),
								'nama_akun'=>$this->db->escape_str($this->input->post('nama_akun')),
								'jenis'=>$this->db->escape_str($this->input->post('jenis')));
                    
            
            $this->model_app->insert('akun',$data);
			redirect('app/akun');
			
		}else{
            $data['record'] = $this->db->query("Select * From akun ");
			$this->template->load('app/template','app/mod_master/view_akun',$data);
		}
	}
	function edit_akun(){
			$id=$this->uri->segment(3);
			$data = array('kode_akun'=>$this->db->escape_str($this->input->post('kode_akun')),
								'nama_akun'=>$this->db->escape_str($this->input->post('nama_akun')),
								'jenis'=>$this->db->escape_str($this->input->post('jenis')));
            $where = array('id_akun' => $id);
            $this->model_app->update('akun', $data, $where);

             

			redirect('app/akun');
	
		
	}
	function hapus_akun(){
		$id = array('id_akun' => $this->uri->segment(3));
		$this->model_app->delete('akun',$id);
		
		redirect('app/akun');
		
	}
	function bank(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('no_rek'=>$this->db->escape_str($this->input->post('no_rek')),
								'nama_bank'=>$this->db->escape_str($this->input->post('nama_bank')),
								'saldo'=>$this->db->escape_str($this->input->post('saldo')));
                    
            
            $this->model_app->insert('bank',$data);
			redirect('app/bank');
			
		}else{
            $data['record'] = $this->db->query("Select * From bank ");
			$this->template->load('app/template','app/mod_master/view_bank',$data);
		}
	}
	function edit_bank(){
			$id=$this->uri->segment(3);
			$data = array('no_rek'=>$this->db->escape_str($this->input->post('no_rek')),
								'nama_bank'=>$this->db->escape_str($this->input->post('nama_bank')),
								'saldo'=>$this->db->escape_str($this->input->post('saldo')));
            $where = array('id_bank' => $id);
            $this->model_app->update('bank', $data, $where);

             

			redirect('app/bank');
	
		
	}
	function hapus_bank(){
		$id = array('id_bank' => $this->uri->segment(3));
		$this->model_app->delete('bank',$id);
		
		redirect('app/bank');
		
	}
	public function backupdb() {
		$rand=rand('10,50');
			$this->load->dbutil();
			$this->load->helper('file');
			
			$config = array(
				'format'	=> 'zip',
				'filename'	=> 'database.sql'
			);
			
			$backup =& $this->dbutil->backup($config);
			
			$save = FCPATH.'backup/backup-'.date("Y-m-d").'-db.zip';
			
			write_file($save, $backup);
			redirect('app/backupdata');
		}
	
	function backupdata(){

		$data['title'] = 'Data backupdata';
		
		$this->template->load('app/template','app/mod_master/view_backup',$data);
			
	
	}
		function menu(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_menu'=>$this->db->escape_str($this->input->post('nama_menu')),
								'aktif'=>$this->db->escape_str($this->input->post('aktif')),
								'icon'=>$this->db->escape_str($this->input->post('icon')),
								'urutan'=>$this->db->escape_str($this->input->post('urutan')),
								'url'=>$this->db->escape_str($this->input->post('url')),
								'kelompok'=>$this->db->escape_str($this->input->post('kelompok')));
                    
            
            $this->model_app->insert('menu',$data);
			redirect('app/menu');
			
		}else{
            $data['record'] = $this->db->query("Select * From menu ");
			$this->template->load('app/template','app/mod_master/view_menu',$data);
		}
	}
	function edit_menu(){
			$id=$this->uri->segment(3);
			$data = array('nama_menu'=>$this->db->escape_str($this->input->post('nama_menu')),
								'aktif'=>$this->db->escape_str($this->input->post('aktif')),
								'icon'=>$this->db->escape_str($this->input->post('icon')),
								'urutan'=>$this->db->escape_str($this->input->post('urutan')),
								'url'=>$this->db->escape_str($this->input->post('url')),
								'kelompok'=>$this->db->escape_str($this->input->post('kelompok')));
            $where = array('id_menu' => $id);
            $this->model_app->update('menu', $data, $where);

             

			redirect('app/menu');
	
		
	}
		function hapus_menu(){
		$id = array('id_menu' => $this->uri->segment(3));
		$this->model_app->delete('menu',$id);
		
		redirect('app/menu');
		
	}
	function submenu(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_submenu'=>$this->db->escape_str($this->input->post('nama_submenu')),
								'aktif'=>$this->db->escape_str($this->input->post('aktif')),
								'icon'=>$this->db->escape_str($this->input->post('icon')),
								'urutan'=>$this->db->escape_str($this->input->post('urutan')),
								'url'=>$this->db->escape_str($this->input->post('url')),
								'id_menu'=>$this->db->escape_str($this->input->post('id_menu')));
                    
            
            $this->model_app->insert('submenu',$data);
			redirect('app/submenu');
			
		}else{
            $data['record'] = $this->db->query("Select a.id_submenu, a.aktif, a.nama_submenu, a.urutan, a.url, a.icon, a.id_menu, b.id_menu, b.nama_menu From submenu a, menu b where a.id_menu=b.id_menu ");
			$this->template->load('app/template','app/mod_master/view_submenu',$data);
		}
	}
	function edit_submenu(){
			$id=$this->uri->segment(3);
			$data = array('nama_submenu'=>$this->db->escape_str($this->input->post('nama_submenu')),
								'aktif'=>$this->db->escape_str($this->input->post('aktif')),
								'icon'=>$this->db->escape_str($this->input->post('icon')),
								'urutan'=>$this->db->escape_str($this->input->post('urutan')),
								'url'=>$this->db->escape_str($this->input->post('url')),
								'id_menu'=>$this->db->escape_str($this->input->post('id_menu')));
            $where = array('id_submenu' => $id);
            $this->model_app->update('submenu', $data, $where);

             

			redirect('app/submenu');
	
		
	}
		function hapus_submenu(){
		$id = array('id_submenu' => $this->uri->segment(3));
		$this->model_app->delete('submenu',$id);
		
		redirect('app/submenu');
		
	}
	function hapusbackup(){
		$id=$this->uri->segment(3);
		unlink('backup/'.$id);
		redirect('app/backupdata');
	}
	function bagian(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_bagian'=>$this->db->escape_str($this->input->post('nama_bagian')));
                    
            
            $this->model_app->insert('bagian',$data);
			redirect('app/bagian');
			
		}else{
            $data['record'] = $this->db->query("Select * From bagian ");
			$this->template->load('app/template','app/mod_master/view_bagian',$data);
		}
	}
	function edit_bagian(){
			$id=$this->uri->segment(3);
			 $data = array('nama_bagian'=>$this->db->escape_str($this->input->post('nama_bagian')));
            $where = array('id_bagian' => $id);
            $this->model_app->update('bagian', $data, $where);

             

			redirect('app/bagian');
	
		
	}
	function hapus_bagian(){
		$id = array('id_bagian' => $this->uri->segment(3));
		$this->model_app->delete('bagian',$id);
		
		redirect('app/bagian');
		
	}
		function diskon(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_diskon'=>$this->db->escape_str($this->input->post('nama_diskon')),
					'prosentase'=>$this->db->escape_str($this->input->post('prosentase')));
                    
            
            $this->model_app->insert('diskon',$data);
			redirect('app/diskon');
			
		}else{
            $data['record'] = $this->db->query("Select * From diskon ");
			$this->template->load('app/template','app/mod_master/view_diskon',$data);
		}
	}
	function edit_diskon(){
			$id=$this->uri->segment(3);
			  $data = array('nama_diskon'=>$this->db->escape_str($this->input->post('nama_diskon')),
					'prosentase'=>$this->db->escape_str($this->input->post('prosentase')));
            $where = array('id_diskon' => $id);
            $this->model_app->update('diskon', $data, $where);

             

			redirect('app/diskon');
	
		
	}
	function hapus_diskon(){
		$id = array('id_diskon' => $this->uri->segment(3));
		$this->model_app->delete('diskon',$id);
		
		redirect('app/diskon');
		
	}
	function icon(){
		$data="Lihat Icon";
		$this->load->view('app/icon_page',$data);	
	}
	function ukuran(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_ukuran'=>$this->db->escape_str($this->input->post('nama_ukuran')));
                    
            
            $this->model_app->insert('ukuran',$data);
			redirect('app/ukuran');
			
		}else{
            $data['record'] = $this->db->query("Select * From ukuran ");
			$this->template->load('app/template','app/mod_master/view_ukuran',$data);
		}
	}
	function edit_ukuran(){
			$id=$this->uri->segment(3);
			 $data = array('nama_ukuran'=>$this->db->escape_str($this->input->post('nama_ukuran')));
            $where = array('id_ukuran' => $id);
            $this->model_app->update('ukuran', $data, $where);

             

			redirect('app/ukuran');
	
		
	}
	function hapus_ukuran(){
		$id = array('id_ukuran' => $this->uri->segment(3));
		$this->model_app->delete('ukuran',$id);
		
		redirect('app/ukuran');
		
	}
	function logout(){
		$this->session->sess_destroy();
		redirect('app');
	}
	function currency(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_currency'=>$this->db->escape_str($this->input->post('nama_currency')));
                    
            
            $this->model_app->insert('currency',$data);
			redirect('app/currency');
			
		}else{
            $data['record'] = $this->db->query("Select * From currency ");
			$this->template->load('app/template','app/mod_master/view_currency',$data);
		}
	}
	function edit_currency(){
			$id=$this->uri->segment(3);
			 $data = array('nama_currency'=>$this->db->escape_str($this->input->post('nama_currency')));
            $where = array('id_currency' => $id);
            $this->model_app->update('currency', $data, $where);

             

			redirect('app/currency');
	
		
	}
	function hapus_currency(){
		$id = array('id_currency' => $this->uri->segment(3));
		$this->model_app->delete('currency',$id);
		
		redirect('app/currency');
		
	}
	function typebayar(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_typebayar'=>$this->db->escape_str($this->input->post('nama_typebayar')));
                    
            
            $this->model_app->insert('typebayar',$data);
			redirect('app/typebayar');
			
		}else{
            $data['record'] = $this->db->query("Select * From typebayar ");
			$this->template->load('app/template','app/mod_master/view_typebayar',$data);
		}
	}
	function edit_typebayar(){
			$id=$this->uri->segment(3);
			 $data = array('nama_typebayar'=>$this->db->escape_str($this->input->post('nama_typebayar')));
            $where = array('id_typebayar' => $id);
            $this->model_app->update('typebayar', $data, $where);

             

			redirect('app/typebayar');
	
		
	}
	function hapus_typebayar(){
		$id = array('id_typebayar' => $this->uri->segment(3));
		$this->model_app->delete('typebayar',$id);
		
		redirect('app/typebayar');
		
	}
	function pemakaian(){
		
		if (isset($_POST['submit'])){
	
				    $data = array('nama_pemakaian'=>$this->db->escape_str($this->input->post('nama_pemakaian')));
                    
            
            $this->model_app->insert('pemakaian',$data);
			redirect('app/pemakaian');
			
		}else{
            $data['record'] = $this->db->query("Select * From pemakaian ");
			$this->template->load('app/template','app/mod_master/jenis_pemakaian',$data);
		}
	}
	function edit_pemakaian(){
			$id=$this->uri->segment(3);
			 $data = array('nama_pemakaian'=>$this->db->escape_str($this->input->post('nama_pemakaian')));
            $where = array('id_pemakaian' => $id);
            $this->model_app->update('pemakaian', $data, $where);

             

			redirect('app/pemakaian');
	
		
	}
	function hapus_pemakaian(){
		$id = array('id_pemakaian' => $this->uri->segment(3));
		$this->model_app->delete('pemakaian',$id);
		
		redirect('app/pemakaian');
		
	}
}
