<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Pembayaran extends CI_Controller {
	function payment(){
		if (isset($_POST['submit'])){
	
		$mitra=$this->input->post('mitra');
		if($mitra == "karyawan"){
		$data = array('no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
		'id_karyawan'=>$this->db->escape_str($this->input->post('id_mitra')),
		'id_typebayar'=>$this->db->escape_str($this->input->post('id_typebayar')),
		'id_currency'=>$this->db->escape_str($this->input->post('id_currency')),
		'tgl_pembayaran'=>$this->db->escape_str($this->input->post('tgl_pembayaran')),
		'jumlah_pembayaran'=>$this->db->escape_str($this->input->post('jumlah_pembayaran')),
		'id_users'=>$this->session->id_users);	
		}elseif($mitra == "customer"){
		$data = array('no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
		'id_customer'=>$this->db->escape_str($this->input->post('id_mitra')),
		'id_typebayar'=>$this->db->escape_str($this->input->post('id_typebayar')),
		'id_currency'=>$this->db->escape_str($this->input->post('id_currency')),
		'tgl_pembayaran'=>$this->db->escape_str($this->input->post('tgl_pembayaran')),
		'jumlah_pembayaran'=>$this->db->escape_str($this->input->post('jumlah_pembayaran')),
		'id_users'=>$this->session->id_users);	
		}else{
		$data = array('no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
		'id_suplier'=>$this->db->escape_str($this->input->post('id_mitra')),
		'id_typebayar'=>$this->db->escape_str($this->input->post('id_typebayar')),
		'id_currency'=>$this->db->escape_str($this->input->post('id_currency')),
		'tgl_pembayaran'=>$this->db->escape_str($this->input->post('tgl_pembayaran')),
		'jumlah_pembayaran'=>$this->db->escape_str($this->input->post('jumlah_pembayaran')),
		'id_users'=>$this->session->id_users);	
		}
		 
            $this->model_app->insert('payment',$data);
			$data2 = array('account_no'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_jurnal'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'credit'=>$this->db->escape_str($this->input->post('jumlah_pembayaran')),
		'cost_center'=>$this->db->escape_str($this->input->post('id_bank')),
		'deskripsi'=>"Payment");
		$this->model_app->insert('jurnal',$data2);
			redirect('pembayaran/payment');
			
		}else{
		$data['record'] = $this->db->query("Select * from payment a, typebayar b, currency c , bank d where a.id_typebayar=b.id_typebayar and a.id_currency=c.id_currency and a.id_bank=d.id_bank ");
		$this->template->load('app/template','app/mod_pembayaran/payment',$data);
		}
		
	}
	function edit_payment(){
		$data = array('no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
		'id_typebayar'=>$this->db->escape_str($this->input->post('id_typebayar')),
		'id_currency'=>$this->db->escape_str($this->input->post('id_currency')),
		'tgl_pembayaran'=>$this->db->escape_str($this->input->post('tgl_pembayaran')),
		'jumlah_pembayaran'=>$this->db->escape_str($this->input->post('jumlah_pembayaran')),
		'id_users'=>$this->session->id_users);
                    
            
            $where = array('id_payment' => $this->uri->segment(3));
            $this->model_app->update('payment', $data, $where);
			redirect('pembayaran/payment');
		
	}
	function hapus_pembayaran(){
		
		$id = array('id_payment' => $this->uri->segment(3));
		$this->model_app->delete('payment',$id);
		
		redirect('pembayaran/payment');
	}
	function ambilkaryawan(){
		$id_karyawan=$this->input->post('id_karyawan');
		$dataKaryawan=$this->db->query("Select * From karyawan where id_karyawan='$id_karyawan' ")->result_array();
		
		echo json_encode($dataKaryawan);
	}
	function ambilcustomer(){
		$id_customer=$this->input->post('id_customer');
		$dataCustomer=$this->db->query("Select * From customer where id_customer='$id_customer' ")->result_array();
		
		echo json_encode($dataCustomer);
	}
	function ambilsuplier(){
		$id_suplier=$this->input->post('id_suplier');
		$dataSuplier=$this->db->query("Select * From suplier where id_suplier='$id_suplier' ")->result_array();
		
		echo json_encode($dataSuplier);
	}
	function kirimbb(){
		
		$data['record'] = $this->db->query("Select * from payment a, typebayar b, currency c , bank d where a.id_typebayar=b.id_typebayar and a.id_currency=c.id_currency and a.id_bank=d.id_bank ");
		$this->template->load('app/template','app/mod_kirim/view_kirimbb');
		
	}
	function transfergudang(){
		if (isset($_POST['submit'])){
			$data = array(
								'gudang_kirim'=>$this->db->escape_str($this->input->post('pengirim')),
								'no_transaksi'=>$this->db->escape_str($this->input->post('no_poorder')),
								'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
								'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')),
								'id_users'=>$this->session->id_users,
								'penerima_gudang'=>$this->db->escape_str($this->input->post('penerima')));
                    
            
            $this->model_app->insert('kirimbb',$data);
			redirect('pembayaran/transfergudang');
		}else
		{
		$data['gudang'] = $this->db->query("Select * from kirimbb a, gudang b where a.gudang_kirim=b.id_gudang ");
		$this->template->load('app/template','app/mod_kirim/transfer_gudang',$data);
		}
	}
	function ambilbarang(){
		$id_barang=$this->input->post('id_barang');
		$data=$this->db->query("Select * From barang where id_barang='$id_barang' ")->result_array();
		
		echo json_encode($data);
	}
	function basket(){
		
		$id_barang=$this->input->post('id_barang');
		$qty=$this->input->post('qty');
		$no_poorder=$this->input->post('no_poorder');
		$id_detpo=$this->input->post('id_detpo');
		$data=array(
				'id_barang'=>$id_barang,
				'qty'=>$qty,
				'no_transaksi'=>$no_poorder
				);
		if($id_detpo !=""){
			$where = array('id_detail' => $id_detpo);
            $this->model_app->update('detail_kirimbb', $data, $where);	
		}else{
		$this->model_app->insert('detail_kirimbb',$data);	
		}		
				
		
		
		echo json_encode($dataBarang);
	}
	function ambilbasket(){
		$no_poorder=$this->input->post('no_poorder');
		$dataBarang=$this->db->query("Select * From detail_kirimbb a, barang b where a.id_barang=b.id_barang and a.no_transaksi='$no_poorder' ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function hapusdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		
		$id = array('id_detail' => $id_detpo);
		$this->model_app->delete('detail_kirimbb',$id);
		
	}
	function editdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		$dataBarang=$this->db->query("Select * From detail_kirimbb a, barang b where a.id_barang=b.id_barang and a.id_detail='$id_detpo' ")->result_array();
		echo json_encode($dataBarang);
	}
	function hapus_kirimbb(){
		$id_kirim=$this->uri->segment(3);
		$cari=$this->db->query("Select * From kirimbb where id_kirim='$id_kirim' ")->row_array();
		$id = array('id_kirim' => $id_kirim);
		$this->model_app->delete('kirimbb',$id);
		$id2 = array('no_transaksi' => $cari['no_transaksi']);
		$this->model_app->delete('detail_kirimbb',$id2);
		redirect('pembayaran/transfergudang');
	}
}


?>