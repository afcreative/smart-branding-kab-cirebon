<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Penjualan extends CI_Controller {

	function index(){
		
            
			$this->template->load('app/template','app/mod_penjualan/view_penjualan');
		
	}
	function ambildata(){
		$session_id=session_id();
		$dataBarang=$this->db->query("Select * From temp a, produk b  where a.id_produk=b.id_produk and a.session='$session_id' ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function ambilproduk(){
		$id_produk=$this->input->post('id_produk');
		$dataBarang=$this->db->query("Select * From produk where id_produk='$id_produk' ")->result_array();
		
		echo json_encode($dataBarang);
	}
	function ambilpelanggan(){
		$id_pelanggan=$this->input->post('id_pelanggan');
		$data=$this->db->query("Select * From pelanggan where id_pelanggan='$id_pelanggan' ")->result_array();
		
		echo json_encode($data);
	}
	function ambilKaryawan(){
		$id_karyawan=$this->input->post('id_karyawan');
		$data=$this->db->query("Select * From karyawan where id_karyawan='$id_karyawan' ")->result_array();
		
		echo json_encode($data);
	}
	function edittemp(){
		$id_temp=$this->input->post('id_temp');
		$dataBarang=$this->db->query("Select * From temp a, produk b where a.id_produk=b.id_produk and a.id_temp='$id_temp' ")->result_array();
		echo json_encode($dataBarang);
	}
	function hapustemp(){
		$id_temp=$this->input->post('id_temp');
		
		$id = array('id_temp' => $id_temp);
		$this->model_app->delete('temp',$id);
		
	}
	function keranjang(){
		$session_id=session_id();
		$id_produk=$this->input->post('id_produk');
		$qty=$this->input->post('jumlah_jual');
		$harga_jual=$this->input->post('harga_jual');
		$data=array(
				'id_produk'=>$id_produk,
				'qty'=>$qty,
				'harga'=>$harga_jual,
				'session'=>$session_id
				);
				
			$this->model_app->insert('temp',$data);
		echo json_encode($dataBarang);
	}
	
	
}
?>