<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Bayar extends CI_Controller {
	function piutang(){
		if (isset($_POST['submit'])){
	
		$mitra=$this->input->post('mitra');
		if($mitra == "karyawan"){
		$data = array('no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
		'id_karyawan'=>$this->db->escape_str($this->input->post('id_mitra')),
		'id_typebayar'=>$this->db->escape_str($this->input->post('id_typebayar')),
		'id_currency'=>$this->db->escape_str($this->input->post('id_currency')),
		'tgl_pembayaran'=>$this->db->escape_str($this->input->post('tgl_pembayaran')),
		'id_order'=>$this->db->escape_str($this->input->post('id_order')),
		'jumlah_pembayaran'=>$this->db->escape_str($this->input->post('tot_tagihan')),
		'id_users'=>$this->session->id_users);	
		}elseif($mitra == "customer"){
		$data = array('no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
		'id_customer'=>$this->db->escape_str($this->input->post('id_mitra')),
		'id_typebayar'=>$this->db->escape_str($this->input->post('id_typebayar')),
		'id_currency'=>$this->db->escape_str($this->input->post('id_currency')),
		'tgl_pembayaran'=>$this->db->escape_str($this->input->post('tgl_pembayaran')),
		'id_order'=>$this->db->escape_str($this->input->post('id_order')),
		'jumlah_pembayaran'=>$this->db->escape_str($this->input->post('tot_tagihan')),
		'id_users'=>$this->session->id_users);	
		}else{
		$data = array('no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
		'id_suplier'=>$this->db->escape_str($this->input->post('id_mitra')),
		'id_typebayar'=>$this->db->escape_str($this->input->post('id_typebayar')),
		'id_currency'=>$this->db->escape_str($this->input->post('id_currency')),
		'tgl_pembayaran'=>$this->db->escape_str($this->input->post('tgl_pembayaran')),
		'id_order'=>$this->db->escape_str($this->input->post('id_order')),
		'jumlah_pembayaran'=>$this->db->escape_str($this->input->post('tot_tagihan')),
		'id_users'=>$this->session->id_users);	
		}
	
		
                    
            
            $this->model_app->insert('piutang',$data);
			redirect('bayar/piutang');
			
		}else{
		$data['record'] = $this->db->query("Select * from piutang a, typebayar b, currency c , bank d where a.id_typebayar=b.id_typebayar and a.id_currency=c.id_currency and a.id_bank=d.id_bank ");
		$this->template->load('app/template','app/mod_finance/piutang',$data);
		}
		
	}
	function edit_piutang(){
		$data = array('no_transaksi'=>$this->db->escape_str($this->input->post('no_transaksi')),
		'tgl_transaksi'=>$this->db->escape_str($this->input->post('tgl_transaksi')),
		'id_bank'=>$this->db->escape_str($this->input->post('id_bank')),
		'id_typebayar'=>$this->db->escape_str($this->input->post('id_typebayar')),
		'id_currency'=>$this->db->escape_str($this->input->post('id_currency')),
		'tgl_pembayaran'=>$this->db->escape_str($this->input->post('tgl_pembayaran')),
		'jumlah_pembayaran'=>$this->db->escape_str($this->input->post('jumlah_pembayaran')),
		'id_users'=>$this->session->id_users);
                    
            
            $where = array('id_piutang' => $this->uri->segment(3));
            $this->model_app->update('piutang', $data, $where);
			redirect('bayar/piutang');
		
	}
	function hapus_piutang(){
		
		$id = array('id_piutang' => $this->uri->segment(3));
		$this->model_app->delete('piutang',$id);
		
		redirect('bayar/piutang');
	}
	function ambilkaryawan(){
		$id_karyawan=$this->input->post('id_karyawan');
		$dataKaryawan=$this->db->query("Select * From karyawan where id_karyawan='$id_karyawan' ")->result_array();
		
		echo json_encode($dataKaryawan);
	}
	function ambilcustomer(){
		$id_customer=$this->input->post('id_customer');
		$dataCustomer=$this->db->query("Select * From customer where id_customer='$id_customer' ")->result_array();
		
		echo json_encode($dataCustomer);
	}
	function ambilsuplier(){
		$id_suplier=$this->input->post('id_suplier');
		$dataSuplier=$this->db->query("Select * From suplier where id_suplier='$id_suplier' ")->result_array();
		
		echo json_encode($dataSuplier);
	}
	function ambilorder(){
		$id_order=$this->input->post('id_order');
		$dataOrder=$this->db->query("Select * From orders where id_order='$id_order' ")->result_array();
		
		echo json_encode($dataOrder);
	}
	
}


?>