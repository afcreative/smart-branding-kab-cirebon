<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Stok extends CI_Controller {

	function adjusment(){
		if (isset($_POST['submit'])){
			$data = array(
								'no_adjusment'=>$this->db->escape_str($this->input->post('no_poorder')),
								'tgl_adjusment'=>$this->db->escape_str($this->input->post('tgl_adjusment')),
								'id_pemakaian'=>$this->db->escape_str($this->input->post('id_pemakaian')),
								'id_gudang'=>$this->db->escape_str($this->input->post('id_gudang')),
								'no_referensi'=>$this->db->escape_str($this->input->post('no_referensi')),
								'id_users'=>$this->session->id_users);
                    
            
            $this->model_app->insert('adjusment',$data);
			redirect('stok/adjusment');
		}else
		{
			$data['gudang'] = $this->db->query("Select * From adjusment a, pemakaian b, gudang c where a.id_gudang=c.id_gudang and a.id_pemakaian=b.id_pemakaian ");
			$this->template->load('app/template','app/mod_produksi/adjusment_stok',$data);
		}
	}
	function basket(){
		
		$id_barang=$this->input->post('id_barang');
		$qty=$this->input->post('qty');
		$harga=$this->input->post('harga');
		$no_poorder=$this->input->post('no_poorder');
		$id_detpo=$this->input->post('id_detpo');
		$data=array(
				'id_barang'=>$id_barang,
				'qty'=>$qty,
				'harga'=>$harga,
				'no_adjusment'=>$no_poorder
				);
		if($id_detpo !=""){
			$where = array('id_detail' => $id_detpo);
            $this->model_app->update('detail_adjusment', $data, $where);	
		}else{
		$this->model_app->insert('detail_adjusment',$data);	
		}		
				
		
		
		echo json_encode($dataBarang);
	}
	function ambilbasket(){
		$no_poorder=$this->input->post('no_poorder');
		$dataBarang=$this->db->query("Select * From detail_adjusment a, barang b where a.id_barang=b.id_barang and a.no_adjusment='$no_poorder' ")->result_array();
		echo json_encode($dataBarang);
		
	}
	function hapusdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		
		$id = array('id_detail' => $id_detpo);
		$this->model_app->delete('detail_adjusment',$id);
		
	}
	function editdetpo(){
		$id_detpo=$this->input->post('id_detpo');
		$dataBarang=$this->db->query("Select * From detail_adjusment a, barang b where a.id_barang=b.id_barang and a.id_detail='$id_detpo' ")->result_array();
		echo json_encode($dataBarang);
	}
	function hapus_adjusment(){
		$id_adjusment=$this->uri->segment(3);
		$cari=$this->db->query("Select * From adjusment where id_adjusment='$id_adjusment' ")->row_array();
		$id = array('id_adjusment' => $id_adjusment);
		$this->model_app->delete('adjusment',$id);
		$id2 = array('no_adjusment' => $cari['no_adjusment']);
		$this->model_app->delete('detail_adjusment',$id2);
		redirect('stok/adjusment');
	}
	
	
	
	
}
?>