<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Transaksi extends CI_Controller {

	function absensi(){
		
		if (isset($_POST['submit'])){
	
				$data = array('tgl_absensi'=>$this->db->escape_str($this->input->post('tgl_absensi')),	
				'id_karyawan'=>$this->db->escape_str($this->input->post('id_karyawan')),					
				'ket'=>$this->db->escape_str($this->input->post('ket')));
                    
            
            $this->model_app->insert('absensi',$data);
			redirect('transaksi/absensi');
			
		}else{
            $data['record'] = $this->db->query("Select * from absensi a, karyawan b where a.id_karyawan=b.id_karyawan");
			$this->template->load('app/template','app/mod_presensi/view_absensi',$data);
		}
	}
	function edit_absensi(){
			$id=$this->uri->segment(3);
			$data = array('tgl_absensi'=>$this->db->escape_str($this->input->post('tgl_absensi')),	
				'id_karyawan'=>$this->db->escape_str($this->input->post('id_karyawan')),	
				'datang'=>$this->db->escape_str($this->input->post('datang')),
				'pulang'=>$this->db->escape_str($this->input->post('pulang')),				
				'ket'=>$this->db->escape_str($this->input->post('ket')));
            $where = array('id_absensi' => $id);
            $this->model_app->update('absensi', $data, $where);

             

			redirect('transaksi/absensi');
	
		
	}
	function hapus_absensi(){
		$id = array('id_absensi' => $this->uri->segment(3));
		$this->model_app->delete('absensi',$id);
		
		redirect('transaksi/absensi');
		
	}
	function rekap(){
		
		if (isset($_POST['submit'])){
	
			$data['dari']=$this->input->post('dari');
			$data['sampai']=$this->input->post('sampai');
                    
            
            $data['record'] = $this->db->query("Select * From karyawan a, jabatan b, bagian c where a.id_bagian=c.id_bagian and a.id_jabatan=b.id_jabatan ");
			$this->template->load('app/template','app/mod_presensi/view_rekap',$data);
			
		}else{
			$sekarang=date('Y-m-d');
			$ini=date('Y-m-d', strtotime($sekarang. '- 1 month'));
			$data['dari']=$this->input->post('dari');
			$data['sampai']=date('Y-m-d');
            $data['record'] = $this->db->query("Select * From karyawan a, jabatan b, bagian c where a.id_bagian=c.id_bagian and a.id_jabatan=b.id_jabatan ");
			$this->template->load('app/template','app/mod_presensi/view_rekap',$data);
		}
	}
	function rekapabsensi(){
		$session_id=session_id();
		$data=$this->db->query("Select * From karyawan a, jabatan b, bagian c where a.id_bagian=c.id_bagian and a.id_jabatan=b.id_jabatan ")->result_array();
		
		echo json_encode($data);
		
	}
	function ambilKaryawan(){
		$id_karyawan=$this->input->post('id_karyawan');
		$data=$this->db->query("Select * From karyawan a, jabatan b where a.id_jabatan=b.id_jabatan and a.id_karyawan='$id_karyawan'")->result_array();
		
		echo json_encode($data);
		
	}
	function gaji(){
		
		if (isset($_POST['submit'])){
				$xc=$this->input->post('tgl_gaji');
				$gapok=$this->input->post('gapok');
				$potongan=$this->input->post('potongan');
				$cek=explode('-',$xc);
				$data = array('tgl_gaji'=>$this->db->escape_str($this->input->post('tgl_gaji')),	
				'id_karyawan'=>$this->db->escape_str($this->input->post('id_karyawan')),	
				'jumlah_gaji'=>$this->db->escape_str($this->input->post('jumlah_gaji')),
				'potongan'=>$this->db->escape_str($this->input->post('potongan')),
				'bulan'=>$cek[1],				
				'tahun'=>$cek[0]);
                    
            
            $this->model_app->insert('gaji',$data);
			$last_id = $this->db->insert_id();
			
			$jml=count($this->input->post('id_intensif'));
			$id_intensif=$this->input->post('id_intensif');
			$jumlah=$this->input->post('jumlah');
			for ($i=0; $i < $jml; $i++){
			$data = array('id_gaji'=>$last_id ,	
				'id_intensif'=>$id_intensif[$i],	
				'jumlah'=>$jumlah[$i]);
                    
            
            $this->model_app->insert('detail_gaji',$data);	
			$total=($total + $jumlah[$i])-$potongan;
			
			}
			$total_gaji = $total + $gapok;
			$data2 = array('jumlah_gaji'=>$total_gaji);
            $where = array('id_gaji' => $last_id  );
            $this->model_app->update('gaji', $data2, $where);
			redirect('transaksi/gaji');
		}else{
            $data['record'] = $this->db->query("Select * from gaji a, karyawan b, jabatan c where a.id_karyawan=b.id_karyawan and b.id_jabatan=c.id_jabatan");
			$this->template->load('app/template','app/mod_presensi/view_gaji',$data);
		}
	}
	function edit_gaji(){
	
	$id = array('id_gaji' => $this->uri->segment(3));
		$this->model_app->delete('gaji',$id);
		
		$id = array('id_gaji' => $this->uri->segment(3));
		$this->model_app->delete('detail_gaji',$id);
				$potongan=$this->input->post('potongan');
				$xc=$this->input->post('tgl_gaji');
				$gapok=$this->input->post('gapok');
				$cek=explode('-',$xc);
				$data = array('tgl_gaji'=>$this->db->escape_str($this->input->post('tgl_gaji')),	
				'id_karyawan'=>$this->db->escape_str($this->input->post('id_karyawan')),	
				'jumlah_gaji'=>$this->db->escape_str($this->input->post('jumlah_gaji')),
				'potongan'=>$this->db->escape_str($this->input->post('potongan')),
				'bulan'=>$cek[1],				
				'tahun'=>$cek[0]);
                    
            
            $this->model_app->insert('gaji',$data);
			$last_id = $this->db->insert_id();
			
			$jml=count($this->input->post('id_intensif'));
			$id_intensif=$this->input->post('id_intensif');
			$jumlah=$this->input->post('jumlah');
			for ($i=0; $i < $jml; $i++){
			$data = array('id_gaji'=>$last_id ,	
				'id_intensif'=>$id_intensif[$i],	
				'jumlah'=>$jumlah[$i]);
                    
            
            $this->model_app->insert('detail_gaji',$data);	
			$total=($total + $jumlah[$i])-$potongan;
			
			}
			$total_gaji = $total + $gapok;
			$data2 = array('jumlah_gaji'=>$total_gaji);
            $where = array('id_gaji' => $last_id  );
            $this->model_app->update('gaji', $data2, $where);
			redirect('transaksi/gaji');	
		
	}
	function hapus_gaji(){
		$id = array('id_gaji' => $this->uri->segment(3));
		$this->model_app->delete('gaji',$id);
		
		$id = array('id_gaji' => $this->uri->segment(3));
		$this->model_app->delete('detail_gaji',$id);
		
		redirect('transaksi/gaji');
		
	}
	function getHutang(){
		$id_karyawan=$this->input->post('id_karyawan');
		$tgl_gaji=$this->input->post('tgl_gaji');
		$data=$this->db->query("Select * From hutang where id_karyawan='$id_karyawan' and sampai_tgl > '$tgl_gaji' ")->result_array();
		
		echo json_encode($data);
		
	}
	function pobarang(){
		if (isset($_POST['submit'])){
		$data = array('no_order'=>$this->db->escape_str($this->input->post('no_order')),	
				'tgl_order'=>$this->db->escape_str($this->input->post('tgl_order')),	
				'no_account'=>$this->db->escape_str($this->input->post('no_account')),
				'tot_penjualan'=>$this->db->escape_str($this->input->post('grand')),
				'tot_uangmuka'=>$this->db->escape_str($this->input->post('jumlah_dp')),
				'id_users'=>$this->session->id_users,
				'id_customer'=>$this->db->escape_str($this->input->post('id_customer')),				
				'tot_tagihan'=>$this->db->escape_str($this->input->post('newtotTagihan')));
                    
            
            $this->model_app->insert('orders',$data);
			redirect('transaksi/pobarang');
		}else{
			
			$this->template->load('app/template','app/mod_po/view_pobarang');
		}
		
	}
	function hapus_pobarang(){
		$rs=$this->db->query("Select * From orders where id_order='".$this->uri->segment(3)."'")->row_array();
		$no_order=$rs['no_order'];
		
		
		$id2 = array('no_order' => $no_order);
		$this->model_app->delete('detail_pobarang',$id2);
		$id = array('id_order' => $this->uri->segment(3));
		$this->model_app->delete('orders',$id);
		redirect('transaksi/pobarang');	
		
	}
	function pengiriman(){
		if (isset($_POST['submit'])){
			$id=$this->uri->segment(3);
			$data = array('id_order'=>$id,	
				'tgl_pengiriman'=>$this->db->escape_str($this->input->post('tgl_pengiriman')),
				'id_customer'=>$this->db->escape_str($this->input->post('id_customer')),
				'alamat_kirim'=>$this->db->escape_str($this->input->post('alamat_kirim')),				
				'id_gudang'=>$this->db->escape_str($this->input->post('id_gudang')));
                    
            
            $this->model_app->insert('pengiriman',$data);
			$data2 = array('status_order'=>"Dikirim");
            $where = array('id_order' => $id  );
            $this->model_app->update('orders', $data2, $where);
			
			redirect('transaksi/pengiriman');
		}else{
		$data['record'] = $this->db->query("Select * from orders a, customer b where a.id_customer=b.id_customer ");
		
		$this->template->load('app/template','app/mod_po/view_pengiriman',$data);	
		}
			
	}
	function hapus_pengiriman(){
		$rs=$this->db->query("Select * From pengiriman where id_pengiriman='".$this->uri->segment(3)."'")->row_array();
		$id_order=$rs['id_order'];
		
			$data = array('status_order'=>"Baru");
            $where = array('id_order' => $id_order  );
            $this->model_app->update('orders', $data, $where);
			
		$id = array('id_pengiriman' => $this->uri->segment(3));
		$this->model_app->delete('pengiriman',$id);
		
		redirect('transaksi/pengiriman');		
		
	}
	function edit_pengiriman(){
		
		$data = array(
				'tgl_pengiriman'=>$this->db->escape_str($this->input->post('tgl_pengiriman')),
				'alamat_kirim'=>$this->db->escape_str($this->input->post('alamat_kirim')),				
				'id_gudang'=>$this->db->escape_str($this->input->post('id_gudang')));
            
            $where = array('id_pengiriman' => $this->uri->segment(3)  );
            $this->model_app->update('pengiriman', $data, $where);
		redirect('transaksi/pengiriman');
	}
	
	
}
?>