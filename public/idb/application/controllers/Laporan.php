<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Laporan extends CI_Controller {
	function barang(){
		cek_session_admin();
		$data['judul'] = 'Laporan Barang';
		$data = $this->model_laporan->view_barang();
        $dataa = $this->model_app->view_one('mu_conf_barang',array('id_conf_barang' => 1),'id_conf_barang')->row_array();
        $data = array('record' => $data, 'conf' => $dataa);
		$this->template->load('app/template','app/mod_laporan/view_barang',$data);
	}
	function lapcreditnote(){
	$data['record'] = $this->db->query("Select * From jurnal a, bank b where a.cost_center=b.id_bank and a.credit !=0 ");
		$this->load->view('app/mod_laporan/lap_creditnote',$data);	
	}
	function lapdebtnote(){
	$data['record'] = $this->db->query("Select * From jurnal a, bank b where a.cost_center=b.id_bank and a.debt !=0 ");
		$this->load->view('app/mod_laporan/lap_debtnote',$data);	
	}
	function lappiutang(){
		$data['record'] = $this->db->query("Select * from piutang a, typebayar b, currency c , bank d where a.id_typebayar=b.id_typebayar and a.id_currency=c.id_currency and a.id_bank=d.id_bank ");
		$this->load->view('app/mod_laporan/lap_piutang',$data);	
		
	}


}
