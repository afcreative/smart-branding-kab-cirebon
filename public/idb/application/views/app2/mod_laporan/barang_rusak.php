<!DOCTYPE html>
 <?php 

 

				
 ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Barang Rusak</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
	
<style>
    @page { size: A4 }
  
    h1 {
        font-weight: bold;
        font-size: 20pt;
        text-align: center;
    }
	img{
		width:20%;
	}
	
	.logo{
		
		text-align: center;
	}
	h3 {
        font-weight: bold;
        font-size: 14pt;
        text-align: center;
    }
	p{
		margin-left:450px;
	}
	.capbawah{
		position: relative;
		z-index: 1;
		top: 0px;
		width:20%;
		margin-left:500px;
	}
	p12{
		position: relative;
	margin-left:520px;	
	z-index: 2;
	}
	.cap{
		margin-left:450px;
		text-align: center;
		
	}
  
    table {
        border-collapse: collapse;
        width: 100%;
    }
	identitas {
        border-collapse: collapse;
        width: 100%;
		text-align: left;
    }
	.identitas th {
        padding: 8px 8px;
        text-align: left;
    }
	.identitas td {
        padding: 8px 8px;
        text-align: left;
    }
    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
  
    .table td {
        padding: 3px 3px;
        border:1px solid #000000;
    }
  
    .text-center {
        text-align: center;
    }
	p7{
		text-align: center;
	}
</style>
</head>
<body class="A4"  onload="window.print()">
    <section class="sheet padding-10mm">
	<div class="logo">
	<img src="<?php echo base_url(); ?>assets/jagat.jpeg">
	</div>
        <h1>SMKN 1 BALONGAN</h1>
		<h3>INVENTARIS SEKOLAH</h3>
		
		<hr />
		<h3>DAFTAR BARANG RUSAK</h3>
		
		
        <table class="table">
            <thead>
                <tr >
                    <th >NO.</th>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
					<th>Sumber Dana</th>
					<th>Tahun</th>
					<th>Ruang</th>
                </tr>
            </thead>
            <tbody>
			<?php 
			$no=1;
			
			foreach ($record->result_array() as $r){
			$x=explode('-',$r['tgl_masuk']);
				?>
                <tr>
                    <td class="text-center" width="20"><?php echo $no; ?></td>
                    <td><?php echo $r['nama_barang']; ?></td>
                    <td align="center"><?php echo $r['jml_barang']; ?></td>
					<td align="center"><?php echo $r['sumber_dana']; ?></td>
					<td align="center"><?php echo $x[0]; ?></td>
					<td align="center"><?php echo $r['nama_ruang']; ?></td>
					
                </tr>
            <?php $no++; } ?>
						
            </tbody>
        </table>
		<p>Cirebon, <?php echo date('d F Y'); ?></p>
		<div class="cap">Wk. Sarpras
		
		
		
		</div>
		<img src="<?php echo base_url(); ?>assets/cap.png" class="capbawah">
		
		<p12><B>Ir. Budiyanto, MM</B></p12>
		
		
		</p1>
    </section>
</body>
</html>