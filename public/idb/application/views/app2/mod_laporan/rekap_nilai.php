<!DOCTYPE html>
 <?php 
 $id_ukk=$id_ukk;
	$rw=$this->db->query("Select * From ukk a, sekolah b, kompetensi c where a.id_sekolah=b.id_sekolah and a.id_kompetensi=c.id_kompetensi and a.id_ukk='$id_ukk'")->row_array();
				
				
 ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan UKK SMK & PT. Jagat Digital</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
	
<style>
    @page { size: A4 }
  
    h1 {
        font-weight: bold;
        font-size: 20pt;
        text-align: center;
    }
	img{
		width:20%;
	}
	
	.logo{
		
		text-align: center;
	}
	h3 {
        font-weight: bold;
        font-size: 14pt;
        text-align: center;
    }
	p{
		margin-left:450px;
	}
	.capbawah{
		position: relative;
		z-index: 1;
		top: 0px;
		width:20%;
		margin-left:500px;
	}
	p12{
		position: relative;
	margin-left:520px;	
	z-index: 2;
	}
	.cap{
		margin-left:450px;
		text-align: center;
		
	}
  
    table {
        border-collapse: collapse;
        width: 100%;
    }
	identitas {
        border-collapse: collapse;
        width: 100%;
		text-align: left;
    }
	.identitas th {
        padding: 8px 8px;
        text-align: left;
    }
	.identitas td {
        padding: 8px 8px;
        text-align: left;
    }
    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
  
    .table td {
        padding: 3px 3px;
        border:1px solid #000000;
    }
  
    .text-center {
        text-align: center;
    }
	p7{
		text-align: center;
	}
</style>
</head>
<body class="A4"  onload="window.print()">
    <section class="sheet padding-10mm">
	<div class="logo">
	<img src="<?php echo base_url(); ?>assets/jagat.jpeg">
	</div>
        <h1>PT. JAVA GENIUS ALL TECHNOLOGY</h1>
		<h3>Connect Your Business Digital</h3>
		
		<hr />
				<h3>REKAP NILAI UJIAN</h3>
		<table class="identitas">
		
                
				<tr>
                    <td width="100px">Kompetensi </td>
                    <td>: <?php echo $rw['nama_kompetensi'];?></td>
                </tr>
				<tr>
                    <td width="100px">Sekolah </td>
                    <td>: <?php echo $rw['nama_sekolah'];?></td>
                </tr>
		</table>
		
        <table class="table">
            <thead>
                <tr >
                    <th >NO.</th>
                    <th>Nama Siswa</th>
                    <th>Nis</th>
					<th>Nilai</th>
					<th>Keterangan</th>
                </tr>
            </thead>
            <tbody>
			<?php 
			$no=1;
			
			foreach ($record->result_array() as $r){
				$rt=$this->db->query("Select sum(nilai) as global From detail_nilai where id_peserta='".$r['id_peserta']."'")->row_array();
				$tot=$rt['global'];
				if ($tot >= 91 ){
					$pesan="Sangat Kompeten";
				}else if ($tot >= 80 ){
					$pesan="Kompeten";
				}else if ($tot >= 70 ){
					$pesan="Kurang Kompeten";
				}else{
					$pesan="Tidak Kompeten";
				}
				?>
                <tr>
                    <td class="text-center" width="20"><?php echo $no; ?></td>
                    <td><?php echo $r['nama_peserta']; ?></td>
                    <td align="center"><?php echo $r['nis']; ?></td>
					<td align="center"><?php echo $rt['global']; ?></td>
					<td><?php echo $pesan; ?></td>
                </tr>
            <?php $no++; } ?>
						
            </tbody>
        </table>
		<p>Cirebon, <?php echo date('d F Y'); ?></p>
		<div class="cap">Assesor PT. Jagat Digital
		
		
		
		</div>
		<img src="<?php echo base_url(); ?>assets/cap.png" class="capbawah">
		
		<p12><B><?php 
		$c=$this->db->query("Select * From penguji where id_penguji='$id_penguji'")->row_array();
		echo $c['nama_lengkap']; ?></B></p12>
		
		
		</p1>
    </section>
</body>
</html>