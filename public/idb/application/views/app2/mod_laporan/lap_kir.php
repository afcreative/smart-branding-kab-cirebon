<!DOCTYPE html>
 <?php 

 
$r=$this->db->query("Select * From sekolah where id_sekolah='1'")->row_array();
				
 ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Barang Rusak</title>
    
	
<style>

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
  
    .table td {
        padding: 3px 3px;
        border:1px solid #000000;
    }
	@media print
{
  table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display: table-footer-group}
}

</style>
</head>
<body  onload="window.print()">
	<table><tr><td width="10%"><img src="<?php echo base_url();?>assets/logo_jb.png" style="height:120px; margin-left:80px;"><br></td><td>
	<div align="center" style=" line-height: 40%;">
		
        <h3>PEMERINTAH PROVINSI JAWA BARAT</h3>
		<h3>DINAS PENDIDIKAN</h3>
		<h2>SMK NEGERI 1 BALONGAN</h2>
		</div>
		<div align="center" style=" line-height: 3%; position: relative;">
		<h5>Terakreditasi dan Berstandar SNI ISO 9001:2008 No. 824 100 11043</h5>
		<h5>Jl. Raya Sukaurip No. 35 Telp. (0234) 428146 Balongan – Indramayu 45285</h5>
		<h5>Website: www.smkn1-balongan.sch.id Email: smkn_1balongan@yahoo.co.id</h5>
		</div>
		
		
	
	</td><td width="10%"><img src="<?php echo base_url();?>assets/logo/<?php echo $r['logo']; ?>" style="height:120px; margin-right:80px;"></td></tr>
	<tr><td colspan="3"><hr style="height:3px; solid #000000; " /></td></tr>
	</table>
   <table width="100%">
   <tr><td width="15%">Nama Satuan Kerja</td><td>: <?php echo $r['nama_sekolah']; ?></td></tr>
   <tr><td>Kode UPB</td><td>: <?php echo $r['kode_upb']; ?></td></tr>
   <tr><td>Kecamatan</td><td>: <?php echo $r['kecamatan']; ?></td></tr>
   <tr><td>Alamat UPB</td><td>: <?php echo $r['alamat']; ?></td></tr>
   <tr><td>Ruang</td><td>: <?php $rt=$this->db->query("Select * From ruang where id_ruang='$id_ruang'")->row_array(); echo $rt['nama_ruang']; ?></td></tr>
   <br>
   </table>
		
		
		
        <table class="table">
            <thead>
                <tr align="center">
                   <td rowspan="2">No</td>
						<td rowspan="2">Nama barang</td>
						<td rowspan="2">Merek</td>
						<td rowspan="2">Ukuran</td>
						<td rowspan="2">Bahan</td>
						
						<td rowspan="2">Tahun Pembuatan</td>
						<td rowspan="2">Kode Barang</td>
						<td rowspan="2">Jumlah</td>
						<td rowspan="2">Harga</td>
						
						<td colspan="3">Kondisi Barang</td>
						<td rowspan="2">Keterangan</td>
					
						
                </tr>
				<tr align="center">
						
						<td>Baik</td>
						
						<td>Kurang Baik</td>
						<td>Rusak berat</td>
					
						
                </tr>
				
				<tr  style=" line-height: 3%;">
						<?php for($i=1; $i < 14; $i++){ 
						echo"<th>$i</th>";
						 } ?></tr>
            </thead>
			 
            <tbody>
			<?php 
			$no=1;
			$record=$this->db->query("Select * From rb_penempatan c, detail_pengadaan a, barang b where a.id_barang=b.id_barang and c.id_detail=a.id_detail and c.id_ruang='$id_ruang'");
			foreach ($record->result_array() as $r){
				
				?>
                <tr>
                  <td align="center"><?php echo $no; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['merk']; ?></td>
						<td><?php echo $r['ukuran']; ?></td>
						<td><?php echo $r['bahan']; ?></td>
						
						<td><?php echo tgl_indo($r['tahun_pengadaan']); ?></td>
						<td><?php echo $r['no_register']; ?></td>
						<td><?php echo rupiah($r['jumlah']); ?></td>
						<td><?php echo rupiah($r['harga_total']); ?></td>
						<?php if($r['status']=="B"){ ?>
						<td align="center"> &#10004 </td>
						<td align="center"></td>
						<td align="center"></td>
						<?php } elseif($r['status']=="KB"){ ?>
						<td align="center">  </td>
						<td align="center">&#10004 </td>
						<td align="center"></td>
						<?php }else{ ?>
						
						<td align="center"></td>
						<td align="center"></td>
						<td align="center">&#10004</td>
						<?php } ?>
						<td> <?php echo $r['keterangan']; ?>  </td>
					
                </tr>
				<?php  $no++; } ?>
				
            </tbody>
			
					
			
        </table>
		
		
<?php 
		include "titimangsa.php";
		
		?>
   
	
</body>
</html>