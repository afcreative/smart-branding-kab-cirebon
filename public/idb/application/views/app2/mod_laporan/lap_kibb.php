<!DOCTYPE html>
 <?php 

 
$r=$this->db->query("Select * From sekolah where id_sekolah='1'")->row_array();
				
 ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Barang Rusak</title>
    
	
<style>

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
  
    .table td {
        padding: 3px 3px;
        border:1px solid #000000;
    }
	@media print
{
  table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display: table-footer-group}
}

</style>
</head>
<body  onload="window.print()">
	<table><tr><td width="10%"><img src="<?php echo base_url();?>assets/logo_jb.png" style="height:120px; margin-left:80px;"><br></td><td>
	<div align="center" style=" line-height: 40%;">
		
        <h3>PEMERINTAH PROVINSI JAWA BARAT</h3>
		<h3>DINAS PENDIDIKAN</h3>
		<h2>SMK NEGERI 1 BALONGAN</h2>
		</div>
		<div align="center" style=" line-height: 3%; position: relative;">
		<h5>Terakreditasi dan Berstandar SNI ISO 9001:2008 No. 824 100 11043</h5>
		<h5>Jl. Raya Sukaurip No. 35 Telp. (0234) 428146 Balongan – Indramayu 45285</h5>
		<h5>Website: www.smkn1-balongan.sch.id Email: smkn_1balongan@yahoo.co.id</h5>
		</div>
		
		
	
	</td><td width="10%"><img src="<?php echo base_url();?>assets/logo/<?php echo $r['logo']; ?>" style="height:120px; margin-right:80px;"></td></tr>
	<tr><td colspan="3"><hr style="height:3px; solid #000000; " /></td></tr>
	<tr><td colspan="3"><h3 align="center">KARTU INVENTARIS BARANG (KIB A)</h3></td></tr>
	</table>
   
		
		
		
        <table class="table">
            <thead>
                <tr >
                   <th width="50px" rowspan="2">No</th>
						<th rowspan="2">Nama</th>
						<th rowspan="2">Register</th>
						<th rowspan="2">Merk</th>
						<th rowspan="2">Ukuran</th>
						<th rowspan="2">Bahan</th>
						<th rowspan="2">Tahun Beli</th>
						<th colspan="5">Nomor</th>
						
						<th rowspan="2">Perolehan</th>
						<th rowspan="2">Harga</th>
						<th rowspan="2">Keterangan</th>
						
                </tr><tr >
				
						
					
						
						<th>Pabrik</th>
						<th>Rangka</th>
						<th>Mesin</th>
						<th>Polisi</th>
						<th>BPKB</th>
						
                </tr>
				<tr  style=" line-height: 3%;">
						<?php for($i=1; $i < 16; $i++){ 
						echo"<th>$i</th>";
						 } ?></tr>
            </thead>
			 
            <tbody>
			<?php 
			$no=1;
			$record=$this->db->query("Select * From detail_pengadaan a, barang b where a.id_barang=b.id_barang and a.id_kriteria='2'");
			foreach ($record->result_array() as $r){
				
				?>
                <tr>
                    <td align="center"><?php echo $no; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['no_register']; ?></td>
						<td><?php echo $r['merk']; ?></td>						
						<td><?php echo $r['ukuran']; ?></td>
						<td><?php echo $r['bahan']; ?></td>
						<td><?php echo tgl_indo($r['tahun_pengadaan']); ?></td>
						<td><?php echo $r['pabrik']; ?></td>
						<td><?php echo $r['rangka']; ?></td>
						<td><?php echo $r['mesin']; ?></td>
						<td><?php echo $r['polisi']; ?></td>
						<td><?php echo $r['bpkb']; ?></td>
						<td><?php echo $r['asal_usul']; ?></td>
						<td><?php echo rupiah($r['harga_total']); ?></td>
						<td><?php echo $r['keterangan']; ?></td>
					
                </tr>
				<?php  $no++; } ?>
				
            </tbody>
			
					
			
        </table>
		
		
<?php 
		include "titimangsa.php";
		
		?>
   
	
</body>
</html>