<div class="row">
        <div class="col">
          <div class="card-wrapper">
            <!-- Custom form validation -->
            <div class="card">
              <!-- Card header -->
              <div class="card-header">
                <h3 class="mb-0">Detail Info Sekolah</h3>
              </div>
			  
				<div class="card-body">
                
                <form action="<?php echo base_url(); ?>app/sekolah/<?= $rows['id_sekolah']; ?>" method="post"  enctype="multipart/form-data">
                  <div class="form-row">
					<div class="col-md-12 mb-3">
                      <label class="form-control-label">Nama Sekolah</label>
                      <input type="text" class="form-control" value="<?= $rows['nama_sekolah']; ?>" name="nama_sekolah" required>
                      
                    </div>
					<div class="col-md-12 mb-3">
                      <label class="form-control-label">Alamat Sekolah</label>
                      <input type="text" class="form-control" value="<?= $rows['alamat']; ?>" name="alamat" required>
                      
                    </div>
					
                    <div class="col-md-6 mb-3">
                      <label class="form-control-label">Kepala sekolah</label>
                      <input type="text" class="form-control" value="<?= $rows['nama_kepsek']; ?>" name="nama_kepsek" required>
                      
                    </div>
					<div class="col-md-6 mb-3">
                      <label class="form-control-label">Email</label>
                      <input type="text" class="form-control" value="<?= $rows['email']; ?>" name="email" required>
                      
                    </div>
					<div class="col-md-6 mb-3">
                      <label class="form-control-label">Kecamatan</label>
                      <input type="text" class="form-control" value="<?= $rows['kecamatan']; ?>" name="kecamatan" required>
                      
                    </div>
					<div class="col-md-6 mb-3">
                      <label class="form-control-label">Kode UPB</label>
                      <input type="text" class="form-control" value="<?= $rows['kode_upb']; ?>" name="kode_upb" required>
                      
                    </div>
					<div class="col-md-6 mb-3">
                      <label class="form-control-label">No Telp</label>
                      <input type="text" class="form-control" value="<?= $rows['no_telp']; ?>" name="no_telp" required>
                      
                    </div>
					<div class="col-md-6 mb-3">
                      <label class="form-control-label">Aktif</label>
                      <select name="aktif"  class="form-control">
					  <?php if ( $rows['aktif']) { ?>
					  <option value="Y" selected>Aktif</option>
					  <option value="N">Tidak aktif</option>
					  <?php } else { ?>
					  <option value="Y">Aktif</option>
					  <option value="N" selected>Tidak aktif</option>
					  <?php } ?>
					  </select>
                      
                    </div>
					<div class="col-md-12 mb-3">
                      <label class="form-control-label">Preview</label><br>
                      <img src="<?= base_url('assets/logo/'.$rows['logo']); ?>">
                      
                    </div>
					<div class="col-md-12 mb-3">
                      <label class="form-control-label">Logo</label>
                      <input type="file" class="form-control" name="logo" required>
                      
                    </div>
					<div >
                      <button type="button" class="btn btn-info">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
              </div>
            </div>
            <!-- Default browser form validation -->
            
</div>
</div>
</div>
</div>