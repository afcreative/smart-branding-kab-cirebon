<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><a href="<?php echo base_url(); ?>app/tambah_sekolah"><button type="button" class="btn btn-outline-info" >
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Sekolah</span></button></a></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Logo</th>
                        <th>Nama Sekolah</th>
						<th>Kepala Sekolah</th>
						<th>Alamat</th>
						<th>Email</th>
						<th>No Telp</th>
						<th>Aktif</th>
						
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/logo/<?php echo $r['logo']; ?>">
                  </span></td>
                        <td>
						
						<?php echo $r['nama_sekolah']; ?></td>
						<td><?php echo $r['nama_kepsek']; ?></td>
						<td><?php echo $r['alamat']; ?></td>
						<td><?php echo $r['email']; ?></td>
						<td><?php echo $r['no_telp']; ?></td>
						<td><?php echo $r['aktif']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#user<?php echo "$r[id_sekolah]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_sekolah/$r[id_sekolah]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_sekolah/<?php echo $r['id_sekolah']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="user<?php echo $r['id_sekolah']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Sekolah</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                         <div class="form-group">
                        <label>Nama sekolah</label>
                        <input type="text" name="nama_sekolah" value="<?php echo $r['nama_sekolah']; ?>" required="required" class="form-control" >
                      </div>
					  
					  <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" name="alamat"  value="<?php echo $r['alamat']; ?>" required="required" class="form-control" >
                      </div>
					  <div class="form-group">
                        <label>Nama Kepala sekolah</label>
                        <input type="text" name="nama_kepsek" value="<?php echo $r['nama_kepsek']; ?>" required="required" class="form-control">
                      </div>
					  <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" value="<?php echo $r['email']; ?>" required="required" class="form-control">
                      </div>
					  <div class="form-group">
                        <label>No Telp</label>
                        <input type="text" name="no_telp" value="<?php echo $r['no_telp']; ?>" required="required" class="form-control">
                      </div>
					  <div class="form-group">
                        <label>Aktif</label>
                        <select name="aktif"  class="form-control">
						<?php if ($r['aktif']=='Y'){ ?>
					  <option value="Y" selected>Aktif</option>
					  <option value="N">Tidak aktif</option>
					  <?php } else { ?>
					  <option value="Y" >Aktif</option>
					  <option value="N" selected>Tidak aktif</option>
					  <?php }  ?>
					  </select>
                      </div>
						
					<div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="logo"  required="required" class="form-control" >
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-info" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>