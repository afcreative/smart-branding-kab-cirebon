
    <div class="row">
        <div class="col-xl-8">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Video Penggunaan</h3>
                </div>
                <div class="col text-right">
                  <a href="https://www.youtube.com/embed/96bxFhd1-tQ" class="btn btn-sm btn-primary" target="_blank">Lihat</a>
                </div>
              </div>
            </div>
			<div class="col-xl-12">
            <iframe width="100%" height="450" src="https://www.youtube.com/embed/96bxFhd1-tQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
		  </div>
        </div>
        <div class="col-xl-4">
          <div class="card">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Buku Panduan</h3>
                </div>
                <div class="col text-right">
                  <a href="<?= base_url('assets/info/modul.pdf'); ?>" target="_blank" class="btn btn-sm btn-primary">Download</a>
                </div>
              </div>
            </div>
            <div class="col-xl-12">
              
										<img alt="Image placeholder" src="<?= base_url('assets/info/buku_panduan.jpg'); ?>" width="100%">
									  
              
            </div>
          </div>
        </div>
      </div>
     
	