<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah dokument</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Kode Dokument </th>
						<th>Nama Dokument</th>
						<th>Arsip File</th>
						<th>Update Time</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
				
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						
						<td><?php echo $r['kode_dokument']; ?></td>
						<td><?php echo $r['nama_dokument']; ?></td>
						<td><?php echo $r['file_dokument']; ?></td>
						<td><?php echo $r['update_data']; ?></td>
                        <td>
						<a href="<?php echo base_url();?>assets/arsip_dokument/<?php echo $r['file_dokument']; ?>" button title="Download" type="button" class="btn btn-success btn-sm" target="_blank">
                          <i class="fa fa-download" ></i>
                        </button></a>
						
						  <button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#user<?php echo "$r[id_dokument]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_dokument/$r[id_dokument]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</td></tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/dokument" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah dokument</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div class="form-group">
                        <label>Kode dokument</label>
                        <input type="text" name="kode_dokument" required="required" class="form-control" placeholder="Kode dokument ..">
                      </div>
					  <div class="form-group">
                        <label>Nama dokument</label>
                        <input type="text" name="nama_dokument" required="required" class="form-control" placeholder="Nama dokument ..">
                      </div>
					  
					  
					  
					    <div class="form-group">
                        <label>Arsip File</label>
                        <input type="file" name="arsip"  class="form-control" >
                      </div>                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_dokument/<?php echo $r['id_dokument']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="user<?php echo $r['id_dokument']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit dokument</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                         <div class="form-group">
                        <label>Kode dokument</label>
                        <input type="text" name="kode_dokument" value="<?php echo $r['kode_dokument']; ?>"  required="required" class="form-control" >
                      </div>
					  <div class="form-group">
                        <label>Nama dokument</label>
                        <input type="text" name="nama_dokument" value="<?php echo $r['nama_dokument']; ?>" required="required" class="form-control" >
                      </div>
					  
					  
					  
					    <div class="form-group">
                        <label>Foto</label><br>
						
                        <input type="file" name="arsip" class="form-control" >
						<?php echo $r['file_dokument']; ?>
                      </div>   

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>