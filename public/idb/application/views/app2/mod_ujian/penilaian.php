<?php
if($rows['foto_peserta']==''){
						$gambar="blank.png";
					}else{
						$gambar=$rows['foto_peserta'];
					}
					$id_ukk=$rows['id_ukk'];
	$rw=$this->db->query("Select * From ukk a, sekolah b, kompetensi c where a.id_sekolah=b.id_sekolah and a.id_kompetensi=c.id_kompetensi and a.id_ukk='$id_ukk'")->row_array();
?>
<div class="row">
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">
            <img src="<?php echo base_url(); ?>assets/img/theme/img-1-1000x600.jpg" alt="Image placeholder" class="card-img-top">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    
					   <img src="<?php echo base_url(); ?>assets/foto_peserta/<?php echo $gambar; ?>" class="rounded-circle">
                 
                  </a>
                </div>
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
                <a href="<?php echo base_url(); ?>app/cetak_ukk/<?php echo $rows['id_peserta']; ?>" class="btn btn-sm btn-info mr-4" target="_blank">Cetak lap</a>
                <a href="#" class="btn btn-sm btn-default float-right">Kirim Lap</a>
              </div>
            </div>
            <div class="card-body pt-0">
              
              <div class="text-center">
                <h5 class="h3">
                  <?php echo $rows['nama_peserta']; ?><span class="font-weight-light">, <?php echo $rows['id_ukk']; ?></span>
                </h5>
                <div class="h5 font-weight-300">
                  <i class="ni location_pin mr-2"></i><?php echo $rows['tempat_lahir']; ?>, <?php echo $rows['tgl_lahir']; ?>
                </div>
                <div class="h5 mt-4">
                  <i class="ni business_briefcase-24 mr-2"></i><?php echo $rw['nama_kompetensi']; ?>
                </div>
                <div>
                  <i class="ni education_hat mr-2"></i><?php echo $rw['nama_sekolah']; ?>
                </div>
              </div>
            </div>
          </div>
          <!-- Progress track -->
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <!-- Title -->
              <h5 class="h3 mb-0">Detail Nilai</h5>
            </div>
            <!-- Card body -->
            <div class="card-body">
              <!-- List group -->
              <ul class="list-group list-group-flush list my--3">
			  <?php
			  $detail=$this->db->query("Select * From detail_nilai where id_ukk='".$rows['id_ukk']."' and id_peserta='".$rows['id_peserta']."'");
			  
			  $no=1;
				foreach ($detail->result_array() as $det){ 
				$n=$this->db->query("Select * From examp where id_examp='".$det['id_examp']."'")->row_array();
				$nilai=$det['nilai'];
				$pros=($det['nilai']/$n['nilai'])*100;
				?>
                <li class="list-group-item px-0">
                  <div class="row align-items-center">
                    <div class="col-auto">
                      <!-- Avatar -->
                      <a href="#" class="avatar rounded-circle">
                        <?php echo $no; ?>
                      </a>
                    </div>
                    <div class="col">
                      <h5><?php echo $det['nilai']; ?></h5>
                      <div class="progress progress-xs mb-0">
                        <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="6" aria-valuemin="0" aria-valuemax="10" style="width: <?php echo $pros; ?>%;"></div>
                      </div>
                    </div>
                  </div>
                </li>
                <?php $no++; } ?>
                
                
                
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="row">
            <div class="col-lg-6">
              <div class="card bg-gradient-info border-0">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">Total Penilaian</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">
					  <?php $dt=$this->db->query("Select sum(nilai) as tot From detail_nilai where id_ukk='".$rows['id_ukk']."' and id_peserta='".$rows['id_peserta']."'")->row_array(); 
					  
					  $dtexamp=$this->db->query("Select sum(nilai) as totex From examp where id_ukk='".$rows['id_ukk']."' ")->row_array(); 
					  
					  $pronilai=$dt['tot']/$dtexamp['totex'] * 100;
					  echo $dt['tot']; ?>
					  
					  </span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-white mr-2"><i class="fa fa-arrow-up"></i> <?php echo $pronilai; ?>%</span>
                    <span class="text-nowrap text-light">Keahlian</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card bg-gradient-danger border-0">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">Performance</h5>
					  <?php 
					  if($pronilai > 80 ){
						  $grade="Baik";
						  
					  }elseif($pronilai > 90 ){
						  $grade="Amat Baik";
						  
					  }else{
						  $grade="Perbaikan";
					  }
					  
					  ?>
                      <span class="h2 font-weight-bold mb-0 text-white"><?php echo $grade; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                        <i class="ni ni-spaceship"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-white mr-2"><i class="fa fa-arrow-up"></i> <?php echo $dt['tot']; ?></span>
                    <span class="text-nowrap text-light">Up Skill Anda</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Detail Penilaian </h3>
                </div>
                
              </div>
            </div>
            <div class="card-body">
             
               
					
               
                <!-- Address -->
               
                <div class="pl-lg-12">
				<div class="row">
				<table class="table table-flush" id="datatable-basic">
				<tr><td width="10px">No</td><td>Indikator</td><td width="50px">Nilai</td></tr>
				<?php 
				$no=1;
				$indikator=$this->db->query("Select * From examp a, ukk b where a.id_ukk=b.id_ukk and a.id_ukk='$id_ukk'")->result_array();
				foreach($indikator as $rs){ 
				$cekdet=$this->db->query("Select * From detail_nilai where id_examp='".$rs['id_examp']."' and id_peserta='".$rows['id_peserta']."'");
				$ir=$cekdet->row_array();
				$ada=$cekdet->num_rows();
				if($ada > 0 ){
					$bg="#7FFFD4";
				}else{
				$bg="";	
				}
				
				?>
				
					<form action="<?php echo base_url();?>app/input_nilai/<?php echo $rows['id_peserta']; ?>" method="post">
                    <input type="hidden" name="id_ukk" value="<?php echo $id_ukk; ?>">
					<input type="hidden" name="id_examp" value="<?php echo $rs['id_examp']; ?>">
					<tr style="background:<?php echo $bg; ?>;"><td width="10px"><?php echo $no;  ?></td><td><?php echo $rs['indikator'];  ?></td><td width="50px"><input type="text" name="nilai" value ="<?php echo $ir['nilai'];  ?>" title="<?php echo $rs['nilai']; ?>" class="form-control "></td></tr>
					
					
					
                  </form>
                  <?php $no++; } ?>
				  <form action="<?php echo base_url();?>app/input_keterangan/<?php echo $rows['id_peserta']; ?>" method="post">
				  <tr><td colspan="4"><input type="text" name="keterangan" value="<?php echo $rows['keterangan']; ?>"  placeholder="Catatan sidang" class="form-control "></td></tr>
				  </form>
				  </table>
				  
				  
					
				  </div>
                </div>
                
                
              
            </div>
          </div>
        </div>
      </div>
	   <script type="text/javascript">
		 function FocusOnInput()
		 {
		 document.getElementById("nilai").focus();
		 }
 </script>
 <script type="text/javascript">
		 function FocusOnInput()
		 {
		 document.getElementById("keterangan").focus();
		 }
 </script>