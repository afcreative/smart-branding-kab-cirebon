<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Indikator</span></button></h4>
              Sekolah : <p><?php echo $rows['nama_sekolah'];?>||<?php echo $rows['nama_kompetensi'];?>|<?php echo $rows['tahun'];?></p>
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Indikator </th>
						<th>Nilai Max</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
			
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						
						<td><?php echo $r['indikator']; ?></td>
						<td><?php echo $r['nilai']; ?></td>
						
                        <td>
						  <button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#examp<?php echo "$r[id_examp]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_examp/$r[id_examp]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</td></tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/examp" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Soal Examp</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div class="form-group">
                        <label>Indikator</label>
                        <textarea name="indikator" required="required" class="form-control" placeholder="Indikator penilaian .."></textarea>
                      </div>
					  <div class="form-group">
                        <label>Nilai Max</label>
                        <input type="text" name="nilai" required="required" class="form-control" placeholder="Nilai maximum ..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_examp/<?php echo $r['id_examp']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="examp<?php echo $r['id_examp']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit indikator examp</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                         <div class="form-group">
                        <label>Indikator</label>
                        <textarea name="indikator" required="required" class="form-control" placeholder="Indikator penilaian .."><?php echo $r['indikator']; ?></textarea>
                      </div>
					  <div class="form-group">
                        <label>Nilai Max</label>
                        <input type="text" name="nilai" value="<?php echo $r['nilai']; ?>" required="required" class="form-control" >
                      </div> 

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>