<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0">Penilaian UKK : <p><?php echo $rows['nama_sekolah'];?></p></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>NIS </th>
						<th>Nama Peserta</th>
						<th>Tempat Lahir</th>
						<th>Tanggal lahir</th>
						<th>Foto</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
				$ada=$this->db->query("Select * From detail_nilai where id_peserta='".$r['id_peserta']."'")->num_rows();
					if($r['foto_peserta']==''){
						$gambar="blank.png";
					}else{
						$gambar=$r['foto_peserta'];
					}
					if($ada > 0 ){
						$bg="yellow";
					}else{
						$bg="";
					}
					
					?>
					
                      <tr style="background:<?php echo $bg; ?>">
                        <td><?php echo $no; ?></td>
						
						
						<td><?php echo $r['nis']; ?></td>
						<td><?php echo $r['nama_peserta']; ?></td>
						<td><?php echo $r['tempat_lahir']; ?></td>
						<td><?php echo $r['tgl_lahir']; ?></td>
						<td><span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/foto_peserta/<?php echo $gambar; ?>">
                  </span></td>
                        <td>
						  <a href="<?php echo base_url(); ?>app/penilaian/<?php echo $r['id_peserta'];?>"><button title="Mulai Penilaian" type="button" class="btn btn-info btn-sm" >
                          <i class="ni ni-check-bold"></i>
                        </button></a>
					<?php	echo"
					<a href='".base_url()."app/hapus_peserta/$r[id_peserta]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk mereset nilai?')\">
                            <i class='fa fa-trash'></i>
					
					</td></tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/peserta" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Peserta</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div class="form-group">
                        <label>Nis</label>
                        <input type="text" name="nis" required="required" class="form-control" placeholder="Nis ..">
                      </div>
					  <div class="form-group">
                        <label>Nama Peserta</label>
                        <input type="text" name="nama_peserta" required="required" class="form-control" placeholder="Nama peserta ..">
                      </div>
					  <div class="form-group">
                        <label>Tempat lahir</label>
                        <input type="text" name="tempat_lahir" required="required" class="form-control" placeholder="Tempat Lahir..">
                      </div>
					  <div class="form-group">
                        <label>Tanggal lahir</label>
                        <input type="text" name="tgl_lahir" required="required" class="form-control" placeholder="Tanggal lahir ..">
                      </div>
					  
					    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="foto"  class="form-control" >
                      </div>                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_peserta/<?php echo $r['id_peserta']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="user<?php echo $r['id_peserta']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Peserta</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                         <div class="form-group">
                        <label>Nis</label>
                        <input type="text" name="nis" value="<?php echo $r['nis']; ?>"  required="required" class="form-control" >
                      </div>
					  <div class="form-group">
                        <label>Nama Peserta</label>
                        <input type="text" name="nama_peserta" value="<?php echo $r['nama_peserta']; ?>" required="required" class="form-control" >
                      </div>
					  <div class="form-group">
                        <label>Tempat lahir</label>
                        <input type="text" name="tempat_lahir" value="<?php echo $r['tempat_lahir']; ?>" required="required" class="form-control" >
                      </div>
					  <div class="form-group">
                        <label>Tanggal lahir</label>
                        <input type="date" name="tgl_lahir"  value="<?php echo $r['tgl_lahir']; ?>" required="required" class="form-control" >
                      </div>
					  
					    <div class="form-group">
                        <label>Foto</label><br>
						<span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/foto_peserta/<?php echo $gambar; ?>">
                  </span>
                        <input type="file" name="foto" class="form-control" >
                      </div>   

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>