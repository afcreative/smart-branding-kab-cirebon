<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Users</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Username</th>
						<th>Nama lengkap</th>
						<th>Nama lengkap</th>
						<th>Email</th>
						<th>No Telp</th>
						<th>Level</th>
						<th>Blokir</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/img/theme/team-4.jpg">
                  </span></td>
                        <td>
						
						<?php echo $r['username']; ?></td>
						<td><?php echo $r['nama_lengkap']; ?></td>
						<td><?php echo $r['email']; ?></td>
						<td><?php echo $r['no_telp']; ?></td>
						<td><?php echo $r['level']; ?></td>
						<td><?php echo $r['blokir']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#user<?php echo "$r[id_users]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."administrator/hapus_user/$r[id_users]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/users" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah user</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" required="required" class="form-control" placeholder="Username ..">
                      </div>
					  <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="password" required="required" class="form-control" placeholder="Password user ..">
                      </div>
					  <div class="form-group">
                        <label>Nama lengkap</label>
                        <input type="text" name="nama_lengkap" required="required" class="form-control" placeholder="Nama user ..">
                      </div>
					  <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" required="required" class="form-control" placeholder="Email user ..">
                      </div>
					  <div class="form-group">
                        <label>No Telp</label>
                        <input type="text" name="no_telp" required="required" class="form-control" placeholder="No Telp user ..">
                      </div>
					  <div class="form-group">
                        <label>Level</label>
                        <input type="text" name="level" required="required" class="form-control" placeholder="Level user ..">
                      </div>
					  <div class="form-group">
                        <label>Blokir</label>
                        <input type="text" name="blokir" required="required" class="form-control" placeholder="Blokir user ..">
                      </div>
					    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="foto"  required="required" class="form-control" placeholder="Blokir user ..">
                      </div>                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>administrator/edit_users/<?php echo $r['id_users']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="user<?php echo $r['id_users']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                                                                 <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" value="<?php echo $r['username']; ?>" required="required" class="form-control" placeholder="Username ..">
                      </div>
					  <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="password" value="" required="required" class="form-control" placeholder="Password user ..">
                      </div>
					  <div class="form-group">
                        <label>Nama lengkap</label>
                        <input type="text" name="nama_lengkap" value="<?php echo $r['nama_lengkap']; ?>" required="required" class="form-control" placeholder="Nama user ..">
                      </div>
					  <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" value="<?php echo $r['email']; ?>" required="required" class="form-control" placeholder="Email user ..">
                      </div>
					  <div class="form-group">
                        <label>No Telp</label>
                        <input type="text" name="no_telp" value="<?php echo $r['no_telp']; ?>" required="required" class="form-control" placeholder="No Telp user ..">
                      </div>
					  <div class="form-group">
                        <label>Level</label>
                        <input type="text" name="level" value="<?php echo $r['level']; ?>" required="required" class="form-control" placeholder="Level user ..">
                      </div>
					  <div class="form-group">
                        <label>Blokir</label>
                        <input type="text" name="blokir" value="<?php echo $r['blokir']; ?>" required="required" class="form-control" placeholder="Blokir user ..">
                      </div>
					<div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="foto" value="<?php echo $r['foto_user']; ?>" required="required" class="form-control" placeholder="Blokir user ..">
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>