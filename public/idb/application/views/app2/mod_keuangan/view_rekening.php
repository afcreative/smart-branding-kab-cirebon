<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Rekening</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Kode Rekening</th>
						<th>Nama Rekening</th>
						<th>Rincian</th>
						<th>PPN</th>
						<th>PPH21</th>
						<th>PPH22</th>
						<th>PPH23</th>
						<th>PPH24</th>
						<th>SSPD</th>
						<th>Blok Belanja</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['kode_rekening']; ?></td>
						<td><?php echo $r['nama_rekening']; ?></td>
						<td><?php echo $r['rincian_objek']; ?></td>
						<td><?php echo $r['PPN']; ?></td>
						<td><?php echo $r['PPH21']; ?></td>
						<td><?php echo $r['PPH22']; ?></td>
						<td><?php echo $r['PPH23']; ?></td>
						<td><?php echo $r['PPH24']; ?></td>
						<td><?php echo $r['SSPD']; ?></td>
						<td><?php echo $r['blok_belanja']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#rekening<?php echo "$r[id_rekening]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_rekening/$r[id_rekening]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/rekening" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Rekening</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					
					  <div class="form-group">
                        <label>Kode rekening </label>
                        <input type="text" name="kode_rekening" required="required" class="form-control" placeholder="Kode rekening ..">
                      </div>
					  <div class="form-group">
                        <label>Nama rekening </label>
                        <input type="text" name="nama_rekening" required="required" class="form-control" placeholder="Nama rekening ..">
                      </div>
					  <div class="form-group">
                        <label>Rincian objek </label>
                        <input type="text" name="rincian_objek" required="required" class="form-control" placeholder="Rincian objek ..">
                      </div>
					  <table width="100%">
					  <tr><td width="50%" align="center">
					  <div class="form-group">
							<label>PPN </label><br>
							<label class="custom-toggle custom-toggle-info">
							<input type="checkbox" name="PPN" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  <div class="form-group">
							<label>PPH21 </label><br>
							<label class="custom-toggle custom-toggle-danger">
							<input type="checkbox" name="PPH21" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  <div class="form-group">
							<label>PPH22 </label><br>
							<label class="custom-toggle custom-toggle-success">
							<input type="checkbox" name="PPH22" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  
					  </td><td  align="center">
					  <div class="form-group">
							<label>PPH23 </label><br>
							<label class="custom-toggle custom-toggle-info">
							<input type="checkbox" name="PPH23" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  <div class="form-group">
							<label>PPH24 </label><br>
							<label class="custom-toggle custom-toggle-danger">
							<input type="checkbox" name="PPH24" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  <div class="form-group">
							<label>SSPD </label><br>
							<label class="custom-toggle custom-toggle-success">
							<input type="checkbox" name="SSPD" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  </td></tr>
					  </table>
					  
					  
					  <div class="form-group">
							<label>Blok Belanja </label>
						  <input type="text" name="blok_belanja" required="required" class="form-control" placeholder="Blok belanja ..">
                      
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_rekening/<?php echo $r['id_rekening']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="rekening<?php echo $r['id_rekening']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit rekening</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					
					  <div class="form-group">
                        <label>Kode rekening </label>
                        <input type="text" name="kode_rekening" value="<?php echo $r['kode_rekening']; ?>" required="required" class="form-control" placeholder="Kode rekening ..">
                      </div>
					  <div class="form-group">
                        <label>Nama rekening </label>
                        <input type="text" name="nama_rekening" value="<?php echo $r['nama_rekening']; ?>" required="required" class="form-control" placeholder="Nama rekening ..">
                      </div>
					  <div class="form-group">
                        <label>Rincian objek </label>
                        <input type="text" name="rincian_objek" value="<?php echo $r['rincian_objek']; ?>" required="required" class="form-control" placeholder="Rincian objek ..">
                      </div>
					  <table width="100%">
					  <tr><td width="50%" align="center">
					  <div class="form-group">
							<label>PPN </label><br>
							<label class="custom-toggle custom-toggle-info">
							<input type="checkbox" name="PPN" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  <div class="form-group">
							<label>PPH21 </label><br>
							<label class="custom-toggle custom-toggle-danger">
							<input type="checkbox" name="PPH21" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  <div class="form-group">
							<label>PPH22 </label><br>
							<label class="custom-toggle custom-toggle-success">
							<input type="checkbox" name="PPH22" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  
					  </td><td  align="center">
					  <div class="form-group">
							<label>PPH23 </label><br>
							<label class="custom-toggle custom-toggle-info">
							<input type="checkbox" name="PPH23" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  <div class="form-group">
							<label>PPH24 </label><br>
							<label class="custom-toggle custom-toggle-danger">
							<input type="checkbox" name="PPH24" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  <div class="form-group">
							<label>SSPD </label><br>
							<label class="custom-toggle custom-toggle-success">
							<input type="checkbox" name="SSPD" value="Y" checked="">
							<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
							</label>
                      </div>
					  </td></tr>
					  </table>
					  
					  
					  <div class="form-group">
							<label>Blok Belanja </label>
						  <input type="text" name="blok_belanja" value="<?php echo $r['blok_belanja']; ?>" required="required" class="form-control" placeholder="Blok belanja ..">
                      
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>