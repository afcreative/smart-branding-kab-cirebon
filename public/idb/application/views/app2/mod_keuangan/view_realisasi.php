<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Realisasi</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama rekening</th>
						<th>Kode Pemasukan</th>
						<th>Tanggal Realisasi</th>
						<th>Jumlah</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_rekening']; ?></td>
						<td><?php echo $r['kode_pemasukan']; ?></td>
						<td><?php echo $r['tgl_realisasi']; ?></td>
						<td><?php echo $r['jumlah']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pemasukan<?php echo "$r[id_pemasukan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_realisasi/$r[id_realisasi]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/realisasi" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Realisasi</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     <div class="form-group">
                        <label>Pemasukan Utama</label>
                        <?php
						$pemasukan =$this->db->query("Select * From pemasukan a, rekening b where a.id_rekening=b.id_rekening ")->result_array();
						echo"
						<select class='form-control' name='id_pemasukan' required>
                    <option value=''>- Pilih Pemasukan Utama -</option>";
                    foreach ($pemasukan as $row) {
                       if ($r['id_pemasukan']==$row['id_pemasukan']){
                            echo "<option value='$row[id_pemasukan]' selected>$row[nama_rekening]</option>";
                            }else{
                            echo "<option value='$row[id_pemasukan]'>$row[nama_rekening]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					  <div class="form-group">
                        <label>Tanggal Realisasi </label>
                        <input type="date" name="tgl_realisasi" required="required" class="form-control" placeholder="Tanggal Realisasi ..">
                      </div>
					  <div class="form-group">
                        <label>Jumlah Realisasi </label>
                        <input type="text" name="jumlah" required="required" class="form-control" placeholder="Jumlah Realisasi ..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_pemasukan/<?php echo $r['id_pemasukan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="pemasukan<?php echo $r['id_pemasukan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit pemasukan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    <div class="form-group">
                        <label>Pemasukan Utama</label>
                        <?php
						$pemasukan =$this->db->query("Select * From rekening ")->result_array();
						echo"
						<select class='form-control' name='id_rekening' required>
                    <option value=''>- Pilih Pemasukan Utama -</option>";
                    foreach ($pemasukan as $row) {
                       if ($r['id_rekening']==$row['id_rekening']){
                            echo "<option value='$row[id_rekening]' selected>$row[nama_rekening]</option>";
                            }else{
                            echo "<option value='$row[id_rekening]'>$row[nama_rekening]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Pemasukan </label>
                        <input type="text" name="kode_pemasukan" value="<?php echo $r['kode_pemasukan']; ?>" required="required" class="form-control">
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>