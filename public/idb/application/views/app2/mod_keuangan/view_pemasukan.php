<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Pemasukan</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama rekening</th>
						<th>Kode Pemasukan</th>
						<th>Tanggal Pemasukan</th>
						<th>Jumlah</th>
						<th>Realisasi (s.d bulan ini)</th>
						<th>Selisih</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					$t=$this->db->query("Select sum(jml_realisasi) as tot From realisasi where id_pemasukan='".$r['id_pemasukan']."'")->row_array();
					$selisih=$r['jumlah']-$t['tot'];
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_rekening']; ?></td>
						<td><?php echo $r['kode_pemasukan']; ?></td>
						<td><?php echo $r['tgl_pemasukan']; ?></td>
						<td><?php echo rupiah($r['jumlah']); ?></td>
						<td><?php echo rupiah($t['tot']); ?></td>
						<td><?php echo rupiah($selisih); ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pemasukan<?php echo "$r[id_pemasukan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_pemasukan/$r[id_pemasukan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/pemasukan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Pemasukan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					<input type="hidden" name="id_rekening" id="id_rekening" required="required" class="form-control" >
                     <div class="form-group">
                        <label>Pemasukan Utama</label>
                        <input type="text" name="nama_rekening" id="rekening" required="required" class="form-control" placeholder="Rekening Pemasukan ..">
						<a href="#" data-toggle="modal" data-target="#kodeModal">Lihat Kode Rekening</a>
                      </div>
					  <div class="form-group">
                        <label>Kode Pemasukan </label>
                        <input type="text" name="kode_pemasukan" id="kode" required="required" class="form-control" placeholder="Kode Pemasukan ..">
						
                      </div>
					  <div class="form-group">
                        <label>Tanggal Pemasukan </label>
                        <input type="date" name="tgl_pemasukan" required="required" class="form-control" placeholder="Tanggal Pemasukan ..">
                      </div>
					  <div class="form-group">
                        <label>Jumlah Pemasukan </label>
                        <input type="text" name="jumlah" required="required" class="form-control" placeholder="Jumlah Pemasukan ..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			<div class="modal fade " id="kodeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Kode Asset Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
				<div class="table-responsive py-4">
              <table class="table table-flush display">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Kode Rekening </th>
						<th>Nama Rekening</th>
						<th>Rincian Objek</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; 
				$kriteria=$this->db->query("Select * From rekening");
				
				foreach($kriteria->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['kode_rekening']; ?></td>
						<td><a class="button-detail-tab-item" href="javascript:void(0)" data-isi="<?php echo "$r[kode_rekening]"; ?>" data-id="<?php echo "$r[id_rekening]"; ?>" data-rekening="<?php echo "$r[nama_rekening]"; ?>"><span><?php echo $r['nama_rekening']; ?></span></a></td>
						<td><?php echo $r['rincian_objek']; ?></td>
						  
					
					</tr>
					<?php $no++; } ?> 
                </tbody>
              </table>
            </div>
                    </div>
                    
                  </div>
                </div>
              </div>
			  <script>
			  $(".button-detail-tab-item").click(function(){
				  $('#kodeModal').modal('hide');
				
				var ambilVal = $(this).attr('data-isi');
				var nama_rekening = $(this).attr('data-rekening');
				var id_rekening = $(this).attr('data-id');
					$("#kode").val(ambilVal);
					$("#rekening").val(nama_rekening);
					$("#id_rekening").val(id_rekening);					
								
				})
			  </script>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { 
			$rb=$this->db->query("Select * From rekening where id_rekening='".$r['id_rekening']."'")->row_array();
			?>
			             <form action="<?php echo base_url(); ?>app/edit_pemasukan/<?php echo $r['id_pemasukan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="pemasukan<?php echo $r['id_pemasukan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit pemasukan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					<input type="hidden" name="id_rekening" value="<?php echo $r['id_rekening']; ?>" id="id_rekening2" required="required" class="form-control" >
                     <div class="form-group">
                        <label>Pemasukan Utama</label>
                        <input type="text" name="nama_rekening" value="<?php echo $rb['nama_rekening']; ?>" id="rekening2" required="required" class="form-control" placeholder="Rekening Pemasukan ..">
						<a href="#" data-toggle="modal" data-target="#kodeModal2">Lihat Kode Rekening</a>
                      </div>
					  <div class="form-group">
                        <label>Kode Pemasukan </label>
                        <input type="text" name="kode_pemasukan" value="<?php echo $r['kode_pemasukan']; ?>" id="kode2" required="required" class="form-control" placeholder="Kode Pemasukan ..">
						
                      </div>
					  <div class="form-group">
                        <label>Tanggal Pemasukan </label>
                        <input type="date" name="tgl_pemasukan" value="<?php echo $r['tgl_pemasukan']; ?>" required="required" class="form-control" placeholder="Tanggal Pemasukan ..">
                      </div>
					  <div class="form-group">
                        <label>Jumlah Pemasukan </label>
                        <input type="text" name="jumlah" value="<?php echo $r['jumlah']; ?>" required="required" class="form-control" placeholder="Jumlah Pemasukan ..">
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>
			 <div class="modal fade " id="kodeModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Kode Asset Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
				<div class="table-responsive py-4">
              <table class="table table-flush display">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Kode Rekening </th>
						<th>Nama Rekening</th>
						<th>Rincian Objek</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; 
				$kriteria=$this->db->query("Select * From rekening");
				
				foreach($kriteria->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['kode_rekening']; ?></td>
						<td><a class="button-detail-tab-item2" href="javascript:void(0)" data-isi2="<?php echo "$r[kode_rekening]"; ?>" data-id2="<?php echo "$r[id_rekening]"; ?>" data-rekening2="<?php echo "$r[nama_rekening]"; ?>"><span><?php echo $r['nama_rekening']; ?></span></a></td>
						<td><?php echo $r['rincian_objek']; ?></td>
						  
					
					</tr>
					<?php $no++; } ?> 
                </tbody>
              </table>
            </div>
                    </div>
                    
                  </div>
                </div>
              </div>
			  <script>
			  $(".button-detail-tab-item2").click(function(){
				  $('#kodeModal2').modal('hide');
				
				var ambilVal2 = $(this).attr('data-isi2');
				var nama_rekening2 = $(this).attr('data-rekening2');
				var id_rekening2 = $(this).attr('data-id2');
					$("#kode2").val(ambilVal2);
					$("#rekening2").val(nama_rekening2);
					$("#id_rekening2").val(id_rekening2);					
								
				})
			  </script>