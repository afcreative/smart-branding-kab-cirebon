<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
        <a class="navbar-brand" href="<?php echo base_url(); ?>app/home">
          <img src="<?php echo base_url(); ?>assets/logo/<?php echo $iden['logo']; ?>" class="navbar-brand-img" alt="...">
        </a>
        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
		  <?php 
		  $menu=$this->db->query("Select * From menu order by urutan ")->result_array();
		  foreach($menu as $rm){
		  ?>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-<?php echo $rm['id_menu']; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-dashboards">
                <i class="ni ni-<?php echo $rm['icon']; ?> text-<?php echo $rm['teks']; ?>"></i>
                <span class="nav-link-text"><?php echo $rm['nama_menu']; ?></span>
              </a>
              <div class="collapse" id="navbar-<?php echo $rm['id_menu']; ?>">
                <ul class="nav nav-sm flex-column">
				<?php 
				  $submenu=$this->db->query("Select * From submenu where id_menu='".$rm['id_menu']."' ")->result_array();
				  foreach($submenu as $r){
					  $cek=$this->db->query("Select * From childmenu where id_submenu='".$r['id_submenu']."'")->num_rows();
					  if($cek > 0 ){
				  ?>
				  <li class="nav-item">
              <a class="nav-link" href="#navbar-<?php echo $r['id_submenu']; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-<?php echo $r['id_submenu']; ?>">
                <i class="ni ni-<?php echo $r['icons']; ?> text-<?php echo $r['tekss']; ?>"></i>
                <span class="nav-link-text">yy<?php echo $r['nama_submenu'];?></span>
              </a>
              <div class="collapse" id="navbar-<?php echo $r['id_submenu']; ?>">
                <ul class="nav nav-sm flex-column">
				<?php $childmenu=$this->db->query("Select * From childmenu where id_submenu='".$r['id_submenu']."' ")->result_array();
				  foreach($childmenu as $rq){ ?>
                  <li class="nav-item">
                    <a href="<?php echo base_url(); ?><?php echo $rq['app_link']; ?>" class="nav-link"target="_blank"><i class="ni ni-<?php echo $rq['icons']; ?> text-<?php echo $rq['tekss']; ?>"></i><?php echo $rq['nama_childmenu'];?></a>
                  </li>
                  <?php } ?>
				  
                </ul>
              </div>
            </li>
				  <?php }else{ ?>
                  <li class="nav-item">
                    <a href="<?php echo base_url(); ?><?php echo $r['app_link']; ?>" class="nav-link"><i class="ni ni-<?php echo $r['icons']; ?> text-<?php echo $r['tekss']; ?>"></i>xx<?php echo $r['nama_submenu']; ?></a>
                  </li>
                  <?php } } ?>
                </ul>
              </div>
            </li>
		  <?php } ?>
        
		  
          </ul>
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading 
          <h6 class="navbar-heading p-0 text-muted">Documentation</h6>
          
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html" target="_blank">
                <i class="ni ni-spaceship"></i>
                <span class="nav-link-text">Getting started</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html" target="_blank">
                <i class="ni ni-palette"></i>
                <span class="nav-link-text">Foundation</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html" target="_blank">
                <i class="ni ni-ui-04"></i>
                <span class="nav-link-text">Components</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/plugins/charts.html" target="_blank">
                <i class="ni ni-chart-pie-35"></i>
                <span class="nav-link-text">Plugins</span>
              </a>
            </li>-->
          </ul>
        </div>
      </div>
    </div>
  </nav>