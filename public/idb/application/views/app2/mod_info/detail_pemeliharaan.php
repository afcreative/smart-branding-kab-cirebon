<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" >
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Detail Pemeliharan barang</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Jenis Barang</th>
						<th>Jumlah</th>
						<th>Sumber Dana</th>
						<th>Tahun Anggaran</th>
						<th>No Register</th>
						<th>Kategori Barang</th>
						<th>Baik</th>
						<th>Rusak</th>
						<th>Rusak Berat</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
				$x=explode('-',$r['tahun_pengadaan']);
				$B=$this->db->query("Select sum(jml_barang)as tot_b From pemeliharaan where id_detail='".$r['id_detail']."' and status='B' ")->row_array();
				$R=$this->db->query("Select sum(jml_barang)as tot_b From pemeliharaan where id_detail='".$r['id_detail']."' and status='R' ")->row_array();
				$RB=$this->db->query("Select sum(jml_barang)as tot_b From pemeliharaan where id_detail='".$r['id_detail']."' and status='RB' ")->row_array();				
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['jumlah']; ?></td>
						<td><?php echo $r['nama_dana']; ?></td>
						<td><?php echo $x[0]; ?></td>
						<td><?php echo $r['no_register']; ?></td>
						<td><?php echo $r['ket']; ?></td>
						<td><?php echo $B['tot_b']; ?></td>
						<td><?php echo $R['tot_b']; ?></td>
						<td><?php echo $RB['tot_b']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pengadaan<?php echo "$r[id_detail]"; ?>">
                          <i class="ni ni-controller" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_pemeliharaan/$r[id_detail]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data pemeliharaan ini?')\">
                            <i class='fa fa-trash'></i></td>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/pemeliharaan/<?php echo $r['id_detail']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="pengadaan<?php echo $r['id_detail']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Pemeliharaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					<input type="hidden" name="id_barang" required="required" class="form-control" value="<?php echo $r['id_barang']; ?>">
                    
					  <div class="form-group">
                        <label>Nama barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang44' disabled>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($barang as $row) {
                       if ($r['id_barang']==$row['id_barang']){
                            echo "<option value='$row[id_barang]' selected>$row[nama_barang]</option>";
                            }else{
                            echo "<option value='$row[id_barang]'>$row[nama_barang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Status barang</label>
						<select class="form-control" name="status" required>
						<option value="B">Baik</option>
						<option value="R">Rusak</option>
						<option value="RB">Rusak Berat</option>
						</select>
						</div>
					  
					  
					  <div class="form-group">
                        <label>Jumlah </label>
                        <input type="text" name="jumlah" required="required" class="form-control" value="<?php echo $r['jumlah']; ?>">
                      </div>
					   
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>