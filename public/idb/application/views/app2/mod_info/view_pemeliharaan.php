<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 >
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Pemeliharaan Barang</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Register Barang</th>
						<th>Nama Barang</th>
						<th>Tahun Pengadaan</th>
						<th>Ruang</th>
						<th>Gambar</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
				
				if($r['foto_barang']==''){
						$gambar="blank.png";
					}else{
						$gambar=$r['foto_barang'];
					}
					if($r['id_ruang']=='0'){
						$ruang="Belum ditempatkan";
					}else{
						$rb=$this->db->query("Select * From ruang where id_ruang='".$r['id_ruang']."'")->row_array();
						$ruang=$rb['nama_ruang'];
					}
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						
						<td><?php echo $r['no_register']; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['tahun_pengadaan']; ?></td>
						<td><?php echo $ruang; ?></td>
						<td><span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/foto_barang/<?php echo $gambar; ?>">
                  </span></td><td>
						  
					<?php	echo"
					<a href='".base_url()."app/detail_pemeliharaan/$r[id_barang]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Pemeliharaan barang item ini?')\">
                            <i class='ni ni-shop'></i></a></td>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  