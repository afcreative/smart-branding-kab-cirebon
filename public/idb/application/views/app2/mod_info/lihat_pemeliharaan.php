<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><a href="<?php echo base_url();?>app/pemeliharaan" button type="button" class="btn btn-outline-info" 
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Pemeliharaan</span></button></a></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Tanggal pemeliharaan</th>
						<th>Nama Barang</th>
						<th>Status</th>
						<th>Jumlah</th>
						<th>Pengguna</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['tgl_pemeliharaan']; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['status']; ?></td>
						<td><?php echo $r['jml_barang']; ?></td>
						<td><?php echo $r['nama_lengkap']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#kategori<?php echo "$r[id_pemeliharaan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/delete_pemeliharaan/$r[id_pemeliharaan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/kategori" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     <div class="form-group">
                        <label>Kategori Utama</label>
                        <?php
						$kategori =$this->db->query("Select * From main ")->result_array();
						echo"
						<select class='form-control' name='id_main' required>
                    <option value=''>- Pilih Kategori Utama -</option>";
                    foreach ($kategori as $row) {
                       if ($r['id_main']==$row['id_main']){
                            echo "<option value='$row[id_main]' selected>$row[nama_main]</option>";
                            }else{
                            echo "<option value='$row[id_main]'>$row[nama_main]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Kategori </label>
                        <input type="text" name="nama_kategori" required="required" class="form-control" placeholder="Nama Kategori ..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_pemeliharaan/<?php echo $r['id_pemeliharaan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="kategori<?php echo $r['id_pemeliharaan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Pemeliharaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					<input type="hidden" name="id_barang" required="required" class="form-control" value="<?php echo $r['id_pemeliharaan']; ?>">
                    
					  <div class="form-group">
                        <label>Nama barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang44' disabled>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($barang as $row) {
                       if ($r['id_barang']==$row['id_barang']){
                            echo "<option value='$row[id_barang]' selected>$row[nama_barang]</option>";
                            }else{
                            echo "<option value='$row[id_barang]'>$row[nama_barang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Status barang</label>
						<select class="form-control" name="status" required>
						<?php if($r['status']=="B"){ ?>
						<option value="B" selected>Baik</option>
						<option value="R">Rusak</option>
						<option value="RB">Rusak Berat</option>
						
						<?php } elseif($r['status']=="R"){?>
						<option value="B" >Baik</option>
						<option value="R" selected>Rusak</option>
						<option value="RB">Rusak Berat</option>
						<?php } else {?>
						<option value="B" >Baik</option>
						<option value="R" >Rusak</option>
						<option value="RB" selected>Rusak Berat</option>
						<?php } ?>
					  </select>
						</div>
					  <div class="form-group">
                        <label>Jumlah </label>
                        <input type="text" name="jumlah" required="required" class="form-control" value="<?php echo $r['jml_barang']; ?>">
                      </div>
					   
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>