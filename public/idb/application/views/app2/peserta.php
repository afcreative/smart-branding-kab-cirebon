
<!-- =========================================================
* Argon Dashboard PRO v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 -->
 <?php 
 $id_peserta=$this->session->id_peserta;
 
 $rsis=$this->db->query("Select * From peserta where id_peserta='$id_peserta'")->row_array();
 if($rsis['foto_peserta']==''){
						$gambar="blank.png";
					}else{
						$gambar=$rsis['foto_peserta'];
					}
	$id_ukk=$rsis['id_ukk'];
	$rw=$this->db->query("Select * From ukk a, sekolah b, kompetensi c where a.id_sekolah=b.id_sekolah and a.id_kompetensi=c.id_kompetensi and a.id_ukk='$id_ukk'")->row_array();				
 ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>UKK PT. Jagat & Mitra SMK </title>
  <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url(); ?>assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/argon.css?v=1.1.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
    <!-- Header -->
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 500px; background-image: url(<?php echo base_url(); ?>assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask bg-gradient-default opacity-8"></span>
      <!-- Header container -->
      <div class="container-fluid d-flex align-items-center">
        <div class="row">
          <div class="col-lg-7 col-md-10">
            <h1 class="display-2 text-white">Hello <?php echo $rsis['nama_peserta']; ?></h1>
            <p class="text-white mt-0 mb-5">Terima kasih telah menggunakan halaman ini.. untuk memperdalam dibidang IT kunjungi <a href="http://jagatedu.online" target="_blank"> LINK ini </a></p>
            <a href="<?php echo base_url(); ?>app/logout" class="btn btn-neutral">Keluar</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">
            <img src="<?php echo base_url(); ?>assets/img/theme/img-1-1000x600.jpg" alt="Image placeholder" class="card-img-top">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    
					   <img src="<?php echo base_url(); ?>assets/foto_peserta/<?php echo $gambar; ?>" class="rounded-circle">
                 
                  </a>
                </div>
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
                <a href="<?php echo base_url(); ?>app/cetak_ukk/<?php echo $rsis['id_peserta']; ?>" class="btn btn-sm btn-info mr-4" target="_blank">Cetak lap</a>
                <a href="#" class="btn btn-sm btn-default float-right">Kirim Lap</a>
              </div>
            </div>
            <div class="card-body pt-0">
              
              <div class="text-center">
                <h5 class="h3">
                  <?php echo $rsis['nama_peserta']; ?><span class="font-weight-light">, <?php echo $rsis['id_ukk']; ?></span>
                </h5>
                <div class="h5 font-weight-300">
                  <i class="ni location_pin mr-2"></i><?php echo $rsis['tempat_lahir']; ?>, <?php echo $rsis['tgl_lahir']; ?>
                </div>
                <div class="h5 mt-4">
                  <i class="ni business_briefcase-24 mr-2"></i><?php echo $rw['nama_kompetensi']; ?>
                </div>
                <div>
                  <i class="ni education_hat mr-2"></i><?php echo $rw['nama_sekolah']; ?>
                </div>
              </div>
            </div>
          </div>
          <!-- Progress track -->
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <!-- Title -->
              <h5 class="h3 mb-0">Detail Nilai</h5>
            </div>
            <!-- Card body -->
            <div class="card-body">
              <!-- List group -->
              <ul class="list-group list-group-flush list my--3">
			  <?php
			  $detail=$this->db->query("Select * From detail_nilai where id_ukk='".$rw['id_ukk']."' and id_peserta='".$rsis['id_peserta']."'");
			  
			  $no=1;
				foreach ($detail->result_array() as $det){ 
				$n=$this->db->query("Select * From examp where id_examp='".$det['id_examp']."'")->row_array();
				$nilai=$det['nilai'];
				$pros=($det['nilai']/$n['nilai'])*100;
				?>
                <li class="list-group-item px-0">
                  <div class="row align-items-center">
                    <div class="col-auto">
                      <!-- Avatar -->
                      <a href="#" class="avatar rounded-circle">
                        <?php echo $no; ?>
                      </a>
                    </div>
                    <div class="col">
                      <h5><?php echo $det['nilai']; ?></h5>
                      <div class="progress progress-xs mb-0">
                        <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="6" aria-valuemin="0" aria-valuemax="10" style="width: <?php echo $pros; ?>%;"></div>
                      </div>
                    </div>
                  </div>
                </li>
                <?php $no++; } ?>
                
                
                
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="row">
            <div class="col-lg-6">
              <div class="card bg-gradient-info border-0">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">Total Penilaian</h5>
                      <span class="h2 font-weight-bold mb-0 text-white">
					  <?php $dt=$this->db->query("Select sum(nilai) as tot From detail_nilai where id_ukk='".$rsis['id_ukk']."' and id_peserta='".$rsis['id_peserta']."'")->row_array(); 
					  
					  $dtexamp=$this->db->query("Select sum(nilai) as totex From examp where id_ukk='".$rsis['id_ukk']."' ")->row_array(); 
					  
					  $pronilai=$dt['tot']/$dtexamp['totex'] * 100;
					  echo $dt['tot']; ?>
					  
					  </span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-white mr-2"><i class="fa fa-arrow-up"></i> <?php echo $pronilai; ?>%</span>
                    <span class="text-nowrap text-light">Keahlian</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card bg-gradient-danger border-0">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0 text-white">Performance</h5>
					  <?php 
					  if($pronilai > 80 ){
						  $grade="Baik";
						  
					  }elseif($pronilai > 90 ){
						  $grade="Amat Baik";
						  
					  }else{
						  $grade="Perbaikan";
					  }
					  
					  ?>
                      <span class="h2 font-weight-bold mb-0 text-white"><?php echo $grade; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                        <i class="ni ni-spaceship"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-white mr-2"><i class="fa fa-arrow-up"></i> <?php echo $dt['tot']; ?></span>
                    <span class="text-nowrap text-light">Up Skill Anda</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Edit profile </h3>
                </div>
                
              </div>
            </div>
            <div class="card-body">
              <form action="<?php echo base_url(); ?>app/peserta" method="post"  enctype="multipart/form-data">
                <h6 class="heading-small text-muted mb-4">User information</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Nama Peserta</label>
                        <input type="text" name="nama_peserta" class="form-control" value="<?php echo $rsis['nama_peserta']; ?>">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Tempat Lahir</label>
                        <input type="text" name="tempat_lahir" value="<?php echo $rsis['tempat_lahir']; ?>" class="form-control" >
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Tanggal lahir</label>
                        <input type="text" name="tgl_lahir" value="<?php echo $rsis['tgl_lahir']; ?>"  class="form-control" >
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">NIS Siswa</label>
                        <input type="text" name="nis" value="<?php echo $rsis['nis']; ?>" class="form-control" >
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Catatan Sidang</h6>
                
                <div class="pl-lg-4">
                  <div class="form-group">
                    
                    <textarea rows="4" class="form-control" placeholder="Catatan  ukk ..."><?php echo $rsis['keterangan']; ?></textarea>
                  </div>
                </div>

				<button type="submit" name="submit" class="btn btn-success">Update</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center text-lg-left text-muted">
              &copy; 2019 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/license" class="nav-link" target="_blank">License</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="<?php echo base_url(); ?>assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url(); ?>assets/js/argon.js?v=1.1.0"></script>
  <!-- Demo JS - remove this in your project -->
  <script src="<?php echo base_url(); ?>assets/js/demo.min.js"></script>
</body>

</html>