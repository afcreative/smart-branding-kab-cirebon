<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Gedung</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama Gedung</th>
						<th>Tahun Bantuan</th>
						<th>Letak</th>
						<th>Luas</th>
						<th>Keterangan</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['nama_gedung']; ?></td>
						<td><?php echo $r['tahun_bantuan']; ?></td>
						<td><?php echo $r['letak']; ?></td>
						<td><?php echo $r['luas']; ?></td>
						<td><?php echo $r['keterangan']; ?></td>
						  <td>
						  <?php	echo"
					<a href='".base_url()."app/ruang/$r[id_gedung]'><button type='button' class='btn btn-danger btn-sm' onclick=\"return confirm('Apa anda ingin manambah detail ruang gedung')\">
                            <i class='fa fa-eye'></i>"; ?>
						  
						  
						  <button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#gedung<?php echo "$r[id_gedung]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_gedung/$r[id_gedung]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/gedung" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Gedung</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     
					  <div class="form-group">
                        <label>Nama Gedung </label>
                        <input type="text" name="nama_gedung" required="required" class="form-control" placeholder="Nama Gedung ..">
                      </div>
					  <div class="form-group">
                        <label>Tahun bantuan </label>
                        <input type="text" name="tahun_bantuan" required="required" class="form-control" placeholder="Tahun bantuan ..">
                      </div>
					  <div class="form-group">
                        <label>Letak</label>
                        <input type="text" name="letak" required="required" class="form-control" placeholder="Letak Gedung ..">
                      </div>
					  <div class="form-group">
                        <label>Luas </label>
                        <input type="text" name="luas" required="required" class="form-control" placeholder="Luas Gedung ..">
                      </div>
					  <div class="form-group">
                        <label>Keterangan </label>
                        <textarea name="keterangan" required="required" class="form-control" ></textarea>
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_gedung/<?php echo $r['id_gedung']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="gedung<?php echo $r['id_gedung']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit gedung</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					  <div class="form-group">
                        <label>Nama Gedung </label>
                        <input type="text" name="nama_gedung" value="<?php echo $r['nama_gedung']; ?>" required="required" class="form-control">
                      </div>
                      
					  <div class="form-group">
                        <label>Tahun bantuan </label>
                        <input type="text" name="tahun_bantuan" value="<?php echo $r['tahun_bantuan']; ?>" required="required" class="form-control" placeholder="Tahun bantuan ..">
                      </div>
					  <div class="form-group">
                        <label>Letak</label>
                        <input type="text" name="letak" value="<?php echo $r['letak']; ?>" required="required" class="form-control" placeholder="Letak Gedung ..">
                      </div>
					  <div class="form-group">
                        <label>Luas </label>
                        <input type="text" name="luas" value="<?php echo $r['luas']; ?>" required="required" class="form-control" placeholder="Luas Gedung ..">
                      </div>
					  <div class="form-group">
                        <label>Keterangan </label>
                        <textarea name="keterangan" required="required" class="form-control" > <?php echo $r['keterangan']; ?></textarea>
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>