<!DOCTYPE html>
 <?php 
 
 
 $rsis=$this->db->query("Select * From peserta where id_peserta='$id_peserta'")->row_array();
 if($rsis['foto_peserta']==''){
						$gambar="blank.png";
					}else{
						$gambar=$rsis['foto_peserta'];
					}
	$id_ukk=$rsis['id_ukk'];
	$rw=$this->db->query("Select * From ukk a, sekolah b, kompetensi c where a.id_sekolah=b.id_sekolah and a.id_kompetensi=c.id_kompetensi and a.id_ukk='$id_ukk'")->row_array();				
 ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan UKK SMK & PT. Jagat Digital</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
	
<style>
    @page { size: A4 }
  
    h1 {
        font-weight: bold;
        font-size: 20pt;
        text-align: center;
    }
	img{
		width:20%;
	}
	
	.logo{
		
		text-align: center;
	}
	h3 {
        font-weight: bold;
        font-size: 14pt;
        text-align: center;
    }
	p{
		margin-left:450px;
	}
	.capbawah{
		position: relative;
		z-index: 1;
		top: 0px;
		width:20%;
		margin-left:500px;
	}
	p12{
		position: relative;
	margin-left:520px;	
	z-index: 2;
	}
	.cap{
		margin-left:450px;
		text-align: center;
		
	}
  
    table {
        border-collapse: collapse;
        width: 100%;
    }
	identitas {
        border-collapse: collapse;
        width: 100%;
		text-align: left;
    }
	.identitas th {
        padding: 8px 8px;
        text-align: left;
    }
	.identitas td {
        padding: 8px 8px;
        text-align: left;
    }
    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
  
    .table td {
        padding: 3px 3px;
        border:1px solid #000000;
    }
  
    .text-center {
        text-align: center;
    }
</style>
</head>
<body class="A4"  onload="window.print()">
    <section class="sheet padding-10mm">
	<div class="logo">
	<img src="<?php echo base_url(); ?>assets/jagat.jpeg">
	</div>
        <h1>PT. JAVA GENIUS ALL TECHNOLOGY</h1>
		<h3>Connect Your Business Digital</h3>
		<hr />
		<table class="identitas">
		
                <tr>
                    <th width="100px">Nama </th>
                    <th>: <?php echo  $rsis['nama_peserta'];?></th>
                </tr>
            
			<tr>
                    <td width="100px">Nis </td>
                    <td>: <?php echo $rsis['nis'];?>/<?php 
					
					echo date('Y'). $rsis['id_peserta'];?></td>
                </tr>
				<tr>
                    <td width="100px">Kompetensi </td>
                    <td>: <?php echo $rw['nama_kompetensi'];?></td>
                </tr>
				<tr>
                    <td width="100px">Sekolah </td>
                    <td>: <?php echo $rw['nama_sekolah'];?></td>
                </tr>
		</table>
        <table class="table">
            <thead>
                <tr >
                    <th >NO.</th>
                    <th>INDIKATOR</th>
                    <th>NILAI</th>
					<th>KET</th>
                </tr>
            </thead>
            <tbody>
			<?php 
			$no=1;
			$tot=0;
			$detail = $this->db->query("Select a.nilai, b.indikator From detail_nilai a, examp b where a.id_examp=b.id_examp and a. id_peserta='".$rsis['id_peserta']."' order by b.id_examp ASC");
			foreach ($detail->result_array() as $r){
				if ($r['nilai'] >= 9 ){
					$ket="Sangat Baik";
				}else if ($r['nilai'] >= 8 ){
					$ket="Baik";
				}else{
					$ket="Perbaikan";
				}
				$tot=$tot+$r['nilai'];
				
				if ($tot >= 91 ){
					$pesan="Sangat Kompeten";
				}else if ($tot >= 80 ){
					$pesan="Kompeten";
				}else if ($tot >= 70 ){
					$pesan="Kurang Kompeten";
				}else{
					$pesan="Tidak Kompeten";
				}
				?>
                <tr>
                    <td class="text-center" width="20"><?php echo $no; ?></td>
                    <td><?php echo $r['indikator']; ?></td>
                    <td align="center"><?php echo $r['nilai']; ?></td>
					<td><?php echo $ket; ?></td>
                </tr>
            <?php $no++; } ?>
			<tr>
                   <td colspan="2">Total Skor</td>
                    <td align="center" ><?php echo $tot; ?></td>
					<td ><?php echo $pesan; ?></td>
                </tr>	
			<tr>
                   
                    <td align="center" colspan="4"><?php echo $rsis['keterangan']; ?></td>
                </tr>			
            </tbody>
        </table>
		<p>Cirebon, <?php echo date('d F Y'); ?></p>
		<div class="cap">Assesor PT. Jagat Digital
		
		
		
		</div>
		<img src="<?php echo base_url(); ?>assets/cap.png" class="capbawah">
		
		<p12><B><?php 
		$c=$this->db->query("Select * From peserta a, penguji b where a.id_penguji=b.id_penguji")->row_array();
		echo $c['nama_lengkap']; ?></B></p12>
		
		
		</p1>
    </section>
</body>
</html>