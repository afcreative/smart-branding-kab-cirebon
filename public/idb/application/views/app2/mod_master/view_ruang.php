<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Ruang</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama Gedung</th>
						<th>Nama Ruang</th>
						<th>Peruntukan</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['nama_gedung']; ?></td>
						<td><?php echo $r['nama_ruang']; ?></td>
						<td><?php echo $r['peruntukan']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ruang<?php echo "$r[id_ruang]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_ruang/$r[id_ruang]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/ruang" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Ruang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					<input type="text" name="id_gedung" value="<?php echo $id_gedung; ?>" required="required" class="form-control" placeholder="Nama Ruang ..">
                     
					  <div class="form-group">
                        <label>Nama Ruang </label>
                        <input type="text" name="nama_ruang" required="required" class="form-control" placeholder="Nama Ruang ..">
                      </div>
					  <div class="form-group">
                        <label>Peruntukan </label>
                        <input type="text" name="peruntukan" required="required" class="form-control" placeholder="Peruntukan ..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_ruang/<?php echo $r['id_ruang']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="ruang<?php echo $r['id_ruang']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit ruang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					<input type="text" name="id_gedung" value="<?php echo $id_gedung; ?>" required="required" class="form-control" placeholder="Nama Ruang ..">
                    
					  <div class="form-group">
                        <label>Nama Ruang </label>
                        <input type="text" name="nama_ruang" value="<?php echo $r['nama_ruang']; ?>" required="required" class="form-control">
                      </div>
                       <div class="form-group">
                        <label>Peruntukan </label>
                        <input type="text" name="peruntukan" value="<?php echo $r['peruntukan']; ?>" required="required" class="form-control" placeholder="Peruntukan ..">
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>