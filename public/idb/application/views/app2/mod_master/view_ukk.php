<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah UKK</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama sekolah</th>
						<th>Nama Kompetensi</th>
						<th>Tahun</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['nama_sekolah']; ?></td>
						<td><?php echo $r['nama_kompetensi']; ?></td>
						<td><?php echo $r['tahun']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#ukk<?php echo "$r[id_ukk]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_ukk/$r[id_ukk]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/ukk" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah UKK</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div class="form-group">
                        <label>Nama sekolah</label>
                        <?php
						$sekolah =$this->db->query("Select * From sekolah order by nama_sekolah ASC")->result_array();
						echo"
						<select class='form-control' name='id_sekolah' required>
                    <option value=''>- Pilih Sekolah -</option>";
                    foreach ($sekolah as $rows) {
                        echo "<option value='$rows[id_sekolah]'>$rows[nama_sekolah] </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Kompetensi</label>
                        <?php
						$kompetensi =$this->db->query("Select * From kompetensi order by nama_kompetensi ASC")->result_array();
						echo"
						<select class='form-control' name='id_kompetensi' required>
                    <option value=''>- Pilih kompetensi -</option>";
                    foreach ($kompetensi as $rows) {
                        echo "<option value='$rows[id_kompetensi]'>$rows[nama_kompetensi] </option>";
                    }
                echo "</select>
						";
						?>
                      </div> 
					  <div class="form-group">
                        <label>Tahun Pelaksanaan</label>
						<input type="text" name="tahun" required="required" class="form-control" placeholder="Tahun Pelaksanaan..">
						</div>                  

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_ukk/<?php echo $r['id_ukk']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="ukk<?php echo $r['id_ukk']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit UKK</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                        <label>Sekolah</label>
                        <?php
						$sekolah =$this->db->query("Select * From sekolah order by nama_sekolah ASC")->result_array();
						echo"
						<select name='id_sekolah' class='form-control' required>
                            <option value='' selected>- Pilih sekolah -</option>";
                                foreach ($sekolah as $row){
                                    if ($r['id_sekolah']==$row['id_sekolah']){
                                        echo "<option value='$row[id_sekolah]' selected>$row[nama_sekolah]</option>";
                                            }else{
                                                echo "<option value='$row[id_sekolah]'>$row[nama_sekolah]</option>";
                                                }
                                }
                    echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Kompetensi</label>
                        <?php
						$kompetensi =$this->db->query("Select * From kompetensi order by nama_kompetensi ASC")->result_array();
						echo"
						<select name='id_kompetensi' class='form-control' required>
                            <option value='' selected>- Pilih Bank -</option>";
                                foreach ($kompetensi as $row){
                                    if ($r['id_kompetensi']==$row['id_kompetensi']){
                                        echo "<option value='$row[id_kompetensi]' selected>$row[nama_kompetensi]</option>";
                                            }else{
                                                echo "<option value='$row[id_kompetensi]'>$row[nama_kompetensi]</option>";
                                                }
                                }
                    echo "</select>
						";
						?>
                      </div>
                      <div class="form-group">
                        <label>Tahun Pelaksanaan</label>
						<input type="text" name="tahun" value="<?php echo $r['tahun']; ?>" required="required" class="form-control">
						</div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>