<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Kategori</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Kategori Utama</th>
						<th>Nama Kategori</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_main']; ?></td>
						<td><?php echo $r['nama_kategori']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#kategori<?php echo "$r[id_kategori]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_kategori/$r[id_kategori]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/kategori" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     <div class="form-group">
                        <label>Kategori Utama</label>
                        <?php
						$kategori =$this->db->query("Select * From main ")->result_array();
						echo"
						<select class='form-control' name='id_main' required>
                    <option value=''>- Pilih Kategori Utama -</option>";
                    foreach ($kategori as $row) {
                       if ($r['id_main']==$row['id_main']){
                            echo "<option value='$row[id_main]' selected>$row[nama_main]</option>";
                            }else{
                            echo "<option value='$row[id_main]'>$row[nama_main]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Kategori </label>
                        <input type="text" name="nama_kategori" required="required" class="form-control" placeholder="Nama Kategori ..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_kategori/<?php echo $r['id_kategori']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="kategori<?php echo $r['id_kategori']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit kategori</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    <div class="form-group">
                        <label>Kategori Utama</label>
                        <?php
						$kategori =$this->db->query("Select * From main ")->result_array();
						echo"
						<select class='form-control' name='id_main' required>
                    <option value=''>- Pilih Kategori Utama -</option>";
                    foreach ($kategori as $row) {
                       if ($r['id_main']==$row['id_main']){
                            echo "<option value='$row[id_main]' selected>$row[nama_main]</option>";
                            }else{
                            echo "<option value='$row[id_main]'>$row[nama_main]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Kategori </label>
                        <input type="text" name="nama_kategori" value="<?php echo $r['nama_kategori']; ?>" required="required" class="form-control">
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>