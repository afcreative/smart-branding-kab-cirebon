<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Data Siswa</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama Jurusan</th>
						<th>Tingkat</th>
						<th>Laki-laki</th>
						<th>Perempuan</th>
						<th>Total</th>
						
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					$total=$r['perempuan'] + $r['laki_laki'];
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['nama_kompetensi']; ?></td>
						<td><?php echo $r['nama_tingkat']; ?></td>
						<td><?php echo $r['laki_laki']; ?></td>
						<td><?php echo $r['perempuan']; ?></td>
						<td><?php echo rupiah($total); ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#jurusan<?php echo "$r[id_jurusan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_jurusan/$r[id_jurusan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/jurusan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Data Siswa jurusan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     
					  
					  <div class="form-group">
                        <label>Nama Jurusan/Kompetensi</label>
                        <?php
						$kompetensi =$this->db->query("Select * From kompetensi ")->result_array();
						echo"
						<select class='form-control' name='id_kompetensi' required>
                    <option value=''>- Pilih kompetensi -</option>";
                    foreach ($kompetensi as $rows) {
                        echo "<option value='$rows[id_kompetensi]'>$rows[nama_kompetensi] </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Tingkat / Kelas</label>
                        <?php
						$tingkat =$this->db->query("Select * From tingkat ")->result_array();
						echo"
						<select class='form-control' name='id_tingkat' required>
                    <option value=''>- Pilih kompetensi -</option>";
                    foreach ($tingkat as $rows) {
                        echo "<option value='$rows[id_tingkat]'>$rows[nama_tingkat] </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Jumlah siswa laki-laki</label>
                        <input type="text" name="laki_laki" class="form-control" placeholder="Jumlah Siswa laki-laki ..">
                      </div>
					  <div class="form-group">
                        <label>Jumlah siswa perempuan</label>
                        <input type="text" name="perempuan" class="form-control" placeholder="Jumlah Siswa perempuan..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_jurusan/<?php echo $r['id_jurusan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="jurusan<?php echo $r['id_jurusan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Data siswa jurusan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					
					  <div class="form-group">
                        <label>Nama Jurusan/Kompetensi</label>
                        <?php
						$kompetensi =$this->db->query("Select * From kompetensi ")->result_array();
						echo"
						<select class='form-control' name='id_kompetensi' required>
                    <option value=''>- Pilih kompetensi -</option>";
                    foreach ($kompetensi as $rows) {
                        if ($r['id_kompetensi']==$rows['id_kompetensi']){
                            echo "<option value='$rows[id_kompetensi]' selected>$rows[nama_kompetensi]</option>";
                            }else{
                            echo "<option value='$rows[id_kompetensi]'>$rows[nama_kompetensi]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Tingkat / Kelas</label>
                        <?php
						$tingkat =$this->db->query("Select * From tingkat ")->result_array();
						echo"
						<select class='form-control' name='id_tingkat' required>
                    <option value=''>- Pilih kompetensi -</option>";
                    foreach ($tingkat as $rows) {
                        if ($r['id_tingkat']==$rows['id_tingkat']){
                            echo "<option value='$rows[id_tingkat]' selected>$rows[nama_tingkat]</option>";
                            }else{
                            echo "<option value='$rows[id_tingkat]'>$rows[nama_tingkat]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Jumlah siswa laki-laki</label>
                        <input type="text" name="laki_laki" value="<?php echo $r['laki_laki']; ?>" class="form-control" placeholder="Jumlah Siswa laki-laki ..">
                      </div>
					  <div class="form-group">
                        <label>Jumlah siswa perempuan</label>
                        <input type="text" name="perempuan" value="<?php echo $r['perempuan']; ?>" class="form-control" placeholder="Jumlah Siswa perempuan..">
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>