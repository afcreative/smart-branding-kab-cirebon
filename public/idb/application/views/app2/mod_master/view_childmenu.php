<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icons"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Child Menu</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table align-items-center table-flush"  id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Sub Menu</th>
						<th>Child Menu</th>
						<th>Link</th>
						<th>aktifs</th>
						<th>icons</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_submenu']; ?></td>
						<td><?php echo $r['nama_childmenu']; ?></td>
						<td><?php echo $r['app_link']; ?></td>
						<td><?php echo $r['aktifs']; ?></td>
						<td><i class="ni ni-<?php echo $r['icons']; ?> text-<?php echo $r['tekss']; ?>" style="font-size: 25px;"></i></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#submenu<?php echo "$r[id_childmenu]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."barang/hapus_childmenu/$r[id_childmenu]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>barang/childmenu" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah submenu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Menu Utama </label>
                                    <?php
							$submenu =$this->db->query("Select * From submenu order by id_submenu ASC")->result_array();
						echo"
						<select name='id_submenu' class='form-control' required>
                            <option value='' selected>- Pilih Sub Menu  -</option>";
                                foreach ($submenu as $row){
                                    if ($r['id_submenu']==$row['id_submenu']){
                                        echo "<option value='$row[id_submenu]' selected>$row[nama_submenu]</option>";
                                            }else{
                                                echo "<option value='$row[id_submenu]'>$row[nama_submenu]</option>";
                                                }
                                }
                    echo "</select>
						";
						?>
                                </div>
					  <div class="form-group">
                        <label>Nama Child menu </label>
                        <input type="text" name="nama_childmenu" required="required" class="form-control" placeholder="Nama childmenu ..">
                      </div>
					  <div class="form-group">
                                    <label for="example-search-input" class="form-control-label">Link App</label>
                                    <input class="form-control" type="text" name="app_link" placeholder="Link App">
                                </div>
					  <div class="form-group">
                        <label>icons </label>
                        <input type="text" name="icons" required="required" class="form-control" placeholder="icons submenu .."><br> <a href="<?php echo base_url(); ?>app/icon" target="_blank" >Lihat icons</a>
                      </div>
								<div class="form-group">
                                    <label for="exampleFormControlSelect2">aktifs</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="aktifs">
                                        <option value="Y">aktifs</option>
                                        <option value="N">Non aktifs</option>
                                    </select>
                                </div>
								<div class="form-group">
                                    <label for="exampleFormControlSelect2">Warna tekss</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="tekss">
                                        <option value="primary">primary</option>
                                        <option value="orange">orange</option>
										<option value="info">info</option>
										<option value="default">default</option>
                                    </select>
                                </div>
								
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>barang/edit_childmenu/<?php echo $r['id_childmenu']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="submenu<?php echo $r['id_childmenu']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit submenu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					
                    <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Menu Utama </label>
                                    <?php
							$submenu =$this->db->query("Select * From submenu order by id_submenu ASC")->result_array();
						echo"
						<select name='id_submenu' class='form-control' required>
                            <option value='' selected>- Pilih Sub Menu  -</option>";
                                foreach ($submenu as $row){
                                    if ($r['id_submenu']==$row['id_submenu']){
                                        echo "<option value='$row[id_submenu]' selected>$row[nama_submenu]</option>";
                                            }else{
                                                echo "<option value='$row[id_submenu]'>$row[nama_submenu]</option>";
                                                }
                                }
                    echo "</select>
						";
						?>
                                </div>
					  <div class="form-group">
                        <label>Nama Child Menu </label>
                        <input type="text" name="nama_childmenu" value="<?php echo $r['nama_childmenu']; ?>" required="required" class="form-control" placeholder="Nama Child Menu ..">
                      </div>
					  <div class="form-group">
                                    <label for="example-search-input" class="form-control-label">Link App</label>
                                    <input class="form-control" type="text" name="app_link"  value="<?php echo $r['app_link']; ?>" placeholder="Link App">
                                </div>
					  <div class="form-group">
                        <label>icons </label>
                        <input type="text" name="icons" value="<?php echo $r['icons']; ?>"  required="required" class="form-control" placeholder="icons childmenu .."><br> <a href="<?php echo base_url(); ?>app/icon" target="_blank" >Lihat icons</a>
                      </div>
								<div class="form-group">
                                    <label for="exampleFormControlSelect2">Level akses</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="aktifs">
									<?php if($r['aktifs']=="Y") { ?>
                                        <option value="Y" selected>aktifs</option>
                                        <option value="N">Non aktifs</option>
									<?php } else{ ?>
									<option value="Y" >aktifs</option>
                                        <option value="N" selected>Non aktifs</option>
									<?php } ?>
                                    </select>
									</div>
								<div class="form-group">
                                    <label for="exampleFormControlSelect2">Warna tekss</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="tekss">
                                        <option value="primary">primary</option>
                                        <option value="orange">orange</option>
										<option value="info">info</option>
										<option value="default">default</option>
                                    </select>
                                </div>
								

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>
			 
			 
			 