<div class="row">
        <div class="col-xl-4">
          <div class="card bg">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-light text-uppercase ls-1 mb-1">Demografi Siswa Jurusan/Tingkat</h6>
                  <h5 class="h3 text-white mb-0">Visitors value</h5>
                </div>
                <div class="col">
                  
                </div>
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              
                <canvas id="myChart" width="30" height="30"></canvas><!-- Chart wrapper -->
              
              
            </div>
          </div>
        </div>
		<div class="col-xl-4">
          <div class="card bg">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-light text-uppercase ls-1 mb-1">Jumlah Siswa Laki-laki</h6>
                  
                </div>
                <div class="col">
                  
                </div>
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              
                <canvas id="jeniskelamin" width="50" height="50"></canvas><!-- Chart wrapper -->
              
              
            </div>
          </div>
        </div>
		
		<div class="col-xl-4">
          <div class="card bg">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-light text-uppercase ls-1 mb-1">Jumlah Siswa Laki-laki</h6>
                  
                </div>
                <div class="col">
                  
                </div>
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              
                <canvas id="perempuan" width="50" height="50"></canvas><!-- Chart wrapper -->
              
              
            </div>
          </div>
        </div>
      </div>
	<script>
            var ctx = document.getElementById("myChart");
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: [<?php 
					$jurusan=$this->db->query('Select * From jurusan a, kompetensi b, tingkat c where a.id_kompetensi=b.id_kompetensi and a.id_tingkat=c.id_tingkat order by a.id_jurusan ASC ')->result_array();
					foreach($jurusan as $b ) { 
					$ay=$b['kode_tingkat']."-".$b['nama_kompetensi'];
					echo '"' . $ay . '",'; }?>],
                    datasets: [{
                            label: '# Jumlah Siswa Perkompetensi',
                            data: [<?php 
							foreach($jurusan as $p ) { 
							$total=$p['laki_laki'] +$p['perempuan'] ;
							echo '"' . $total . '",';}?>],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>
		<script>
            var ctx = document.getElementById("jeniskelamin");
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [<?php 
					$jurusan=$this->db->query('Select * From jurusan a, kompetensi b, tingkat c where a.id_kompetensi=b.id_kompetensi and a.id_tingkat=c.id_tingkat order by a.id_jurusan ASC ')->result_array();
					foreach($jurusan as $b ) { 
					$ay=$b['kode_tingkat']."-".$b['nama_kompetensi'];
					echo '"' . $ay . '",'; }?>],
                    datasets: [{
                            label: '# Jumlah Siswa Perkompetensi',
                            data: [<?php 
							foreach($jurusan as $p ) { 
							$laki=$p['laki_laki']  ;
							$perempuan=$p['perempuan']  ;
							echo '"' . $laki . '",';}?>],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script> 
<script>
            var ctx = document.getElementById("perempuan");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: [<?php 
					$jurusan=$this->db->query('Select * From jurusan a, kompetensi b, tingkat c where a.id_kompetensi=b.id_kompetensi and a.id_tingkat=c.id_tingkat order by a.id_jurusan ASC ')->result_array();
					foreach($jurusan as $b ) { 
					$ay=$b['kode_tingkat']."-".$b['nama_kompetensi'];
					echo '"' . $ay . '",'; }?>],
                    datasets: [{
                            label: '# Jumlah Siswa Perkompetensi',
                            data: [<?php 
							foreach($jurusan as $p ) { 
							$laki=$p['laki_laki']  ;
							$perempuan=$p['perempuan']  ;
							echo '"' . $perempuan . '",';}?>],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>		