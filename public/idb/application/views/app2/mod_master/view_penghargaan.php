<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Penghargaan</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
					<th>Foto</th>
						<th>Kejuaraan</th>
						<th>Nama Penghargaan</th>
						<th>Tingkat/Juara</th>
						<th>Tahun</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/foto_penghargaan/<?php echo $r['foto']; ?>">
                  </span></td>
						<td><?php echo $r['nama_kejuaraan']; ?></td>
						<td><?php echo $r['nama_penghargaan']; ?></td>
						<td><?php echo $r['tingkat']; ?></td>
						<td><?php echo $r['tahun_penghargaan']; ?></td>
						
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#penghargaan<?php echo "$r[id_penghargaan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_penghargaan/$r[id_penghargaan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/penghargaan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Penghargaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     <div class="form-group">
                        <label>Kejuaraan</label>
                        <?php
						$penghargaan =$this->db->query("Select * From kejuaraan ")->result_array();
						echo"
						<select class='form-control' name='id_kejuaraan' required>
                    <option value=''>- Pilih Penghargaan Utama -</option>";
                    foreach ($penghargaan as $row) {
                       if ($r['id_kejuaraan']==$row['id_kejuaraan']){
                            echo "<option value='$row[id_kejuaraan]' selected>$row[nama_kejuaraan]</option>";
                            }else{
                            echo "<option value='$row[id_kejuaraan]'>$row[nama_kejuaraan]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Penghargaan </label>
                        <input type="text" name="nama_penghargaan" required="required" class="form-control" placeholder="Nama Penghargaan ..">
                      </div>
					  <div class="form-group">
                        <label>Tingkat </label>
                        <input type="text" name="tingkat" required="required" class="form-control" placeholder="Tingkat ..">
                      </div>
					  <div class="form-group">
                        <label>Tahun</label>
                        <input type="text" name="tahun_penghargaan" required="required" class="form-control" placeholder="Tahun ..">
                      </div>
					    <div class="form-group">
                        <label>Foto</label>
						
                  </span>
                        <input type="file" name="foto" class="form-control" >
                      </div>                  

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_penghargaan/<?php echo $r['id_penghargaan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="penghargaan<?php echo $r['id_penghargaan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit penghargaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    <div class="form-group">
                        <label>Penghargaan Utama</label>
                        <?php
						$penghargaan =$this->db->query("Select * From kejuaraan ")->result_array();
						echo"
						<select class='form-control' name='id_kejuaraan' required>
                    <option value=''>- Pilih Penghargaan Utama -</option>";
                    foreach ($penghargaan as $row) {
                       if ($r['id_kejuaraan']==$row['id_kejuaraan']){
                            echo "<option value='$row[id_kejuaraan]' selected>$row[nama_kejuaraan]</option>";
                            }else{
                            echo "<option value='$row[id_kejuaraan]'>$row[nama_kejuaraan]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Penghargaan </label>
                        <input type="text" name="nama_penghargaan" value="<?php echo $r['nama_penghargaan']; ?>" required="required" class="form-control">
                      </div>
					  <div class="form-group">
                        <label>Tingkat </label>
                        <input type="text" name="tingkat" value="<?php echo $r['tingkat']; ?>"  required="required" class="form-control" placeholder="Tingkat ..">
                      </div>
					  <div class="form-group">
                        <label>Tahun</label>
                        <input type="text" name="tahun_penghargaan" value="<?php echo $r['tahun_penghargaan']; ?>"  required="required" class="form-control" placeholder="Tahun ..">
                      </div>
                       <div class="form-group">
                        <label>Foto</label><br>
						<span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/foto_penghargaan/<?php echo $r['foto']; ?>">
                  </span>
                        <input type="file" name="foto" class="form-control" >
                      </div> 

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>