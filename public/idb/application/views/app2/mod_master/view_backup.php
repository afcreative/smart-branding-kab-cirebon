<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><a href="<?php echo base_url(); ?>app/backupdb" button type="button" class="btn btn-outline-info" >
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Update Backup</span></button></a></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Backup</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
								<?php
	$dir='./backup/';
	$no=1;
	$dh=opendir($dir) or die('error');
	while(($f=readdir($dh)) !== false){
		if(is_file($dir.$f)){ ?>
			<tr><td><?php echo $no; ?></td>
			<td><?php echo  $f ; ?> </td>
			<td><a href="../backup/<?php echo   $f ; ?>" button title="Download" type="button" class="btn btn-info btn-sm">
                          <i class="fa fa-download" ></i>
                        </button></a>
						<?php	echo"
					<a href='".base_url()."app/hapusbackup/$f'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>"?>
			
						</td></tr>
		<?php } $no++;
	}
	closedir($dh);
?>
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  