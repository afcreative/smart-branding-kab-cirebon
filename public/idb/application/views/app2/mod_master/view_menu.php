<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Menu</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table align-items-center table-flush"  id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama Menu</th>
						<th>Aktif</th>
						<th>Icon</th>
						<th>Urutan</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['nama_menu']; ?></td>
						<td><?php echo $r['aktif']; ?></td>
						<td><i class="ni ni-<?php echo $r['icon']; ?> text-<?php echo $r['teks']; ?>" style="font-size: 25px;"></i></td>
						<td><?php echo $r['urutan']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#menu<?php echo "$r[id_menu]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_menu/$r[id_menu]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/menu" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah menu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                     
					  <div class="form-group">
                        <label>Nama menu </label>
                        <input type="text" name="nama_menu" required="required" class="form-control" placeholder="Nama menu ..">
                      </div>
					  <div class="form-group">
                        <label>Icon </label>
                        <input type="text" name="icon" required="required" class="form-control" placeholder="Icon menu .."><br> <a href="<?php echo base_url(); ?>app/icon" target="_blank" >Lihat icon</a>
                      </div>
								<div class="form-group">
                                    <label for="exampleFormControlSelect2">Aktif</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="aktif">
                                        <option value="Y">Aktif</option>
                                        <option value="N">Non Aktif</option>
                                    </select>
                                </div>
								<div class="form-group">
                                    <label for="exampleFormControlSelect2">Warna Teks</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="teks">
                                        <option value="primary">primary</option>
                                        <option value="orange">orange</option>
										<option value="info">info</option>
										<option value="default">default</option>
                                    </select>
                                </div>
								<div class="form-group">
                                    <label for="example-search-input" class="form-control-label">Urutan menu</label>
                                    <input class="form-control" type="text" name="urutan" placeholder="Urutan menu">
                                </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_menu/<?php echo $r['id_menu']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="menu<?php echo $r['id_menu']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit menu</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
					
                    
					  <div class="form-group">
                        <label>Nama menu </label>
                        <input type="text" name="nama_menu" value="<?php echo $r['nama_menu']; ?>" required="required" class="form-control" placeholder="Nama menu ..">
                      </div>
					  <div class="form-group">
                        <label>Icon </label>
                        <input type="text" name="icon" value="<?php echo $r['icon']; ?>"  required="required" class="form-control" placeholder="Icon menu .."><br> <a href="<?php echo base_url(); ?>app/icon" target="_blank" >Lihat icon</a>
                      </div>
								<div class="form-group">
                                    <label for="exampleFormControlSelect2">Level akses</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="aktif">
									<?php if($r['aktif']=="Y") { ?>
                                        <option value="Y" selected>Aktif</option>
                                        <option value="N">Non Aktif</option>
									<?php } else{ ?>
									<option value="Y" >Aktif</option>
                                        <option value="N" selected>Non Aktif</option>
									<?php } ?>
                                    </select>
									</div>
								<div class="form-group">
                                    <label for="exampleFormControlSelect2">Warna Teks</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" name="teks">
                                        <option value="primary">primary</option>
                                        <option value="orange">orange</option>
										<option value="info">info</option>
										<option value="default">default</option>
                                    </select>
                                </div>
								<div class="form-group">
                                    <label for="example-search-input" class="form-control-label">Urutan menu</label>
                                    <input class="form-control" type="text" name="urutan" placeholder="Urutan menu">
                                </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>
			 
			 
			 