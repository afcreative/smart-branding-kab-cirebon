<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Kode</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama Kode</th>
						<th>Nama Aset</th>
						<th>Umur Ekonomis</th>
						<th>Kriteria</th>
						
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['nama_kode']; ?></td>
						<td><?php echo $r['nama_asset']; ?></td>
						<td><?php echo $r['umur_ekonomis']; ?></td>
						<td><?php echo $r['nama_kriteria']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#kode<?php echo "$r[id_kode]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_kode/$r[id_kode]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>barang/kode" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Kode</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     
					  <div class="form-group">
                        <label>Nama Kode </label>
                        <input type="text" name="nama_kode" required="required" class="form-control" placeholder="Nama Kode ..">
                      </div>
					  <div class="form-group">
                        <label>Kriteria KIB</label>
                        <?php
						$kategori =$this->db->query("Select * From kriteria ")->result_array();
						echo"
						<select class='form-control' name='id_kriteria' required>
                    <option value=''>- Pilih Kriteria Barang -</option>";
                    foreach ($kategori as $rows) {
                        echo "<option value='$rows[id_kriteria]'>$rows[nama_kriteria]  | $rows[ket] </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Assets </label>
                        <input type="text" name="nama_asset" required="required" class="form-control" placeholder="Nama Assets ..">
                      </div>
					  <div class="form-group">
                        <label>Umur Ekonomis</label>
                        <input type="text" name="umur_ekonomis" class="form-control" placeholder="Umur Ekonomis ..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>barang/edit_kode/<?php echo $r['id_kode']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="kode<?php echo $r['id_kode']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit kode</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					
                      <div class="form-group">
                        <label>Nama Kode </label>
                        <input type="text" name="nama_kode" value="<?php echo $r['nama_kode']; ?>" required="required" class="form-control" placeholder="Nama Kode ..">
                      </div>
					  <div class="form-group">
                        <label>Kriteria Barang</label>
                        <?php
						$kategori =$this->db->query("Select * From kriteria ")->result_array();
						echo"
						<select class='form-control' name='id_kriteria' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($kategori as $row) {
                       if ($r['id_kriteria']==$row['id_kriteria']){
                            echo "<option value='$row[id_kriteria]' selected>$row[nama_kriteria] | $row[ket]</option>";
                            }else{
                            echo "<option value='$row[id_kriteria]'>$row[nama_kriteria] | $row[ket]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Nama Assets </label>
                        <input type="text" name="nama_asset" value="<?php echo $r['nama_asset']; ?>" required="required" class="form-control" placeholder="Nama Assets ..">
                      </div>
					  <div class="form-group">
                        <label>Umur Ekonomis</label>
                        <input type="text" name="umur_ekonomis" value="<?php echo $r['umur_ekonomis']; ?>"  class="form-control" placeholder="Umur Ekonomis ..">
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>