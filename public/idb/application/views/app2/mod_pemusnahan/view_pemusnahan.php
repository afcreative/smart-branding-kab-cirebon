<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Pemusnahan</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush"  id="datatable-basic" >
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Kode Barang </th>
						<th>No Register</th>
						<th>Nama Barang</th>
						<th>Jumlah</th>
						<th>Tanggal</th>
						<th>Foto</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					if($r['foto_barang']==''){
						$gambar="blank.png";
					}else{
						$gambar=$r['foto_barang'];
					}
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						
						<td><?php echo $r['kode_barang']; ?></td>
						<td><?php echo $r['no_register']; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['jml_musnah']; ?></td>
						<td><?php echo $r['tgl_musnah']; ?></td>
						<td><span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/foto_barang/<?php echo $gambar; ?>">
                  </span></td>
                        <td>
						  <button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#user<?php echo "$r[id_pemusnahan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."barang/hapus_pemusnahan/$r[id_pemusnahan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</td></tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>barang/pemusnahan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Pemusnahan Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
						<input type="hidden" name="id_detail" id="kode" required="required" class="form-control" placeholder="Kode barang ..">
                      <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" id="barang" required="required" class="form-control" placeholder="Kode barang .."><br>
						<a href="#" data-toggle="modal" data-target="#kodeModal">Lihat Kode Barang</a>
                      </div>
					  <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jml_musnah" required="required" class="form-control" placeholder="Jumlah barang ..">
                      </div>
					  <div class="form-group">
                        <label>Alasan pemusnahan</label>
                        <textarea  class="form-control" name="alasan" colspan="3"></textarea>
                      </div>
					  <div class="form-group">
                        <label>Tanggal Pemusnahan</label>
                        <input type="date" name="tgl_musnah" required="required" class="form-control" placeholder="Tanggal pemusnahan barang ..">
                      </div>
					  
					  
					  
					                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			
			
			<div class="modal fade " id="kodeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Kode Asset Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
				<div class="table-responsive py-4">
              <table class="table table-flush display">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Kode Asset </th>
						<th>Nama Barang</th>
						<th>Kriteria KIB</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; 
				$kriteria=$this->db->query("Select * From detail_pengadaan a, barang b where a.id_barang=b.id_barang ");
				
				foreach($kriteria->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['no_register']; ?></td>
						<td><a class="button-detail-tab-item" href="javascript:void(0)" data-isi="<?php echo "$r[id_detail]"; ?>" data-barang="<?php echo "$r[nama_barang]"; ?>"><span><?php echo $r['nama_barang']; ?></span></a></td>
						<td><?php echo $r['nama_barang']; ?></td>
						  
					
					</tr>
					<?php $no++; } ?> 
                </tbody>
              </table>
            </div>
                    </div>
                    
                  </div>
                </div>
              </div>
			  <script>
			  $(".button-detail-tab-item").click(function(){
				  $('#kodeModal').modal('hide');
				
				var ambilVal = $(this).attr('data-isi');
				var nama_barang = $(this).attr('data-barang');
					$("#kode").val(ambilVal);
					$("#barang").val(nama_barang);					
								
				})
			  </script>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>barang/edit_pemusnahan/<?php echo $r['id_pemusnahan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="user<?php echo $r['id_pemusnahan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Pemusnahan Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
						<input type="hidden" name="id_detail"  value="<?php echo $r['id_detail']; ?>" id="kode2" required="required" class="form-control" placeholder="Kode barang ..">
                        <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang"  value="<?php echo $r['nama_barang']; ?>" id="barang2" required="required" class="form-control" placeholder="Kode barang .."><br>
						<a href="#" data-toggle="modal" data-target="#kodeModal2">Lihat Kode Barang</a>
                      </div>
					  <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jml_musnah" value="<?php echo $r['jml_musnah']; ?>" required="required" class="form-control" placeholder="Jumlah barang ..">
                      </div>
					  <div class="form-group">
                        <label>Alasan pemusnahan</label>
                        <textarea  class="form-control" name="alasan" colspan="3"><?php echo $r['alasan']; ?></textarea>
                      </div>
					  <div class="form-group">
                        <label>Tanggal Pemusnahan</label>
                        <input type="date" name="tgl_musnah"  value="<?php echo $r['tgl_musnah']; ?>" required="required" class="form-control" placeholder="Tanggal pemusnahan barang ..">
                      </div> 

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>
			 <div class="modal fade " id="kodeModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Kode Asset Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
				<div class="table-responsive py-4">
              <table class="table table-flush display">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Kode Asset </th>
						<th>Nama Barang</th>
						<th>Kriteria KIB</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; 
				$kriteria=$this->db->query("Select * From detail_pengadaan a, barang b where a.id_barang=b.id_barang ");
				
				foreach($kriteria->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['no_register']; ?></td>
						<td><a class="button-detail-tab-item2" href="javascript:void(0)" data-isi="<?php echo "$r[id_detail]"; ?>" data-barang="<?php echo "$r[nama_barang]"; ?>"><span><?php echo $r['nama_barang']; ?></span></a></td>
						<td><?php echo $r['nama_barang']; ?></td>
						  
					
					</tr>
					<?php $no++; } ?> 
                </tbody>
              </table>
            </div>
                    </div>
                    
                  </div>
                </div>
              </div>
			  <script>
			  $(".button-detail-tab-item2").click(function(){
				  $('#kodeModal2').modal('hide');
				
				var ambilVal = $(this).attr('data-isi');
				var nama_barang = $(this).attr('data-barang');
					$("#kode2").val(ambilVal);
					$("#barang2").val(nama_barang);					
								
				})
			  </script>
			 <script>
			 $(document).ready(function() {
    $('table.display').DataTable();
} );
			 </script>