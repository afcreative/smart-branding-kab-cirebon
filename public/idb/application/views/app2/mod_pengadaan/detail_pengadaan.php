<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Detail Pengadaan</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Jenis Barang</th>
						<th>Jumlah</th>
						<th>Sumber Dana</th>
						<th>Tahun Anggaran</th>
						<th>Ruang</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
				$x=explode('-',$r['tgl_masuk']);
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['jumlah']; ?></td>
						<td><?php echo $r['sumber_dana']; ?></td>
						<td><?php echo $x[0]; ?></td>
						<td><?php echo $r['nama_ruang']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pengadaan<?php echo "$r[id_detail]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_detail_pengadaan/$r[id_detail]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/detail_pengadaan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Detail Pengadaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     
					  <div class="form-group">
                        <label>Nama Barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
                    <option value=''>- Pilih Nama Barang -</option>";
                    foreach ($barang as $rows) {
                        echo "<option value='$rows[id_barang]'>$rows[nama_barang] </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Sumber Dana</label>
                        <?php
						$pengadaan =$this->db->query("Select * From pengadaan ")->result_array();
						echo"
						<select class='form-control' name='id_pengadaan' required>
                    <option value=''>- Pilih Nama Barang -</option>";
                    foreach ($pengadaan as $rows) {
                        echo "<option value='$rows[id_pengadaan]'>$rows[sumber_dana] </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Jumlah </label>
                        <input type="text" name="jumlah" required="required" class="form-control" placeholder="Jumlah Barang..">
                      </div>
					    <div class="form-group">
                        <label>Di Ruang</label>
                        <?php
						$ruang =$this->db->query("Select * From ruang ")->result_array();
						echo"
						<select class='form-control' name='id_ruang' required>
                    <option value=''>- Pilih Nama Barang -</option>";
                    foreach ($ruang as $rows) {
                        echo "<option value='$rows[id_ruang]'>$rows[nama_ruang] </option>";
                    }
                echo "</select>
						";
						?>
                      </div>                

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_detail_pengadaaan/<?php echo $r['id_detail']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="pengadaan<?php echo $r['id_detail']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit pengadaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					  <div class="form-group">
                        <label>Nama barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($barang as $row) {
                       if ($r['id_barang']==$row['id_barang']){
                            echo "<option value='$row[id_barang]' selected>$row[nama_barang]</option>";
                            }else{
                            echo "<option value='$row[id_barang]'>$row[nama_barang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					  <div class="form-group">
                        <label>Sumber dana</label>
                        <?php
						$pengadaan =$this->db->query("Select * From pengadaan ")->result_array();
						echo"
						<select class='form-control' name='id_pengadaan' required>
                    <option value=''>- Pilih Sumber Dana -</option>";
                    foreach ($pengadaan as $row) {
                       if ($r['id_pengadaan']==$row['id_pengadaan']){
                            echo "<option value='$row[id_pengadaan]' selected>$row[sumber_dana]</option>";
                            }else{
                            echo "<option value='$row[id_pengadaan]'>$row[sumber_dana]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Jumlah </label>
                        <input type="text" name="jumlah" required="required" class="form-control" value="<?php echo $r['jumlah']; ?>">
                      </div>
					   <div class="form-group">
                        <label>Sumber dana</label>
                        <?php
						$ruangan =$this->db->query("Select * From ruang ")->result_array();
						echo"
						<select class='form-control' name='id_ruang' required>
                    <option value=''>- Pilih Ruangan-</option>";
                    foreach ($ruangan as $row) {
                       if ($r['id_ruang']==$row['id_ruang']){
                            echo "<option value='$row[id_ruang]' selected>$row[nama_ruang]</option>";
                            }else{
                            echo "<option value='$row[id_ruang]'>$row[nama_ruang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>