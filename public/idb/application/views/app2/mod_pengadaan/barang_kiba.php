<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Detail Pengadaan KIB A</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama</th>
						<th>Register</th>
						<th>Luas</th>
						<th>Tahun</th>
						<th>Letak</th>
						<th>Hak</th>
						<th>Tgl</th>
						<th>No Sertifikat</th>
						<th>Penggunaan</th>
						<th>Asal - usul</th>
						<th>Harga</th>
						<th>Harga Total</th>
						<th>Keterangan</th>
						<th>Doc</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
				
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['no_register']; ?></td>
						<td><?php echo $r['luas']; ?></td>
						
						<td><?php echo $r['tahun_pengadaan']; ?></td>
						<td><?php echo $r['alamat']; ?></td>
						<td><?php echo $r['status_lahan']; ?></td>
						<td><?php echo $r['tgl_sertifikat']; ?></td>
						<td><?php echo $r['no_sertifikat']; ?></td>
						<td><?php echo $r['penggunaan']; ?></td>
						<td><?php echo $r['asal_usul']; ?></td>
						<td><?php echo $r['harga']; ?></td>
						<td><?php echo $r['harga_total']; ?></td>
						<td><?php echo $r['keterangan']; ?></td>
						<td>
						
						<a href="<?php echo base_url();?>assets/doc_barang/<?php echo $r['foto']; ?>" target="_blank"><span class="avatar avatar-sm rounded-circle"><img src="<?php echo base_url(); ?>assets/doc_barang/<?php echo $r['foto']; ?>"></span></a></td>
						  <td>
						  <a href="<?php echo base_url();?>barang/penempatan/<?php echo $r['id_detail'];?>" button title="Penempatan" type="button" class="btn btn-info btn-sm" >
                          <i class="fa fa-plus" ></i>
                        </button></a>
						  <button title="Upload Document" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#upload<?php echo "$r[id_detail]"; ?>">
                          <i class="fa fa-eye" ></i>
                        </button>
						<button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pengadaan<?php echo "$r[id_detail]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_detail_pengadaan/$r[id_detail]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/detail_bantuan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Detail Pengadaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     <input type="hidden" name="id_kriteria" value="<?php echo $id_kriteria;  ?>" required="required" class="form-control" placeholder="Jumlah Barang..">
					  <div class="form-group">
                        <label>Data usulan</label>
                        <?php
						$usulan =$this->db->query("Select * From usulan ")->result_array();
						echo"
						<select class='form-control' id='id_usulan' name='id_usulan' required>
                    <option value=''>- Pilih Nama usulan -</option>";
                    foreach ($usulan as $rows) {
                        echo "<option value='$rows[id_usulan]'>$rows[unit_kerja] | $rows[jenis_kegiatan] | $rows[tahun_anggaran]   </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					  <div class="form-group">
                        <label>Nama Barang</label>
                       <select class="form-control" id="id_barang" name="id_barang" required>
                        <option>No Selected</option>
 
                    </select>
                      </div>
					  
					  
					  <div class="form-group">
                        <label>No Register </label>
                        <input type="text" name="no_register" required="required" class="form-control" placeholder="Register Barang..">
                      </div>
					  <div class="form-group">
                        <label>Luas </label>
                        <input type="text" name="luas" required="required" class="form-control" placeholder="Luas lahan..">
                      </div>
					  <div class="form-group">
                        <label>Tahun Pengadaan</label>
                        <input type="date" name="tahun_pengadaan" required="required" class="form-control" placeholder="Tahun Pengadaan">
                      </div>
					  <div class="form-group">
                        <label>Letak /  Alamat</label>
                        <input type="text" name="alamat" required="required" class="form-control" placeholder="Letak alamat objek..">
                      </div>
					  <div class="form-group">
                        <label>Hak (Status tanah)</label>
                        <input type="text" name="status_lahan" required="required" class="form-control" placeholder="Status Lahan..">
                      </div>
					  <div class="form-group">
                        <label>Tgl Sertifikat</label>
                        <input type="date" name="tgl_sertifikat" required="required" class="form-control" placeholder="Tanggal ..">
                      </div>
					  <div class="form-group">
                        <label>No Sertifikat</label>
                        <input type="text" name="no_sertifikat" required="required" class="form-control" placeholder="No Sertifikat..">
                      </div>
					   <div class="form-group">
                        <label>Penggunaan</label>
                        <input type="text" name="penggunaan" class="form-control" placeholder="Penggunaan..">
                      </div>
					   <div class="form-group">
                        <label>Asal-usul</label>
                        <?php
						$dana =$this->db->query("Select * From pengadaan a, dana b where a.sumber_dana=b.id_dana ")->result_array();
						
						echo"
						<select class='form-control' name='asal_usul' required>
                    <option value=''>- Pilih Sumber dana -</option>";
                    foreach ($dana as $row) {
						$x=explode('-',$row['tgl_masuk']);
                       if ($r['asal_usul']==$row['id_pengadaan']){
                            echo "<option value='$row[id_pengadaan]' selected>$row[nama_dana] || $x[0] </option>";
                            }else{
                            echo "<option value='$row[id_pengadaan]'>$row[nama_dana] || $x[0]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Harga</label>
                        <input type="text" name="harga" r class="form-control" placeholder="Harga objek..">
                      </div>
					  <div class="form-group">
                        <label>Harga Total</label>
                        <input type="text" name="harga_total"  class="form-control" placeholder="Harga Total..">
                      </div>
					  <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="keterangan"  class="form-control" placeholder="Keterangan..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			<script type="text/javascript">
							$(document).ready(function(){
					 
								$('#id_usulan').change(function(){ 
									var id_usulan=$(this).val();
									$.ajax({
										url : "<?php echo site_url('barang/get_id_usulan');?>",
										method : "POST",
										data : {id_usulan: id_usulan},
										async : true,
										dataType : 'json',
										success: function(data){
											 
											var html = '';
											var i;
											for(i=0; i<data.length; i++){
												html += '<option value='+data[i].id_barang+'>'+data[i].nama_barang+'</option>';
											}
											$('#id_barang').html(html);
					 
										}
									});
									return false;
								}); 
								 
							});
						</script>
			<?php  foreach($record->result_array() as $r) { ?>
			<form action="<?php echo base_url(); ?>app/upload_document/<?php echo $r['id_detail']; ?>" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="upload<?php echo $r['id_detail']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Upload Document</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

					
					  <div class="form-group">
                        <label>Document</label>
                        <input type="file" name="upload"  class="form-control" placeholder="Dokument..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Upload</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			<?php } ?>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_detail_pengadaaan/<?php echo $r['id_detail']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="pengadaan<?php echo $r['id_detail']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit pengadaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					  <div class="form-group">
                        <label>Nama barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($barang as $row) {
                       if ($r['id_barang']==$row['id_barang']){
                            echo "<option value='$row[id_barang]' selected>$row[nama_barang]</option>";
                            }else{
                            echo "<option value='$row[id_barang]'>$row[nama_barang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					   <div class="form-group">
                        <label>No Register </label>
                        <input type="text" name="no_register" value="<?php echo $r['no_register']; ?>" required="required" class="form-control" placeholder="Jumlah Barang..">
                      </div>
					  <div class="form-group">
                        <label>Luas </label>
                        <input type="text" name="luas" value="<?php echo $r['luas']; ?>"  required="required" class="form-control" placeholder="Luas lahan..">
                      </div>
					  <div class="form-group">
                        <label>Tahun Pengadaan</label>
                        <input type="date" name="tahun_pengadaan" value="<?php echo $r['tahun_pengadaan']; ?>"  required="required" class="form-control" placeholder="Tahun Pengadaan">
                      </div>
					  <div class="form-group">
                        <label>Letak /  Alamat</label>
                        <input type="text" name="alamat" value="<?php echo $r['alamat']; ?>"  required="required" class="form-control" placeholder="Letak alamat objek..">
                      </div>
					  <div class="form-group">
                        <label>Hak (Status tanah)</label>
                        <input type="text" name="status_lahan" value="<?php echo $r['status_lahan']; ?>"  required="required" class="form-control" placeholder="Status Lahan..">
                      </div>
					  <div class="form-group">
                        <label>Tgl Sertifikat</label>
                        <input type="text" name="tgl_sertifikat" value="<?php echo $r['tgl_sertifikat']; ?>"  required="required" class="form-control" placeholder="Tanggal ..">
                      </div>
					  <div class="form-group">
                        <label>No Sertifikat</label>
                        <input type="text" name="no_sertifikat" value="<?php echo $r['no_sertifikat']; ?>"  required="required" class="form-control" placeholder="No Sertifikat..">
                      </div>
					   <div class="form-group">
                        <label>Penggunaan</label>
                        <input type="text" name="penggunaan" value="<?php echo $r['penggunaan']; ?>"  class="form-control" placeholder="Penggunaan..">
                      </div>
					   <div class="form-group">
                        <label>Asal-usul</label>
                        <?php
						$dana =$this->db->query("Select * From pengadaan a, dana b where a.sumber_dana=b.id_dana ")->result_array();
						
						echo"
						<select class='form-control' name='asal_usul' required>
                    <option value=''>- Pilih Sumber dana -</option>";
                    foreach ($dana as $row) {
						$x=explode('-',$row['tgl_masuk']);
                       if ($r['asal_usul']==$row['id_pengadaan']){
                            echo "<option value='$row[id_pengadaan]' selected>$row[nama_dana] || $x[0] </option>";
                            }else{
                            echo "<option value='$row[id_pengadaan]'>$row[nama_dana] || $x[0]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Harga</label>
                        <input type="text" name="harga" value="<?php echo $r['harga']; ?>"  class="form-control" placeholder="Harga objek..">
                      </div>
					  <div class="form-group">
                        <label>Harga Total</label>
                        <input type="text" name="harga_total" value="<?php echo $r['harga_total']; ?>"   class="form-control" placeholder="Harga Total..">
                      </div>
					  <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="keterangan" value="<?php echo $r['keterangan']; ?>"   class="form-control" placeholder="Keterangan..">
                      </div>
					             
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>