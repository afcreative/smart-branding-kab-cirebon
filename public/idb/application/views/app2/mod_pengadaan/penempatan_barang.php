<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Penempatan</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama Ruang</th>
						<th>Nama Barang</th>
						<th>Jumlah</th>
						<th>Tahun penempatan</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_ruang']; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['jml']; ?></td>
						<td><?php echo $r['tgl_penempatan']; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#penempatan<?php echo "$r[id_penempatan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."barang/hapus_penempatan/$r[id_penempatan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>barang/penempatan/<?php echo $id_detail; ?>" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Penempatan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     
					  <div class="form-group">
                        <label>Nama Ruang</label>
                        <?php
						$dana =$this->db->query("Select * From ruang ")->result_array();
						echo"
						<select class='form-control' name='id_ruang' required>
                    <option value=''>- Pilih Sumber dana -</option>";
                    foreach ($dana as $row) {
                       if ($r['id_ruang']==$row['id_ruang']){
                            echo "<option value='$row[id_ruang]' selected>$row[nama_ruang]</option>";
                            }else{
                            echo "<option value='$row[id_ruang]'>$row[nama_ruang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Jumlah </label>
                        <input type="text" name="jml" required="required" class="form-control" placeholder="Jumlah barang">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>barang/edit_penempatan/<?php echo $r['id_penempatan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="penempatan<?php echo $r['id_penempatan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Penempatan </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					  <div class="form-group">
                        <label>Nama Ruang</label>
                        <?php
						$dana =$this->db->query("Select * From ruang ")->result_array();
						echo"
						<select class='form-control' name='id_ruang' required>
                    <option value=''>- Pilih Ruang -</option>";
                    foreach ($dana as $row) {
                       if ($r['id_ruang']==$row['id_ruang']){
                            echo "<option value='$row[id_ruang]' selected>$row[nama_ruang]</option>";
                            }else{
                            echo "<option value='$row[id_ruang]'>$row[nama_ruang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Jumlah </label>
                        <input type="text" name="jml" value="<?php echo $r['jml']; ?>" required="required" class="form-control" placeholder="Jumlah barang">
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>