<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Pengadaan</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Kode Pengadaan</th>
						<th>Sumber Dana</th>
						<th>Tahun Anggaran</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					$x=explode('-',$r['tgl_masuk']);
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['kode_pengadaan']; ?></td>
						<td><?php echo $r['nama_dana']; ?></td>
						<td><?php echo $x[0]; ?></td>
						  <td><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pengadaan<?php echo "$r[id_pengadaan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_pengadaan/$r[id_pengadaan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/pengadaan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Pengadaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     
					  <div class="form-group">
                        <label>Kode Pengadaan </label>
                        <input type="text" name="kode_pengadaan" required="required" class="form-control" placeholder="Kode Pengadanaan..">
                      </div>
					  <div class="form-group">
                        <label>Sumber dana</label>
                        <?php
						$dana =$this->db->query("Select * From dana ")->result_array();
						echo"
						<select class='form-control' name='sumber_dana' required>
                    <option value=''>- Pilih Sumber dana -</option>";
                    foreach ($dana as $row) {
                       if ($r['asal_usul']==$row['id_dana']){
                            echo "<option value='$row[id_dana]' selected>$row[nama_dana]</option>";
                            }else{
                            echo "<option value='$row[id_dana]'>$row[nama_dana]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Tanggal pengadaan </label>
                        <input type="date" name="tgl_masuk" required="required" class="form-control" placeholder="Tanggal masuk..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_pengadaan/<?php echo $r['id_pengadaan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="pengadaan<?php echo $r['id_pengadaan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit pengadaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					  <div class="form-group">
                        <label>Kode Pengadaan </label>
                        <input type="text" name="kode_pengadaan" required="required" class="form-control" value="<?php echo $r['kode_pengadaan']; ?>">
                      </div>
					  <div class="form-group">
                        <label>Sumber dana</label>
                        <?php
						$dana =$this->db->query("Select * From dana ")->result_array();
						echo"
						<select class='form-control' name='sumber_dana' required>
                    <option value=''>- Pilih Sumber dana -</option>";
                    foreach ($dana as $row) {
                       if ($r['sumber_dana']==$row['id_dana']){
                            echo "<option value='$row[id_dana]' selected>$row[nama_dana]</option>";
                            }else{
                            echo "<option value='$row[id_dana]'>$row[nama_dana]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Tanggal pengadaan </label>
                        <input type="date" name="tgl_masuk"  value="<?php echo $r['tgl_masuk']; ?>" required="required" class="form-control" >
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>