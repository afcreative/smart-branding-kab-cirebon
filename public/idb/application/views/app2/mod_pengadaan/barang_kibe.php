<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Detail Pengadaan KIB E</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama</th>
						<th>Kode Barang</th>
						<th>Register</th>
						<th>Judul</th>
						<th>Spesifikasi</th>
						<th>Asal daerah</th>
						<th>Pencipta</th>
						<th>Bahan</th>
						<th>Jenis</th>
						<th>Ukuran</th>
						<th>Jumlah</th>
						<th>Tahun Cetak</th>
						<th>Asal-usul</th>
						<th>Harga</th>
						<th>Kondisi</th>
						<th>Keterangan</th>
						<th>Doc</th>
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
				$luas = $r['panjang'] * $r['lebar'];
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['kode_barang']; ?></td>
						<td><?php echo $r['no_register']; ?></td>
						<td><?php echo $r['judul']; ?></td>
						
						<td><?php echo $r['spesifikasi']; ?></td>
						<td><?php echo $r['asal_daerah']; ?></td>
						<td><?php echo $r['pencipta']; ?></td>
						<td><?php echo $r['bahan']; ?></td>
						<td><?php echo $r['jenis']; ?></td>
						<td><?php echo $r['ukuran']; ?></td>
						<td><?php echo $r['jumlah']; ?></td>
						<td><?php echo $r['tahun_pengadaan']; ?></td>
						<td><?php echo $r['asal_usul']; ?></td>
						<td><?php echo $r['harga_total']; ?></td>
						<td><?php echo $r['status']; ?></td>
						<td><?php echo $r['keterangan']; ?></td>
						 <td><a href="<?php echo base_url();?>assets/doc_barang/<?php echo $r['foto']; ?>" target="_blank"><span class="avatar avatar-sm rounded-circle"><img src="<?php echo base_url(); ?>assets/doc_barang/<?php echo $r['foto']; ?>"></span></a></td>
						  <td>
						  <button title="Upload Document" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#upload<?php echo "$r[id_detail]"; ?>">
                          <i class="fa fa-eye" ></i>
                        </button><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pengadaan<?php echo "$r[id_detail]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_detail_pengadaan/$r[id_detail]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  <?php  foreach($record->result_array() as $r) { ?>
			<form action="<?php echo base_url(); ?>app/upload_document/<?php echo $r['id_detail']; ?>" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="upload<?php echo $r['id_detail']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Upload Document</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

					
					  <div class="form-group">
                        <label>Document</label>
                        <input type="file" name="upload"  class="form-control" placeholder="Dokument..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Upload</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			<?php } ?>
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/detail_bantuan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Buku, Barang bercorak, hewan/ternak, tumbuhan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     <input type="hidden" name="id_kriteria" value="<?php echo $id_kriteria;  ?>" required="required" class="form-control" placeholder="Jumlah Barang..">
					  <div class="form-group">
                        <label>Data usulan</label>
                        <?php
						$usulan =$this->db->query("Select * From usulan ")->result_array();
						echo"
						<select class='form-control' id='id_usulan' name='id_usulan' required>
                    <option value=''>- Pilih Nama usulan -</option>";
                    foreach ($usulan as $rows) {
                        echo "<option value='$rows[id_usulan]'>$rows[unit_kerja] | $rows[jenis_kegiatan] | $rows[tahun_anggaran]   </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					  <div class="form-group">
                        <label>Nama Barang</label>
                       <select class="form-control" id="id_barang" name="id_barang" required>
                        <option>No Selected</option>
 
                    </select>
                      </div>
					  
					  <div class="form-group">
                        <label>No Register </label>
                        <input type="text" name="no_register" required="required" class="form-control" placeholder="No register..">
                      </div>
					  <div class="form-group">
                        <label>Judul </label>
                        <input type="text" name="judul" required="required" class="form-control" placeholder="Judul Buku ..">
                      </div>
					  <div class="form-group">
                        <label>Spesifikasi </label>
                        <input type="text" name="spesifikasi" required="required" class="form-control" placeholder="Spesifikasi ..">
                      </div>
					  
					  <div class="form-group">
                        <label>Asal Daerah</label>
                        <input type="text" name="asal_daerah" class="form-control" placeholder="Asal Daerah..">
                      </div>
					  <div class="form-group">
                        <label>Pencipta</label>
                        <input type="text" name="pencipta" required="required" class="form-control" placeholder="Pencipta ..">
                      </div>
					  <div class="form-group">
                        <label>Bahan</label>
                        <input type="text" name="bahan" class="form-control" placeholder="Bahan ..">
                      </div>
					  <div class="form-group">
                        <label>Jenis</label>
                        <input type="text" name="jenis" class="form-control" placeholder="Jenis..">
                      </div>
					   <div class="form-group">
                        <label>Ukuran</label>
                        <input type="text" name="ukuran" class="form-control" placeholder="Ukuran..">
                      </div>
					  <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" class="form-control" placeholder="Jumlah..">
                      </div>
					   <div class="form-group">
                        <label>Asal-usul</label>
                        <?php
						$dana =$this->db->query("Select * From pengadaan a, dana b where a.sumber_dana=b.id_dana ")->result_array();
						
						echo"
						<select class='form-control' name='asal_usul' required>
                    <option value=''>- Pilih Sumber dana -</option>";
                    foreach ($dana as $row) {
						$x=explode('-',$row['tgl_masuk']);
                       if ($r['asal_usul']==$row['id_pengadaan']){
                            echo "<option value='$row[id_pengadaan]' selected>$row[nama_dana] || $x[0] </option>";
                            }else{
                            echo "<option value='$row[id_pengadaan]'>$row[nama_dana] || $x[0]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Harga Total</label>
                        <input type="text" name="harga_total"  class="form-control" placeholder="Harga Total..">
                      </div>
					  <div class="form-group">
                        <label>Status</label><br>
                        <input type="radio" name="status" value="B"> Baik<br>
						<input type="radio" name="status" value="KB"> Kurang Baik<br>
						<input type="radio" name="status" value="RB"> Rusak Berat<br>
                      </div>
					  <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="keterangan"  class="form-control" placeholder="Keterangan..">
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			<script type="text/javascript">
							$(document).ready(function(){
					 
								$('#id_usulan').change(function(){ 
									var id_usulan=$(this).val();
									$.ajax({
										url : "<?php echo site_url('barang/get_id_usulan');?>",
										method : "POST",
										data : {id_usulan: id_usulan},
										async : true,
										dataType : 'json',
										success: function(data){
											 
											var html = '';
											var i;
											for(i=0; i<data.length; i++){
												html += '<option value='+data[i].id_barang+'>'+data[i].nama_barang+'</option>';
											}
											$('#id_barang').html(html);
					 
										}
									});
									return false;
								}); 
								 
							});
						</script>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_detail_pengadaaan/<?php echo $r['id_detail']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="pengadaan<?php echo $r['id_detail']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit pengadaan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					  <div class="form-group">
                        <label>Nama barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($barang as $row) {
                       if ($r['id_barang']==$row['id_barang']){
                            echo "<option value='$row[id_barang]' selected>$row[nama_barang]</option>";
                            }else{
                            echo "<option value='$row[id_barang]'>$row[nama_barang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					   <div class="form-group">
                        <label>No Register </label>
                        <input type="text" name="no_register" value="<?php echo $r['no_register']; ?>" required="required" class="form-control" placeholder="Jumlah Barang..">
                      </div>
					  <div class="form-group">
                        <label>Judul </label>
                        <input type="text" name="judul" value="<?php echo $r['judul']; ?>" required="required" class="form-control" placeholder="Judul Buku ..">
                      </div>
					  <div class="form-group">
                        <label>Spesifikasi </label>
                        <input type="text" name="spesifikasi" value="<?php echo $r['spesifikasi']; ?>" required="required" class="form-control" placeholder="Spesifikasi ..">
                      </div>
					  
					  <div class="form-group">
                        <label>Asal Daerah</label>
                        <input type="text" name="asal_daerah" value="<?php echo $r['asal_daerah']; ?>" class="form-control" placeholder="Asal Daerah..">
                      </div>
					  <div class="form-group">
                        <label>Pencipta</label>
                        <input type="text" name="pencipta" value="<?php echo $r['pencipta']; ?>" required="required" class="form-control" placeholder="Pencipta ..">
                      </div>
					  <div class="form-group">
                        <label>Bahan</label>
                        <input type="text" name="bahan" value="<?php echo $r['bahan']; ?>" class="form-control" placeholder="Bahan ..">
                      </div>
					  <div class="form-group">
                        <label>Jenis</label>
                        <input type="text" name="jenis" value="<?php echo $r['jenis']; ?>" class="form-control" placeholder="Jenis..">
                      </div>
					   <div class="form-group">
                        <label>Ukuran</label>
                        <input type="text" name="ukuran" value="<?php echo $r['ukuran']; ?>" class="form-control" placeholder="Ukuran..">
                      </div>
					  <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" value="<?php echo $r['jumlah']; ?>" class="form-control" placeholder="Jumlah..">
                      </div>
					   <div class="form-group">
                        <label>Asal-usul</label>
                        <?php
						$dana =$this->db->query("Select * From pengadaan a, dana b where a.sumber_dana=b.id_dana ")->result_array();
						
						echo"
						<select class='form-control' name='asal_usul' required>
                    <option value=''>- Pilih Sumber dana -</option>";
                    foreach ($dana as $row) {
						$x=explode('-',$row['tgl_masuk']);
                       if ($r['asal_usul']==$row['id_pengadaan']){
                            echo "<option value='$row[id_pengadaan]' selected>$row[nama_dana] || $x[0] </option>";
                            }else{
                            echo "<option value='$row[id_pengadaan]'>$row[nama_dana] || $x[0]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Harga Total</label>
                        <input type="text" name="harga_total" value="<?php echo $r['harga_total']; ?>"  class="form-control" placeholder="Harga Total..">
                      </div>
					  <div class="form-group">
                        <label>Status</label>
                        <input type="text" name="status" value="<?php echo $r['status']; ?>"  class="form-control" placeholder="Harga Total..">
						
                      </div>
					  <div class="form-group">
                        <label>Status</label><br>
						<?php if($r['status']=="B"){ ?>
                        <input type="radio" name="status" value="B" checked> Baik<br>
						<input type="radio" name="status" value="KB"> Kurang Baik<br>
						<input type="radio" name="status" value="RB"> Rusak Berat<br>
						<?php } elseif($r['status']=="KB"){ ?>
                        <input type="radio" name="status" value="B" > Baik<br>
						<input type="radio" name="status" value="KB" checked> Kurang Baik<br>
						<input type="radio" name="status" value="RB"> Rusak Berat<br>
						<?php } else { ?>
                        <input type="radio" name="status" value="B" > Baik<br>
						<input type="radio" name="status" value="KB"> Kurang Baik<br>
						<input type="radio" name="status" value="RB" checked> Rusak Berat<br>
						<?php } ?>
                      </div>
					  <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="keterangan" value="<?php echo $r['keterangan']; ?>"  class="form-control" placeholder="Keterangan..">
                      </div>
					             
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>