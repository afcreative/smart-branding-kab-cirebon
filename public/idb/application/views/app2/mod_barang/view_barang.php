<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Barang</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush"  id="datatable-basic" >
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Kode Barang </th>
						<th>Nama Barang</th>
						<th>Kategori</th>
						<th>Foto</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					if($r['foto_barang']==''){
						$gambar="blank.png";
					}else{
						$gambar=$r['foto_barang'];
					}
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						
						<td><?php echo $r['kode_barang']; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['nama_kategori']; ?></td>
						<td><span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/foto_barang/<?php echo $gambar; ?>">
                  </span></td>
                        <td>
						  <button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#user<?php echo "$r[id_barang]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_barang/$r[id_barang]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</td></tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/barang" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      <div class="form-group">
                        <label>Kode Barang</label>
                        <input type="text" name="kode_barang" id="kode" required="required" class="form-control" placeholder="Kode barang .."><br>
						<a href="#" data-toggle="modal" data-target="#kodeModal">Lihat Kode Asset</a>
                      </div>
					  <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" required="required" class="form-control" placeholder="Nama barang ..">
                      </div>
					  <div class="form-group">
                        <label>Kategori</label>
                        <?php
						$kategori =$this->db->query("Select * From kategori a, main b where a.id_main=b.id_main ")->result_array();
						echo"
						<select class='form-control' name='id_kategori' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($kategori as $rows) {
                        echo "<option value='$rows[id_kategori]'>$rows[nama_main] || $rows[nama_kategori] </option>";
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					  
					    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="foto"  class="form-control" >
                      </div>                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			
			
			<div class="modal fade" id="kodeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Kode Asset Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
				<div class="table-responsive py-4">
              <table class="table table-flush display">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
                        <th>Kode Asset </th>
						<th>Nama Asset</th>
						<th>Kriteria KIB</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; 
				$kriteria=$this->db->query("Select * From kode a, kriteria b where a.id_kriteria=b.id_kriteria ");
				
				foreach($kriteria->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['nama_kode']; ?></td>
						<td><a class="button-detail-tab-item" href="javascript:void(0)" data-isi="<?php echo "$r[nama_kode]"; ?>"><span><?php echo $r['nama_asset']; ?></span></a></td>
						<td><?php echo $r['nama_kriteria']; ?></td>
						  
					
					</tr>
					<?php $no++; } ?> 
                </tbody>
              </table>
            </div>
                    </div>
                    
                  </div>
                </div>
              </div>
			  <script>
			  $(".button-detail-tab-item").click(function(){
				  $('#kodeModal').modal('hide');
				
				var ambilVal = $(this).attr('data-isi');
					$("#kode").val(ambilVal);								
								
				})
			  </script>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_barang/<?php echo $r['id_barang']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="user<?php echo $r['id_barang']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                         <div class="form-group">
                        <label>Kode Barang</label>
                        <input type="text" name="kode_barang" value="<?php echo $r['kode_barang']; ?>"  required="required" class="form-control" ><br>
						<a href="">Lihat Kode Asset</a>
                      </div>
					  <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" value="<?php echo $r['nama_barang']; ?>" required="required" class="form-control" >
                      </div>
					  <div class="form-group">
                        <label>Kategori</label>
                        <?php
						$kategori =$this->db->query("Select * From kategori ")->result_array();
						echo"
						<select class='form-control' name='id_kategori' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($kategori as $row) {
                       if ($r['id_kategori']==$row['id_kategori']){
                            echo "<option value='$row[id_kategori]' selected>$row[nama_kategori]</option>";
                            }else{
                            echo "<option value='$row[id_kategori]'>$row[nama_kategori]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					  
					    <div class="form-group">
                        <label>Foto</label><br>
						<span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="<?php echo base_url(); ?>assets/foto_barang/<?php echo $gambar; ?>">
                  </span>
                        <input type="file" name="foto" class="form-control" >
                      </div>   

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>
			 <script>
			 $(document).ready(function() {
    $('table.display').DataTable();
} );
			 </script>