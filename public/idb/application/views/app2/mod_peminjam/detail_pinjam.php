<div class="row">
<div class="col-md-3" align="center">
<div class="card" style="width: 18rem; " >
  <img class="card-img-top" src="<?php echo base_url(); ?>assets/foto_guru/<?php echo $rows['foto_guru']; ?>" alt="Card image cap" style="width:100px; margin-left:90px; margin-top:20px;">
  <div class="card-body">
    <h5 class="card-title"><?php echo $rows['username']; ?></h5>
    <p class="card-text"><?php echo $rows['nama_lengkap']; ?></p>
    <a href="#" class="btn btn-primary"><?php echo $rows['blokir']; ?></a>
  </div>
</div>
</div>
        <div class="col-md-9">
		
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Pinjam </span></button></h4>
              
            </div>
			
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama Barang</th>
						<th>Jml Pinjam</th>
						<th>Tgl Kembali</th>
						<th>Status</th>
						<th>Jml Kembali</th>
						<th>Tgl Dikembalikan</th>
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php 
				$pinjam = $this->db->query("Select * From peminjaman a, barang b where a.id_barang=b.id_barang and a.id_guru='".$rows['id_guru']."'");
				$no=1; foreach($pinjam->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['jumlah']; ?></td>
						<td><?php echo $r['tgl_kembali']; ?></td>
						<td><?php echo $r['status']; ?></td>
						<td><?php echo $r['jml_kembalikan']; ?></td>
						<td><?php echo $r['tgl_kembalikan']; ?></td>
						  <td>
						  <button title="Pengembalian" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#pengadaan<?php echo "$r[id_peminjaman]"; ?>">
                          <i class="fa fa-book" ></i>
                        </button><button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#peminjaman<?php echo "$r[id_peminjaman]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
						
					<?php	echo"
					<a href='".base_url()."app/hapus_peminjaman/$r[id_peminjaman]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/detail_pinjam/<?php echo $rows['id_guru']; ?>" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Pinjam Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      
					  
					  <div class="form-group">
                        <label>Nama Barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($barang as $rows) {
                        echo "<option value='$rows[id_barang]'>$rows[nama_barang] </option>";
                    }
                echo "</select>
						";
						?>
						
                      </div>
					  
					  
					    <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah"  class="form-control" >
                      </div>  
					  <div class="form-group">
                        <label>Tanggal Pinjam</label>
                        <input type="date" name="tgl_pinjam"  class="form-control" >
                      </div>
					<div class="form-group">
                        <label>Tanggal kembali</label>
                        <input type="date" name="tgl_kembali"  class="form-control" >
                      </div> 					  

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
	<?php foreach ($pinjam->result_array() as $r ) { ?>		
				  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/kembali_barang/<?php echo $r['id_peminjaman']; ?>" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="pengadaan<?php echo "$r[id_peminjaman]"; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Pengembalian Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      
					  
					 <div class="form-group">
                        <label>Barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($barang as $row) {
                       if ($r['id_barang']==$row['id_barang']){
                            echo "<option value='$row[id_barang]' selected>$row[nama_barang]</option>";
                            }else{
                            echo "<option value='$row[id_barang]'>$row[nama_barang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					  
					    <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" value="<?php echo "$r[jumlah]"; ?>"  class="form-control" >
                      </div>  
					  <div class="form-group">
                        <label>Tanggal Pinjam</label>
                        <input type="date" name="tgl_pinjam" value="<?php echo "$r[tgl_pinjam]"; ?>"  class="form-control" >
                      </div>
					<div class="form-group">
                        <label>Tanggal kembali</label>
                        <input type="date" name="tgl_kembali"  value="<?php echo "$r[tgl_kembali]"; ?>" class="form-control" >
                      </div> 					  

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
	<?php } ?>
	
	<?php foreach ($pinjam->result_array() as $r ) { ?>		
				  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/edit_peminjaman/<?php echo $r['id_peminjaman']; ?>" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="peminjaman<?php echo "$r[id_peminjaman]"; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Peminjaman   Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                      
					  
					 <div class="form-group">
                        <label>Barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
                    <option value=''>- Pilih Kategori Barang -</option>";
                    foreach ($barang as $row) {
                       if ($r['id_barang']==$row['id_barang']){
                            echo "<option value='$row[id_barang]' selected>$row[nama_barang]</option>";
                            }else{
                            echo "<option value='$row[id_barang]'>$row[nama_barang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  
					  
					    <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" value="<?php echo "$r[jumlah]"; ?>"  class="form-control" >
                      </div>  
					  <div class="form-group">
                        <label>Tanggal Pinjam</label>
                        <input type="date" name="tgl_pinjam" value="<?php echo "$r[tgl_pinjam]"; ?>"  class="form-control" >
                      </div>
					<div class="form-group">
                        <label>Tanggal kembali</label>
                        <input type="date" name="tgl_kembali"  value="<?php echo "$r[tgl_kembali]"; ?>" class="form-control" >
                      </div> 					  

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
	<?php } ?>