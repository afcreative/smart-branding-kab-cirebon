<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Detail Usulan</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Nama Barang & Jasa</th>
						<th>Spesifikasi</th>
						<th>Jumlah</th>
						<th>Satuan</th>
						<th>Harga</th>
						<th>Subtotal</th>
						<th>Keterangan</th>
						
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					$sub =  $r['harga'] * $r['jumlah'];
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['nama_barang']; ?></td>
						<td><?php echo $r['spesifikasi']; ?></td>
						<td><?php echo $r['jumlah']; ?></td>
						<td><?php echo $r['satuan']; ?></td>
						<td><?php echo rupiah($r['harga']); ?></td>
						<td><?php echo rupiah($sub); ?></td>
						<td><?php echo $r['keterangan']; ?></td>
						  <td>
						  
						  
						  <button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#usulan<?php echo "$r[id_detusulan]"; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
					<?php	echo"
					<a href='".base_url()."app/hapus_detusulan/$r[id_detusulan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/detail_usulan/<?php echo $id_usulan; ?>" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Usulan Pengadaan Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     
					  <div class="form-group">
                       <label>Nama Barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
							<option value=''>- Pilih nama barang -</option>";
							foreach ($barang as $rows) {
								echo "<option value='$rows[id_barang]'>$rows[nama_barang] </option>";
							}
						echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Spesifikasi</label>
                        <textarea name="spesifikasi" required="required" class="form-control" placeholder="Jenis kegiatan .."></textarea>
                      </div>
					  
					  <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" required="required" class="form-control" placeholder="Jumlah usulan">
                      </div>
					  <div class="form-group">
                        <label>Satuan</label>
                        <input type="text" name="satuan" required="required" class="form-control" placeholder="Satuan">
                      </div>
					  <div class="form-group">
                        <label>Harga</label>
                        <input type="text" name="harga" required="required" class="form-control" placeholder="Harga satuan">
                      </div>
					  <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="keterangan" required="required" class="form-control" placeholder="Keterangan">
                      </div>
					  
					  
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_detusulan/<?php echo $r['id_usulan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="usulan<?php echo $r['id_detusulan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit detail usulan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
					<input type="hidden" name="id_detusulan" value="<?php echo $r['id_detusulan']; ?>" required="required" class="form-control" placeholder="Jumlah usulan">
                    <div class="modal-body">
				
					  <div class="form-group">
                       <label>Nama Barang</label>
                        <?php
						$barang =$this->db->query("Select * From barang ")->result_array();
						echo"
						<select class='form-control' name='id_barang' required>
                    <option value=''>- Pilih  Barang -</option>";
                    foreach ($barang as $row) {
                       if ($r['id_barang']==$row['id_barang']){
                            echo "<option value='$row[id_barang]' selected>$row[nama_barang]</option>";
                            }else{
                            echo "<option value='$row[id_barang]'>$row[nama_barang]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Spesifikasi</label>
                        <textarea name="spesifikasi" required="required" class="form-control"><?php echo $r['spesifikasi']; ?></textarea>
                      </div>
					  
					  <div class="form-group">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" value="<?php echo $r['jumlah']; ?>" required="required" class="form-control" placeholder="Jumlah usulan">
                      </div>
					  <div class="form-group">
                        <label>Satuan</label>
                        <input type="text" name="satuan" value="<?php echo $r['satuan']; ?>" required="required" class="form-control" placeholder="Satuan">
                      </div>
					  <div class="form-group">
                        <label>Harga</label>
                        <input type="text" name="harga" value="<?php echo $r['harga']; ?>" required="required" class="form-control" placeholder="Harga satuan">
                      </div>
					  <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="keterangan" value="<?php echo $r['keterangan']; ?>" required="required" class="form-control" placeholder="Keterangan">
                      </div>
					  
					  
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>