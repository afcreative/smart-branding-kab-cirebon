<div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <h4 class="mb-0"><button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">
			  <span class="btn-inner--icon"><i class="ni ni-atom"></i></span>
			  <span class="btn-inner--text">Tambah Usulan</span></button></h4>
              
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th width="50px">No</th>
						<th>Unit Kerja</th>
						<th>Jenis Kegiatan</th>
						<th>Tahun Anggaran</th>
						<th>Triwulan</th>
						<th>Sumber Dana</th>
						<th>Update Data</th>
						
						
                        <th width="100px">Action</th>
                  </tr>
                </thead>
                
                <tbody>
				<?php $no=1; foreach($record->result_array() as $r) { 
					
					
					?>
					
                      <tr>
                        <td><?php echo $no; ?></td>
						
						<td><?php echo $r['unit_kerja']; ?></td>
						<td><?php echo $r['jenis_kegiatan']; ?></td>
						<td><?php echo $r['tahun_anggaran']; ?></td>
						<td><?php echo $r['nama_periode']; ?></td>
						<td><?php echo $r['nama_dana']; ?></td>
						<td><?php echo $r['update_usulan']; ?></td>
						  <td>
						  <?php	echo"
					<a href='".base_url()."app/detail_usulan/$r[id_usulan]'><button type='button' class='btn btn-danger btn-sm' onclick=\"return confirm('Apa anda ingin manambah detail usulan')\">
                            <i class='fa fa-eye'></i></a>"; ?>
						  
						  <button title="Edit" type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#usulan<?php echo $r['id_usulan']; ?>">
                          <i class="fa fa-edit" ></i>
                        </button>
						
					<?php	echo"
					<a href='".base_url()."app/hapus_usulan/$r[id_usulan]'><button type='button' class='btn btn-warning btn-sm' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\">
                            <i class='fa fa-trash'></i></a><td>
					
					</tr>";
					 $no++; } ?> 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>
      </div>
	  
	  
	  <!-- Modal Input  -->
            <form action="<?php echo base_url(); ?>app/usulan" method="post"  enctype="multipart/form-data">
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Usulan Pengadaan Barang</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     
					  <div class="form-group">
                        <label>Unit Kerja</label>
                        <input type="text" name="unit_kerja" value="<?php echo $r['unit_kerja']; ?>" required="required" class="form-control" placeholder="Nama unit kerja ..">
                      </div>
					  <div class="form-group">
                        <label>Jenis Kegiatan</label>
                        <input type="text" name="jenis_kegiatan" value="<?php echo $r['jenis_kegiatan']; ?>" required="required" class="form-control" placeholder="Jenis kegiatan ..">
                      </div>
					  
					  <div class="form-group">
                        <label>Tahun Anggaran </label>
                        <input type="text" name="tahun_anggaran" value="<?php echo $r['tahun_anggaran']; ?>" required="required" class="form-control" placeholder="Nama Periode ..">
                      </div>
					  <div class="form-group">
                        <label>Triwulan</label>
                        <?php
						$periode =$this->db->query("Select * From periode ")->result_array();
						echo"
						<select class='form-control' name='id_periode' required>
							<option value=''>- Pilih Kategori periode -</option>";
							foreach ($periode as $rows) {
								echo "<option value='$rows[id_periode]'>$rows[nama_periode] </option>";
							}
						echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Sumber Dana</label>
                        <?php
						$dana =$this->db->query("Select * From dana ")->result_array();
						echo"
						<select class='form-control' name='id_dana' required>
							<option value=''>- Pilih Sumber Dana -</option>";
							foreach ($dana as $rows) {
								echo "<option value='$rows[id_dana]'>$rows[nama_dana] </option>";
							}
						echo "</select>
						";
						?>
                      </div>
					                    

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <!-- Modal Edit  -->
			<?php  foreach($record->result_array() as $r) { ?>
			             <form action="<?php echo base_url(); ?>app/edit_usulan/<?php echo $r['id_usulan']; ?>" method="post"  enctype="multipart/form-data">
        
			  <div class="modal fade" id="usulan<?php echo $r['id_usulan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit usulan</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                    
					  <div class="form-group">
                        <label>Unit Kerja</label>
                        <input type="text" name="unit_kerja" value="<?php echo $r['unit_kerja']; ?>" required="required" class="form-control" placeholder="Nama unit kerja ..">
                      </div>
					  <div class="form-group">
                        <label>Jenis Kegiatan</label>
                        <input type="text" name="jenis_kegiatan" value="<?php echo $r['jenis_kegiatan']; ?>" required="required" class="form-control" placeholder="Jenis kegiatan ..">
                      </div>
					  
					  <div class="form-group">
                        <label>Tahun Anggaran </label>
                        <input type="text" name="tahun_anggaran" value="<?php echo $r['tahun_anggaran']; ?>" required="required" class="form-control" placeholder="Nama Periode ..">
                      </div>
					  <div class="form-group">
                        <label>Triwulan</label>
                        <?php
						$periode =$this->db->query("Select * From periode ")->result_array();
						echo"
						<select class='form-control' name='id_periode' required>
                    <option value=''>- Pilih Periode -</option>";
                    foreach ($periode as $row) {
                       if ($r['id_periode']==$row['id_periode']){
                            echo "<option value='$row[id_periode]' selected>$row[nama_periode]</option>";
                            }else{
                            echo "<option value='$row[id_periode]'>$row[nama_periode]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
					  <div class="form-group">
                        <label>Sumber Dana</label>
                        <?php
						$dana =$this->db->query("Select * From dana ")->result_array();
						echo"
						<select class='form-control' name='id_dana' required>
                    <option value=''>- Pilih Periode -</option>";
                    foreach ($dana as $row) {
                       if ($r['id_dana']==$row['id_dana']){
                            echo "<option value='$row[id_dana]' selected>$row[nama_dana]</option>";
                            }else{
                            echo "<option value='$row[id_dana]'>$row[nama_dana]</option>";
                            }
                    }
                echo "</select>
						";
						?>
                      </div>
                      

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      <button type="submit" name="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
			 <?php } ?>