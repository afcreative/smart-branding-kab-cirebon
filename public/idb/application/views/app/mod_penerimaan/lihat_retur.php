	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Retur Bahan Baku </h4>
										<a href="<?php echo base_url(); ?>pembelian/receive" button class="btn btn-primary btn-round ml-auto" >
											<i class="fa fa-plus"></i>
											Retur
										</button></a>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Retur</th>
													<th>No Invoice</th>
													<th>Suplier</th>
													<th>Total</th>
													
													<th width="80px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											$rd=$this->db->query("Select sum(harga*qty)as totalx From detail_retur where id_retur='".$r['id_retur']."'")->row_array();
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_invoice']; ?></td>
													<td><button class="btn btn-warning" data-toggle="modal" data-target="#lihatDetail<?php echo "$r[id_retur]"; ?>">
														<?php echo $r['no_invoice']; ?>
													</button>
													</td>
													<td><?php echo $r['nama_suplier']; ?></td>
													<td><?php echo rupiah($rd['totalx']); ?></td>
													<td>
														<div class="form-button-action">
														
															<a href="<?php echo base_url();?>pembelian/edit_retur/<?php echo $r['id_retur']; ?>" button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  >
																<i class="fa fa-edit"></i>
															</button>
															
															<a href="<?php echo base_url();?>pembelian/hapus_retur/<?php echo $r['id_retur']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
	<?php $no=1; foreach($record->result_array() as $r) { 
	?>	
	<div class="modal fade" id="lihatDetail<?php echo "$r[id_retur]"; ?>">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Detail Retur bahan baku <?php echo $r['no_invoice']; ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
           <!-- Tabel daftar pelanggan -->
           <div class="table-responsive">
              <table class="display table table-striped table-hover" id="multi-filter-select" width="100%" cellspacing="0">
                <thead>
                  <tr>
					<th>No</th>
                    <th>Tanggal Retur</th>
                    <th>Nama Bahan baku</th>
                    <th>Qty</th>
                    <th>Harga</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
					$no=1;
                        $pelanggan = $this->db->query("Select * From detail_retur a, retur b, bahanbaku c  where a.id_bahanbaku=c.id_bahanbaku and  a.id_retur=b.id_retur and a.id_retur='".$r['id_retur']."' ")->result_array();
						foreach($pelanggan as $data){
							$total = $data['qty'] * $data['harga'];
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $data['tgl_retur']; ?></td>
                        <td><?php echo $data['nama_bahanbaku']; ?></td>
                        <td><?php echo $data['qty']; ?></td>
                        <td><?php echo $data['harga']; ?></td>
                        <td><?php echo rupiah($total) ;?></td>
                        
                    </tr>
                    <!-- bagian akhir (penutup) while -->
						<?php $no++; }  ?>
                </tbody>
              </table>
			  
            </div>
            <!-- bagian akhir Tabel daftar pelanggan -->
    <!-- akhir body -->
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>	
	<?php } ?>							
            