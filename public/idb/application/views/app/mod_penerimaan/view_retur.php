	<form action="<?php echo base_url(); ?>pembelian/retur" method="post">
	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Retur Penerimaan Bahan Baku </h4>
										
									</div>
									
									<table style="font-size: 14pt;
									font-weight: bold;">
									<tr><th width="150px">No Invoice </th><th>:</th><th><?php echo $row['no_invoice']; ?><input type="hidden" name="id_penerimaan" value="<?php echo $row['id_penerimaan']; ?>"></th></tr>
									<tr><th>Tanggal Penerimaan </th><th>:</th><th><?php echo $row['tgl_penerimaan']; ?></th></tr>
									<tr><th>Nama Suplier</th><th>:</th><th><?php echo $row['nama_suplier']; ?></th></tr>
									<tr><th>Tanggal terima</th><th>:</th><th><input type="date"value="<?php echo date('Y-m-d'); ?>" name="tgl_retur" class="form-control"></th></tr>
									</table>
									
																		
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Bahan Baku</th>
													<th>Qty</th>
													<th>Harga</th>
													<th>Subtotal</th>
													
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; $grantotal=0; foreach($record->result_array() as $r) { 
											
											$subtotal=$r['qty']*$r['harga'];
											$grantotal=$subtotal+$grantotal;
											?>
											
												<tr>
													<td><?php echo $no; ?><input type="hidden" name="id_bahanbaku[]" value="<?php echo $r['id_bahanbaku']; ?>"><input type="checkbox"name="box[]" style="width:25px;"></td>
													<td><?php echo $r['nama_bahanbaku']; ?></td>
													
													<td><input type="text" name="qty[]" value="<?php echo $r['qty']; ?>" ></td>
													<td><input type="text" name="harga[]" value="<?php echo rupiah($r['harga']); ?>" ></td>
													<td><?php echo rupiah($subtotal); ?></td>
													
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
										<div class="row">
											<div class="col-sm-2">
												
												<div class="form-group">
													<label>Total belanja:</label>
													<input name="total" value="<?php echo rupiah($grantotal); ?>"  type="text"  class="form-control" disabled>
												</div>
												
												
												<div class="form-group">
													<button  type="submit" name="submit" class="btn btn-success btn-block"><span class="text">Buat Retur</span></button>
												</div>
											</div>
</div>
									</div>
								</div>
							</div>
						</div>
			</div>
			</form>
							
            