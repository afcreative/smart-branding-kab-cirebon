	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Penerimaan Bahan baku </h4>
										
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Penerimaan</th>
													<th>Invoice Pembelian</th>
													<th>Suplier</th>
													<th>Total</th>
													
													<th width="80px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											$rd=$this->db->query("Select sum(harga*qty)as totalx From detail_penerimaan where id_penerimaan='".$r['id_penerimaan']."'")->row_array();
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_invoice']; ?></td>
													<td><button class="btn btn-warning" data-toggle="modal" data-target="#lihatDetail<?php echo "$r[id_penerimaan]"; ?>">
														<?php echo $r['no_invoice']; ?>
													</button>
													</td>
													<td><?php echo $r['nama_suplier']; ?></td>
													<td><?php echo rupiah($rd['totalx']); ?></td>
													<td>
														<div class="form-button-action">
														<a href="<?php echo base_url();?>pembelian/retur/<?php echo $r['id_penerimaan']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Retur Bahan Baku" <?php echo" onclick=\"return confirm('Penerimaan bahan baku dari suplier?')\" "; ?> >
																<i class="fa fa-history"></i>
															</button></a>
															
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
	<?php $no=1; foreach($record->result_array() as $r) { 
	?>	
	<div class="modal fade" id="lihatDetail<?php echo "$r[id_penerimaan]"; ?>">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Detail Penerimaan bahan baku <?php echo $r['no_invoice']; ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
           <!-- Tabel daftar pelanggan -->
           <div class="table-responsive">
              <table class="display table table-striped table-hover" id="multi-filter-select" width="100%" cellspacing="0">
                <thead>
                  <tr>
					<th>No</th>
                    <th>Tanggal Penerimaan</th>
                    <th>Nama Bahan baku</th>
                    <th>Qty</th>
                    <th>Harga</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
					$no=1;
                        $pelanggan = $this->db->query("Select * From detail_penerimaan a, penerimaan b, bahanbaku c  where a.id_bahanbaku=c.id_bahanbaku and  b.id_pembelian=b.id_pembelian and a.id_penerimaan='".$r['id_penerimaan']."' ")->result_array();
						foreach($pelanggan as $data){
							$total = $data['qty'] * $data['harga'];
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>
						<td><?php echo $data['tgl_penerimaan']; ?></td>
                        <td><?php echo $data['nama_bahanbaku']; ?></td>
                        <td><?php echo $data['qty']; ?></td>
                        <td><?php echo $data['harga']; ?></td>
                        <td><?php echo rupiah($total) ;?></td>
                        
                    </tr>
                    <!-- bagian akhir (penutup) while -->
						<?php $no++; }  ?>
                </tbody>
              </table>
			  
            </div>
            <!-- bagian akhir Tabel daftar pelanggan -->
    <!-- akhir body -->
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>	
	<?php } ?>							
            