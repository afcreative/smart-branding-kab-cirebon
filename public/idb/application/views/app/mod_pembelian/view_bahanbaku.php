	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Bahan Baku</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Bahan Baku
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Bahan Baku</th>
													<th>Satuan</th>
													<th>Deskripsi</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_bahanbaku']; ?></td>
													<td><?php echo $r['nama_satuan']; ?></td>
													<td><?php echo $r['deskripsi']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editBahan Baku<?php echo "$r[id_bahanbaku]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>app/hapus_bahanbaku/<?php echo $r['id_bahanbaku']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>pembelian/bahanbaku" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Bahan Baku
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Bahan Baku</label>
																	<input  type="text" name="nama_bahanbaku" class="form-control" placeholder="Nama Bahan Baku">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Satuan</label>
																	<?php
																	$satuan =$this->db->query("Select * From satuan order by id_satuan ASC")->result_array();
																	echo"
																	<select name='id_satuan' class='form-control' required>
																		<option value='' selected>- Pilih Satuan  -</option>";
																			foreach ($satuan as $row){
																				if ($r['id_satuan']==$row['id_satuan']){
																					echo "<option value='$row[id_satuan]' selected>$row[nama_satuan]</option>";
																						}else{
																							echo "<option value='$row[id_satuan]'>$row[nama_satuan]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Deskripsi</label>
																	<textarea name="deskripsi" rows="3" class="form-control" ></textarea>
																	
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>pembelian/edit_bahanbaku/<?php echo $r['id_bahanbaku']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editBahan Baku<?php echo $r['id_bahanbaku']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Bahan Baku
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Bahan Baku</label>
																	<input  type="text" name="nama_bahanbaku" value="<?php echo $r['nama_bahanbaku']; ?>" class="form-control" placeholder="Nama Bahan Baku">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Satuan</label>
																	<?php
																	$satuan =$this->db->query("Select * From satuan order by id_satuan ASC")->result_array();
																	echo"
																	<select name='id_satuan' class='form-control' required>
																		<option value='' selected>- Pilih Satuan  -</option>";
																			foreach ($satuan as $row){
																				if ($r['id_satuan']==$row['id_satuan']){
																					echo "<option value='$row[id_satuan]' selected>$row[nama_satuan]</option>";
																						}else{
																							echo "<option value='$row[id_satuan]'>$row[nama_satuan]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Deskripsi</label>
																	<textarea name="deskripsi" rows="3" class="form-control" ><?php echo $r['deskripsi']; ?></textarea>
																	
																</div>
															</div>

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
            