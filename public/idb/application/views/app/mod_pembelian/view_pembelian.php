	<?php 
	
	$pel="PG.";
	$y=substr($pel,0,2);
	$cek=$this->db->query("Select * from pembelian where substr(no_invoice,1,2)='$y' order by no_invoice desc limit 0,1");
	$rowx=$cek->num_rows();
	$data=$cek->row_array();

	if ($rowx >0){
	$no=substr($data['no_invoice'],-3) +1;
	}
	else{
	$no=1;
	}
	$nourut=1000+$no;
	$no_pel=$pel.substr($nourut,-3);
	
	
	?>
	
	<form action="<?php echo base_url(); ?>pembelian/permintaan" method="post">
	<div class="card shadow mb-4">
	<div class="card-header py-3">
          <div class="row">
              <div class="col-sm-6">
                  <h5 style="text-align:left">No Invoice : <?php echo $no_pel; ?></h5>
				  <input type="hidden" name="no_invoice" value="<?php echo $no_pel; ?>">
              </div>
              <div class="col-sm-6">
                <h5 style="text-align:right">Tanggal : <?php echo date('Y-m-d'); ?> </h5>
				<input type="hidden" name="tgl_invoice" value="<?php echo date('Y-m-d'); ?>">
              </div>
        
          </div>
      </div>
	
	<div class="row">
	<div class="col-md-12">
	
	<div class="card-body">
          <!-- rows -->
            <div class="row">
              <div class="col-sm-4">
                  <!-- Overflow Hidden -->
                  <div class="card mb-4">
                      <div class="card-header py-3">
                        <h5 class="m-0 font-weight-bold text-primary">Tambah PO Bahan</h5>
                      </div>
                      <div class="card-body">
                        <!-- rows -->
                          <div class="row">
                              <div class="col-sm-12">
                                  <div class="form-group">
                                    <button type="button" data-toggle="modal" data-target="#modal" class="btn btn-primary"><span class="text"><i class="fas fa-shopping-bag fa-sm"></i> Pilih Bahan</span></button>
                                  </div>
                              </div>
                            </div>
                          <!-- rows -->
                          <div class="row">
                              <div class="col-sm-12">
                                <div id="tampil_deskripsi_bahanbaku">
                                </div>
                                  <div class="form-group">
                                        <label>Bahan baku:</label>
                                        <input name="id_bahanbaku" type="hidden" id="id_bahanbaku" type="text" class="form-control">
										<input name="id_temp" type="hidden" id="id_temp" type="text" class="form-control">
                                        <input name="nama_bahanbaku" id="nama_bahanbaku" type="text" class="form-control" disabled>
										
                                  </div>
                                  <div class="form-group">
                                        <label>Harga:</label>
                                        <input name="harga_beli" id="harga_beli"  type="text" class="form-control" >
                                  </div>
                                  <div class="form-group">
                                        <label>Jumlah order : </label>
                                        
                                        <input name="jml_beli" id="jml_beli" type="text"   class="form-control">
                                  </div>
                                  
                                  <div class="form-group">
                                        <button  type="button" id="tambahkan_ke_keranjang" aksi="tambahkan_ke_keranjang"  class="btn btn-primary btn-block"><span class="text"><i class="fas fa-shopping-cart fa-sm"></i> Masukan keranjang</span></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-sm-8">
                <!-- Overflow Hidden -->
                <div class="card mb-4">
                    <div class="card-header py-3">
                      
                      <h4>Pilih Suplier : - <a href=""  data-toggle="modal" data-target="#pilih_suplier" ><b>Pilih</b></a></h>
					  <input name="id_suplier" id="id_suplier"  type="hidden"  class="form-control">
					  <input name="nama_suplier" id="nama_suplier"  type="text"  class="form-control" disabled width="50%">
                    </div>
                    <div class="card-body">
                        <!--Menampilkan cart (keranjang belanja) --> 
                        <table class="table table-bordered table-striped" style="font-size:12px;">
                    <thead>
                      <tr>
                        <th width="50px">.::.</th>
                        <th>Qty </th>
						<th>Nama Produk</th>
						<th>Harga</th>
						
                        <th width="200px">Action</th>
                      </tr>
                    </thead>
                        <tbody id="targetData">
        
						</tbody>
                    
                  </table>
				  <div class="row">
    <div class="col-sm-7">
        
        <div class="form-group">
            <label>Total belanja:</label>
            <input name="total" id="total"  type="text"  class="form-control">
        </div>
        
        
        <div class="form-group">
            <button  type="submit" name="submit" class="btn btn-success btn-block"><span class="text">Buat Transaksi</span></button>
        </div>
    </div>
</div>
                    </div>
                </div>
              </div>
          </div>
        <!-- rows -->
			</div>				
		</div>		
	</div>
</div>	
</form>
<div class="modal fade" id="modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Bagian header -->
      <div class="modal-header">
        <h4 class="modal-title" id="judul"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Bagian body -->
      <div class="modal-body">
        <!-- Tabel daftar bahanbaku -->
        <div class="table-responsive">
              <table class="display table table-striped table-hover" id="basic-datatables" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Bahan Baku</th>
                    <th>Satuan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        
                        $no=0;
                        $bahanbaku = $this->db->query("Select * from bahanbaku a, satuan b  where a.id_satuan=b.id_satuan ")->result_array();
                        foreach ($bahanbaku as $data ){
                    ?>
                    <tr>
                        <td><?php echo $data['nama_bahanbaku']; ?></td>
                        <td><?php echo $data['nama_satuan']; ?></td>
                        <td>
                        <button type="button" class="btn-pilih-bahanbaku btn btn-primary btn-block" id_bahanbaku="<?php echo $data['id_bahanbaku']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-paper-plane fa-sm"></i> Pilih</span></button>
                        </td>
                    </tr>
                    <!-- bagian akhir (penutup) while -->
                    <?php } ?>
                </tbody>
              </table>
            </div>
      </div>
      <!-- Bagian footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pilih_suplier">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Pilih Suplier</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
           <!-- Tabel daftar suplier -->
           <div class="table-responsive">
              <table class="display table table-striped table-hover" id="multi-filter-select" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>No Telp</th>
                    <th>Alamat</th>
                    <th>Email</th>
                    <th width="15%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        $suplier = $this->db->query("Select * From suplier ")->result_array();
						foreach($suplier as $data){
                    ?>
                    <tr>
                        <td><?php echo $data['nama_suplier']; ?></td>
                        <td><?php echo $data['no_telp']; ?></td>
                        <td><?php echo $data['alamat']; ?></td>
                        <td><?php echo $data['email'] ;?></td>
                        <td>
                        <button class="btn-pilih-suplier btn btn-primary btn-block" id_suplier="<?php echo $data['id_suplier']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-address-card fa-sm"></i> Pilih</span></button>
                        </td>
                    </tr>
                    <!-- bagian akhir (penutup) while -->
						<?php }  ?>
                </tbody>
              </table>
			  
            </div>
            <!-- bagian akhir Tabel daftar suplier -->
    <!-- akhir body -->
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
		
	<script>
	ambilData();
	ambilTemp();
			function ambilData(){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url()."index.php/pembelian/ambildata" ?>',
					dataType: 'json',
					success: function(data){
						var baris='';
						$no=1;
						for (var i=0;i<data.length; i++){
							baris += '<tr>'+
									'<td>'+ $no+'</td>' +
									'<td>'+ data[i].qty+ '</td>' +
									'<td>'+ data[i].nama_bahanbaku+ '</td>' +
									'<td>'+ data[i].harga+ '</td>' +
									'<td><a  class="btn-edit-bahanbaku btn btn-primary" onclick="editdata('+data[i].id_temp+')"><i class="fa fa-edit"></i> </a> <a  onclick="hapusdata('+data[i].id_temp+')" class="btn btn-warning"><i class="fa fa-eraser"></i> </a></td>' +
									'</tr>';
									$no++;
						}
						$('#targetData').html(baris);
					}
				});
				
			}
			//Event saat pengguna memilih bahanbaku yang ingin dibeli
		  $('.btn-pilih-bahanbaku').on('click',function(){
			  var id_bahanbaku = $(this).attr("id_bahanbaku");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/pembelian/ambilbahanbaku" ?>',
				  method: 'post',
				  
				  data: {id_bahanbaku:id_bahanbaku},
				  dataType :'json',
				  success:function(data){
					 
					  $('#tampil_deskripsi_bahanbaku').html(data);
					  $('[name="id_bahanbaku"]').val(data[0].id_bahanbaku);
					  $('[name="nama_bahanbaku"]').val(data[0].nama_bahanbaku);
				  }
			  }); 		  
		  });
		  
		  
		  
		  
		  // event hapus detail penjualan 
		  function hapusdata(id_temp){
				var tanya=confirm('Apakah anda yahin akan menghapus data ini');
				
				if(tanya){
					$.ajax({
						type:'POST',
						data:'id_temp='+id_temp,
						url:'<?php echo base_url()."index.php/pembelian/hapustemp" ?>',
						success: function(){
						ambilData();
						ambilTemp();						
						}
					});
					
				}
			}
			function editdata(id_temp){
				var tanya=confirm('Apakah anda akan mengedit');
				
				if(tanya){
					$.ajax({
						type:'POST',
						data:'id_temp='+id_temp,
						dataType :'json',
						url:'<?php echo base_url()."index.php/pembelian/edittemp" ?>',
						success:function(data){
					 
					  $('#tampil_deskripsi_bahanbaku').html(data);
					  $('[name="id_bahanbaku"]').val(data[0].id_bahanbaku);
					  $('[name="id_temp"]').val(data[0].id_temp);
					  $('[name="nama_bahanbaku"]').val(data[0].nama_bahanbaku);
					  $('[name="harga_beli"]').val(data[0].harga);
					  $('[name="jml_beli"]').val(data[0].qty);
					  ambilTemp();
					  
				  }
					});
					
				}
			}
			function ambilTemp(){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url()."index.php/pembelian/ambiltemp" ?>',
					dataType: 'json',
					success: function(data){
						$('[name="total"]').val(data[0].total);
					}
				});
				
			}
		  
	
		  
		  $('.btn-pilih-suplier').on('click',function(){
			  var id_suplier = $(this).attr("id_suplier");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/pembelian/ambilsuplier" ?>',
				  method: 'post',
				  
				  data: {id_suplier:id_suplier},
				  dataType :'json',
				  success:function(data){
					 
					 $('#tampil_deskripsi_suplier').html(data);
					  $('[name="id_suplier"]').val(data[0].id_suplier);
					  $('[name="nama_suplier"]').val(data[0].nama_suplier);
				  }
			  }); 		  
		  });
		  
		  //Event saat tombol tambahkan keranjang dklik
		  $('#tambahkan_ke_keranjang').on('click',function(){
			  var aksi = $(this).attr("aksi");
			  var jml_beli=$("#jml_beli").val();
			  var harga_beli=$("#harga_beli").val();
			  var id_bahanbaku=$("#id_bahanbaku").val();
			  var id_temp=$("#id_temp").val();
			  
			  $.ajax({
				  url: '<?php echo base_url()."index.php/pembelian/keranjang" ?>',
				  method: 'POST',
				  data:{jml_beli:jml_beli,id_bahanbaku:id_bahanbaku,harga_beli:harga_beli,id_temp:id_temp},
				  success:function(data){
					  ambilData();
					  $('[name="jml_beli"]').val('');
					  $('[name="id_bahanbaku"]').val('');
					  $('[name="harga_beli"]').val('');
					  $('[name="nama_bahanbaku"]').val('');
					  $('[name="id_temp"]').val('');
					  ambilTemp();
				  }
			  }); 
		  });
	</script>	
            