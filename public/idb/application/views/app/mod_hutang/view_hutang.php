	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Hutang</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Hutang
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Hutang</th>
													<th>Nama </th>
													<th>Alamat</th>
													<th>Potongan</th>
													<th>Lama Pinjaman</th>
													<th>Jumlah Hutang</th>
													<th>Tgl Akhir Bayar</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_hutang']; ?></td>
													<td><?php echo $r['nama_karyawan']; ?></td>
													<td><?php echo $r['alamat']; ?></td>
													<td><?php echo rupiah($r['jml_tagihan']); ?></td>
													<td><?php echo $r['lama_pinjaman']; ?></td>
													<td><?php echo rupiah($r['tot_hutang']); ?>
													<td><?php echo $r['sampai_tgl']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editHutang<?php echo "$r[id_hutang]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>keuangan/hapus_hutang/<?php echo $r['id_hutang']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
			
							<form action="<?php echo base_url(); ?>keuangan/hutang" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Hutang
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal</label>
																	<input  type="date" name="tgl_hutang" class="form-control" placeholder="Tanggal Hutang">
																</div>
															</div>

															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Karyawan</label>
																	<input  type="hidden" name="id_karyawan" id="id_karyawan" class="form-control" placeholder= >
																	<input  type="text" name="nama_karyawan" id="nama_karyawan" class="form-control" placeholder="Pilih disini" data-toggle="modal" data-target="#modalKaryawan" >
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah</label>
																	<input  type="text" name="tot_hutang" class="form-control" placeholder="Jumlah">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Lama Pinjaman (Bulan)</label>
																	<input  type="text" name="lama_pinjaman" class="form-control" placeholder="Lama pinjaman">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Keterangan</label>
																	<textarea name="ket" class="form-control"rows="3"></textarea>
																</div>
															</div>
															
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
						<?php $no=1; foreach($record->result_array() as $r) { 
											
					
											?>			
						<form action="<?php echo base_url(); ?>keuangan/edit_hutang/<?php echo $r['id_hutang']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editHutang<?php echo $r['id_hutang']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Edit Hutang
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal</label>
																	<input  type="date" name="tgl_hutang" value="<?php echo $r['tgl_hutang']; ?>" class="form-control" placeholder="Tanggal Hutang">
																</div>
															</div>

															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Karyawan</label>
																	<input  type="hidden" name="id_karyawan" value="<?php echo $r['id_karyawan']; ?>" id="id_karyawan" class="form-control" placeholder= >
																	<input  type="text" name="nama_karyawan" value="<?php echo $r['nama_karyawan']; ?>" id="nama_karyawan" class="form-control" placeholder="Pilih disini" data-toggle="modal" data-target="#modalKaryawan" >
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah</label>
																	<input  type="text" name="tot_hutang" value="<?php echo $r['tot_hutang']; ?>" class="form-control" placeholder="Jumlah">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Lama Pinjaman (Bulan)</label>
																	<input  type="text" name="lama_pinjaman" value="<?php echo $r['lama_pinjaman']; ?>" class="form-control" placeholder="Lama pinjaman">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Keterangan</label>
																	<textarea name="ket" class="form-control"rows="3"> <?php echo $r['ket']; ?></textarea>
																</div>
															</div>
															
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
						<?php } ?>
	<div class="modal fade" id="modalKaryawan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Bagian header -->
      <div class="modal-header">
        <h4 class="modal-title" id="judul"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Bagian body -->
      <div class="modal-body">
        <!-- Tabel daftar produk -->
        <div class="table-responsive">
              <table class="display table table-striped table-hover" id="basic-datatables" width="100%" cellspacing="0">
                <thead>
                  <tr>
				  <th>No</th>
                    <th>Nama Karyawan</th>
                    <th>Alamat</th>
                    <th>No HP</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        
                        $no=1;
                        $karyawan = $this->db->query("Select * from karyawan")->result_array();
                        foreach ($karyawan as $data ){
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data['nama_karyawan']; ?></td>
                        <td><?php echo $data['alamat']; ?></td>
                        <td><?php echo $data['no_hp']; ?></td>
                        <td>
                        <button type="button" class="btn-pilih-karyawan btn btn-primary btn-block" id_karyawan="<?php echo $data['id_karyawan']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-paper-plane fa-sm"></i> Pilih</span></button>
                        </td>
                    </tr>
                    <!-- bagian akhir (penutup) while -->
                    <?php $no++; } ?>
                </tbody>
              </table>
            </div>
      </div>
      <!-- Bagian footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	
<script>
$('.btn-pilih-karyawan').on('click',function(){
			  var id_karyawan = $(this).attr("id_karyawan");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/penjualan/ambilKaryawan" ?>',
				  method: 'post',
				  
				  data: {id_karyawan:id_karyawan},
				  dataType :'json',
				  success:function(data){
					 
					  $('#tampil_deskripsi_produk').html(data);
					  $('[name="id_karyawan"]').val(data[0].id_karyawan);
					  $('[name="nama_karyawan"]').val(data[0].nama_karyawan);
				  }
			  }); 		  
		  });
</script>				