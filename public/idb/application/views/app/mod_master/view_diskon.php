	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Diskon</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Diskon
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Diskon</th>
													<th>Prosentase</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_diskon']; ?></td>
													<td><?php echo $r['prosentase']; ?>&nbsp %</td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editDiskon<?php echo "$r[id_diskon]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>app/hapus_diskon/<?php echo $r['id_diskon']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>app/diskon" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Diskon
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Diskon</label>
																	<input  type="text" name="nama_diskon" class="form-control" placeholder="Nama Diskon">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Prosentase</label>
																	<input  type="text" name="prosentase" class="form-control" placeholder="Prosentase">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>app/edit_diskon/<?php echo $r['id_diskon']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editDiskon<?php echo $r['id_diskon']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Diskon
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Diskon</label>
																	<input  type="text" name="nama_diskon" value="<?php echo $r['nama_diskon']; ?>" class="form-control" placeholder="nama_diskon">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Prosentase</label>
																	<input  type="text" name="prosentase" value="<?php echo $r['prosentase']; ?>" class="form-control" placeholder="Prosentase">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
            