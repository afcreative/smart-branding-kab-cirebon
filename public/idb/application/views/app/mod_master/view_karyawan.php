	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Karyawan</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Karyawan
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>NIK </th>
													<th>Nama Karyawan</th>
													<th>Jabatan</th>
													<th>Bagian</th>
													<th>Tanggal lahir</th>
													<th>Status</th>
													<th>Alamat</th>
													<th>No Hp</th>
													<th>Keterangan</th>
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nik']; ?></td>
													<td><?php echo $r['nama_karyawan']; ?></td>
													<td><?php echo $r['nama_jabatan']; ?></td>
													<td><?php echo $r['nama_bagian']; ?></td>
													<td><?php echo $r['tgl_lahir']; ?></td>
													<td><?php echo $r['status']; ?></td>
													<td><?php echo $r['alamat']; ?></td>
													<td><?php echo $r['no_hp']; ?></td>
													<td><?php echo $r['keterangan']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editKaryawan<?php echo "$r[id_karyawan]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>database/hapus_karyawan/<?php echo $r['id_karyawan']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>database/karyawan" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Karyawan
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>NIK</label>
																	<input  type="text" name="nik" class="form-control" placeholder="NIK Karyawan">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Karyawan</label>
																	<input  type="text" name="nama_karyawan" class="form-control" placeholder="Nama Karyawan">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jabatan</label>
																	<?php
																	$jabatan =$this->db->query("Select * From jabatan order by id_jabatan ASC")->result_array();
																	echo"
																	<select name='id_jabatan' class='form-control' required>
																		<option value='' selected>- Pilih Jabatan  -</option>";
																			foreach ($jabatan as $row){
																				if ($r['id_jabatan']==$row['id_jabatan']){
																					echo "<option value='$row[id_jabatan]' selected>$row[nama_jabatan]</option>";
																						}else{
																							echo "<option value='$row[id_jabatan]'>$row[nama_jabatan]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Bagian</label>
																	<?php
																	$bagian =$this->db->query("Select * From bagian order by id_bagian ASC")->result_array();
																	echo"
																	<select name='id_bagian' class='form-control' required>
																		<option value='' selected>- Pilih Bagian  -</option>";
																			foreach ($bagian as $row){
																				if ($r['id_bagian']==$row['id_bagian']){
																					echo "<option value='$row[id_bagian]' selected>$row[nama_bagian]</option>";
																						}else{
																							echo "<option value='$row[id_bagian]'>$row[nama_bagian]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal lahir</label>
																	<input  type="date" name="tgl_lahir" class="form-control" placeholder="Tanggal lahir">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Status</label>
																	<select  name="status" class="form-control">
																	<option value="">Pilih Status</option>
																	<option value="kontrak">Kontrak</option>
																	<option value="tetap">Tetap</option>
																	</select>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Alamat</label>
																	<textarea name="alamat" class="form-control">
																	</textarea>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Hp</label>
																	<input  type="text" name="no_hp" class="form-control" placeholder="No Hp">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Keterangan</label>
																	<input  type="text" name="keterangan" class="form-control" placeholder="Keterangan">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>database/edit_karyawan/<?php echo $r['id_karyawan']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editKaryawan<?php echo $r['id_karyawan']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Karyawan
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>NIK</label>
																	<input  type="text" name="nik" value="<?php echo $r['nik']; ?>" class="form-control" placeholder="NIK Karyawan">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Karyawan</label>
																	<input  type="text" name="nama_karyawan" value="<?php echo $r['nama_karyawan']; ?>" class="form-control" placeholder="Nama Karyawan">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jabatan</label>
																	<?php
																	$jabatan =$this->db->query("Select * From jabatan order by id_jabatan ASC")->result_array();
																	echo"
																	<select name='id_jabatan' class='form-control' required>
																		<option value='' selected>- Pilih Jabatan  -</option>";
																			foreach ($jabatan as $row){
																				if ($r['id_jabatan']==$row['id_jabatan']){
																					echo "<option value='$row[id_jabatan]' selected>$row[nama_jabatan]</option>";
																						}else{
																							echo "<option value='$row[id_jabatan]'>$row[nama_jabatan]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Bagian</label>
																	<?php
																	$bagian =$this->db->query("Select * From bagian order by id_bagian ASC")->result_array();
																	echo"
																	<select name='id_bagian' class='form-control' required>
																		<option value='' selected>- Pilih Bagian  -</option>";
																			foreach ($bagian as $row){
																				if ($r['id_bagian']==$row['id_bagian']){
																					echo "<option value='$row[id_bagian]' selected>$row[nama_bagian]</option>";
																						}else{
																							echo "<option value='$row[id_bagian]'>$row[nama_bagian]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal lahir</label>
																	<input  type="date" name="tgl_lahir" value="<?php echo $r['tgl_lahir']; ?>" class="form-control" placeholder="Tanggal lahir">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Status</label>
																	<select  name="status" class="form-control">
																	<option value="<?php echo $r['id_jabatan']; ?>"><?php $jab=$this->db->query("Select * From jabatan where id_jabatan='".$r['id_jabatan']."'")->row_array(); echo $jab['nama_jabatan']; ?></option>
																	<option value="">Pilih Status</option>
																	<option value="kontrak">Kontrak</option>
																	<option value="tetap">Tetap</option>
																	</select>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Alamat</label>
																	<textarea name="alamat" class="form-control">
																	 <?php echo $r['alamat']; ?></textarea>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Hp</label>
																	<input  type="text" name="no_hp" value="<?php echo $r['no_hp']; ?>" class="form-control" placeholder="No Hp">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Keterangan</label>
																	<input  type="text" name="keterangan" value="<?php echo $r['keterangan']; ?>" class="form-control" placeholder="Keterangan">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
            