	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Backup</h4>
										<a href="<?php echo base_url();?>app/backupdb" button class="btn btn-primary btn-round ml-auto" >
											<i class="fa fa-plus"></i>
											Backup Database
										</button></a>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Files</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
															<?php
											$dir='./backup/';
											$no=1;
											$dh=opendir($dir) or die('error');
											while(($f=readdir($dh)) !== false){
												if(is_file($dir.$f)){ ?>
													<tr><td><?php echo $no; ?></td><td><?php echo  $f ; ?> </td><td>
													<a href="<?php echo base_url();?><?php echo  $dir.$f ; ?> "><button title="Edit" type="button" class="btn btn-warning btn-sm" >
																  <i class="fa fa-download" ></i>
																</button></a> &nbsp
																<a href="<?php echo base_url();?>app/hapusbackup/<?php echo  $f ; ?> "><button title="Edit" type="button" class="btn btn-warning btn-sm" >
																  <i class="fa fa-trash" ></i>
																</button></a>
																</td></tr>
												<?php } $no++;
											}
											closedir($dh);
										?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							
            