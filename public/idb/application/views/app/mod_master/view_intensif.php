	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Intensif</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Intensif
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Intensif</th>
													<th>Jumlah</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_intensif']; ?></td>
													<td><?php echo rupiah($r['jumlah']); ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editIntensif<?php echo "$r[id_intensif]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>database/hapus_intensif/<?php echo $r['id_intensif']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>database/intensif" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Intensif
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Intensif</label>
																	<input  type="text" name="nama_intensif" class="form-control" placeholder="Nama Intensif">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah</label>
																	<input  type="text" name="jumlah" class="form-control" placeholder="Gaji Pokok">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>database/edit_intensif/<?php echo $r['id_intensif']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editIntensif<?php echo $r['id_intensif']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Intensif
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Intensif</label>
																	<input  type="text" name="nama_intensif" value="<?php echo $r['nama_intensif']; ?>" class="form-control" placeholder="nama_intensif">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah</label>
																	<input  type="text" name="jumlah" value="<?php echo $r['jumlah']; ?>" class="form-control" placeholder="jumlah">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
            