
	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Menu</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Menu
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Menu </th>
													<th>Icon</th>
													<th>Aktif</th>
													<th>Urutan</th>
													<th>Url</th>
													<th>Kelompok</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_menu']; ?></td>
													<td><?php echo $r['icon']; ?></td>
													<td><?php echo $r['aktif']; ?></td>
													<td><?php echo $r['urutan']; ?></td>
													<td><?php echo $r['url']; ?></td>
													<td><?php 
													if ($r['kelompok'] =='1'){
														echo"TRANSACTION";
													}elseif ($r['kelompok'] =='2'){
														echo"FINANCE & ACCOUNTING";
													}else{
														echo"DATABASE";
													}
													?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editMenu<?php echo "$r[id_menu]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>app/hapus_menu/<?php echo $r['id_menu']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>app/menu" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Menu
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Menu</label>
																	<input  type="text" name="nama_menu" class="form-control" placeholder="Nama Menu">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<div class="form-check">
																	<label>Aktif</label><br/>
																	<label class="form-radio-label">
																		<input class="form-radio-input" type="radio" name="aktif" value="Y"  checked="">
																		<span class="form-radio-sign">Y</span>
																	</label>
																	<label class="form-radio-label ml-3">
																		<input class="form-radio-input" type="radio" name="aktif" value="T">
																		<span class="form-radio-sign">T</span>
																	</label>
																</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Icon</label>
																	<input  type="text" name="icon" class="form-control" placeholder="Icon">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Urutan</label>
																	<input  type="text" name="urutan" class="form-control" placeholder="Urutan">
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Url</label>
																	<input  type="text" name="url" class="form-control" placeholder="Url">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Kelompok</label>
																	<select   name="kelompok" class="form-control" >
																	<option value="">Pilih Kelompok</option>
																	<option value="1">TRANSACTION</option>
																	<option value="2">FINANCE & ACCOUNTING</option>
																	<option value="3">DATABASE</option>
																	</select>
																</div>
															</div>
															
															
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>app/edit_menu/<?php echo $r['id_menu']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editMenu<?php echo $r['id_menu']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Menu
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Menu</label>
																	<input  type="text" name="nama_menu" value="<?php echo $r['nama_menu']; ?>" class="form-control" placeholder="Nama Menu">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<div class="form-check">
																	<label>Aktif</label><br/>
																	<label class="form-radio-label">
																		<input class="form-radio-input" type="radio" name="aktif" value="Y"  checked="">
																		<span class="form-radio-sign">Y</span>
																	</label>
																	<label class="form-radio-label ml-3">
																		<input class="form-radio-input" type="radio" name="aktif" value="N">
																		<span class="form-radio-sign">T</span>
																	</label>
																</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Icon</label>
																	<input  type="text" name="icon" value="<?php echo $r['icon']; ?>" class="form-control" placeholder="Icon">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Urutan</label>
																	<input  type="text" name="urutan" value="<?php echo $r['urutan']; ?>" class="form-control" placeholder="Urutan">
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Url</label>
																	<input  type="text" name="url" value="<?php echo $r['url']; ?>" class="form-control" placeholder="Url">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Kelompok</label>
																	<select   name="kelompok" class="form-control" >
																	<option value="">Pilih Kelompok</option>
																	<option value="1">TRANSACTION</option>
																	<option value="2">FINANCE & ACCOUNTING</option>
																	<option value="3">DATABASE</option>
																	</select>
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
            