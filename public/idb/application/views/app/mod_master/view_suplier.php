	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Suplier</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Suplier
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Suplier</th>
													<th>Email</th>
													<th>No Telp</th>
													<th>Alamat</th>
													<th>Status</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_suplier']; ?></td>
													<td><?php echo $r['email']; ?></td>
													<td><?php echo $r['no_telp']; ?></td>
													<td><?php echo $r['alamat']; ?></td>
													<td><?php echo $r['status']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editSuplier<?php echo "$r[id_suplier]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>app/hapus_suplier/<?php echo $r['id_suplier']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>app/suplier" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Suplier
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Suplier</label>
																	<input  type="text" name="nama_suplier" class="form-control" placeholder="Nama Suplier">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Email</label>
																	<input  type="text" name="email" class="form-control" placeholder="Email">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Telp</label>
																	<input  type="text" name="no_telp" class="form-control" placeholder="No Telp">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Alamat</label>
																	<textarea  name="alamat" class="form-control"rows="3"></textarea>
																	
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<div class="form-check">
																	<label>Status</label><br/>
																	<label class="form-radio-label">
																		<input class="form-radio-input" type="radio" name="status" value="Y"  checked="">
																		<span class="form-radio-sign">Y</span>
																	</label>
																	<label class="form-radio-label ml-3">
																		<input class="form-radio-input" type="radio" name="status" value="N">
																		<span class="form-radio-sign">N</span>
																	</label>
																</div>
																</div>
															</div>
															
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>app/edit_suplier/<?php echo $r['id_suplier']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editSuplier<?php echo $r['id_suplier']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Suplier
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Suplier</label>
																	<input  type="text" name="nama_suplier" value="<?php echo $r['nama_suplier']; ?>" class="form-control" placeholder="nama_suplier">
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Email</label>
																	<input  type="text" name="email" value="<?php echo $r['email']; ?>" class="form-control" placeholder="Email">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Telp</label>
																	<input  type="text" name="no_telp" value="<?php echo $r['no_telp']; ?>" class="form-control" placeholder="No Telp">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Alamat</label>
																	<textarea  name="alamat" class="form-control"rows="3"><?php echo $r['alamat']; ?></textarea>
																	
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<div class="form-check">
																	<label>Status</label><br/>
																	<label class="form-radio-label">
																		<input class="form-radio-input" type="radio" name="status" value="Y"  checked="">
																		<span class="form-radio-sign">Y</span>
																	</label>
																	<label class="form-radio-label ml-3">
																		<input class="form-radio-input" type="radio" name="status" value="N">
																		<span class="form-radio-sign">N</span>
																	</label>
																</div>
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
            