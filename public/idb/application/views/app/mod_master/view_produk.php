	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Produk</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Produk
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Kode Produk</th>
													<th>Nama Produk</th>
													<th>Satuan</th>
													<th>Kategori</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['kode_produk']; ?></td>
													<td><?php echo $r['nama_produk']; ?></td>
													<td><?php echo $r['nama_satuan']; ?></td>
													<td><?php echo $r['nama_kategori']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editUser<?php echo "$r[id_produk]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>app/hapus_produk/<?php echo $r['id_produk']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>app/produk" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Produk
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Kode Produk</label>
																	<input  type="text" name="kode_produk" class="form-control" placeholder="Kode Produk">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Produk</label>
																	<input  type="text" name="nama_produk" class="form-control" placeholder="Nama Produk">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Deskripsi Produk</label>
																	<textarea  type="text" name="deskripsi" class="form-control" rows="3"></textarea>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Kategori Produk</label>
																	<?php
																	$kategori =$this->db->query("Select * From kategori order by id_kategori ASC")->result_array();
																	echo"
																	<select name='id_kategori' class='form-control' required>
																		<option value='' selected>- Pilih Kategori Produk  -</option>";
																			foreach ($kategori as $row){
																				if ($r['id_kategori']==$row['id_kategori']){
																					echo "<option value='$row[id_kategori]' selected>$row[nama_kategori]</option>";
																						}else{
																							echo "<option value='$row[id_kategori]'>$row[nama_kategori]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Satuan</label>
																	<?php
																	$satuan =$this->db->query("Select * From satuan order by id_satuan ASC")->result_array();
																	echo"
																	<select name='id_satuan' class='form-control' required>
																		<option value='' selected>- Pilih Satuan Produk  -</option>";
																			foreach ($satuan as $row){
																				if ($r['id_satuan']==$row['id_satuan']){
																					echo "<option value='$row[id_satuan]' selected>$row[nama_satuan]</option>";
																						}else{
																							echo "<option value='$row[id_satuan]'>$row[nama_satuan]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															
															
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>app/edit_produk/<?php echo $r['id_produk']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editUser<?php echo $r['id_produk']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Produk
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Kode Produk</label>
																	<input  type="text" name="kode_produk" value="<?php echo $r['kode_produk']; ?>" class="form-control" placeholder="Kode Produk">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Produk</label>
																	<input  type="text" name="nama_produk" value="<?php echo $r['nama_produk']; ?>" class="form-control" placeholder="Nama Produk">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Deskripsi Produk</label>
																	<textarea  type="text" name="deskripsi" class="form-control" rows="3"><?php echo $r['deskripsi']; ?></textarea>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Kategori Produk</label>
																	<?php
																	$kategori =$this->db->query("Select * From kategori order by id_kategori ASC")->result_array();
																	echo"
																	<select name='id_kategori' class='form-control' required>
																		<option value='' selected>- Pilih Kategori Produk  -</option>";
																			foreach ($kategori as $row){
																				if ($r['id_kategori']==$row['id_kategori']){
																					echo "<option value='$row[id_kategori]' selected>$row[nama_kategori]</option>";
																						}else{
																							echo "<option value='$row[id_kategori]'>$row[nama_kategori]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Satuan</label>
																	<?php
																	$satuan =$this->db->query("Select * From satuan order by id_satuan ASC")->result_array();
																	echo"
																	<select name='id_satuan' class='form-control' required>
																		<option value='' selected>- Pilih Satuan Produk  -</option>";
																			foreach ($satuan as $row){
																				if ($r['id_satuan']==$row['id_satuan']){
																					echo "<option value='$row[id_satuan]' selected>$row[nama_satuan]</option>";
																						}else{
																							echo "<option value='$row[id_satuan]'>$row[nama_satuan]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
            