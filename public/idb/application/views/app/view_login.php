<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Form</title>
    <link rel="shortcut icon" href="assets/imgs/spring_design.jpg" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>
<body>

    <div id="bg">
        
    </div>
    <div class="page-container">
        <div class="login-container">
            <div class="form-header">
                
                <div class="login-txt">
                    Log in
                </div> 
                <div class="dark-side">

                </div>  
            </div>

           
					<form action="<?php echo base_url(); ?>app/index" method="post"  enctype="multipart/form-data">
                    <div class="form-body">
                        
                     <input type="text" name="username" placeholder="User name">
                     <input type="password" name="password" placeholder="Password">
                     <a class="forg-pwd">Forgot your password?</a>
                     <button type="submit" name="submit"  class="btn btn-primary">Login</button>
                    </div>
					</form>
            
        </div>
    </div>
</body>
</html>