	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Gaji Karyawan</h4>
										
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Gaji
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal gaji</th>
													<th>Nama Karyawan</th>
													<th>Total</th>
													<th>Bulan</th>
													<th>Tahun</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_gaji']; ?></td>
													<td><?php echo $r['nama_karyawan']; ?></td>
													<td><a href="#"  data-toggle="modal" data-target="#detailGaji<?php echo $r['id_gaji']; ?>"><?php echo rupiah($r['jumlah_gaji']); ?></a></td>
													<td><?php echo $r['bulan']; ?></td>
													<td><?php echo $r['tahun']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editGaji<?php echo "$r[id_gaji]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>transaksi/hapus_gaji/<?php echo $r['id_gaji']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>transaksi/gaji" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Gaji Karyawan
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal Gaji</label>
																	<input  type="date" name="tgl_gaji" id="tgl_gaji" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Karyawan</label>
																	<input type="hidden" id="id_karyawan" name="id_karyawan">
																	
																<input  type="text" name="nama_karyawan" id="nama_karyawan"   class="form-control" data-toggle="modal" data-target="#pilih_karyawan" >
																</div>
																<h4 id="tampil_karyawan"></h4>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Gaji Pokok</label>
																	<input  type="text" name="gapok" id="gapok" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Potongan</label>
																	<input  type="text" name="potongan" id="potongan" class="form-control" >
																</div>
															</div>
															<?php $intensif=$this->db->query("Select * From intensif")->result_array();
															foreach($intensif as $rx ){
															?>
															<div class="col-sm-12">
																<div class="form-group">
																	<label><?php echo $rx['nama_intensif'];?></label>
																	<input type="hidden" name="id_intensif[]" value="<?php echo $rx['id_intensif'];?>">
																	<input  type="text" name="jumlah[]" value="<?php echo $rx['jumlah'];?>" class="form-control" >
																</div>
															</div>
															
															<?php } ?>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
	<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>								
							<form action="<?php echo base_url(); ?>transaksi/edit_gaji/<?php echo $r['id_gaji']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editGaji<?php echo $r['id_gaji']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Edit Gaji Karyawan
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal Gaji</label>
																	<input  type="date" name="tgl_gaji" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Karyawan</label>
																	<input type="hidden" id="id_karyawan" name="id_karyawan" value="<?php echo $r['id_karyawan']; ?>">
																	
																<input  type="text" name="nama_karyawan" id="nama_karyawan"  value="<?php echo $r['nama_karyawan']; ?>"  class="form-control" data-toggle="modal" data-target="#pilih_karyawan" >
																</div>
																<h4 id="tampil_karyawan"></h4>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Gaji Pokok</label>
																	<input  type="text" name="gapok"  value="<?php echo $r['gapok']; ?>"  id="gapok" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Potongan</label>
																	<input  type="text" name="potongan" value="<?php echo $r['potongan']; ?>" id="potongan" class="form-control" >
																</div>
															</div>
															<?php $intensif=$this->db->query("Select * From intensif")->result_array();
															foreach($intensif as $rx ){
															?>
															<div class="col-sm-12">
																<div class="form-group">
																	<label><?php echo $rx['nama_intensif'];?></label>
																	<input type="hidden" name="id_intensif[]" value="<?php echo $rx['id_intensif'];?>">
																	<input  type="text" name="jumlah[]" value="<?php echo $rx['jumlah'];?>" class="form-control" >
																</div>
															</div>
															
															<?php } ?>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>									
	<?php } ?>								
									
									
<div class="modal fade" id="pilih_karyawan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Pilih Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
           <!-- Tabel daftar pelanggan -->
           <div class="table-responsive">
              <table class="display table table-striped table-hover" id="multi-filter-select" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>No Telp</th>
                    <th>Alamat</th>
                    <th>Jenis Kelamin</th>
                    <th width="15%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        $karyawan = $this->db->query("Select * From karyawan ")->result_array();
						foreach($karyawan as $data){
                    ?>
                    <tr>
                        <td><?php echo $data['nama_karyawan']; ?></td>
                        <td><?php echo $data['id_jabatan']; ?></td>
                        <td><?php echo $data['id_bagian']; ?></td>
                        <td><?php echo $data['status']; ?></td>
                        <td><?php echo $data['no_rekening'] ;?></td>
                        <td>
                        <button class="btn-pilih-karyawan btn btn-primary btn-block" id_karyawan="<?php echo $data['id_karyawan']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-address-card fa-sm"></i> Pilih</span></button>
                        </td>
                    </tr>
                    <!-- bagian akhir (penutup) while -->
						<?php }  ?>
                </tbody>
              </table>
			  
            </div>
            <!-- bagian akhir Tabel daftar pelanggan -->
    <!-- akhir body -->
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
<div class="modal fade" id="detailGaji<?php echo $r['id_gaji']; ?>">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Detail Gaji  Total :  Rp. <?php echo rupiah($r['jumlah_gaji']); ?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
           <!-- Tabel daftar pelanggan -->
           <div class="table-responsive">
              <table class="display table table-striped table-hover" id="multi-filter-select" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Intensif</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        $gaji = $this->db->query("Select * From detail_gaji a, intensif b where a.id_intensif=b.id_intensif and id_gaji='".$r['id_gaji']."' ")->result_array();
						$no=1;
						foreach($gaji as $data){
                    ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $data['nama_intensif']; ?></td>
                        <td><?php echo $data['jumlah']; ?></td>
                        
                    </tr>
                    <!-- bagian akhir (penutup) while -->
						<?php $no++;}  ?>
                </tbody>
              </table>
			  
            </div>
            <!-- bagian akhir Tabel daftar pelanggan -->
    <!-- akhir body -->
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<?php } ?>
<script>

		  $('.btn-pilih-karyawan').on('click',function(){
			  var id_karyawan = $(this).attr("id_karyawan");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/transaksi/ambilKaryawan" ?>',
				  method: 'post',
				  
				  data: {id_karyawan:id_karyawan},
				  dataType :'json',
				  success:function(data){
					 
					  $('#tampil_karyawan').html(data);
					  $('[name="id_karyawan"]').val(data[0].id_karyawan);
					  $('[name="nama_karyawan"]').val(data[0].nama_karyawan);
					  $('[name="gapok"]').val(data[0].gapok);
					  
				  }
			  }); 		  
		  });
		  
		  
 

</script>    
<script type="text/javascript">
        $('.btn-pilih-karyawan').on('click',function(){
			  var id_karyawan = $(this).attr("id_karyawan");
			  var tgl_gaji=$("[name='tgl_gaji']").val();

			$.ajax({
				  url: '<?php echo base_url()."index.php/transaksi/getHutang" ?>',
				  method: 'post',
				  
				  data: {id_karyawan:id_karyawan, tgl_gaji:tgl_gaji},
				  dataType :'json',
				  success:function(data){
					 
					  $('#tampil_karyawan').html(data);
					 
					  $('[name="potongan"]').val(data[0].jml_tagihan);
					  
				  }
			  }); 		  
		  });
    </script>