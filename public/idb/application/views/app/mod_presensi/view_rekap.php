	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Rekap Kehadiran Karyawan</h4> &nbsp &nbsp &nbsp
										<form action="<?php echo base_url();?>transaksi/rekap" method="post">
										<input type="date" title="Absen dari" name="dari"  class="btn btn-warning btn-round ml-auto" >
										<input type="date" title="Sampai tanggal" name="sampai"   class="btn btn-warning btn-round ml-auto" >	
											<button type="submit" name="submit"  class="btn btn-primary btn-round ml-auto">Lihat</button>
										</form>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>NIK </th>
													<th>Nama Karyawan</th>
													<th>Jabatan</th>
													<th>Bagian</th>
													<th>Hadir</th>
													<th>Izin</th>
													<th>Sakit</th>
													<th>Alpha</th>
													<th>Total</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1;  foreach($record->result_array() as $r) { 
											$chadir=$this->db->query("Select * From absensi where id_karyawan='".$r['id_karyawan']."' and ket='Hadir' and tgl_absensi BETWEEN '$dari' and '$sampai' ")->num_rows();
											$csakit=$this->db->query("Select * From absensi where id_karyawan='".$r['id_karyawan']."' and ket='Sakit' and tgl_absensi BETWEEN '$dari' and '$sampai'")->num_rows();
											$cizin=$this->db->query("Select * From absensi where id_karyawan='".$r['id_karyawan']."' and ket='Izin' and tgl_absensi BETWEEN '$dari' and '$sampai'")->num_rows();
											$calpha=$this->db->query("Select * From absensi where id_karyawan='".$r['id_karyawan']."' and ket='Alpha' and tgl_absensi BETWEEN '$dari' and '$sampai'")->num_rows();
											$jml_tot=$chadir+$csakit+$cizin+$calpha;
											$pros= ($chadir!=0)? ($chadir/$jml_tot) *100:0 ;
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nik']; ?></td>
													<td><?php echo $r['nama_karyawan']; ?></td>
													<td><?php echo $r['nama_jabatan']; ?></td>
													<td><?php echo $r['nama_bagian']; ?></td>
													<td><?php echo $chadir; ?></td>
													<td><?php echo $csakit; ?></td>
													<td><?php echo $cizin; ?></td>
													<td><?php echo $calpha; ?></td>
													<td><?php echo $pros; ?> %</td>
													
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							
    