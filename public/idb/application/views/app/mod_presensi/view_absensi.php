	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<a href="<?php echo base_url(); ?>assets/template_absensi.xlsx" target="_blank"><h4 class="card-title">Format Absensi</h4></a>
										<button class="btn btn-warning btn-round ml-auto" data-toggle="modal" data-target="#addRowModalexcel">
											<i class="fa fa-plus"></i>
											Import Excel
										</button>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Absensi
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal absen</th>
													<th>Nama karyawan</th>
													<th>Datang</th>
													<th>Pulang</th>
													<th>Keterangan</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_absensi']; ?></td>
													<td><?php echo $r['nama_karyawan']; ?></td>
													<td><?php echo $r['datang']; ?></td>
													<td><?php echo $r['pulang']; ?></td>
													<td><?php echo $r['ket']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editJabatan<?php echo "$r[id_absensi]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>transaksi/hapus_absensi/<?php echo $r['id_absensi']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>transaksi/absensi" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Absensi
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal absensi</label>
																	<input  type="date" name="tgl_absensi" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Karyawan</label>
																	<?php
																	$karyawan =$this->db->query("Select * From karyawan order by id_karyawan ASC")->result_array();
																	echo"
																	<select name='id_karyawan' class='form-control' required>
																		<option value='' selected>- Pilih karyawan  -</option>";
																			foreach ($karyawan as $row){
																				if ($r['id_karyawan']==$row['id_karyawan']){
																					echo "<option value='$row[id_karyawan]' selected>$row[nama_karyawan]</option>";
																						}else{
																							echo "<option value='$row[id_karyawan]'>$row[nama_karyawan]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Keterangan</label>
																	<select   name="ket" class="form-control" >
																	<option value="">Pilih Ket</option>
																	<option value="Izin">Izin</option>
																	<option value="Sakit">Sakit</option>
																	<option value="Alpha">Alpha</option>
																	</select>
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>transaksi/edit_absensi/<?php echo $r['id_absensi']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editJabatan<?php echo $r['id_absensi']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Absensi
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal absensi</label>
																	<input  type="date" name="tgl_absensi" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Karyawan</label>
																	<?php
																	$karyawan =$this->db->query("Select * From karyawan order by id_karyawan ASC")->result_array();
																	echo"
																	<select name='id_karyawan' class='form-control' required>
																		<option value='' selected>- Pilih karyawan  -</option>";
																			foreach ($karyawan as $row){
																				if ($r['id_karyawan']==$row['id_karyawan']){
																					echo "<option value='$row[id_karyawan]' selected>$row[nama_karyawan]</option>";
																						}else{
																							echo "<option value='$row[id_karyawan]'>$row[nama_karyawan]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Datang</label>
																	<input  type="time" name="datang" value="<?php echo $r['datang']; ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Pulang</label>
																	<input  type="time" name="pulang" value="<?php echo $r['pulang']; ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Keterangan</label>
																	<select   name="ket" class="form-control" >
																	<option value="<?php echo $r['ket']; ?>"><?php echo $r['ket']; ?></option>
																	<option value="">Pilih Ket</option>
																	<option value="Izin">Izin</option>
																	<option value="Sakit">Sakit</option>
																	<option value="Alpha">Alpha</option>
																	</select>
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
							<form action="<?php echo base_url(); ?>import/excel" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModalexcel" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Absensi
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Upload File absensi</label>
																	<input  type="file" name="file" accept=".xls, .xlsx" required class="form-control" >
																</div>
															</div>
															
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Upload</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>            