	<?php 
	
	$pel="PG.";
	$y=substr($pel,0,2);
	$query=$this->db->query("select * from pelanggan where substr(id_pelanggan,1,2)='$y' order by id_pelanggan desc limit 0,1");
	$row=$query->num_rows;
	$data=$query->result_array();

	if ($row>0){
	$no=substr($data['id_pelanggan'],-3)+1;}
	else{
	$no=1;
	}
	$nourut=1000+$no;
	$no_pel=$pel.substr($nourut,-3);
	
	
	?>
	
	
	<div class="card shadow mb-4">
	<div class="card-header py-3">
          <div class="row">
              <div class="col-sm-6">
                  <h5 style="text-align:left">No Invoice : </h5>
              </div>
              <div class="col-sm-6">
                <h5 style="text-align:right">Tanggal : </h5>
              </div>
        
          </div>
      </div>
	
	<div class="row">
	<div class="col-md-12">
	
	<div class="card-body">
          <!-- rows -->
            <div class="row">
              <div class="col-sm-4">
                  <!-- Overflow Hidden -->
                  <div class="card mb-4">
                      <div class="card-header py-3">
                        <h5 class="m-0 font-weight-bold text-primary">Tambah Pembelian</h5>
                      </div>
                      <div class="card-body">
                        <!-- rows -->
                          <div class="row">
                              <div class="col-sm-12">
                                  <div class="form-group">
                                    <button type="button" data-toggle="modal" data-target="#modal" class="btn btn-primary"><span class="text"><i class="fas fa-shopping-bag fa-sm"></i> Pilih Produk</span></button>
                                  </div>
                              </div>
                            </div>
                          <!-- rows -->
                          <div class="row">
                              <div class="col-sm-12">
                                <div id="tampil_deskripsi_produk">
                                </div>
                                  <div class="form-group">
                                        <label>Produk:</label>
                                        <input name="id_produk" id="id_produk" type="text" class="form-control">
                                        <input name="nama_produk" id="nama_produk" type="text" class="form-control" disabled>
										
                                  </div>
                                  <div class="form-group">
                                        <label>Harga:</label>
                                        <input name="harga_jual" id="harga_jual" type="text" class="form-control" disabled>
                                  </div>
                                  <div class="form-group">
                                        <label>Stok Tersedia : <span id="info_stok_produk"> </span> </label>
                                        <input type="hidden" id="stok_produk" name="stok_produk" />
                                        <input name="jumlah_jual" type="text"  id="jumlah_jual" class="form-control">
                                  </div>
                                  <div class="form-group" id="info_ketersediaan">
                                  </div>
                                  <div class="form-group">
                                        <button  type="button" id="tambahkan_ke_keranjang" aksi="tambahkan_ke_keranjang" disabled class="btn btn-primary btn-block"><span class="text"><i class="fas fa-shopping-cart fa-sm"></i> Masukan keranjang</span></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-sm-8">
                <!-- Overflow Hidden -->
                <div class="card mb-4">
                    <div class="card-header py-3">
                      <h5 class="m-0 font-weight-bold text-primary">Keranjang Belanja</h5>
                      <h4 id="tampil_pelanggan">Pilih Distributor : - <a href=""  data-toggle="modal" data-target="#pilih_pelanggan" ><b>Pilih</b></a></h>
                    </div>
                    <div class="card-body">
                        <!--Menampilkan cart (keranjang belanja) --> 
                        <table class="table table-bordered table-striped" style="font-size:12px;">
                    <thead>
                      <tr>
                        <th width="50px">.::.</th>
                        <th>Qty </th>
						<th>Nama Produk</th>
						<th>Harga</th>
						
                        <th width="200px">Action</th>
                      </tr>
                    </thead>
                        <tbody id="targetData">
        
						</tbody>
                    
                  </table>
				  <div class="row">
    <div class="col-sm-7">
        <div class="form-group">
            <input name="total_bayar" id="total_bayar" value="<?php  ?>" type="hidden"  class="form-control">
        </div>
        <div class="form-group">
            <label>Bayar:</label>
            <input name="bayar" id="bayar"  type="text"  class="form-control">
        </div>
        <div class="form-group">
            <div id="nominal_bayar" class='font-weight-bold'></div>
        </div>
        <div class="form-group">
            <label>Kembali:</label>
            <input type="text" id="tampil_kembali" class="form-control" disabled>
            <input type="hidden" name="kembali" id="kembali" class="form-control">
        </div>
        <div class="form-group">
            <button  type="submit" id="buat_transaksi" name="buat_transaksi" class="btn btn-success btn-block" disabled><span class="text">Buat Transaksi</span></button>
        </div>
    </div>
</div>
                    </div>
                </div>
              </div>
          </div>
        <!-- rows -->
			</div>				
		</div>		
	</div>
</div>	
<div class="modal fade" id="modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Bagian header -->
      <div class="modal-header">
        <h4 class="modal-title" id="judul"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Bagian body -->
      <div class="modal-body">
        <!-- Tabel daftar produk -->
        <div class="table-responsive">
              <table class="display table table-striped table-hover" id="basic-datatables" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Kode</th>
                    <th>Produk</th>
                    <th>Satuan</th>
                    <th>Kategori</th>
                    <th>Stok</th>
                    <th>Harga Jual</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        
                        $no=0;
                        $produk = $this->db->query("Select * from produk a, satuan b, kategori c where a.id_satuan=b.id_satuan and a.id_kategori=c.id_kategori ")->result_array();
                        foreach ($produk as $data ){
                    ?>
                    <tr>
                        <td><?php echo $data['kode_produk']; ?></td>
                        <td><?php echo $data['nama_produk']; ?></td>
                        <td><?php echo $data['nama_satuan']; ?></td>
                        <td><?php echo $data['nama_kategori']; ?></td>
                        <td><?php  ?></td>
                        <td>Rp. <?php echo rupiah($data['harga_jual']); ?></td>
                        <td>
                        <button type="button" class="btn-pilih-produk btn btn-primary btn-block" id_produk="<?php echo $data['id_produk']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-paper-plane fa-sm"></i> Pilih</span></button>
                        </td>
                    </tr>
                    <!-- bagian akhir (penutup) while -->
                    <?php } ?>
                </tbody>
              </table>
            </div>
      </div>
      <!-- Bagian footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pilih_pelanggan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Pilih Pelanggan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
           <!-- Tabel daftar pelanggan -->
           <div class="table-responsive">
              <table class="display table table-striped table-hover" id="multi-filter-select" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>No Telp</th>
                    <th>Alamat</th>
                    <th>Jenis Kelamin</th>
                    <th width="15%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        $pelanggan = $this->db->query("Select * From pelanggan ")->result_array();
						foreach($pelanggan as $data){
                    ?>
                    <tr>
                        <td><?php echo $data['kode_pelanggan']; ?></td>
                        <td><?php echo $data['nama_pelanggan']; ?></td>
                        <td><?php echo $data['no_telp']; ?></td>
                        <td><?php echo $data['alamat']; ?></td>
                        <td><?php echo $data['jk'] ;?></td>
                        <td>
                        <button class="btn-pilih-pelanggan btn btn-primary btn-block" id_pelanggan="<?php echo $data['id_pelanggan']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-address-card fa-sm"></i> Pilih</span></button>
                        </td>
                    </tr>
                    <!-- bagian akhir (penutup) while -->
						<?php }  ?>
                </tbody>
              </table>
			  
            </div>
            <!-- bagian akhir Tabel daftar pelanggan -->
    <!-- akhir body -->
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
		
	<script>
	ambilData();
			function ambilData(){
				$.ajax({
					type:'POST',
					url:'<?php echo base_url()."index.php/penjualan/ambildata" ?>',
					dataType: 'json',
					success: function(data){
						var baris='';
						$no=1;
						for (var i=0;i<data.length; i++){
							baris += '<tr>'+
									'<td>'+ $no+'</td>' +
									'<td>'+ data[i].qty+ '</td>' +
									'<td>'+ data[i].nama_produk+ '</td>' +
									'<td>'+ data[i].harga+ '</td>' +
									'<td><a  class="btn-edit-produk btn btn-primary" onclick="editdata('+data[i].id_temp+')"><i class="fa fa-edit"></i> </a> <a  onclick="hapusdata('+data[i].id_temp+')" class="btn btn-warning"><i class="fa fa-eraser"></i> </a></td>' +
									'</tr>';
									$no++;
						}
						$('#targetData').html(baris);
					}
				});
				
			}
			//Event saat pengguna memilih produk yang ingin dibeli
		  $('.btn-pilih-produk').on('click',function(){
			  var id_produk = $(this).attr("id_produk");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/penjualan/ambilproduk" ?>',
				  method: 'post',
				  
				  data: {id_produk:id_produk},
				  dataType :'json',
				  success:function(data){
					 
					  $('#tampil_deskripsi_produk').html(data);
					  $('[name="id_produk"]').val(data[0].id_produk);
					  $('[name="nama_produk"]').val(data[0].nama_produk);
					  $('[name="harga_jual"]').val(data[0].harga_jual);
					  $('[name="jumlah_jual"]').val(8);
					  $('[name="stok_produk"]').val(7);
				  }
			  }); 		  
		  });
		  
		  
		  
		  
		  // event hapus detail penjualan 
		  function hapusdata(id_temp){
				var tanya=confirm('Apakah anda yahin akan menghapus data ini');
				
				if(tanya){
					$.ajax({
						type:'POST',
						data:'id_temp='+id_temp,
						url:'<?php echo base_url()."index.php/penjualan/hapustemp" ?>',
						success: function(){
						ambilData();			
						}
					});
					
				}
			}
			function editdata(id_temp){
				var tanya=confirm('Apakah anda akan mengedit');
				
				if(tanya){
					$.ajax({
						type:'POST',
						data:'id_temp='+id_temp,
						dataType :'json',
						url:'<?php echo base_url()."index.php/penjualan/edittemp" ?>',
						success:function(data){
					 
					  $('#tampil_deskripsi_produk').html(data);
					  $('[name="id_produk"]').val(data[0].id_produk);
					  $('[name="nama_produk"]').val(data[0].nama_produk);
					  $('[name="harga_jual"]').val(data[0].harga_jual);
					  $('[name="jumlah_jual"]').val(data[0].qty);
					  $('[name="stok_produk"]').val(7);
				  }
					});
					
				}
			}
		  
		  //Event saat pengguna memasukan jumlah beli
		  $("#jumlah_jual").bind('keyup', function () {
			  var jumlah_jual = $('#jumlah_jual').val();
			  var stok_produk = $('#stok_produk').val();
			  
			  var jum_beli = parseInt(jumlah_jual);
			  var jum_max = parseInt(stok_produk);

			  if (jum_beli!=0 && jum_beli<=jum_max){
				  document.getElementById("tambahkan_ke_keranjang").disabled = false;
				  $('#info_ketersediaan').hide();
			  }else {
				  document.getElementById("tambahkan_ke_keranjang").disabled = true;
				  $('#info_ketersediaan').show();
				  $('#info_ketersediaan').html("<div class='alert alert-danger'>Jumlah beli melebihi stok produk</div>");
			  }      
		  });
		    //Event saat pengguna memilih pelanggan
	
		  
		  $('.btn-pilih-pelanggan').on('click',function(){
			  var id_pelanggan = $(this).attr("id_pelanggan");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/penjualan/ambilproduk" ?>',
				  method: 'post',
				  
				  data: {id_pelanggan:id_pelanggan},
				  dataType :'json',
				  success:function(data){
					 
					  $('#tampil_pelanggan').html(data);
					  $('[name="id_pelanggan"]').val(data[0].id_pelanggan);
				  }
			  }); 		  
		  });
		  //Event saat tombol tambahkan keranjang dklik
		  $('#tambahkan_ke_keranjang').on('click',function(){
			  var aksi = $(this).attr("aksi");
			  var jumlah_jual=$("#jumlah_jual").val();
			  var harga_jual=$("#harga_jual").val();
			  var id_produk=$("#id_produk").val();
			  
			  $.ajax({
				  url: '<?php echo base_url()."index.php/penjualan/keranjang" ?>',
				  method: 'POST',
				  data:{jumlah_jual:jumlah_jual,id_produk:id_produk,harga_jual:harga_jual},
				  success:function(data){
					  ambilData();
					  $('[name="jumlah_jual"]').val('');
					  $('[name="id_produk"]').val('');
					  $('[name="harga_jual"]').val('');
					  $('[name="nama_produk"]').val('');
				  }
			  }); 
		  });
	</script>	
            