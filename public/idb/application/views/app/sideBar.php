<div class="sidebar sidebar-style-2">     
      <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
          <ul class="nav nav-primary">
            <li class="nav-item active">
              <a data-toggle="collapse" href="#dashboard" class="collapsed" aria-expanded="false">
                <i class="fas fa-home"></i>
                <p>Dashboard</p>
                <span class="caret"></span>
              </a>
              <div class="collapse" id="dashboard">
                <ul class="nav nav-collapse">
                  <li>
                    <a href="index.php?page=dashboard">
                      <span class="sub-item">Dashboard</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="nav-section">
              <span class="sidebar-mini-icon">
                <i class="fa fa-ellipsis-h"></i>
              </span>
              <h4 class="text-section">Transaction</h4>
            </li>
            
			<?php $menu = $this->db->query("Select * From menu where aktif='Y' and kelompok='1'  order by urutan ASC ")->result_array();
			foreach($menu as $rs ){?>

            <li class="nav-item">
              <a data-toggle="collapse" href="#<?php echo $rs['id_menu']; ?>">
                <i class="<?php echo $rs['icon']; ?>"></i>
			
                <p><?php echo $rs['nama_menu']; ?></p>
                <span class="caret"></span>
              </a>
              <div class="collapse" id="<?php echo $rs['id_menu']; ?>">
                <ul class="nav nav-collapse">
				<?php $submenu = $this->db->query("Select * From submenu where aktif='Y' and id_menu='".$rs['id_menu']."'  order by urutan ASC ")->result_array();
				foreach($submenu as $r ){?>
                  <li>
                    <a href="<?php echo base_url(); ?><?php echo $r['url']; ?>">
					<i class="<?php echo $r['icon']; ?>"></i>
                      <span ><?php echo $r['nama_submenu']; ?></span>
                    </a>
                  </li>
				<?php } ?>  
                </ul>
              </div>
            </li>
			<?php } ?>
            <li class="nav-section">
              <span class="sidebar-mini-icon">
                <i class="fa fa-ellipsis-h"></i>
              </span>
              <h4 class="text-section">Finance & Accounting</h4>
            </li>
           <?php $menu = $this->db->query("Select * From menu where aktif='Y' and kelompok='2'  order by urutan ASC ")->result_array();
			foreach($menu as $rs ){?>

            <li class="nav-item">
              <a data-toggle="collapse" href="#<?php echo $rs['id_menu']; ?>">
                <i class="<?php echo $rs['icon']; ?>"></i>
			
                <p><?php echo $rs['nama_menu']; ?></p>
                <span class="caret"></span>
              </a>
              <div class="collapse" id="<?php echo $rs['id_menu']; ?>">
                <ul class="nav nav-collapse">
				<?php $submenu = $this->db->query("Select * From submenu where aktif='Y' and id_menu='".$rs['id_menu']."'  order by urutan ASC ")->result_array();
				foreach($submenu as $r ){?>
				<?php if ($rs['id_menu'] !='9'){?>
                  <li>
                    <a href="<?php echo base_url(); ?><?php echo $r['url']; ?>">
					<i class="<?php echo $r['icon']; ?>"></i>
                      <span ><?php echo $r['nama_submenu']; ?></span>
                    </a>
                  </li>
				<?php }else{ ?>  
				<li>
                    <a href="<?php echo base_url(); ?><?php echo $r['url']; ?>" target="_blank">
					<i class="<?php echo $r['icon']; ?>"></i>
                      <span >Lap <?php echo $r['nama_submenu']; ?></span>
                    </a>
                  </li>
				<?php } ?> 
				<?php } ?>  
                </ul>
              </div>
            </li>
			<?php } ?><li class="nav-section">
              <span class="sidebar-mini-icon">
                <i class="fa fa-ellipsis-h"></i>
              </span>
              <h4 class="text-section">Database</h4>
            </li>
            <?php $menu = $this->db->query("Select * From menu where aktif='Y' and kelompok='3' order by urutan ASC ")->result_array();
			foreach($menu as $rs ){?>

            <li class="nav-item">
              <a data-toggle="collapse" href="#<?php echo $rs['id_menu']; ?>">
                <i class="<?php echo $rs['icon']; ?>"></i>
			
                <p><?php echo $rs['nama_menu']; ?></p>
                <span class="caret"></span>
              </a>
              <div class="collapse" id="<?php echo $rs['id_menu']; ?>">
                <ul class="nav nav-collapse">
				<?php $submenu = $this->db->query("Select * From submenu where aktif='Y' and id_menu='".$rs['id_menu']."'  order by urutan ASC ")->result_array();
				foreach($submenu as $r ){?>
                  <li>
                    <a href="<?php echo base_url(); ?><?php echo $r['url']; ?>">
					<i class="<?php echo $r['icon']; ?>"></i>
                      <span ><?php echo $r['nama_submenu']; ?></span>
                    </a>
                  </li>
				<?php } ?>  
                </ul>
              </div>
            </li>
			<?php } ?><li class="mx-4 mt-2">
              <a href="<?php echo base_url(); ?>app/logout" class="btn btn-primary btn-block" ><span class="btn-label mr-2"> <i class="fas fa-power-off"></i> </span>Logout</a> 
            </li>
          </ul>
        </div>
      </div>
    </div>
    