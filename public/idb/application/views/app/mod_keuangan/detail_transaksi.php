		<?php 
	$cek=$this->db->query("Select * From bank where id_bank='$id_bank'")->row_array();
	
	?>
	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Transaksi Bank <b style="font-size:24;"><?php echo $cek['nama_bank']; ?><b></h4>
										
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Transaksi</th>
													<th>Kode Akun</th>
													<th>Nama Akun</th>
													<th>Keterangan</th>
													<th>Debet</th>
													<th>Kredit</th>
													
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											$akun=$r['jenis'];
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_transaksi']; ?></td>
													<td><?php echo $r['kode_akun']; ?></td>
													<td><?php echo $r['nama_akun']; ?></td>
													<td><?php echo $r['keterangan']; ?></td>
													<?php if($akun =="debet") { ?>
													<td><?php echo rupiah($r['jumlah']); ?></td>
													<td></td>
													<?php }else{ ?>
													<td></td>
													<td><?php echo rupiah($r['jumlah']); ?></td>
													<?php } ?>
													
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
			
							<form action="<?php echo base_url(); ?>keuangan/transaksi" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Transaksi
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal</label>
																	<input  type="date" name="tgl_transaksi" class="form-control" placeholder="Nama Bank">
																</div>
															</div>

															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jenis transaksi</label>
																	<select name="jenis" id="jenis" class="form-control">
																	<option value="debet">Debet</option>
																	<option value="kredit">Kredit</option>
																	</select>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Akun Transaksi</label>
																	<select class="form-control" id="id_akun" name="id_akun" required>
																	<option>No Selected</option>
																	</select>
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah</label>
																	<input  type="text" name="jumlah" class="form-control" placeholder="Jumlah">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Keterangan</label>
																	<textarea name="keterangan" class="form-control"rows="3"></textarea>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Sumber Dana / Ke </label>
																	<?php
																	$bank =$this->db->query("Select * From bank order by id_bank ASC")->result_array();
																	echo"
																	<select name='id_bank' class='form-control' required>
																		<option value='' selected>- Pilih Bank   -</option>";
																			foreach ($bank as $row){
																				if ($r['id_bank']==$row['id_bank']){
																					echo "<option value='$row[id_bank]' selected>$row[nama_bank]</option>";
																						}else{
																							echo "<option value='$row[id_bank]'>$row[nama_bank]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									
		<script type="text/javascript">
        $(document).ready(function(){
 
            $('#jenis').change(function(){ 
                var jenis=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('keuangan/getAkun');?>",
                    method : "POST",
                    data : {jenis: jenis},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id_akun+'>'+data[i].nama_akun+'</option>';
                        }
                        $('#id_akun').html(html);
 
                    }
                });
                return false;
            }); 
             
        });
    </script>								