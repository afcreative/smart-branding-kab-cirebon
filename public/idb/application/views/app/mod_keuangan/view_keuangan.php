
	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Bank</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Bank 
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Bank</th>
													<th>No Rekening</th>
													<th>Saldo awal</th>
													<th>Debet</th>
													<th>Kredit</th>
													<th>Saldo</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											$deb=$this->db->query("Select * from akun where jenis='debet'")->result_array();
											foreach($deb as $ry){
												$tdeb=$this->db->query("Select sum(jumlah) as totdebet From transaksi where id_akun='".$ry['id_akun']."' and id_bank='".$r['id_bank']."'")->row_array();
												
											}
											$kre=$this->db->query("Select * from akun where jenis='kredit'")->result_array();
											foreach($kre as $rx){
												
												$tkre=$this->db->query("Select sum(jumlah) as totdebet From transaksi where id_akun='".$rx['id_akun']."' and id_bank='".$r['id_bank']."'")->row_array();
											}
											$saldo=($r['saldo'] - $tkre['totdebet'] ) + $tdeb['totdebet'];
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_bank']; ?></td>
													<td><?php echo $r['no_rek']; ?></td>
													<td><?php echo rupiah($r['saldo']); ?></td>
													<td><?php echo rupiah($tdeb['totdebet']); ?></td>
													<td><?php echo rupiah($tkre['totdebet']); ?></td>
													<td><?php echo rupiah($saldo); ?></td>
													<td>
														<div class="form-button-action">
															<a href="<?php echo base_url(); ?>keuangan/detail_transaksi/<?php echo $r['id_bank']; ?>" button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  >
																<i class="fa fa-eye"></i>
															</button></a>
															
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>app/bank" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Bank
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Bank</label>
																	<input  type="text" name="nama_bank" class="form-control" placeholder="Nama Bank">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No rek Bank</label>
																	<input  type="text" name="no_rek" class="form-control" placeholder="Nama Bank">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Saldo awal Bank</label>
																	<input  type="text" name="saldo" class="form-control" placeholder="Saldo Bank">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>app/edit_bank/<?php echo $r['id_bank']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editBank<?php echo $r['id_bank']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Bank
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Bank</label>
																	<input  type="text" name="nama_bank" value="<?php echo $r['nama_bank']; ?>" class="form-control" placeholder="Nama Bank">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No rek Bank</label>
																	<input  type="text" name="no_rek" value="<?php echo $r['no_rek']; ?>" class="form-control" placeholder="Nama Bank">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Saldo awal Bank</label>
																	<input  type="text" name="saldo" value="<?php echo $r['saldo']; ?>" class="form-control" placeholder="Saldo Bank">
																</div>
															</div>

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>
            