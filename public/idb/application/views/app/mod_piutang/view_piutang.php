	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Piutang</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Piutang
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Piutang</th>
													<th>Kode Pelanggan</th>
													<th>Nama </th>
													<th>Alamat</th>
													<th>Jumlah Piutang</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_piutang']; ?></td>
													<td><?php echo $r['kode_pelanggan']; ?></td>
													<td><?php echo $r['nama_pelanggan']; ?></td>
													<td><?php echo $r['alamat']; ?></td>
													<td><?php echo rupiah($r['jml_piutang']); ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editBank<?php echo "$r[id_piutang]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>app/hapus_bank/<?php echo $r['id_piutang']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
			
							<form action="<?php echo base_url(); ?>keuangan/piutang" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Piutang
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal</label>
																	<input  type="date" name="tgl_piutang" class="form-control" placeholder="Nama Bank">
																</div>
															</div>

															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Pelanggan</label>
																	<input  type="hidden" name="id_pelanggan" id="id_pelanggan" class="form-control" placeholder= >
																	<input  type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control" placeholder="Pilih disini" data-toggle="modal" data-target="#modalPelanggan" >
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah</label>
																	<input  type="text" name="jml_piutang" class="form-control" placeholder="Jumlah">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Keterangan</label>
																	<textarea name="keterangan" class="form-control"rows="3"></textarea>
																</div>
															</div>
															
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									
	<div class="modal fade" id="modalPelanggan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Bagian header -->
      <div class="modal-header">
        <h4 class="modal-title" id="judul"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Bagian body -->
      <div class="modal-body">
        <!-- Tabel daftar produk -->
        <div class="table-responsive">
              <table class="display table table-striped table-hover" id="basic-datatables" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Kode</th>
                    <th>Nama Pelanggan</th>
                    <th>Alamat</th>
                    <th>Email</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                        
                        $no=0;
                        $pelanggan = $this->db->query("Select * from pelanggan")->result_array();
                        foreach ($pelanggan as $data ){
                    ?>
                    <tr>
                        <td><?php echo $data['kode_pelanggan']; ?></td>
                        <td><?php echo $data['nama_pelanggan']; ?></td>
                        <td><?php echo $data['alamat']; ?></td>
                        <td><?php echo $data['email']; ?></td>
                        <td>
                        <button type="button" class="btn-pilih-pelanggan btn btn-primary btn-block" id_pelanggan="<?php echo $data['id_pelanggan']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-paper-plane fa-sm"></i> Pilih</span></button>
                        </td>
                    </tr>
                    <!-- bagian akhir (penutup) while -->
                    <?php } ?>
                </tbody>
              </table>
            </div>
      </div>
      <!-- Bagian footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	
<script>
$('.btn-pilih-pelanggan').on('click',function(){
			  var id_pelanggan = $(this).attr("id_pelanggan");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/penjualan/ambilpelanggan" ?>',
				  method: 'post',
				  
				  data: {id_pelanggan:id_pelanggan},
				  dataType :'json',
				  success:function(data){
					 
					  $('#tampil_deskripsi_produk').html(data);
					  $('[name="id_pelanggan"]').val(data[0].id_pelanggan);
					  $('[name="nama_pelanggan"]').val(data[0].nama_pelanggan);
				  }
			  }); 		  
		  });
</script>				