		<?php 
	
	$pel="BY.";
	$y=substr($pel,0,2);
	$cek=$this->db->query("Select * from payment where substr(no_transaksi,1,2)='$y' order by no_transaksi desc limit 0,1");
	$rowx=$cek->num_rows();
	$data=$cek->row_array();

	if ($rowx >0){
	$no=substr($data['no_transaksi'],-3) +1;
	}
	else{
	$no=1;
	}
	$nourut=1000+$no;
	$no_pel=$pel.substr($nourut,-3);
	
	
	?>
	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Data Payment</h4>
										<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
											<i class="fa fa-plus"></i>
											Payment
										</button>
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>No Transaksi</th>
													<th>Nama Mitra</th>
													<th>Nama Bank</th>
													<th>Type Bayar</th>
													<th>Mata Uang</th>
													<th>Tgl Bayar</th>
													<th>Jumlah</th>
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['no_transaksi']; ?></td>
													<td><?php echo $r['no_transaksi']; ?></td>
													<td><?php echo $r['nama_bank']; ?></td>
													<td><?php echo $r['nama_typebayar']; ?></td>
													<td><?php echo $r['nama_currency']; ?></td>
													<td><?php echo $r['tgl_pembayaran']; ?></td>
													<td><?php echo $r['jumlah_pembayaran']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editBagian<?php echo "$r[id_payment]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>pembayaran/hapus_pembayaran/<?php echo $r['id_payment']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							<form action="<?php echo base_url(); ?>pembayaran/payment" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Payment
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Transaksi</label>
																	<input  type="text" name="no_transaksi" value="<?php echo $no_pel; ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tgl Transaksi</label>
																	<input  type="text" name="tgl_transaksi" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
															<div class="form-check">
												<label>Pemohon</label><br/>
												<label class="form-radio-label">
													<input class="form-radio-input" type="radio" name="mitra" value="karyawan"  checked="" data-toggle="modal" data-target="#addRowModalKaryawan">
													<span class="form-radio-sign">Karyawan</span>
												</label>
												<label class="form-radio-label ml-3">
													<input class="form-radio-input" type="radio" name="mitra"  data-toggle="modal" data-target="#addRowModalCustomer" value="kustomer">
													<span class="form-radio-sign">Kustomer</span>
												</label>
												<label class="form-radio-label ml-3">
													<input class="form-radio-input" type="radio" name="mitra"  data-toggle="modal" data-target="#addRowModalSuplier" value="suplier">
													<span class="form-radio-sign">Suplier</span>
												</label>
											</div>
											<div class="form-group">
											<input type="text" name="nama_mitra" id="nama_mitra" class="form-control">
											<input type="hidden" name="id_mitra" id="id_mitra" class="form-control">
											</div>
											</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Bank</label>
																	<?php
																	$kategori =$this->db->query("Select * From bank order by id_bank ASC")->result_array();
																	echo"
																	<select name='id_bank' class='form-control' required>
																		<option value='' selected>- Pilih Bank  -</option>";
																			foreach ($kategori as $row){
																				if ($r['id_bank']==$row['id_bank']){
																					echo "<option value='$row[id_bank]' selected>$row[nama_bank]</option>";
																						}else{
																							echo "<option value='$row[id_bank]'>$row[nama_bank]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Type Pembayaran</label>
																	<?php
																	$type =$this->db->query("Select * From typebayar order by id_typebayar ASC")->result_array();
																	echo"
																	<select name='id_typebayar' class='form-control' required>
																		<option value='' selected>- Pilih Type Pembayaran  -</option>";
																			foreach ($type as $row){
																				if ($r['id_typebayar']==$row['id_typebayar']){
																					echo "<option value='$row[id_typebayar]' selected>$row[nama_typebayar]</option>";
																						}else{
																							echo "<option value='$row[id_typebayar]'>$row[nama_typebayar]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Currecy Payment</label>
																	<?php
																	$type =$this->db->query("Select * From currency order by id_currency ASC")->result_array();
																	echo"
																	<select name='id_currency' class='form-control' required>
																		<option value='' selected>- Pilih Type Pembayaran  -</option>";
																			foreach ($type as $row){
																				if ($r['id_currency']==$row['id_currency']){
																					echo "<option value='$row[id_currency]' selected>$row[nama_currency]</option>";
																						}else{
																							echo "<option value='$row[id_currency]'>$row[nama_currency]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal Bayar</label>
																	<input  type="text" name="tgl_pembayaran" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah Bayar</label>
																	<input  type="text" name="jumlah_pembayaran"  class="form-control" >
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>pembayaran/edit_payment/<?php echo $r['id_payment']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editBagian<?php echo $r['id_payment']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit</span> 
														<span class="fw-light">
															Payment
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Transaksi</label>
																	<input  type="text" name="no_transaksi" value="<?php echo $r['no_transaksi']; ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tgl Transaksi</label>
																	<input  type="text" name="tgl_transaksi" value="<?php echo $r['tgl_transaksi']; ?>" class="form-control" >
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Bank</label>
																	<?php
																	$kategori =$this->db->query("Select * From bank order by id_bank ASC")->result_array();
																	echo"
																	<select name='id_bank' class='form-control' required>
																		<option value='' selected>- Pilih Bank  -</option>";
																			foreach ($kategori as $row){
																				if ($r['id_bank']==$row['id_bank']){
																					echo "<option value='$row[id_bank]' selected>$row[nama_bank]</option>";
																						}else{
																							echo "<option value='$row[id_bank]'>$row[nama_bank]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Type Pembayaran</label>
																	<?php
																	$type =$this->db->query("Select * From typebayar order by id_typebayar ASC")->result_array();
																	echo"
																	<select name='id_typebayar' class='form-control' required>
																		<option value='' selected>- Pilih Type Pembayaran  -</option>";
																			foreach ($type as $row){
																				if ($r['id_typebayar']==$row['id_typebayar']){
																					echo "<option value='$row[id_typebayar]' selected>$row[nama_typebayar]</option>";
																						}else{
																							echo "<option value='$row[id_typebayar]'>$row[nama_typebayar]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Currecy Payment</label>
																	<?php
																	$type =$this->db->query("Select * From currency order by id_currency ASC")->result_array();
																	echo"
																	<select name='id_currency' class='form-control' required>
																		<option value='' selected>- Pilih Type Pembayaran  -</option>";
																			foreach ($type as $row){
																				if ($r['id_currency']==$row['id_currency']){
																					echo "<option value='$row[id_currency]' selected>$row[nama_currency]</option>";
																						}else{
																							echo "<option value='$row[id_currency]'>$row[nama_currency]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal Bayar</label>
																	<input  type="text" name="tgl_pembayaran" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah Bayar</label>
																	<input  type="text" name="jumlah_pembayaran" value="<?php echo $r['jumlah_pembayaran']; ?>"  class="form-control" >
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>

							<div class="modal fade" id="addRowModalKaryawan" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														View Data</span> 
														<span class="fw-light">
															Karyawan
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Karyawan</th>
													<th>Alamat</th>
													<th>No Hp</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$karyawan=$this->db->query("Select * From karyawan a, bagian b where a.id_bagian=b.id_bagian");
											foreach($karyawan->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_karyawan']; ?></td>
													<td><?php echo $r['alamat']; ?></td>
													<td><?php echo $r['nama_bagian']; ?></td>
													<td>
														<div class="form-button-action">
													
															 <button type="button" class="btn-pilih-karyawan btn btn-primary btn-block" id_karyawan="<?php echo $r['id_karyawan']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-paper-plane fa-sm"></i> Pilih</span></button>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
													
												</div>
												<div class="modal-footer no-bd">
													
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>

<div class="modal fade" id="addRowModalCustomer" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														View Data</span> 
														<span class="fw-light">
															Customer
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Customer</th>
													<th>Alamat</th>
													<th>No Hp</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$customer=$this->db->query("Select * From customer");
											foreach($customer->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_customer']; ?></td>
													<td><?php echo $r['email']; ?></td>
													<td><?php echo $r['no_telp']; ?></td>
													<td>
														<div class="form-button-action">
													
															 <button type="button" class="btn-pilih-customer btn btn-primary btn-block" id_customer="<?php echo $r['id_customer']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-paper-plane fa-sm"></i> Pilih</span></button>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
													
												</div>
												<div class="modal-footer no-bd">
													
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	

<div class="modal fade" id="addRowModalSuplier" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														View Data</span> 
														<span class="fw-light">
															Suplier
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Nama Suplier</th>
													<th>Alamat</th>
													<th>No Hp</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$suplier=$this->db->query("Select * From suplier");
											foreach($suplier->result_array() as $r) { 
					
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['nama_suplier']; ?></td>
													<td><?php echo $r['email']; ?></td>
													<td><?php echo $r['no_telp']; ?></td>
													<td>
														<div class="form-button-action">
													
															 <button type="button" class="btn-pilih-suplier btn btn-primary btn-block" id_suplier="<?php echo $r['id_suplier']; ?>" data-dismiss="modal" ><span class="text"><i class="fas fa-paper-plane fa-sm"></i> Pilih</span></button>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
													
												</div>
												<div class="modal-footer no-bd">
													
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>
									
	<script>
	$('.btn-pilih-karyawan').on('click',function(){
			  var id_karyawan = $(this).attr("id_karyawan");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/pembayaran/ambilkaryawan" ?>',
				  method: 'post',
				  
				  data: {id_karyawan:id_karyawan},
				  dataType :'json',
				  success:function(data){
					 
					  
					  $('[name="nama_mitra"]').val(data[0].nama_karyawan);
					  $('[name="id_mitra"]').val(data[0].id_karyawan);
				  }
			  }); 		  
		  });
		  $('.btn-pilih-suplier').on('click',function(){
			  var id_suplier = $(this).attr("id_suplier");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/pembayaran/ambilsuplier" ?>',
				  method: 'post',
				  
				  data: {id_suplier:id_suplier},
				  dataType :'json',
				  success:function(data){
					 
					  
					  $('[name="nama_mitra"]').val(data[0].nama_suplier);
					  $('[name="id_mitra"]').val(data[0].id_suplier);
				  }
			  }); 		  
		  });
		   $('.btn-pilih-customer').on('click',function(){
			  var id_customer = $(this).attr("id_customer");
			  

			$.ajax({
				  url: '<?php echo base_url()."index.php/pembayaran/ambilcustomer" ?>',
				  method: 'post',
				  
				  data: {id_customer:id_customer},
				  dataType :'json',
				  success:function(data){
					 
					  
					  $('[name="nama_mitra"]').val(data[0].nama_customer);
					  $('[name="id_mitra"]').val(data[0].id_customer);
				  }
			  }); 		  
		  });
</script>	
            