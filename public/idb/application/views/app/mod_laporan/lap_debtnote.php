<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Credit Note</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
	
<style>
    @page { size: landscape }
  
    h2 {
        font-weight: bold;
        font-size: 20pt;
        text-align: center;
    }
	img{
		width:20%;
	}
	
	.logo{
		
		text-align: center;
	}
	h4 {
        font-weight: bold;
        font-size: 14pt;
        text-align: center;
    }
	p{
		margin-left:750px;
	}
	.capbawah{
		position: relative;
		z-index: 1;
		top: 0px;
		width:20%;
		margin-left:500px;
	}
	p12{
		position: relative;
	margin-left:750px;	
	z-index: 2;
	}
	.cap{
		margin-left:550px;
		text-align: center;
		
	}
  
    table {
        border-collapse: collapse;
        width: 100%;
    }
	identitas {
        border-collapse: collapse;
        width: 100%;
		text-align: left;
    }
	.identitas th {
        padding: 8px 8px;
        text-align: left;
    }
	.identitas td {
        padding: 8px 8px;
        text-align: left;
    }
    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
  
    .table td {
        padding: 3px 3px;
        border:1px solid #000000;
    }
  
    .text-center {
        text-align: center;
    }
	p7{
		text-align: center;
	}
</style>
</head>
<body class="A4 landscape"  onload="window.print()">
    <section class="sheet padding-10mm">
	<div class="logo">
	<img src="<?php echo base_url(); ?>assets/logo_idb.jpeg">
	</div>
        <h2>PT. Indo Hijab Sukses Pratama</h2>
		<h4>Jl. Raya KM. 2 Tegalgub Kec. Arjawinangun Kab. Cirebon</h4>
		
		<hr />
		<h4>Daftar Credit Note</h4>
		
		
        <table class="table">
            <thead>
                <tr >
                    <th width="50px">No</th>
					<th>Tanggal</th>
					<th>Account No</th>
					<th>Deskripsi</th>
					<th>Cost Center</th>
					<th>Debet</th>
					<th>Credit</th>
					<th>Amount</th>
                </tr>
            </thead>
            <tbody>
			<?php $no=1; $saldo=0; foreach($record->result_array() as $r) { 
					
											$saldo=($r['debt']) + $saldo;
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_jurnal']; ?></td>
													<td><?php echo $r['account_no']; ?></td>
													<td><?php echo $r['deskripsi']; ?></td>
													<td><?php echo $r['nama_bank']; ?></td>
													<td><?php echo rupiah($r['debt']); ?></td>
													<td><?php echo rupiah($r['credit']); ?></td>
													<td><?php echo rupiah($saldo); ?></td>
													
												</tr>
											<?php $no++; } ?>
						
            </tbody>
        </table>
		<p>Cirebon, <?php echo date('d F Y'); ?></p>
		<div class="cap">Direktur Keuangan
		
		
		
		</div>
		<br><br><br><br>
		
		<p12><B>Ir. Budiyanto, MM</B></p12>
		
		
		</p1>
    </section>
</body>
</html>