<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Credit Note</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
	
<style>
    @page { size: landscape }
  
    h2 {
        font-weight: bold;
        font-size: 20pt;
        text-align: center;
    }
	img{
		width:20%;
	}
	
	.logo{
		
		text-align: center;
	}
	h4 {
        font-weight: bold;
        font-size: 14pt;
        text-align: center;
    }
	p{
		margin-left:750px;
	}
	.capbawah{
		position: relative;
		z-index: 1;
		top: 0px;
		width:20%;
		margin-left:500px;
	}
	p12{
		position: relative;
	margin-left:750px;	
	z-index: 2;
	}
	.cap{
		margin-left:550px;
		text-align: center;
		
	}
  
    table {
        border-collapse: collapse;
        width: 100%;
    }
	identitas {
        border-collapse: collapse;
        width: 100%;
		text-align: left;
    }
	.identitas th {
        padding: 8px 8px;
        text-align: left;
    }
	.identitas td {
        padding: 8px 8px;
        text-align: left;
    }
    .table th {
        padding: 8px 8px;
        border:1px solid #000000;
        text-align: center;
    }
  
    .table td {
        padding: 3px 3px;
        border:1px solid #000000;
    }
  
    .text-center {
        text-align: center;
    }
	p7{
		text-align: center;
	}
</style>
</head>
<body class="A4 landscape"  onload="window.print()">
    <section class="sheet padding-10mm">
	<div class="logo">
	<img src="<?php echo base_url(); ?>assets/logo_idb.jpeg">
	</div>
        <h2>PT. Indo Hijab Sukses Pratama</h2>
		<h4>Jl. Raya KM. 2 Tegalgub Kec. Arjawinangun Kab. Cirebon</h4>
		
		<hr />
		<h4>Daftar Piutang</h4>
		
		
        <table class="table">
            <thead>
                <tr>
													<th width="50px">No</th>
													<th>No Transaksi</th>
													<th>Nama Mitra</th>
													<th>Nama Bank</th>
													<th>Type Bayar</th>
													<th>Mata Uang</th>
													<th>Tgl Bayar</th>
													<th>Jumlah</th>
													
												</tr>
            </thead>
            <tbody>
			<?php $no=1; foreach($record->result_array() as $r) { 
											if($r['id_customer'] != 0 ) {
												$id_mitra=$r['id_customer'];
												$rx=$this->db->query("Select * From customer where id_customer='$id_mitra'")->row_array();
												$nm_mitra2=$rx['nama_customer'];
											}elseif($r['id_suplier'] != 0 ){
												$id_mitra=$r['id_suplier'];
												$rx=$this->db->query("Select * From suplier where id_suplier='$id_mitra'")->row_array();
												$nm_mitra=$rx['nama_suplier'];
											}else{
												$id_mitra=$r['id_karyawan'];
												$rx=$this->db->query("Select * From karyawan where id_karyawan='$id_mitra'")->row_array();
												$nm_mitra=$rx['nama_karyawan'];
											}
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['no_transaksi']; ?></td>
													<td><?php echo $nm_mitra; ?></td>
													<td><?php echo $r['nama_bank']; ?></td>
													<td><?php echo $r['nama_typebayar']; ?></td>
													<td><?php echo $r['nama_currency']; ?></td>
													<td><?php echo $r['tgl_pembayaran']; ?></td>
													<td><?php echo rupiah($r['jumlah_pembayaran']); ?></td>
													
												</tr>
											<?php $no++; } ?>
						
            </tbody>
        </table>
		<p>Cirebon, <?php echo date('d F Y'); ?></p>
		<div class="cap">Direktur Keuangan
		
		
		
		</div>
		<br><br><br><br>
		
		<p12><B>Ir. Budiyanto, MM</B></p12>
		
		
		</p1>
    </section>
</body>
</html>