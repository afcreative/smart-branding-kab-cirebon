	<?php 
	
	$pel="FK.";
	$y=substr($pel,0,2);
	$cek=$this->db->query("Select * from pembayaran where substr(no_transaksi,1,2)='$y' order by no_transaksi desc limit 0,1");
	$rowx=$cek->num_rows();
	$data=$cek->row_array();

	if ($rowx >0){
	$no=substr($data['no_transaksi'],-3) +1;
	}
	else{
	$no=1;
	}
	$nourut=1000+$no;
	$no_pel=$pel.substr($nourut,-3);
	
	
	?>
	<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Tagihan PO Suplier</h4>
								</div>
								<div class="card-body">
									<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" id="pills-orders-tab" data-toggle="pill" href="#pills-orders" role="tab" aria-controls="pills-orders" aria-selected="true">PO Barang</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="false">Pembayaran PO</a>
										</li>
										
										
									</ul>
									<div class="tab-content mt-2 mb-3" id="pills-tabContent">
										<div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
											
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Pembayaran PO</h4>
										
									</div>
								
																		
								</div>
								<div class="card-body">
								<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>No Transaksi</th>
													<th>Tgl Faktur</th>
													<th>Nama Suplier</th>
													<th>Jml Bayar</th>
													<th>Bank</th>
													<th>Deksripsi</th>
													
													<th >Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$pembayaran=$this->db->query("Select * From pembayaran a, bank b where a.id_bank=b.id_bank");
											foreach($pembayaran->result_array() as $r) { 
											
											$cx=$this->db->query("Select * From poorders a, suplier b where a.id_suplier=b.id_suplier and a.id_poorder='".$r['id_poorder']."'")->row_array();
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['no_transaksi']; ?></td>
													<td><?php echo $r['tgl_faktur']; ?></td>
													<td><?php echo $cx['nama_suplier']; ?></td>
													<td><?php echo rupiah($r['jml_bayar']); ?></td>
													<td><?php echo $r['nama_bank']; ?></td>
													<td><?php echo $r['deskripsi']; ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editPembayaran<?php echo "$r[id_pembayaran]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>finance/hapus_pembayaran/<?php echo $r['id_pembayaran']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
									

									
								</div>
							
										</div>
										
									
									<div class="tab-pane fade  show active" id="pills-orders" role="tabpanel" aria-labelledby="pills-orders-tab">
										
											<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">PO Barang</div>
									
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="view table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal PO</th>
													<th>No PO</th>
													<th>Suplier</th>
													<th>Tgl Pengiriman</th>
													<th>Total</th>
													<th>Status</th>
													<th width="80px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											$rd=$this->db->query("Select sum(harga*qty)as totalx From detail_poorder where no_poorder='".$r['no_poorder']."'")->row_array();
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_poorder']; ?></td>
													<td><button class="btn btn-warning" data-toggle="modal" data-target="#lihatDetail<?php echo "$r[id_poorder]"; ?>">
														<?php echo $r['no_poorder']; ?>
													</button>
													</td>
													<td><?php echo $r['nama_suplier']; ?></td>
													<td><?php echo $r['tgl_kirim']; ?></td>
													<td><?php echo rupiah($rd['totalx']); ?></td>
													<td><?php echo $r['status_po']; ?></td>
													<td>
														<div class="form-button-action">
														<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Pembayaran Tagihan"  data-toggle="modal" data-target="#pembayaranPO<?php echo "$r[id_poorder]"; ?>">
																<i class="fa fa-bars" aria-hidden="true"></i>
															</button>
															
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
									
								</div>
							</div>	
						</div>					
										</div>
										
									</div>
								</div>
							</div>
						</div>

			<?php $no=1; foreach($record->result_array() as $r) { 
			$row=$this->db->query("Select * From poorders a, suplier b, gudang c where a.id_suplier=b.id_suplier and a.id_gudang=c.id_gudang and a.id_poorder='".$r['id_poorder']."'")->row_array();

			?>
									<form action="<?php echo base_url(); ?>app/edit_bagian/<?php echo $r['id_poorder']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="lihatDetail<?php echo $r['id_poorder']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Detail PO barang Suplier</span> 
														<span class="fw-light">
															NO PO: <?php echo $row['no_poorder']; ?>
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
												<table style="font-size: 12pt;
									">
									<tr><th width="150px">No PO Order </th><th>:</th><th><?php echo $row['no_poorder']; ?></th></tr>
									<tr><th>Tanggal PO order </th><th>:</th><th><?php echo $row['tgl_poorder']; ?></th></tr>
									<tr><th>Nama Suplier</th><th>:</th><th><?php echo $row['nama_suplier']; ?></th></tr>
									<tr><th>Gudang</th><th>:</th><th><?php echo $row['nama_gudang']; ?></th></tr>
									</table>	
													
														<div class="row">
															<table id="add-row" class="view table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Qty</th>
													<th>Nama Barang</th>
													<th>Harga</th>
													<th>Subtotal</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$detail=$this->db->query("Select * From detail_poorder a, barang b where a.id_barang=b.id_barang and a.no_poorder='".$r['no_poorder']."'");
											foreach($detail->result_array() as $rr) { 
											$sub_total=$rr['qty'] * $rr['harga'];
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $rr['qty']; ?></td>
													<td><?php echo $rr['nama_barang']; ?></td>
													<td><?php echo $rr['harga']; ?></td>
													<td><?php echo $sub_total; ?></td>
													
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
															

															
														</div>
													<table 
									>
									<tr><th width="400px" align="right"></th><th width="200px" class="text-right">Jenis Pemakaian</th><th>:</th><th class="text-right" width="100px"><?php echo $row['jenis_pemakaian']; ?></th></tr>
									<tr><th width="400px" ></th><th class="text-right">Termin Bayar </th><th>:</th><th class="text-right"><?php echo $row['termin_bayar']; ?></th></tr>
									<tr><th width="400px" align="right"></th><th class="text-right">Currency</th><th>:</th><th class="text-right"><?php echo $row['currency']; ?></th></tr>
									<tr><th width="400px" align="right"></th><th class="text-right">Keterangan</th><th>:</th><th class="text-right"><?php echo $row['keterangan']; ?></th></tr>
									</table>
												</div>
												<div class="modal-footer no-bd">
													
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>	
							<script>

$(document).ready(function() {
    $('table.view').DataTable();
} );
</script>	
	
									
<?php $no=1; foreach($record->result_array() as $r) { 
$row=$this->db->query("Select * From poorders a, suplier b, gudang c where a.id_suplier=b.id_suplier and a.id_gudang=c.id_gudang and a.id_poorder='".$r['id_poorder']."'")->row_array();
?>
									<form action="<?php echo base_url(); ?>finance/tagihanpo/<?php echo $r['id_poorder']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="pembayaranPO<?php echo $r['id_poorder']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Pembayaran</span> 
														<span class="fw-light">
															Tagihan Suplier
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Invoice Suplier</label>
																	<input  type="text" name="kode_produk" value="<?php echo $r['no_posuplier']; ?>" class="form-control" placeholder="Kode Produk">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Faktur</label>
																	<input  type="text" name="no_transaksi" value="<?php echo $no_pel; ?>" class="form-control" placeholder="No Transaksi">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal Faktur</label>
																	<input  type="date" name="tgl_faktur" value="<?php echo date('Y-m-d'); ?>" class="form-control" placeholder="Tanggal Faktur">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Deskripsi </label>
																	<textarea  type="text" name="deskripsi" class="form-control" rows="3"></textarea>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Bank</label>
																	<?php
																	$kategori =$this->db->query("Select * From bank order by id_bank ASC")->result_array();
																	echo"
																	<select name='id_bank' class='form-control' required>
																		<option value='' selected>- Pilih Bank  -</option>";
																			foreach ($kategori as $row){
																				if ($r['id_bank']==$row['id_bank']){
																					echo "<option value='$row[id_bank]' selected>$row[nama_bank]</option>";
																						}else{
																							echo "<option value='$row[id_bank]'>$row[nama_bank]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah Bayar</label>
																	<input  type="text" name="jml_bayar" value="" class="form-control" placeholder="Jumlah Bayar">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>		

									
<?php $no=1; foreach($pembayaran->result_array() as $r) { 
$row=$this->db->query("Select * From pembayaran d, poorders a, suplier b, gudang c where a.id_suplier=b.id_suplier and a.id_gudang=c.id_gudang and d.id_poorder=a.id_poorder and d.id_pembayaran='".$r['id_pembayaran']."'")->row_array();
?>
									<form action="<?php echo base_url(); ?>finance/edit_tagihanpo/<?php echo $r['id_pembayaran']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editPembayaran<?php echo $r['id_pembayaran']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form Edit Pembayaran</span> 
														<span class="fw-light">
															Tagihan Suplier
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Invoice Suplier</label>
																	<input  type="text" name="kode_produk" value="<?php echo $row['no_posuplier']; ?>" class="form-control" placeholder="Kode Produk">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Faktur</label>
																	<input  type="text" name="no_transaksi" value="<?php echo $r['no_transaksi']; ?>" class="form-control" placeholder="No Transaksi">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal Faktur</label>
																	<input  type="date" name="tgl_faktur" value="<?php echo  $r['tgl_faktur']; ?>" class="form-control" placeholder="Tanggal Faktur">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Deskripsi </label>
																	<textarea  type="text" name="deskripsi" class="form-control" rows="3"><?php echo  $r['deskripsi']; ?></textarea>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Bank</label>
																	<?php
																	$kategori =$this->db->query("Select * From bank order by id_bank ASC")->result_array();
																	echo"
																	<select name='id_bank' class='form-control' required>
																		<option value='' selected>- Pilih Bank  -</option>";
																			foreach ($kategori as $row){
																				if ($r['id_bank']==$row['id_bank']){
																					echo "<option value='$row[id_bank]' selected>$row[nama_bank]</option>";
																						}else{
																							echo "<option value='$row[id_bank]'>$row[nama_bank]</option>";
																							}
																			}
																echo "</select>
																	";
																	?>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Jumlah Bayar</label>
																	<input  type="text" name="jml_bayar"  value="<?php echo  $r['jml_bayar']; ?>" class="form-control" placeholder="Jumlah Bayar">
																</div>
															</div>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>										
            