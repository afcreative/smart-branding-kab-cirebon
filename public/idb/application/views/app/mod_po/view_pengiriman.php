	
	<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Detail Pengiriman Customer</h4>
								</div>
								<div class="card-body">
									<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" id="pills-orders-tab" data-toggle="pill" href="#pills-orders" role="tab" aria-controls="pills-orders" aria-selected="true">List  Orders </a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="false">Pengiriman</a>
										</li>
										
										
									</ul>
									<div class="tab-content mt-2 mb-3" id="pills-tabContent">
										<div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
								<div class="card">			
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Pengiriman Customer </h4>
										
									</div>
							
								
									
																		
								</div>
								
								<div class="card-body">
									<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Order</th>
													<th>No Order</th>
													<th>Customer</th>
													<th>Alamat Kirim</th>
													<th>Gudang</th>
													<th width="80px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											
											$pengiriman = $this->db->query("Select * from pengiriman a, customer b, orders c, gudang d where a.id_gudang=d.id_gudang and c.id_order=a.id_order and a.id_customer=b.id_customer ");
											foreach($pengiriman->result_array() as $rq) { 
											
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $rq['tgl_pengiriman']; ?></td>
													<td>
														<?php echo $rq['no_order']; ?>
													
													</td>
													<td><?php echo $rq['nama_customer']; ?></td>
													<td><?php echo $rq['alamat_kirim']; ?></td>
													<td><?php echo $rq['nama_gudang']; ?></td>
													<td>
														<div class="form-button-action">
														<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editModal<?php echo "$rq[id_pengiriman]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>transaksi/hapus_pengiriman/<?php echo $rq['id_pengiriman']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
															
															
															
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									

									
								</div>
							</div>
										</div>
										
									
									<div class="tab-pane fade  show active" id="pills-orders" role="tabpanel" aria-labelledby="pills-orders-tab">
										
											<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">List order customer</h4>
										
									</div>
								</div>		
									<div class="card-body">
									<!-- Modal -->
									

									<table id="add-row" class="view table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Order</th>
													<th>No Order</th>
													<th>Customer</th>
													<th>Total</th>
													<th>Status</th>
													<th width="80px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_order']; ?></td>
													<td><button class="btn btn-warning" data-toggle="modal" data-target="#<?php echo "$r[id_order]"; ?>">
														<?php echo $r['no_order']; ?>
													</button>
													</td>
													<td><?php echo $r['nama_customer']; ?></td>
													<td><?php echo rupiah($r['tot_penjualan']); ?></td>
													<td><?php echo $r['status_order']; ?></td>
													<td>
														<div class="form-button-action">
														<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Pengiriman barang" data-toggle="modal" data-target="#addRowModal"  >
																<i class="fa fa-bus"></i>
															</button>
															
															
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
								    </div>
								
								</div>
							</div>	
						</div>					
										</div>
										
									</div>
								</div>
							</div>
						</div>
						
		<?php $no=1; foreach($record->result_array() as $r) { 
											
					
											?>				
					<form action="<?php echo base_url(); ?>transaksi/pengiriman/<?php echo $r['id_order']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Pengiriman Barang
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
												<input  type="hidden" name="id_customer" value="<?php echo $r['id_customer']; ?>">	
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Customer</label>
																	<input  type="text" name="nama_customer" value="<?php echo $r['nama_customer']; ?>" class="form-control" placeholder="Nama Suplier">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Alamat</label>
																	
																	<textarea name="alamat_kirim" class="form-control"rows="3"><?php echo $r['alamat']; ?></textarea>
																</div>
															</div>
															
	<div class="col-sm-12">
		<div class="form-group">
			<label>Gudang</label>
				<?php $kategori =$this->db->query("Select * From gudang order by id_gudang ASC")->result_array();
				echo"<select name='id_gudang' class='form-control' required>
					<option value='' selected>- Pilih Gudang  -</option>";
						foreach ($kategori as $row){
							if ($r['id_gudang']==$row['id_gudang']){
								echo "<option value='$row[id_gudang]' selected>$row[nama_gudang]</option>";
								}else{ echo "<option value='$row[id_gudang]'>$row[nama_gudang]</option>";}
								}
							echo "</select>";?>
									</div>
								</div>														
							</div>
						</div>
						<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal pengiriman</label>
																	<input  type="date" name="tgl_pengiriman" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
						<div class="col-sm-12">
						<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Qty</th>
													<th>Nama Barang</th>
													<th>Harga</th>
													<th>Subtotal</th>
													
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$detail=$this->db->query("Select * From detail_pobarang a, barang b where a.id_barang=b.id_barang and a.no_order='".$r['no_order']."'");
											foreach($detail->result_array() as $rd) { 
											$sub_total=$rd['qty'] * $rd['harga'];
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $rd['qty']; ?></td>
													<td><?php echo $rd['nama_barang']; ?></td>
													<td><?php echo rupiah($rd['harga']); ?></td>
													<td><?php echo rupiah($sub_total); ?></td>
													
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
						</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>	
		<?php } ?>


				<?php $no=1; foreach($pengiriman->result_array() as $r) { 
											
					
											?>				
					<form action="<?php echo base_url(); ?>transaksi/edit_pengiriman/<?php echo $r['id_pengiriman']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editModal<?php echo $r['id_pengiriman']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Edit</span> 
														<span class="fw-light">
															Pengiriman Barang
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
												<input  type="hidden" name="id_customer" value="<?php echo $r['id_customer']; ?>">	
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Customer</label>
																	<input  type="text" name="nama_customer" value="<?php echo $r['nama_customer']; ?>" class="form-control" placeholder="Nama Suplier">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Alamat</label>
																	
																	<textarea name="alamat_kirim" class="form-control"rows="3"><?php echo $r['alamat']; ?></textarea>
																</div>
															</div>
															
	<div class="col-sm-12">
		<div class="form-group">
			<label>Gudang</label>
				<?php $kategori =$this->db->query("Select * From gudang order by id_gudang ASC")->result_array();
				echo"<select name='id_gudang' class='form-control' required>
					<option value='' selected>- Pilih Gudang  -</option>";
						foreach ($kategori as $row){
							if ($r['id_gudang']==$row['id_gudang']){
								echo "<option value='$row[id_gudang]' selected>$row[nama_gudang]</option>";
								}else{ echo "<option value='$row[id_gudang]'>$row[nama_gudang]</option>";}
								}
							echo "</select>";?>
									</div>
								</div>														
							</div>
						</div>
						<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal pengiriman</label>
																	<input  type="date" name="tgl_pengiriman" value="<?php echo $r['tgl_pengiriman']; ?>" class="form-control" >
																</div>
															</div>
						<div class="col-sm-12">
						<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Qty</th>
													<th>Nama Barang</th>
													<th>Harga</th>
													<th>Subtotal</th>
													
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$detail=$this->db->query("Select * From detail_pobarang a, barang b where a.id_barang=b.id_barang and a.no_order='".$r['no_order']."'");
											foreach($detail->result_array() as $rd) { 
											$sub_total=$rd['qty'] * $rd['harga'];
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $rd['qty']; ?></td>
													<td><?php echo $rd['nama_barang']; ?></td>
													<td><?php echo rupiah($rd['harga']); ?></td>
													<td><?php echo rupiah($sub_total); ?></td>
													
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
						</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>	
		<?php } ?>		
						<script>

$(document).ready(function() {
    $('table.view').DataTable();
} );
</script>

	
				
            