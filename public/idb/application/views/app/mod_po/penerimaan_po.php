
	<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Penerimaan Purchase Order</h4>
								</div>
								<div class="card-body">
									<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" id="pills-orders-tab" data-toggle="pill" href="#pills-orders" role="tab" aria-controls="pills-orders" aria-selected="true">Purchase Order Barang</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="false">Penerimaa PO</a>
										</li>
										
										
									</ul>
									<div class="tab-content mt-2 mb-3" id="pills-tabContent">
										<div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
											
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Penerimaan barang PO</h4>
										
									</div>
								
																		
								</div>
								<div class="card-body">
								<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>No PO</th>
													<th>Tgl Penerimaan</th>
													<th>Penerima</th>
													<th>Total Penerimaan</th>
													
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$penerimaan=$this->db->query("Select * From penerimaan_po a, users b where a.id_users=b.id_users");
											foreach($penerimaan->result_array() as $r) { 
											$rx=$this->db->query("Select sum(qty*harga)as grantot From detail_penerimaanpo")->row_array();
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['no_poorder']; ?></td>
													<td><?php echo $r['tgl_penerimaanpo']; ?></td>
													<td><?php echo $r['nama_lengkap']; ?></td>
													<td><?php echo rupiah($rx['grantot']); ?></td>
													<td>
														<div class="form-button-action">
															<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editPenerimaanPO<?php echo "$r[id_penerimaanpo]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>finance/hapus_penerimaanpo/<?php echo $r['id_penerimaanpo']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
									

									
								</div>
							
										</div>
										
									
									<div class="tab-pane fade  show active" id="pills-orders" role="tabpanel" aria-labelledby="pills-orders-tab">
										
											<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="card-title">PO Barang</div>
									
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="view table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal PO</th>
													<th>No PO</th>
													<th>Suplier</th>
													<th>Tgl Pengiriman</th>
													<th>Total</th>
													<th>Status</th>
													<th width="80px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											$rd=$this->db->query("Select sum(harga*qty)as totalx From detail_poorder where no_poorder='".$r['no_poorder']."'")->row_array();
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_poorder']; ?></td>
													<td><button class="btn btn-warning" data-toggle="modal" data-target="#lihatDetail<?php echo "$r[id_poorder]"; ?>">
														<?php echo $r['no_poorder']; ?>
													</button>
													</td>
													<td><?php echo $r['nama_suplier']; ?></td>
													<td><?php echo $r['tgl_kirim']; ?></td>
													<td><?php echo rupiah($rd['totalx']); ?></td>
													<td><?php echo $r['status_po']; ?></td>
													<td>
														<div class="form-button-action">
														<a href="<?php echo base_url();?>finance/detail_penerimaanpo/<?php echo $r['no_poorder']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Penerimaan Bahan baku" <?php echo" onclick=\"return confirm('Penerimaan barang dari suplier?')\" "; ?> >
																<i class="fa fa-bus"></i>
															</button></a>
															
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
									
								</div>
							</div>	
						</div>					
										</div>
										
									</div>
								</div>
							</div>
						</div>

<?php $no=1; foreach($record->result_array() as $r) { ?>
									<form action="<?php echo base_url(); ?>app/edit_bagian/<?php echo $r['id_poorder']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="lihatDetail<?php echo $r['id_poorder']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Detail PO barang Suplier</span> 
														<span class="fw-light">
															Bagian : <?php echo $r['no_poorder']; ?>
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
													
													
														<div class="row">
															<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Qty</th>
													<th>Nama Barang</th>
													<th>Harga</th>
													<th>Subtotal</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$detail=$this->db->query("Select * From detail_poorder a, barang b where a.id_barang=b.id_barang and a.no_poorder='".$r['no_poorder']."'");
											foreach($detail->result_array() as $rr) { 
											$sub_total=$rr['qty'] * $rr['harga'];
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $rr['qty']; ?></td>
													<td><?php echo $rr['nama_barang']; ?></td>
													<td><?php echo $rr['harga']; ?></td>
													<td><?php echo $sub_total; ?></td>
													
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>	
							<script>

$(document).ready(function() {
    $('table.view').DataTable();
} );
</script>	

<?php $no=1; foreach($penerimaan->result_array() as $r) { 
$row=$this->db->query("Select * From poorders a, suplier b, gudang c where a.id_suplier=b.id_suplier and a.id_gudang=c.id_gudang and a.no_poorder='".$r['no_poorder']."'")->row_array();
?>
									<form action="<?php echo base_url(); ?>finance/update_penerimaanpo/<?php echo $r['id_penerimaanpo']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="editPenerimaanPO<?php echo $r['id_penerimaanpo']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											
											
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Detail Penerimaan PO barang Suplier</span> 
														<span class="fw-light">
															Bagian : <?php echo $r['no_poorder']; ?>
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="card-body">
											<table style="font-size: 12pt;
									font-weight: bold;">
									<tr><th width="150px">No PO Order </th><th>:</th><th><?php echo $row['no_poorder']; ?><input type="hidden" name="id_penerimaanpo" value="<?php echo $r['id_penerimaanpo']; ?>"></th></tr>
									<tr><th>Tanggal PO order </th><th>:</th><th><?php echo $row['tgl_poorder']; ?></th></tr>
									<tr><th>Nama Suplier</th><th>:</th><th><?php echo $row['nama_suplier']; ?></th></tr>
									<tr><th>Tanggal Penerimaan</th><th>:</th><th><input type="date" value="<?php echo $r['tgl_penerimaanpo']; ?>" name="tgl_penerimaanpo" class="form-control"></th></tr>
									</table>
											</div>
												<div class="modal-body">
													
													
														<div class="row">
															<table id="add-row" class="view table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Qty</th>
													<th>Nama Barang</th>
													<th>Harga</th>
													<th>Subtotal</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											$detail=$this->db->query("Select * From detail_penerimaanpo a, barang b where a.id_barang=b.id_barang and a.id_penerimaanpo='".$r['id_penerimaanpo']."'");
											foreach($detail->result_array() as $rr) { 
											$sub_total=$rr['qty'] * $rr['harga'];
					
											?>
												<tr>
													<td><?php echo $no; ?><input type="hidden" name="id_detpenerimaan[]" value="<?php echo $rr['id_detpenerimaan']; ?>"><input type="checkbox"name="box[]" style="width:25px;"></td>
													<td><input type="text" name="qty[]" value="<?php echo $rr['qty']; ?>" ></td>
													<td><?php echo $rr['nama_barang']; ?></td>
													
													
													<td><input type="text" name="harga[]" value="<?php echo rupiah($rr['harga']); ?>" ></td>
													<td><?php echo rupiah($sub_total); ?></td>
													
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
															

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>
									<?php } ?>	
            