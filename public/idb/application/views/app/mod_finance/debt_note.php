	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Debt Note</h4>
										
									</div>
								</div>
								<div class="card-body">
									<!-- Modal -->
									

									<div class="table-responsive">
										<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal</th>
													<th>Account No</th>
													<th>Deskripsi</th>
													<th>Cost Center</th>
													<th>Debet</th>
													<th>Credit</th>
													<th>Amount</th>
													<th width="100px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; $saldo=0; foreach($record->result_array() as $r) { 
					
											$saldo=($r['credit'] - $r['debt']) + $saldo;
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_jurnal']; ?></td>
													<td><?php echo $r['account_no']; ?></td>
													<td><?php echo $r['deskripsi']; ?></td>
													<td><?php echo $r['nama_bank']; ?></td>
													<td><?php echo rupiah($r['debt']); ?></td>
													<td><?php echo rupiah($r['credit']); ?></td>
													<td><?php echo rupiah($saldo); ?></td>
													<td>
														<div class="form-button-action">
															
															<a href="<?php echo base_url();?>finance/hapus_jurnal/<?php echo $r['id_jurnal']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
			</div>	
							
            