	
	<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Pembayaran DP Customer</h4>
								</div>
								<div class="card-body">
									<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" id="pills-orders-tab" data-toggle="pill" href="#pills-orders" role="tab" aria-controls="pills-orders" aria-selected="true">List  Orders </a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="false">Pembayaran</a>
										</li>
										
										
									</ul>
									<div class="tab-content mt-2 mb-3" id="pills-tabContent">
										<div class="tab-pane fade" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
								<div class="card">			
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Pembayaran Customer </h4>
										
									</div>
							
								
									
																		
								</div>
								
								<div class="card-body">
									<table id="add-row" class="display table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Bayar</th>
													<th>No Order</th>
													<th>Customer</th>
													<th>Nama Bank</th>
													<th>Jumlah</th>
													<th width="80px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; 
											
											$pembayaran = $this->db->query("Select * from uangmuka a, customer b, orders c, bank d where a.id_order=c.id_order and a.id_bank=d.id_bank and b.id_customer=c.id_customer ");
											foreach($pembayaran->result_array() as $rq) { 
											
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $rq['tgl_bayar']; ?></td>
													<td>
														<?php echo $rq['no_order']; ?>
													
													</td>
													<td><?php echo $rq['nama_customer']; ?></td>
													<td><?php echo $rq['nama_bank']; ?></td>
													<td><?php echo $rq['jml_bayar']; ?></td>
													<td>
														<div class="form-button-action">
														<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task"  data-toggle="modal" data-target="#editModal<?php echo "$rq[id_muka]"; ?>">
																<i class="fa fa-edit"></i>
															</button>
															<a href="<?php echo base_url();?>transaksi/hapus_pengiriman/<?php echo $rq['id_muka']; ?>" button type="button" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove" <?php echo" onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\" "; ?> >
																<i class="fa fa-times"></i>
															</button></a>
															
															
															
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
									

									
								</div>
							</div>
										</div>
										
									
									<div class="tab-pane fade  show active" id="pills-orders" role="tabpanel" aria-labelledby="pills-orders-tab">
										
											<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">List order customer</h4>
										
									</div>
								</div>		
									<div class="card-body">
									<!-- Modal -->
									

									<table id="add-row" class="view table table-striped table-hover" >
											<thead>
												<tr>
													<th width="50px">No</th>
													<th>Tanggal Order</th>
													<th>No Order</th>
													<th>Customer</th>
													<th>Total</th>
													<th>Status</th>
													<th width="80px">Action</th>
												</tr>
											</thead>
											
											<tbody>
											<?php $no=1; foreach($record->result_array() as $r) { 
											
					
											?>
												<tr>
													<td><?php echo $no; ?></td>
													<td><?php echo $r['tgl_order']; ?></td>
													<td><button class="btn btn-warning" data-toggle="modal" data-target="#<?php echo "$r[id_order]"; ?>">
														<?php echo $r['no_order']; ?>
													</button>
													</td>
													<td><?php echo $r['nama_customer']; ?></td>
													<td><?php echo rupiah($r['tot_tagihan']); ?></td>
													<td><?php echo $r['status_order']; ?></td>
													<td>
														<div class="form-button-action">
														<button type="button"  title="" class="btn btn-link btn-primary btn-lg" data-original-title="Pembayaran uang muka" data-toggle="modal" data-target="#addRowModal"  >
																<i class="fa fa-credit-card"></i>
															</button>
															
															
														</div>
													</td>
												</tr>
											<?php $no++; } ?>
											</tbody>
										</table>
								    </div>
								
								</div>
							</div>	
						</div>					
										</div>
										
									</div>
								</div>
							</div>
						</div>
						
		<?php $no=1; foreach($record->result_array() as $r) { 
											
					
											?>				
					<form action="<?php echo base_url(); ?>finance/advance/<?php echo $r['id_order']; ?>" method="post"  enctype="multipart/form-data">
							<div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header no-bd">
													<h5 class="modal-title">
														<span class="fw-mediumbold">
														Form</span> 
														<span class="fw-light">
															Pembayaran Uang Muka
														</span>
													</h5>
													
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<span class="border-bottom"></span>
												<div class="modal-body">
												<input  type="hidden" name="id_customer" value="<?php echo $r['id_customer']; ?>">	
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>No Order</label>
																	<input  type="text" name="no_order" value="<?php echo $r['no_order']; ?>" class="form-control" placeholder="Nama Suplier">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Customer</label>
																	<input  type="text" name="nama_customer" value="<?php echo $r['nama_customer']; ?>" class="form-control" placeholder="Nama Suplier">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Alamat</label>
																	
																	<textarea name="alamat_kirim" class="form-control"rows="3"><?php echo $r['alamat']; ?></textarea>
																</div>
															</div>
															
	<div class="col-sm-12">
		<div class="form-group">
			<label>Bank</label>
				<?php $kategori =$this->db->query("Select * From bank order by id_bank ASC")->result_array();
				echo"<select name='id_bank' id='id_bank' class='form-control' required>
					<option value='' selected>- Pilih Bank  -</option>";
						foreach ($kategori as $row){
							if ($r['id_bank']==$row['id_bank']){
								echo "<option value='$row[id_bank]' selected>$row[nama_bank]</option>";
								}else{ echo "<option value='$row[id_bank]'>$row[nama_bank]</option>";}
								}
							echo "</select>";?>
									</div>
								</div>														
							</div>
						</div>
						<div class="col-sm-12">
																<div class="form-group">
																	<label>Tanggal Pembayaran</label>
																	<input  type="date" name="tgl_bayar" value="<?php echo date('Y-m-d'); ?>" class="form-control" >
																</div>
															</div>
					<div class="col-sm-12">
						<div class="form-group">
						<label>Jumlah</label>
							<input  type="text" name="tot_Tagihan" id="tot_Tagihan" value="<?php echo $r['tot_tagihan']; ?>" class="form-control" >
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
						<label>PPN</label>
							<input  type="text" name="PPN" id="PPN" value="10" class="form-control" >
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
						<label>Total PPN</label>
							<input  type="text" name="totPPN" id="totPPN" class="form-control" >
						</div>
					</div>					
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Simpan</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
											</div>
										</div>
									</div>	
									</form>	
		<?php } ?>


						
						<script>

$(document).ready(function() {
    $('table.view').DataTable();
} );
		id_bank.oninput = function() {
				var xz=$("#tot_Tagihan").val();
				var PPN=$("#PPN").val();
				var totPPN=(PPN * xz)/100;
				
				$('[name="totPPN"]').val(totPPN);
				
			  };
			  PPN.oninput = function() {
				var xz=$("#tot_Tagihan").val();
				var PPN=$("#PPN").val();
				var totPPN=(PPN * xz)/100;
				
				$('[name="totPPN"]').val(totPPN);
				
			  };
</script>

	
				
            