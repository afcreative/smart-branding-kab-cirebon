	<div class="row">
	<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<div class="d-flex align-items-center">
										<h4 class="card-title">Detail Profile Pengguna</h4>
										
									</div>
								</div>
								<div class="card-body">
									<div class="modal-body">
									<form action="<?php echo base_url(); ?>app/edit_users/<?php   echo $rows['id_users'];?>" method="post"  enctype="multipart/form-data">				
													
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Username</label>
																	<input  type="text" name="username" value="<?php   echo $rows['username'];?>" class="form-control" placeholder="username">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Password</label>
																	<input  type="text" name="password" value="<?php   echo $rows['password'];?>" class="form-control" placeholder="password">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Nama Lengkap</label>
																	<input  type="text" name="nama_lengkap" value="<?php   echo $rows['nama_lengkap'];?>" class="form-control" placeholder="Nama Lengkap">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Email</label>
																	<input  type="text" name="email" value="<?php   echo $rows['email'];?>" class="form-control" placeholder="Email">
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<label>No Telp</label>
																	<input  type="text" name="no_telp" value="<?php   echo $rows['no_telp'];?>" class="form-control" placeholder="No Telp">
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<label>Level</label>
																	<select class="form-control" name="level">
																		<option value="kasir">Kasir</option>
																		<option value="admin">Admin</option>
																		<option value="owner">Owner</option>
																		<option value="superadmim">Superadmin</option>
																	</select>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<div class="form-check">
																	<label>Blokir</label><br/>
																	<label class="form-radio-label">
																		<input class="form-radio-input" type="radio" name="blokir" value="Y"  checked="">
																		<span class="form-radio-sign">Y</span>
																	</label>
																	<label class="form-radio-label ml-3">
																		<input class="form-radio-input" type="radio" name="blokir" value="N">
																		<span class="form-radio-sign">N</span>
																	</label>
																</div>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	
																	<img src="<?php echo base_url(); ?>assets/foto_user/<?php   echo $rows['foto_user'];?>" alt="image profile" >
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Foto</label>
																	<input  type="file" name="foto" class="form-control" placeholder="fill name">
																</div>
															</div>

															
														</div>
													
												</div>
												<div class="modal-footer no-bd">
													<button type="submit" name="submit"  class="btn btn-primary">Update</button>
													<button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
												</div>
									

									</form>
								</div>
							</div>
						</div>
			</div>	
							
									
            