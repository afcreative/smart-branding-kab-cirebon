<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin/login');
});

Route::get('/logout', function () {
    Auth::logout();
	Session::flush();
	return redirect('/');
});

Route::post('/login', 'App@cek_login');

Route::middleware(['user'])->group(function () {
    Route::get('/home', function () {
        return view('admin/mod_master/home');
    });
    Route::get('/cari', 'App@cari_home');

    Route::get('/icon', function () {
        return view('admin/icon');
    });
    Route::get('/website', 'App@website');
    Route::get('/dokumentasi', 'App@dokumentasi');
    Route::post('/edit-websites', 'App@edit_website');


    Route::get('/kategor', 'App@kategori');
    Route::get('/hapus-kategori/{id}', 'App@hapus_kategori');
    Route::post('/simpan-kategori', 'App@simpan_kategori');
    Route::post('/edit-kategori', 'App@edit_kategori');

    Route::get('/brandin', 'App@branding');
    Route::post('/simpan-branding', 'App@simpan_branding');
    Route::get('/hapus-branding/{id}', 'App@hapus_branding');
    Route::post('/edit-branding', 'App@edit_branding');

    Route::get('/menu', 'App@menu');
    Route::get('/hapus-menu/{id}', 'App@hapus_menu');
    Route::post('/simpan-menu', 'App@simpan_menu');
    Route::post('/edit-menu', 'App@edit_menu');

    Route::get('/submenu', 'App@submenu');
    Route::get('/hapus-submenu/{id}', 'App@hapus_submenu');
    Route::post('/simpan-submenu', 'App@simpan_submenu');
    Route::post('/edit-submenu', 'App@edit_submenu');

    Route::get('/user', 'App@user');
    Route::get('/hapus-user/{id}', 'App@hapus_user');
    Route::post('/simpan-user', 'App@simpan_user');
    Route::post('/edit-user', 'App@edit_user');
});
